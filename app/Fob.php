<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fob extends Model
{
    protected $table = 'fob_oauth';

    public $timestamps = false;

    protected $fillable = [
        'access_token', 'refresh_token'];
}
