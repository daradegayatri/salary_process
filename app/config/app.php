<?php
/**
 * Created by PhpStorm.
 * User: Dell-19
 * Date: 5/5/17
 * Time: 1:51 PM
 */
require_once '../QuickBooks.php';

return [
      'QUICKBOOK_TOKEN' => env('QUICKBOOK_TOKEN'),
      'QBO_OAUTH_CONSUMER_KEY' => env('QBO_OAUTH_CONSUMER_KEY'),
      'QBO_CONSUMER_SECRET' => env('QBO_CONSUMER_SECRET'),
      'QBO_SANDBOX' => env('QBO_SANDBOX'),
      'QBO_ENCRYPTION_KEY' => env('QBO_ENCRYPTION_KEY'),
      'QBO_USERNAME' => env('QBO_USERNAME'),
      'QBO_TENANT' => env('QBO_TENANT'),
      'QBO_OAUTH_URL' => env('QBO_OAUTH_URL'),
      'QBO_SUCCESS_URL' => env('QBO_SUCCESS_URL'),
      'QBO_MENU_URL' => env('QBO_MENU_URL'),
];