<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Salary_Process extends Model
{
    protected $table = 'salary_process';

    protected $fillable = [

        'basics','HRA','DA',
        'salary_user_id','month','year','created_at', 'updated_at'
    ];
}
