<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class OrganisationType extends Model
{
    protected $table = 'organisation_type';

    protected $fillable = [

        'OrganisationTypeID', 'OrganisationType', 'CreatedBy', 'updated_by', 'created_at', 'updated_at'
    ];
}
