<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'salary_process';

    protected $fillable = [

          'id', 'basics','HRA','DA',
        'salary_user_id','pay_days','month','year','created_at', 'updated_at'
    ];
}
