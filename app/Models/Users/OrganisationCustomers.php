<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class OrganisationCustomers extends Model
{
    protected $table = 'organisation_customers';
//OrganisationCustomerID, OrganisationID, CustomerID, created_by, updated_by, created_at, updated_at
    protected $fillable = [

        'OrganisationCustomerID', 'OrganisationID', 'CustomerID', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
