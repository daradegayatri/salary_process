<?php

namespace App\Models\Users;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';

    protected $fillable = [
        'id',
        'UserRoleID',
        'OrganisationID',
        'name',
        'email',
        'username',
        'password',
        'FirstName',
        'LastName',
        'PhoneNo',
        'Gender',
        'Address',
        'City',
        'State',
        'Description',
        'Country',
        'Postalcode',
        'AssignedTo',
        'CreatedBy',
        'updated_by',
        'remember_token',
        'created_at',
        'updated_at',
        'provider',
        'provider_id',
        'jobtitle',
        'image'
    ];
}
