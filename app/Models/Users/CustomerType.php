<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    protected $table = 'customer_types';

    protected $fillable = [

        'CustomerTypeID', 'CustomerType', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
