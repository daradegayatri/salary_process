<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Salary_Structure extends Model
{
    protected $table = 'salary_structure';

    protected $fillable = [

            'salary_user_id','basics','HRA','DA','user_id','created_at', 'updated_at'
    ];
}
