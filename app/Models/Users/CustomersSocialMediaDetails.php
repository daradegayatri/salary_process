<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CustomersSocialMediaDetails extends Model
{
    //CustomersSocialMediaID, SocialMediaTypeID, CustomerID, created_by, updated_by, created_at, updated_at

    protected $table = 'customers_socialmedia_details';
//CustomerID, CustomerName, Phoneno, EmailID, UserID, CustomerTypeID, HasSocialMedia, SalesRep, ParentSalesRep, PrincipalAddressID, BillToAddressID, ShipToAddressID, Description, TransactionLink, HasSubCustomer, NoOfSubCustomer, Created_By, Updated_By, created_at, updated_at
    protected $fillable = [

        'CustomersSocialMediaID', 'SocialMediaTypeID', 'CustomerID','SocialMediaID', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
