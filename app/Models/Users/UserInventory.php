<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserInventory extends Model
{
    protected $table = 'user_inventory';

    protected $fillable = [

        'UserInventoryID',
        'UserID',
        'InventoryID',
        'NoOfInventory',
        'IsReturned',
        'ReturnedInventoryCount',
        'AvaliableInventoryCount',
        'CreatedBy',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
