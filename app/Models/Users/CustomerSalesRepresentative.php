<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CustomerSalesRepresentative extends Model
{
    protected $table = 'customer_sales_representative';

    protected $fillable = [

        'CustomerSRID', 'CustomerID', 'SalesRepresentativeID','IsPrimary',
        'OrganizationID', 'CreatedBy', 'updated_by', 'created_at',
        'updated_at'
    ];
}
