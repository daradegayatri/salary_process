<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';
//CustomerID, CustomerName, Phoneno, EmailID, UserID, CustomerTypeID, HasSocialMedia, SalesRep, ParentSalesRep, PrincipalAddressID, BillToAddressID, ShipToAddressID, Description, TransactionLink, HasSubCustomer, NoOfSubCustomer, Created_By, Updated_By, created_at, updated_at
    protected $fillable = [

        'CustomerID', 'CompanyName', 'CustomerName', 'Phoneno','Phoneno2', 'EmailID','UserID','CustomerTypeID',
         'HasSocialMedia','SalesRep','ParentSalesRep','PrincipalAddressID','BillToAddressID','Prefix','FirstName','LastName',
        'ShipToAddressID','Description','TransactionLink','HasSubCustomer','NoOfSubCustomer','website',
        'Group','ParentCustId','ChildCustId','IsDeleted','CustomerStageID','InitialDealValue','ClosedDealValue',
        'Created_By', 'Updated_By', 'SecondarySocialMediaID', 'SecondarySocialMedias',
        'created_at', 'updated_at', 'PrincipalSocialMediaID', 'PrincipalSocialMedias', 'cycle_type', 'cycle_value', 'units', 'jobtitle'
    ];
}
