<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $table = 'organisation';

    protected $fillable = [
        'OrganisationID', 'OrganisationTypeID', 'OrganisationName', 'OrgWebAddress', 'ContactPerson', 'Phoneno', 'EmailID', 'Address', 'ParentOrgID', 'City', 'State', 'Country', 'ZipCode', 'Description', 'SecondaryContactName', 'HasSocialMediaDetails', 'OrgSocialMediaAccount', 'HasSubOrganisation', 'IsSubOrganisation', 'Created_By', 'Updated_By', 'created_at', 'updated_at'
    ];
}
