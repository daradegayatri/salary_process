<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CustomerStage extends Model
{
    protected $table = 'customer_stages';

    protected $fillable = [

        'CustomerStageID', 'CustomerStage', 'created_at', 'updated_at'
    ];
}
