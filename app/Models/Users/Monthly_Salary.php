<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Monthly_Salary extends Model
{
    protected $table = 'monthly_salary';

    protected $fillable = [

            'basics','HRA','DA',
        'salary_user_id','month','year','created_at', 'updated_at'
    ];
}
