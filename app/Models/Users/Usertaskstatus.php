<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Usertaskstatus extends Model
{
    protected $table = 'user_task_status';

    protected $fillable = [

        'UserTaskStatusID',
        'UserTaskStatus',
        'CreatedBy',
        'created_at','updated_at','updated_by'
    ];
}
