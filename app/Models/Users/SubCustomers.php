<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class SubCustomers extends Model
{
    protected $table = 'sub_customers';
//SubCustomerID, ParentCustomerID, CustomerID, created_by, updated_by, created_at, updated_at
    protected $fillable = [

        'SubCustomerID', 'ParentCustomerID', 'CustomerID', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
