<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class SubOrganisations extends Model
{
    protected $table = 'sub_organisations';

    protected $fillable = [
        'SubOrganisationID', 'ParentOrganisationID', 'OrganisationID', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}