<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Worktype extends Model
{
    protected $table = 'work_type';
    protected $fillable = [

        'WorkTypeID',
        'WorkType',
        'CreatedBy',
        'created_at','updated_at','updated_by'
    ];
}
