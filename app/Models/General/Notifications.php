<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications';

    protected $fillable = [

        'NotificationID', 'Notification', 'NotificationFor', 'SourceID', 'SourceName', 'IsRead', 'Status', 'CreatedBy', 'updated_by', 'created_at', 'updated_at'
    ];
}
