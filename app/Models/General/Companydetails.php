<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Companydetails extends Model
{
    protected $table = 'companydetails';

    protected $fillable = [

        'CompanyDetailsID',
        'CompanyName',
        'Address',
        'Phoneno',
        'EmailID',
        'CreatedBy',
        'created_at','updated_at','updated_by'
    ];
}
