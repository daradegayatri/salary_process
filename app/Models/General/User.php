<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [

        'UserID',
        'UserRoleID',
        'UserName',
        'Password',
        'FirstName',
        'LastName',
        'EmailID',
        'PhoneNo',
        'Address',
        'CreatedBy',
        'created_at','updated_at','updated_by',
        'remember_token'
    ];
}
