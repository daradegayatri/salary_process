<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Eventdetails extends Model
{
    protected $table = 'event_details';

    protected $fillable = [

        'EventDetailsID',
        'TaskID',
        'OrganisationID',
        'CustomerID',
        'StartDate',
        'StartTime',
        'EndDate',
        'EndTime',
        'Title',
        'Description',
        'Location',
        'Organizer',
        'created_at',
        'updated_at',
        'RepeatTypeID',
        'AlertTypeID',
        'TimeZoneID',
        'TimeZone'
    ];
}
