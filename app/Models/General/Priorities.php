<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Priorities extends Model
{
    protected $table = 'priorities';

    protected $fillable = [

        'PriorityID','Priority','CreatedBy','created_at','updated_at','updated_by'
    ];
}
