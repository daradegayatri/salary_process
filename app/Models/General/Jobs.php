<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'jobs';

    protected $fillable = [

        'JobID',
        'JobName',
        'JobDescription',
        'CreatedBy',
        'created_at','updated_at','updated_by'
    ];
}
