<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = [

        'CommentID','TasklistID','UserID','Description','HasAttachment','CreatedBy','created_at','updated_at','updated_by'
    ];
}
