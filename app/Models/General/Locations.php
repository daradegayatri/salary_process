<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table = 'locations';

    protected $fillable = [

        'LocationID', 'WorkLocationID', 'LocationName', 'CreatedBy','created_at','updated_at','updated_by'
    ];
}
