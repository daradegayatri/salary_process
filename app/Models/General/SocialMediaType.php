<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class SocialMediaType extends Model
{
    protected $table = 'social_media_type';

    protected $fillable = [

        'SocialMediaTypeID', 'SocialMediaType', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
