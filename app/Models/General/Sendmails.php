<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Sendmails extends Model
{
    protected $table = 'send_mails';

    protected $fillable = [

        'MailID',
        'UserID',
        'Name',
        'EmailID',
        'MailFor',
        'MailSourceID',
        'Subject',
        'Message',
        'Status',
        'HasAttachment',
        'CreatedBy',
        'created_at','updated_at','updated_by'
    ];
}
