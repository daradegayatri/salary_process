<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;

class CreateOrderBasedOnCycleNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateOrderBasedOnCycleNumber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For orders : based on cycle number the repeat orders will be generated with the new number
                              and sent to that customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $j=0;
            $orders = DB::select(DB::raw("SELECT orders.*
                        FROM orders
                        where  orders.IsDeleted = '0'
                        AND orders.cycle_type IS NOT NULL
                        AND orders.cycle_value IS NOT NULL
                         AND orders.cycle_type <> 'NULL'
                        AND orders.cycle_value <> '0'
                        AND orders.RelatedOrderId = '0'


                        "));
            // dd($orders);
            if(!empty($orders))
            {
                foreach ($orders as $order)
                {
                    START:
                    $cycle_value = $order->cycle_value;
                    $cycle_type = $order->cycle_type;
                    $id = $order->OrderID;
                    if(empty($order->OrderRepeatDate) || $order->OrderRepeatDate ==  '0000-00-00')
                        $date = $order->created_at;
                    else
                        $date = $order->OrderRepeatDate;
                    if($order->OrderRepeatDate < date('Y-m-d'))
                    {
                        if (!empty($cycle_type) && !empty($cycle_value)) {
                            if ($cycle_type == 'Days')
                            {
                                if ($cycle_value == "1")
                                    $duration = " +" . $cycle_value . " day";
                                else
                                    $duration = " +" . $cycle_value . " days";
                            }
                            if ($cycle_type == 'Weeks')
                            {
                                if ($cycle_value == "1")
                                    $duration = " +" . $cycle_value . " week";
                                else
                                    $duration = " +" . $cycle_value . " weeks";
                            }
                            if ($cycle_type == 'Months')
                            {
                                if ($cycle_value == "1")
                                    $duration = " +" . $cycle_value . " month";
                                else
                                    $duration = " +" . $cycle_value . " months";
                            }
                            $date = strtotime($date);
                            $order_repeat_date = date('Y-m-d', strtotime($duration, $date));
                            DB::table('orders')->where('OrderID', '=', [$id])->update(array('OrderRepeatDate' => $order_repeat_date));
                            $updated_order = DB::table('orders')->where('OrderID', '=', [$id])->get();
                            $k =0;
                            if(!empty($updated_order))
                            {
                                if($updated_order[0]->OrderRepeatDate < date('Y-m-d'))
                                {
                                    $order->OrderRepeatDate =  $updated_order[0]->OrderRepeatDate;
                                    GOTO START;
                                }
                            }
                        }
                    }
                }
            }

            $results = DB::select(DB::raw("SELECT orders.*,order_status.OrderStatus,order_type.OrderType,orders.OrderTo as OrderToID,
                        u2.CustomerName as CustomerName,u2.Phoneno as CustomerPhoneno,u2.EmailID as CustomerEmailID,priorities.Priority,
                        task_severities.TaskSeverity,locations.LocationID,locations.WorkLocationID,locations.LocationName,u1.FirstName,u1.LastName,
                        u1.email,u1.PhoneNo,u1.Address,u1.Gender,u1.City,u1.Country,u1.Postalcode
                       FROM orders
                        LEFT JOIN  users as u1 ON u1.id = orders.CreatedBy
                        Left Join customers as u2 ON u2.CustomerID  = orders.OrderTo
                        Left JOIN order_status   ON order_status.OrderStatusID = orders.OrderStatusID
                        Left JOIN order_type ON order_type.OrderTypeID  = orders.OrderTypeID
                        Left Join locations ON locations.LocationID  = orders.LocationID
                        Left Join task_severities ON task_severities.TaskSeverityID  = orders.SeverityID
                        Left join priorities ON priorities.PriorityID = orders.PriorityID
                        where  orders.IsDeleted = '0'
                        AND orders.cycle_type IS NOT NULL
                        AND orders.cycle_value IS NOT NULL
                          AND orders.cycle_type <> 'NULL'
                        AND orders.cycle_value <> '0'
                        AND orders.RelatedOrderId = '0'
                        AND orders.OrderRepeatDate =  CURDATE()
                        AND (orders.RelatedOrderCreatedOn <> curdate() OR orders.RelatedOrderCreatedOn IS NULL OR orders.RelatedOrderCreatedOn = '0000-00-00')
                        "));
//dd($results);
            if (!empty($results)) {
                foreach ($results as $result) {
                    DB::beginTransaction();
                    $create_order = Order::create([
                        'OrderName' => $result->OrderName,
                        'UserID' => $result->UserID,
                        'OrderDescription' => $result->OrderDescription,
                        'OrderTypeID' => $result->OrderTypeID,
                        'OrderFrom' => $result->OrderFrom,
                        'OrderTo' => $result->OrderTo,
                        'HasAttachment' => $result->HasAttachment,
                        'SeverityID' => $result->SeverityID,
                        'PriorityID' => $result->PriorityID,
                        'EstimatedShippingTime' => $result->EstimatedShippingTime,
                        'OrderShipDate' => $result->OrderShipDate,
                        'OrganisationID' => $result->OrganisationID,
                        'RelatedOrderId' => $result->OrderID,
                        'JobID' => 1,
                        'LocationID' => $result->LocationID,
                        'BillToAddress' => $result->BillToAddress,
                        'ShipToAddress' => $result->ShipToAddress,
                        'OrderStatusID' => $result->OrderStatusID,
                        'OrderValueTotal' => $result->OrderValueTotal,
                        'OrderTaxTotal' => $result->OrderTaxTotal,
                        'Disclaimer' => $result->Disclaimer,
                        'ShipmentTrackingLink' => $result->ShipmentTrackingLink,
                        'NoOfInventory' => $result->NoOfInventory,
                        'CreatedBy' => $result->CreatedBy,
                        'IsDeleted' => '0',
                        'IsInternal' => $result->IsInternal,
                        'created_at' => Carbon::now(),
                        'cycle_type' => $result->cycle_type,
                        'cycle_value' => $result->cycle_value,
                        'units' => $result->units,
                    ]);
                    $orderid = $create_order->OrderID;
                    $order_id = "WOI" . $orderid;
                    $sourceid = $orderid;
                    $customerid = $result->OrderTo;
                    //echo($orderid);
                    DB::table('orders')->where('OrderID', '=', $create_order->OrderID)->update(array('WorkOrderID' => $order_id));
                    $inventories = DB::table('order_inventory')
                        ->select('order_inventory.*')
                        ->where('order_inventory.OrderID', '=', $result->OrderID)
                        ->get();
                    $result->Inventory = $inventories;
                    $sum = 0;
                    foreach ($inventories as $inventory) {
                        $inventorydetails = DB::table('inventory')->select('AvaliableInventoryCount', 'Price')->where('InventoryID', $inventory->InventoryID)->get();
                        if (!empty($inventorydetails)) {
                            $avaliable_inventory_count = $inventorydetails[0]->AvaliableInventoryCount;
                            $inventory_price = $inventorydetails[0]->Price;
                            $avaliable_inventory = $avaliable_inventory_count - $inventory->NoOfInventory;
                            if ($avaliable_inventory < 0)
                                $avaliable_inventory = '0';
                            DB::table('inventory')->where('InventoryID', '=', $inventory->InventoryID)->update(array('AvaliableInventoryCount' => $avaliable_inventory));

                            $orderinventory = OrderInventory::create([
                                'OrderID' => $orderid,
                                'InventoryID' => $inventory->InventoryID,
                                'NoOfInventory' => $inventory->NoOfInventory,
                                'CreatedBy' => $result->CreatedBy,
                                'created_at' => Carbon::now()
                            ]);
                            $amount = $inventory->NoOfInventory * $inventory_price;
                            $sum = $sum + $amount;
                            $orderinventory = InvoiceInventory::create([
                                'OrderID' => $orderid,
                                'InventoryID' => $inventory->InventoryID,
                                'NoOfInventory' => $inventory->NoOfInventory,
                                'InventoryPrice' => $amount,
                                'CreatedBy' => $result->CreatedBy,
                                'created_at' => Carbon::now()
                            ]);

                            $userinventorydetails = DB::table('user_inventory')
                                ->select('UserInventoryID', 'UserID', 'InventoryID', 'NoOfInventory', 'IsReturned', 'ReturnedInventoryCount', 'AvaliableInventoryCount')
                                ->where('UserID', '=', Input::get('requesterid'))
                                ->where('InventoryID', '=', $inventory->InventoryID)
                                ->get();
                            if (!empty($userinventorydetails)) {
                                $avaliable_count = intval($userinventorydetails[0]->AvaliableInventoryCount) + $inventory->NoOfInventory;
                                $inventory_count = intval($userinventorydetails[0]->NoOfInventory) + $inventory->NoOfInventory;
                                DB::table('user_inventory')->where('UserInventoryID', '=', [$userinventorydetails[0]->UserInventoryID])
                                    ->update(array('NoOfInventory' => $inventory_count, 'AvaliableInventoryCount' => $avaliable_count,
                                        'updated_by' => $result->CreatedBy,
                                        'updated_at' => Carbon::now()));
                            } else {
                                $userinventory = UserInventory::create([
                                    'UserID' => Input::get('requesterid'),
                                    'InventoryID' => $inventory->InventoryID,
                                    'NoOfInventory' => $inventory->NoOfInventory,
                                    'IsReturned' => '0',
                                    'ReturnedInventoryCount' => '0',
                                    'AvaliableInventoryCount' => $inventory->NoOfInventory,
                                    'CreatedBy' => $result->CreatedBy,
                                    'created_at' => Carbon::now()
                                ]);
                            }
                        }
                    }
                    DB::table('orders')->where('OrderID', '=', [$result->OrderID])->update(array('RelatedOrderCreatedOn' => date('Y-m-d')));
                }
                $tasks = DB::table('task_order')
                    ->select('task_order.*')
                    ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'task_order.TasklistID')
                    ->where('task_order.OrderID', '=', $result->OrderID)
                    ->get();
                if (!empty($tasks)) {
                    foreach ($tasks as $task) {
                        $task_details = DB::table('task_order')
                            ->select('OrderID', 'TasklistID', 'TaskOrderID')
                            ->where('TasklistID', '=', $task->TasklistID)
                            ->where('OrderID', '=', $result->OrderID)
                            ->get();
                        if (empty($task_details)) {
                            $order = TaskOrder::create([
                                'OrderID' => $orderid,
                                'TasklistID' => $task->TasklistID,
                                'OrderStatusID' => $task->OrderStatusID,
                                'CreatedBy' => $result->CreatedBy,
                                'created_at' => Carbon::now()
                            ]);
                        }
                    }
                }
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $result->OrderID)
                    ->where('file_attachment.FileSourceID', '=', '4')
                    ->get();
                foreach ($files as $file) {
                    $fileattachment = Fileattachment::create([
                        'FileType' => $file->FileType,
                        'FilePath' => $file->FilePath,
                        'FileSourceID' => $orderid,
                        'SourceID' => $file->SourceID,
                        'CreatedBy' => $file->CreatedBy,
                    ]);
                }
                $customer_signature = DB::table('file_attachment')
                    ->select('file_attachment.*')
                    ->where('file_attachment.FileAttachmentID', '=', $result->SignaturePad)
                    ->get();
                if (!empty($customer_signature)) {
                    foreach ($customer_signature as $cust) {
                        Fileattachment::create([
                            'FileType' => 'image/png',
                            'FilePath' => $cust->FilePath,//$filetype[$i]['name'],
                            'FileSourceID' => $cust->FileSourceID,
                            'SourceID' => $orderid,
                            'CreatedBy' => $cust->CreatedBy,
                        ]);
                    }
                }
                $organisation_name = DB::table('organisation')->where('OrganisationID', $result->OrganisationID)->pluck('OrganisationName');
                $customer_name = DB::table('customers')->select('CustomerName', 'EmailID')->where('CustomerID', $result->OrderTo)->get();
                $ordertype = DB::table('order_type')->where('OrderTypeID', $result->OrderTypeID)->pluck('OrderType');
                If (!empty($customer_name)) {
                    $firstname = $customer_name[0]->CustomerName;
                    $x = $customer_name[0]->EmailID;
                    $var = "<h1>Dear, " . $firstname . "!</h1>" .
                        "<p>" . $ordertype[0] . " Order Confirmation Letter</p>
                   <p>This is a confirmation that your order " . $orderid . " has been successfully placed and is currently under process.</p><p></p>
                   <p>" . $organisation_name[0] . " values your business and is continuously looking for ways to better satisfy their customers. Also Please Acknowledge receipt of this Order.</p>
                   <p>Best Regards,</p>
                   <p>" . $organisation_name[0] . "</p>";

                    $emails_table_input = array(
                        'Name' => $firstname,
                        'EmailID' => $x,
                        'Subject' => $ordertype[0] . " Order Confirmation Letter",
                        'Message' => $var,
                        'Status' => 1,
                        'HasAttachment' => 0,
                        'UserID' => $order->OrderTo,
                        'MailFor' => 'order_confirmation',
                        'updated_by' => Input::get('createdby'),
                        'updated_at' => Carbon::now(),
                        'CreatedBy' => Input::get('createdby'),
                        'created_at' => Carbon::now()
                    );
                }
                $notification = \App\Helpers\Helpers::create_notification($result->CreatedBy, $orderid, $result->OrganisationID, 'order');
                $transactionsourceid = '1';
                $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $result->OrderTo, $result->OrganisationID, $transactionsourceid);
                $j++;
            }
            DB::commit();


        }catch (Exception $ex)
        {
//            $message = $ex->getMessage();
//            $code = $ex->getCode();
//            $content = $code. " " . $message."for UpdateSalesRepresentative Job";;
//            $filedes = public_path().'/error.log';
//            header("plain/text");
//            file_put_contents($filedes, trim($content).PHP_EOL, FILE_APPEND);
        }
    }

}
