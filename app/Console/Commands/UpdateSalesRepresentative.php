<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Users\CustomerSalesRepresentative;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;

class UpdateSalesRepresentative extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateSalesRepresentative';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'If the Primary Sales Rep doesnt modify the Customer Record for 10 Frequency ,
      then that record automatically becomes invisible to that primary as well as all secondary sales
      reps and is visible to their supervisor and administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $j=0;
            $customers = DB::select(DB::raw("SELECT customers.*,customer_types.CustomerType,organisation_customers.OrganisationID
                          FROM customers
                          left JOIN organisation_customers ON  organisation_customers.CustomerID = customers.CustomerID
                          left JOIN customer_types ON  customer_types.CustomerTypeID = customers.CustomerTypeID
                          left JOIN  users ON users.id = customers.Created_By
                          where  customers.IsDeleted = '0' AND
                           customers.updated_at < (CASE WHEN  customers.cycle_type = 'days' THEN ( CURDATE() -   interval customers.cycle_value Day)
                                                        WHEN  customers.cycle_type = 'weeks' THEN ( CURDATE() - interval customers.cycle_value week)
                                                        WHEN  customers.cycle_type = 'months' THEN ( CURDATE() - interval customers.cycle_value month)
                                                        END)  "));
            if(!empty($customers))
            {
                foreach($customers as $customer)
                {
                    $sales_representativeid = '';
                    $orgid = $customer->OrganisationID;
                    $administrator_details = DB::table('users')
                        ->select('*')
                        ->where('users.OrganisationID','=',$orgid)
                        ->where('users.UserRoleID','=','1')
                        ->get();
                    $createdby_details = DB::table('users')
                        ->select('*')
                        ->where('users.OrganisationID','=',$orgid)
                        ->where('users.id','=',$customer->Created_By)
                        ->get();
                    if(!empty($createdby_details))
                    {
                        if(!empty($createdby_details[0]->AssignedTo))
                            $sales_representativeid = $createdby_details[0]->AssignedTo;
                        else
                            $sales_representativeid = $administrator_details[0]->id;
                    }
                    $sales_representative = DB::table('customer_sales_representative')
                        ->select('*')
                        ->where('customer_sales_representative.CustomerID','=',$customer->CustomerID)
                        ->where('customer_sales_representative.IsPrimary','=','1')
                        ->get();
                    if(!empty($sales_representative))
                    {
                        $sales_rep =  DB::table('customer_sales_representative')
                            ->where('CustomerID',$customer->CustomerID)
                            ->where('IsPrimary','1')
                            ->where('OrganizationID',$orgid)
                            ->update([
                                'SalesRepresentativeID'=> $sales_representativeid,
                                'updated_By' => $sales_representativeid,
                                'updated_at' => Carbon::now()
                            ]);
                    }
                    else
                    {
                        $sales_rep =   CustomerSalesRepresentative::create([
                            'CustomerID'=>$customer->CustomerID,
                            'SalesRepresentativeID'=>$sales_representativeid ,
                            'OrganizationID'=>$orgid,
                            'IsPrimary'=>'1',
                            'CreatedBy'=>$sales_representativeid,
                            'created_at'=>Carbon::now(),
                        ]);
                    }
                    $update_customer =  DB::table('customers')
                        ->where('CustomerID',$customer->CustomerID)
                        ->update([
                            'Created_By' => $sales_representativeid,
                            'updated_By' => $sales_representativeid,
                            'updated_at' => Carbon::now()
                        ]);
                    $j++;
                }
                $content = $j ." Records Updated Successfully on ". Carbon::now()."for UpdateSalesRepresentative Job";
                $filedes = public_path().'/error.log';
                header("plain/text");
                file_put_contents($filedes, trim($content).PHP_EOL, FILE_APPEND);

            }
            else
            {
                $content = $j ." Records Updated Successfully on ". Carbon::now()."for UpdateSalesRepresentative Job";;
                $filedes = public_path().'/error.log';
                header("plain/text");
                file_put_contents($filedes, trim($content).PHP_EOL, FILE_APPEND);
            }


        }catch (Exception $ex)
        {
            $message = $ex->getMessage();
            $code = $ex->getCode();
            $content = $code. " " . $message."for UpdateSalesRepresentative Job";;
            $filedes = public_path().'/error.log';
            header("plain/text");
            file_put_contents($filedes, trim($content).PHP_EOL, FILE_APPEND);
         }
    }
}
