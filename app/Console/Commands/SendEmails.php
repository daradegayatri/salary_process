<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\General\Sendmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Response;
use App;
use finfo;
use Barryvdh\DomPDF\PDF as PDF;
use Illuminate\Support\Facades\DB;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will Send Mails to the different users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $emails = DB::table('send_mails')
                ->select('*')
                ->where('send_mails.Status','=',0)
                ->get();
            foreach ($emails as $e) {
              $email = DB::table('send_mails')
                    ->select('*')
                    ->where('send_mails.SendMailID','=',$e->SendMailID)
                    ->get();
                $type = $email[0]->MailFor;
                //SendMailID, UserID, Name, EmailID, MailFor, Subject, Message, Status, HasAttachment, CreatedBy, updated_by, created_at, updated_at
                if ($type == "Invoice") {
                    $return_data="";
                    $error="";
                    $results = DB::table('orders')
                        ->select('orders.OrderID','orders.WorkOrderID','orders.InvoiceID')
                        ->where('orders.InvoiceID','=',$email[0]->MailSourceID)
                        ->get();
                    $count_result = count($results);
                    for($i=0;$i<$count_result;$i++)
                    {
                        $input['orderarray'][$i]= $results[$i]->OrderID;
                        $input['orderids'][$i] = $results[$i]->WorkOrderID;
                    }
                    $count = count($input['orderarray']);
                    $j=0;
                    $l=0;
                    $inventoryid_array = array();
                    for($i=0;$i<$count;$i++)
                    {
                        $invoicedetails[$j]['inventorydetails'] = DB::table('inventory')
                            ->select('inventory.InventoryID','inventory.InventoryName','invoice_inventory.NoOfInventory',
                                'invoice_inventory.InventoryPrice','invoice_inventory.CreatedBy','invoice_inventory.OrderID' )
                            ->leftjoin('invoice_inventory','invoice_inventory.InventoryID','=','inventory.InventoryID')
                            ->leftjoin('users','users.id','=','invoice_inventory.CreatedBy')
                            ->where('invoice_inventory.OrderID','=',$input['orderarray'][$i])
                            ->get();
                        if(!empty($invoicedetails[$j]['inventorydetails']))
                        {
                            $count_array = count($invoicedetails[$j]['inventorydetails']);
                            for($k=0;$k<$count_array;$k++)
                            {
                                $inventoryid_array[$l]= $invoicedetails[$j]['inventorydetails'][$k]->InventoryID;
                                $invoicedetails[$j]['orderid']= $input['orderarray'][$i];
                                $l++;
                            }
                            $j++;
                        }
                        else
                            $error .= "Order id ".$input['orderarray'][$i]." does not exists";
                    }
                    if($error=="") {
                        $unique_inventory = array_values(array_unique($inventoryid_array));
                        $count_uniquearray = count($unique_inventory);
                        $count_invoicedtailsarray = count($invoicedetails);
                        $k = 0;
                        $sum = 0;
                        $invoicedata[$k]['Amount'] = 0;
                        $invoicedata[$k]['NoofInventory'] = 0;
                        for ($p = 0; $p < $count_uniquearray; $p++)
                        {
                            for ($q = 0; $q < $count_invoicedtailsarray; $q++)
                            {
                                $count_inventorydetails = count($invoicedetails[$q]['inventorydetails']);
                                for ($r = 0; $r < $count_inventorydetails; $r++) {
                                    if ($unique_inventory[$p] == $invoicedetails[$q]['inventorydetails'][$r]->InventoryID) {
                                        $inventoryarray[$k] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryName;

                                        $invoicedata[$k]['InventoryID'] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryID;
                                        $invoicedata[$k]['Amount'] = $invoicedata[$k]['Amount'] + $invoicedetails[$q]['inventorydetails'][$r]->InventoryPrice;
                                        $invoicedata[$k]['NoofInventory'] = $invoicedata[$k]['NoofInventory'] + $invoicedetails[$q]['inventorydetails'][$r]->NoOfInventory;
                                        $invoicedata[$k]['InventoryName'] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryName;
                                        $sum = $sum + $invoicedata[$k]['Amount'];
                                    }
                                }
                            }
                            $k++;
                            if ($k < $count_uniquearray) {
                                $invoicedata[$k]['Amount'] = 0;
                                $invoicedata[$k]['NoofInventory'] = 0;
                            }
                        }
                    }
                    if(!empty($invoicedata))
                    {
                        $userdetails = DB::table('invoices')
                            ->select('invoices.created_at','invoices.InvoiceID','invoices.InvoicedisplayID','users.name','users.address')
                            ->leftjoin('users','users.id','=','invoices.CreatedBy')
                            ->where('invoices.InvoiceID','=',$email[0]->MailSourceID)
                            ->get();
                        $invoicetoname = DB::table('invoices')
                            ->select('invoices.created_at','invoices.InvoiceID','invoices.InvoicedisplayID','users.name','users.Address','users.PhoneNo')
                            ->leftjoin('users','users.id','=','invoices.InvoiceTo')
                            ->where('invoices.InvoiceID','=',$email[0]->MailSourceID)
                            ->get();
                        $return_data['inventorydata'] = $invoicedata;
                        $return_data['totalamount']= $sum;
                        $return_data['invoiceto'] = $invoicetoname[0]->name;
                        $return_data['supervisor'] = $userdetails[0]->name;
                        $return_data['createdon'] = $userdetails[0]->created_at;
                        $return_data['invoicedisplayid'] = $userdetails[0]->InvoicedisplayID;
                        $return_data['address']= $invoicetoname[0]->Address;
                        $return_data['phone']= $invoicetoname[0]->PhoneNo;
                        //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        $return_data['amountinwords'] = $sum;//$f->format($sum);
                        $return_data['note'] = "Invoice generated for following Orders IDs:-";
                        $return_data['orderid'] = implode("," ,$input['orderids']);
                    }
                    $pdf = App::make('dompdf.wrapper');
                    $pdf->loadView('invoice',
                        [
                         'inventorydata'=>$return_data['inventorydata'] ,
                        'totalamount'=>$return_data['totalamount'],
                        'invoiceto'=>$return_data['invoiceto'] ,
                        'supervisor'=>$return_data['supervisor'],
                        'createdon'=>$return_data['createdon'] ,
                        'invoicedisplayid'=>$return_data['invoicedisplayid'],
                        'address'=>$return_data['address'],
                        'phone'=>$return_data['phone'],
                        //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        'amountinwords'=>$return_data['amountinwords'],//$f->format($sum);
                        'note'=>$return_data['note'],
                        'orderid'=>$return_data['orderid'],
                        ]);
                    //var_dump($pdf->loadView);
                    $pdf->save(storage_path('email_template\invoice.pdf'));

                    Mail::raw($email[0]->Message, function ($message) use ($email) {
                        $message->to( $email[0]->EmailID, $email[0]->Name)
                            ->subject($email[0]->Subject)
                            ->attach(storage_path('email_template\invoice.pdf'), [
                                'as' => 'invoice.pdf',
                                'mime' => 'application/pdf',
                            ]);
                    });
                    DB::table('send_mails')->where('SendMailID', '=', [$email[0]->SendMailID])->update(array('Status' => '1'));
                    var_dump("Mail Sent");
                } elseif ($type == "Custom Email") {
                    $user = DB::table('users')->where('id','=',$email[0]->CreatedBy)->get();

                    if(!empty($user))
                    {
                        $email[0]->SenderID = $user[0]->email;
                        $email[0]->SenderName = $user[0]->name;
                        Mail::send([],[], function ($message) use ($email)
                        {
                            $message->setFrom($email[0]->SenderID, $email[0]->SenderName);
                            $message->to($email[0]->EmailID, $email[0]->Name);
                            $message->subject($email[0]->Subject);
                            $message->setBody($email[0]->Message, 'text/html');
                        });
                    }
                    else
                    {
                        Mail::send([],[], function ($message) use ($email) {
                            $message->to($email[0]->EmailID, $email[0]->Name);
                            $message->subject($email[0]->Subject);
                            $message->setBody($email[0]->Message, 'text/html');
                        });
                    }

                    DB::table('send_mails')->where('SendMailID', '=', [$email[0]->SendMailID])->update(array('Status' => '1'));
                } else {

                    Mail::raw($email[0]->Message, function ($message) use ($email) {

                        $message->to( $email[0]->EmailID, $email[0]->Name)
                            ->subject($email[0]->Subject);
                    });
                    var_dump("Mail Sent");
                    DB::table('send_mails')->where('SendMailID', '=', [$email[0]->SendMailID])->update(array('Status' => '1'));
                }
            }
        }
        catch (\Exception $e)
        {
            return Response::json([
                'collection' => [
                    'version' => '1.0',
                   // 'href' => $request->url(),
                    'data' => "" . $e->getMessage()
                ]
            ], 400);
        }
    }
}
