<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        \App\Console\Commands\SendEmails::class,
        \App\Console\Commands\UpdateSalesRepresentative::class,
       // \App\Console\Commands\CreateOrderBasedOnCycleNumber::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void UpdateSalesRepresentative
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('SendEmails')
            ->everyMinute();
        $schedule->command('UpdateSalesRepresentative')
            ->dailyAt('1:00');
//        $schedule->command('CreateOrderBasedOnCycleNumber')
//            ->dailyAt('2:00');
    }
}
