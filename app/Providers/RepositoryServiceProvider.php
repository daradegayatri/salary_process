<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Repositories\User\UserRepositoryInterface',
            'App\Repositories\User\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\CMS\ContentRepositoryInterface',
            'App\Repositories\CMS\ContentRepository'
        );
        $this->app->bind(
            'App\Repositories\Email\EmailTemplateRepositoryInterface',
            'App\Repositories\Email\EmailTemplateRepository'
        );
//        $this->app->bind(
//            'App\Repositories\Users\UserRepositoryInterface',
//            'App\Repositories\Users\UserRepository'
//        );
    }
}
