<?php
/**
 * Created by PhpStorm.
 * User: onwer
 * Date: 07-Jun-17
 * Time: 3:21 PM
 */

namespace App\Repositories\Invoice;


interface InvoiceInterface {

    public function create();
    public function customer();
    public function order();
    public function GetCustomerName($customerId);
    public function GetEmaiIdCustomer($customerId);
    public function invoiceType();
    public function getOrders();
    public function taskOrder($OrderID);
    public function getinvoice($userrole,$orgid,$userId);
    public function deleteInvoice($input);
    public function getInvoiceData($InvoiceID);
    public function Update($InvoiceID,array $input);
    public function invoiceTypeName();




}