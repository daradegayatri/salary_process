<?php
/**
 * Created by PhpStorm.
 * User: onwer
 * Date: 07-Jun-17
 * Time: 3:22 PM
 */

namespace App\Repositories\Invoice;
use App\Models\Invoice\InvoiceModel;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Models\Users\Customers;
use Response;
use App\Models\Orders\Order;
use App\Models\Orders\InvoiceType;
use App\Models\Orders\TaskOrder;
use DB;
use Authorizer;
use App\Models\Transactions\TransactionDetails;
use App\Models\Transactions\TransactionLog;
use App\Models\Transactions\TransactionCharges;
class InvoiceRepository  implements InvoiceInterface
{

    protected $invoiceMod;
    protected $custom;

    public function __construct(InvoiceModel $invoiceModel, Customers $customers)
    {
        $this->invoiceMod = $invoiceModel;
        $this->custom = $customers;
    }


    public function customer()
    {
        $customer = Input::get('CustomerID');
        $doctorr_id = array($customer);
        foreach ($doctorr_id as $doct) {
            $d_id = explode(',', $doct);

        }

        $length = count($d_id);
        for ($i = 0; $i < $length; $i++) {
            $customers_table_inputs = array(
                'CustomerID' => $d_id[$i],
                'CompanyName' => null,
                'CustomerName' => null,
                'Phoneno' => null,
                'EmailID' => null,
                'UserID' => null,
                'CustomerTypeID' => 1,
                'HasSocialMedia' => 0,
                'SalesRep' => 100006,
                'ParentSalesRep' => 100004,
                'PrincipalAddressID' => 1,
                'BillToAddressID' => 2,
                'ShipToAddressID' => 1,
                'Description' => null,
                'PrincipalSocialMedia' => null,
                'SecondarySocialMedia' => null,
            );
            $cust = Customers::create($customers_table_inputs);

        }

        if (!empty($record)) {
            $response['data'] = $cust;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }
    }


    public function order()
    {
        $order = Input::get('OrderID');
        $doctorr_id = array($order);
        foreach ($doctorr_id as $doct) {
            $d_id = explode(',', $doct);

        }

        $length = count($d_id);
        for ($i = 0; $i < $length; $i++) {

            $orders_table_inputs = array(
                'OrderID' => $d_id[$i],
                'OrganisationID' => null,
                'OrderName' => null,
                'OrderDescription' => null,
                'OrderTypeID' => null,
                'IsInternal' => null,
                'OrderFrom' => 1,
                'OrderTo' => 0,
                'OrderShipDate' => 100006,
                'EstimatedShippingTime' => 100004,
                'SignaturePad' => 1,
                'WorkOrderID' => 2,
                'UserID' => 1,
                'LocationID' => null,
                'InventoryID' => null,
                'InvoiceID' => null,
                'JobID' => null,
                'OrderStatusID' => null,
                'NoOfInventory' => null,
                'ShipToAddress' => null,
                'BillToAddress' => null,
                'OrderValueTotal' => null,
                'OrderTaxTotal' => null,
                'TaskId' => null,
                'Disclaimer' => null,
                'ShipmentTrackingLink' => null,
                'RelatedOrderId' => null,
                'CreatedBy' => null,
                'updated_by' => null,

            );
            $ord = Order::create($orders_table_inputs);

        }

        if (!empty($record)) {
            $response['data'] = $ord;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }


    }


    public function create()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        $invoice_list_count = DB::table('invoices')
            ->select('*')
            ->count();

        if ($invoice_list_count == "0")
            $invoice_list_id = "6000001";
        else
            $invoice_list_id = '';

        $raisedon = Input::get('Raisedon');
        $dueon = Input::get('DueOn');

        if(!empty($raisedon)||$raisedon != 'undefined')
           $raisedon =  date_format(date_create($raisedon), "Y-m-d");
        else
            $raisedon = '';

        if(!empty($dueon)||$dueon != 'undefined')
            $dueon =  date_format(date_create($dueon), "Y-m-d");
        else
            $dueon = '';

        $invoices_table_inputs = array(
            'InvoiceID' => $invoice_list_id,
            'InvoiceName' => Input::get('InvoiceName'),
            'InvoiceTypeID' => Input::get('InvoiceTypeID'),
            'Amount' => Input::get('Amount'),
            'InvoicedisplayID' => Input::get('InvoicedisplayID'),
            'OrganisationID' => $orgid,
            'OrderID' => Input::get('OrderID'),
            'InvoiceTo' => Input::get('CustomerID'),
            'RaisedInvoice' => Input::get('RaisedInvoice'),
            'Raisedon' =>$raisedon,
            'DueOn' => $dueon,
            'payment_term' => Input::get('payment_term'),
            'PastDue' => Input::get('PastDue'),
            'IsPending' => null,
            'TaskID' => Input::get('TaskID'),
            'SpecialInstructions' => Input::get('SpecialInstructions'),
            'Description' => Input::get('Description'),
            'IsPaid' => Input::get('IsPaid'),
            'EmailTo' => Input::get('EmailTo'),
            'CustomerID' => Input::get('CustomerID'),
            'CreatedBy' => $userId,
            'cycle_type' => Input::get('cycle_type'),
            'cycle_value' => Input::get('cycle_value'),
            'units' => Input::get('units')
        );
        $invoices = InvoiceModel::create($invoices_table_inputs);
        if (!empty($invoices)) {

            $sourceid = $invoices->id;
            $orderid = Input::get('OrderID');
            $customerid = Input::get('CustomerID');
            $transactionsourceid = '2';


            $notification = \App\Helpers\Helpers::create_notification($userId,$sourceid,$orgid,'invoice');

            $transactions =   \App\Helpers\Helpers::transactiondetails($sourceid,$orderid,$customerid,$orgid,$transactionsourceid);
            //$send_mail = \App\Helpers\Helpers::send_invoice_mail($invoices);
            $response['data'] = $invoices;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }



}


    public function GetCustomerName($customerId)
    {
        $results = Customers::where('CustomerID', '=', $customerId)->pluck('CustomerName');
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }


    }

    public function GetEmaiIdCustomer($customerId)
    {
        $total = Customers::where('CustomerID', '=', $customerId)->pluck('EmailID');
        if (!empty($total)) {
            $response['data'] = $total;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }


    }

    public function invoiceType()
    {
        $res = InvoiceType::all();
        //return $res;
        if (!empty($res)) {
            $response['data'] = $res;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }

    }

    public function getOrders()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if($userrole == '1')
        $record =Order::where('OrganisationID', '=', $orgid)->where('IsDeleted', '=', '0')->get();
        if($userrole == '3'|| $userrole == '4'|| $userrole == '5')
            $record =Order::where('OrganisationID', '=', $orgid)
                     ->where('orders.CreatedBy', '=', $userId)
                     ->where('orders.IsDeleted', '=', '0')
                     ->get();
        if($userrole == '2')
        {
            $record =  Order::leftjoin('users','users.id','=','orders.CreatedBy')->where('orders.OrganisationID', '=', $orgid)
                       ->where('orders.IsDeleted', '=', '0')
                       ->where(function($query){
                        $userId = Authorizer::getResourceOwnerId();
                         return $query
                        ->where('orders.CreatedBy', '=', $userId)
                       ->orWhere('users.AssignedTo', '=', $userId);
                         })
                        ->get();
        }



        if (!empty($record)) {
            $response['data'] = $record;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }
    }

    public function taskOrder($OrderID)
    {
        $taskOrder = TaskOrder::where('OrderID', '=', $OrderID)->get();
        if (!empty($taskOrder)) {
            $response['data'] = $taskOrder;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }

    }

    public  function get_Parent_Organisation_invoicelist()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        $response = $this->getinvoice($userrole,$orgid,$userId);
        return $response;
    }
    public  function get_Sub_Organisation_invoicelist($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        //$org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
       // $orgid = $org_id[0];
        $response = $this->getinvoice($userrole,$org_id,$userId);
        return $response;
    }

    public function getinvoice($userrole,$orgid,$userId)
    {

        $query = "SELECT invoices.*, organisation.OrganisationName,invoice_type.InvoiceType ,invoice_type.InvoiceTypeID,
                                        DATE(invoices.Raisedon) as Raisedondate,DATE(invoices.DueOn) as DueOndate
                                        FROM  invoices
                                        left join  invoice_type on invoices.InvoiceTypeID = invoice_type.InvoiceTypeID
                                        left join  organisation on organisation.OrganisationID = invoices.OrganisationID";
        if($userrole == "1")
              $task = DB::select(DB::raw($query ." where invoices.IsDeleted =0 and invoices.OrganisationID = $orgid" ));

        if($userrole == "3" || $userrole == "4" ||  $userrole == "5" )
            $task = DB::select(DB::raw($query ." where invoices.IsDeleted =0 and invoices.OrganisationID = '$orgid'
                                        and invoices.CreatedBy = $userId"));

        if($userrole == "2")
            $task = DB::select(DB::raw( $query . " left join users on users.id = invoices.CreatedBy
                                        where invoices.IsDeleted = 0 and invoices.OrganisationID = $orgid
                                        AND (invoices.CreatedBy = $userId OR users.AssignedTo = $userId)"));
        if (!empty($task)) {
            $response['data'] = $task;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "Invoice does not exists";
            $response['code'] = 400;
            return $response;
        }


    }

    public function deleteInvoice($input)
    {
        $input = Input::all();

        if (!empty($input)) {
            $ids = explode(",", $input['ids']);
        } else {
            $response['data'] = "Select Invoice to delete";
            $response['code'] = 400;
        }

            foreach ($ids as $id) {
                $customers = DB::table('invoices')
                    ->select('invoices.*')
                    ->where('invoices.InvoiceID', '=', $id)
                    ->get();
                if (!empty($customers)) {
                    DB::table('invoices')->where('InvoiceID', '=', $id)->update(array('IsDeleted' => '1'));
                    $response['data'] = "Invoice Details deleted successfully";
                    $response['code'] = 200;
                } else {
                    $response['data'] = "Invoice does not exists";
                    $response['code'] = 400;
                    return $response;
                }
            }
            return $response;


        }


    public function getInvoiceData($InvoiceID)
    {



        $InvoiceData = DB::select(DB::raw("SELECT  organisation.OrganisationName, organisation.Address as OrganisationAddress, organisation.EmailID as OrganisationEmailID,
                                   organisation.Phoneno as OrganisationPhoneno,customers.CustomerID, customers.CustomerName, customer_address.Address1 as CustomerAddress,
                                   customers.EmailID as CustomerEmailID , customers.Phoneno as CustomerPhoneno , tasklist.name as TaskSubject,
                                   orders.OrderName,invoice_type.InvoiceType, invoices.InvoiceTypeID, invoices.InvoiceTo, invoices.IsPaid,
                                   invoices.EmailTo,  invoices.OrganisationID, invoices.InvoiceID,  invoices.InvoiceName, invoices.IsPaid,
                                   invoices.OrderID, invoices.TaskID, invoices.Amount, invoices.SpecialInstructions, invoices.Description,
                                   invoices.PastDue as PastDueDate, DATE(invoices.DueOn) as DueOndate, DATE(invoices.Raisedon) as Raisedondate,
                                   invoices.CompanyLogo as Logo,invoices.cycle_type,invoices.cycle_value,invoices.units
                                   FROM  invoices
                                   left join invoice_type on invoices.InvoiceTypeID = invoice_type.InvoiceTypeID
                                   left join organisation on organisation.OrganisationID=invoices.OrganisationID
                                   left join orders on orders.OrderID=invoices.OrderID
                                   left join customers on customers.CustomerID=invoices.CustomerID
                                   left join customer_address on customer_address.CustomerAddressID=customers.PrincipalAddressID
                                   left join tasklist on tasklist.TasklistID=invoices.TaskID
                                   where invoices.IsDeleted =0 AND invoices.InvoiceID  =$InvoiceID"));


        if (!empty($InvoiceData)) {


            if($InvoiceData[0]->OrderID != null)
            {
                $invoice=$InvoiceData[0]->OrderID;
                //dd($invoice);
                $orders = DB::select(DB::raw("SELECT  orders.OrderID,orders.OrganisationID,orders.OrderName,
                                                orders.OrderDescription,orders.OrderTypeID,orders.IsInternal,orders.HasAttachment,
                                                orders.SeverityID,orders.PriorityID,orders.OrderFrom,orders.OrderTo,orders.OrderShipDate,
                                                EstimatedShippingTime,orders.SignaturePad,orders.WorkOrderID,orders.UserID, orders.LocationID,
                                                orders.InventoryID, orders.InvoiceID, orders.JobID,orders.OrderStatusID, orders.NoOfInventory,
                                                orders.ShipToAddress, orders.BillToAddress,orders.OrderValueTotal as Amount, orders.OrderTaxTotal as Tax,
                                                 orders.TaskId,orders.Disclaimer, orders.ShipmentTrackingLink, orders.RelatedOrderId,orders.CreatedBy,
                                                 orders.updated_by,DATE(orders.created_at) as OrderDate, orders.updated_at,orders.IsDeleted
                                                 FROM orders
                                                 left join invoices on invoices.OrderID=orders.InvoiceID
                                                 where orders.OrderID = '$invoice'"));
                 if(!empty($orders))
                 {
                     $InvoiceData[0]->OrderDetails = $orders;

                     $tasks = DB::table('tasklist')
                         ->select('tasklist.*','task_status.TaskStatus',
                             'jobs.JobName','jobs.JobDescription','priorities.Priority','task_severities.TaskSeverity','tasklist.TaskSeverityID',
                             'locations.WorkLocationID','locations.LocationName','tasklist.CreatedBy','users.name as createdbyname',
                             'assign_user_task.AssignedTo','assign_user_task.AssignedBy',
                             'assign_user_task.UserTaskStatusID','user_task_status.UserTaskStatus')
                         ->leftjoin('users','users.id','=','tasklist.CreatedBy')
                         ->leftjoin('jobs','jobs.JobID','=','tasklist.JobID')
                         ->leftjoin('task_status','task_status.TaskStatusID','=','tasklist.StatusID')
                         ->leftjoin('task_severities','task_severities.TaskSeverityID','=','tasklist.TaskSeverityID')
                         ->leftjoin('priorities','priorities.PriorityID','=','tasklist.PriorityID')
                         ->leftjoin('locations','locations.LocationID','=','tasklist.LocationID')
                         ->leftjoin('assign_user_task','assign_user_task.TasklistID','=','tasklist.TasklistID')
                         ->leftjoin('user_task_status','user_task_status.UserTaskStatusID','=','assign_user_task.UserTaskStatusID')
                         ->leftjoin('task_order','task_order.TasklistID','=','tasklist.TasklistID' )
                         ->where('task_order.OrderID','=',$invoice)
                         ->get();
                     if(!empty($tasks))
                         $InvoiceData[0]->TaskDetails = $tasks;
                     else
                         $InvoiceData[0]->TaskDetails =  array();

                 }
                else
                {
                    $InvoiceData[0]->OrderDetails = array();
                }
            }


            $response['data'] = $InvoiceData;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }

    }


    public function Update($InvoiceID, array $input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        $raisedon = Input::get('Raisedon');
        $dueon = Input::get('DueOn');

        if(!empty($raisedon)||$raisedon != 'undefined')
            $raisedon =  date_format(date_create($raisedon), "Y-m-d");
        else
            $raisedon = '';

        if(!empty($dueon)||$dueon != 'undefined')
            $dueon =  date_format(date_create($dueon), "Y-m-d");
        else
            $dueon = '';
        $invoice_input_array = array();

        if(isset($input['InvoiceName'])&& !empty($input['InvoiceName']))$invoice_input_array['InvoiceName'] = $input['InvoiceName'];
        if(isset($input['InvoiceTypeID'])&& !empty($input['InvoiceTypeID']))$invoice_input_array['InvoiceTypeID'] = $input['InvoiceTypeID'];
        if(isset($input['Amount'])&& !empty($input['Amount']))$invoice_input_array['Amount'] = $input['Amount'];
        if(isset($input['OrderID'])&& !empty($input['OrderID']))$invoice_input_array['OrderID'] = $input['OrderID'];
        if(isset($input['CustomerID'])&& !empty($input['CustomerID']))$invoice_input_array['InvoiceTo'] = $input['CustomerID'];
        if(isset($input['RaisedInvoice'])&& !empty($input['RaisedInvoice']))$invoice_input_array['RaisedInvoice'] = $input['RaisedInvoice'];
        if(isset($input['PastDue'])&& !empty($input['PastDue']))$invoice_input_array['PastDue'] = $input['PastDue'];
        if(isset($input['SpecialInstructions'])&& !empty($input['SpecialInstructions']))$invoice_input_array['SpecialInstructions'] = $input['SpecialInstructions'];
        if(isset($input['Description'])&& !empty($input['Description']))$invoice_input_array['Description'] = $input['Description'];
        if(isset($input['IsPaid'])&& !empty($input['IsPaid']))$invoice_input_array['IsPaid'] = $input['IsPaid'];
        if(isset($input['EmailTo'])&& !empty($input['EmailTo']))$invoice_input_array['EmailTo'] = $input['EmailTo'];
        if(isset($input['TaskID'])&& !empty($input['TaskID']))$invoice_input_array['TaskID'] = $input['TaskID'];
        if(isset($input['cycle_type'])&& !empty($input['cycle_type']))$invoice_input_array['cycle_type'] = $input['cycle_type'];
        if(isset($input['cycle_value'])&& !empty($input['cycle_value']))$invoice_input_array['cycle_value'] = $input['cycle_value'];
        if(isset($input['units'])&& !empty($input['units']))$invoice_input_array['units'] = $input['units'];
        if(isset($input['CustomerID'])&& !empty($input['CustomerID']))$invoice_input_array['CustomerID'] = $input['CustomerID'];
        if(isset($input['payment_term'])&& !empty($input['payment_term']))$invoice_input_array['payment_term'] = $input['payment_term'];
        $invoice_input_array['Raisedon'] = $raisedon;
        $invoice_input_array['DueOn'] = $dueon;
        $invoice_input_array['OrganisationID'] = $orgid;
        $invoice_input_array['updated_by'] = $userId;


       if(!empty($invoice_input_array))

        $color = DB::table('invoices')
             ->where('InvoiceID', $InvoiceID)
             ->update($invoice_input_array);


        if (!empty($color)) {
            $response['data'] = $color;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "Invoice Details Modified Successfully!!";
            $response['code'] = 200;
            return $response;
        }

    }
    public function invoiceTypeName()
    {
        $results = DB::table('invoices')
            ->join('invoice_type', 'invoices.InvoiceTypeID', '=', 'invoice_type.InvoiceTypeID')
            ->select('invoice_type.InvoiceType','invoice_type.InvoiceTypeID','invoices.InvoiceID as InvoiceID','invoices.InvoiceName')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
           return $response;
       } else {
            $response['data'] = "No record exists";
          $response['code'] = 400;
        }
        return $response;


    }


}















