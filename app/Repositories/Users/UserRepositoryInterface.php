<?php
/**
 * Created by PhpStorm.
 * Users: admin-nitai
 * Date: 9/26/2016
 * Time: 3:37 PM
 */
namespace App\Repositories\Users;

interface UserRepositoryInterface{

    public function createuser($input,$url);
    public function updateuser($id);
    public function changePassword($input);
    public function get_userrole_by_id($id);
    public function get_userroles();
    public function get_supervisors_list();
    public function get_employees_list();
    public function get_workers_list();
    public function create_customer($input);
    public function get_customers();
    public function get_customers_by_id($id);
    public function delete_customers_by_id($id);
    public function update_customers_by_id($id);
    public function  get_customerslist($userrole,$orgid,$userId);
    public function get_social_media_types();
    public function GlobalSearch($input);
    public function getDetails();
    public function Change_User_Password($id);
    public function Change_User_Status();
    public function get_all_organisation($OrganisationID);
    public function  getAllUsers($userrole,$org_id,$userId);

}