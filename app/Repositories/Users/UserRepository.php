<?php
/**
 * Created by PhpStorm.
 * Users: admin-nitai
 * Date: 9/26/2016
 * Time: 3:38 PM
 */

namespace App\Repositories\Users;
use App\Models\Users\CustomerAddress;
use App\Models\Users\Customers;
use App\Models\Users\CustomersSocialMediaDetails;
use App\Models\Users\OrganisationCustomers;
use App\Models\Users\SubCustomers;
use Validator;
use Hash;
use App\Models\Users\User;
use App\Models\General\Sendmails;
use App\Models\General\Notifications;
use App\Repositories\Users\UserRepositoryInterface;
use Dingo\Blueprint\Annotation\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\RandomPassword;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Mail;
use App\Models\Transactions\TransactionCharges;
use App\Models\Transactions\TransactionDetails;
use App\Models\Transactions\TransactionLog;
use App\Models\Users\CustomerSalesRepresentative;
use App\Models\Files\Filesource;
use App\Models\Files\Fileattachment;

class UserRepository implements UserRepositoryInterface{
    protected $users;
    //protected $usersrepository;
    public function __construct(User $user,Request $request)
    {
        $this->users = $user;

        $this->request = $request;
    }

    public  function get_Parent_Organisation_Users()
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();
        $response = $this->getAllUsers($userrole,$org_id[0]->OrganisationID,$userId);
        return $response;
    }
    public  function get_Sub_Organisation_Users($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $response =  $this->getAllUsers($userrole,$org_id,$userId);
        return $response;
    }

    public  function getAllUsers($userrole,$org_id,$userId)
    {

         if($userrole == "1")
             $results = DB::table('users')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->join('organisation','organisation.OrganisationID','=','users.OrganisationID')
                ->where('users.OrganisationID','=',$org_id)
                ->where('users.IsDeleted','=',null)
                ->where('users.UserRoleID','<>',6)
                 ->select('users.*','organisation.OrganisationName')
                ->get();
        if($userrole == "2")
            $results = DB::table('users')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->join('organisation','organisation.OrganisationID','=','users.OrganisationID')
                ->where('users.OrganisationID','=',$org_id)
                ->where('users.IsDeleted','=',null)
                ->where('users.UserRoleID','<>',6)
                ->where('users.UserRoleID','<>',1)
                ->where(function($query){
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('users.CreatedBy', '=', $userId)
                        ->orWhere('users.CreatedBy', '=', null)
                        ->orWhere('users.AssignedTo', '=', $userId);
                })
                ->select('users.*','organisation.OrganisationName')
                ->get();
        if($userrole == "3" || $userrole == "4" || $userrole == "5")
            $results = DB::table('users')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->join('organisation','organisation.OrganisationID','=','users.OrganisationID')
                ->where('users.OrganisationID','=',$org_id)
                ->where('users.IsDeleted','=',null)
                ->where('users.UserRoleID','<>',6)
                ->where('users.UserRoleID','<>',1)
                ->where('users.UserRoleID','<>',2)
                ->where(function($query){
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('users.CreatedBy', '=', $userId)
                        ->orWhere('users.CreatedBy', '=', null);
                })
                ->select('users.*','organisation.OrganisationName')
                ->get();
        if(!empty($results))
        {

            $count = count($results);
            for($i=0;$i<$count;$i++)
            {
                $address = explode('?',$results[$i]->Address);

                if(!empty($address))
                {
                    if(isset($address[0]))  $results[$i]->Address = $address[0];
                    if(isset($address[1]))  $results[$i]->Address = $results[$i]->Address." ".$address[1];

                }
                else
                  $results[$i]->Address = "";
            }



            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Users does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public  function find($userId)
    {
        $user=$this->user->findOrFail($userId);
        return $user;
    }
    public function findNoFailUser($id)
    {
        return $this->users->find($id);
    }
    public function createuser($input,$url)
    {
        $input = Input::all();
        $rules = array(
            'email' => 'required|email',
            'userrole' => 'required',
            /*'phoneno' => 'required|numeric|unique:users,PhoneNo|regex:/[0-9]{10}/',*/
            'name' => 'required|'
        );

        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $userId = Authorizer::getResourceOwnerId();
            $org_id = DB::table('users')
                ->select('users.id as UserID', 'users.OrganisationID')
                ->where('users.id', '=', $userId)
                ->get();
            $password = $this->GenerateString();
            RECHECK_USER:
            $exists = DB::table('users')
                ->select('users.*')
                ->where('users.email', '=', Input::get('email'))
                ->get();
            //dd($exists);
            if(!empty($exists))
            {
                if($exists[0]->IsDeleted == "1" )
                {
                    $id = $exists[0]->id;
                    $email = $this->GenerateString();
                    //echo "email:-".$email;
                    DB::table('users')->where('id', '=', [$id])->update(array('username' => $email,'email' => $email));
                    goto RECHECK_USER;

                }
                else
                {
                    $response['data'] = "The email has already been taken";
                    $response['code'] = 400;
                    return $response;

                }
            }

            $street = Input::get('street');
            if(!empty($street))
                $address =  Input::get('address').'?'.Input::get('street');
            else
                $address = Input::get('address');



            DB::beginTransaction();
            $user_list_count = DB::table('users')
                ->select('*')
                ->count();

            if ($user_list_count == "0")
                $user_list_id = "1000001";
            else
                $user_list_id = '';
            $person = User::create([
                'id' => $user_list_id,
                'name' => Input::get('name'),
                'Gender' => Input::get('gender'),//$input['gender'],
                'email' => Input::get('email'),
                'username' => Input::get('email'),// $input['us_citizen'],
                'password' => Hash::make($password),//$input['nationality'],
                'UserRoleID' => Input::get('userrole'),//$input['e_firstname'],
                'OrganisationID' => $org_id['0']->OrganisationID,
                'City' => Input::get('city'),//$input['e_lastname'],
                'State' => Input::get('State'),
                'Country' => Input::get('country'),//$input['e_relation'],
                'Postalcode' => Input::get('postal_code'), //$input['upload_image'],
                'PhoneNo' => Input::get('phoneno'),//$input['type']
                'AssignedTo' => Input::get('assignto'),
                'Address' =>  $address,
                'Description'=> Input::get('Description'),
                'jobtitle' => Input::get('jobtitle'),
                'CreatedBy' => $userId,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            $sourceid = $person->id;
            $orderid = "";
            $customerid = "";
            $transactionsourceid = '5';
            $orgid = $org_id['0']->OrganisationID;
             $transactions = \App\Helpers\Helpers::transactiondetails($sourceid,$orderid,$customerid,$orgid,$transactionsourceid);


            DB::commit();
            $personId = $person->id;

            if (isset($input['child_id']) && !empty($input['child_id']) && is_null($input['child_id'])) {
                $ids = explode(',', Input::get('child_id'));
                foreach ($ids as $id) {
                    $person = User::find($id);
                    $person->AssignedTo = $personId;
                    $person->save();
                }
            }

            $firstname = Input::get('name');
            $x = Input::get('email');
            $var = "<p>Hi, " . $firstname . "!</p>" .
                "<p>Welcome to MobiComm CEM</p>.
                <p>Please Login to our Application using Following Details.</p><p></p>
                <p>Username: $x </p>
                <p>Password: " . $password . "</p>
                <p>Link: $url</p>
                <p></p>
                <p>Thank you for registering.</p>
                <p> Regards,</p>
                <p> Team CEM </p>";
            //SendMailID, UserID, Name, EmailID, Subject, Message, Status, HasAttachment, CreatedBy, updated_by, created_at, updated_at
            $emails_table_input = array(
                'Name' => $firstname,
                'EmailID' => Input::get('email'),
                'Subject' => "Welcome to MobiComm CEM!",
                'Message' => $var,
                'Status' => 0,
                'HasAttachment' => 0,
                'UserID' => $person->id,
                'MailFor' => 'Registration',
                'updated_by' => Input::get('createdby'),
                'updated_at' => Carbon::now(),
                'CreatedBy' => Input::get('createdby'),
                'created_at' => Carbon::now()
            );
            $email = Sendmails::create($emails_table_input);
            Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
                    ->subject($emails_table_input['Subject']);
            });

            $response['data'] = "User Registered Successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }
    public function updateuser($id){
       // dd($id);
        $input = Input::all();
        //dd($input);
        $error = "";
        $check_emails="";
        $number = "";
        $check_phone="";
        if(!empty($input['email']))   $email = $input['email'];
        if(!empty($input['phoneno']))     $number = $input['phoneno'];
        if(!empty($input['username']))  $username = $input['username'];
        //dd($input);
        //check email
        if(!empty($input['email']))
        {
            $check_emails = DB::table('users')
                ->select('id','email')
                ->where('users.email','=',$input['email'])
                ->get();
                if(!empty($check_emails))
                {
                    if(($check_emails[0]->id == $id))
                        $error .= "";
                    else
                        $error .= "Email Id already exists";
                }
                else
                {
                    $emails = User::find($id);
                    $emails->email     = $email;
                    $emails->updated_at =Carbon::now();
                    $emails->save();
                }
        }
        if(!empty($input['phoneno']))
       {
//            $check_phone = DB::table('users')
//                ->select('id','PhoneNo')
//                ->where('users.PhoneNo','=',$input['phoneno'])
//                ->get();
//            if(!empty($check_phone))
//            {
//                if(($check_phone[0]->id == $id))
//                    $error .= "";
//                else
//                    $error .= "Phone number already exists";
//
//            }
//            else
//            {
                 $phone = User::find($id);
                 $phone->PhoneNo  = $number;
                 $phone->save();
//            }
//
       }
        if(!empty($input['username']))
        {
            $check_username = DB::table('users')
                ->select('id','username')
                ->where('users.username','=',$input['username'])
                ->get();
            if(!empty($check_username))
            {
                if(($check_username[0]->id == $id))
                    $error .= "";
                else
                    $error .= "username already exists";
            }
            else
            {
                $userdetails = User::find($id);
                $userdetails->username  = $username;
                $userdetails->save();
            }
        }
        if($error=="")
        {
            if(!empty($input['firstname']))
                DB::table('users')->where('id', '=', [$id])->update(array('FirstName' => $input['firstname']));
            // $affected = DB::update('update people set firstname = ?, where id = ?', [$input['firstname']],[$id]);
            if(!empty($input['lastname']))
                DB::table('users')->where('id', '=', [$id])->update(array('LastName' => $input['lastname']));
            if(!empty($input['name']))
                DB::table('users')->where('id', '=', [$id])->update(array('name' => $input['name']));
            if(!empty($input['gender']))
                DB::table('users')->where('id', '=', [$id])->update(array('Gender' => $input['gender']));
            if(!empty($input['address']))
            {
                $address = DB::table('users')->select('Address')->where('users.id','=',$id)->get();
                if(!empty($address))
                {
                    $address = explode('?',$address[0]->Address);
                    if(!empty($address))
                    {
                        if(isset($address[1]))
                        $updateaddress = $input['address'].'?'.$address[1];
                        else
                            $updateaddress = $input['address'];
                        DB::table('users')->where('id', '=', [$id])->update(array('Address' => $updateaddress));
                    }
                    else{
                        DB::table('users')->where('id', '=', [$id])->update(array('Address' => $input['address']));
                    }
                }
                else
                    DB::table('users')->where('id', '=', [$id])->update(array('Address' => $input['address']));

            }
            if(!empty($input['street']))
            {
                $address = DB::table('users')->select('Address')->where('users.id','=',$id)->get();
                if(!empty($address))
                {
                    $address = explode('?',$address[0]->Address);
                    if(!empty($address))
                    {
                        if(isset($address[0]))
                            $updateaddress = $address[0].'?'.$input['street'];
                        else
                            $updateaddress = $input['street'];
                        DB::table('users')->where('id', '=', [$id])->update(array('Address' => $updateaddress));
                    }
                    else{
                        DB::table('users')->where('id', '=', [$id])->update(array('Address' => $input['street']));
                    }
                }
                else
                    DB::table('users')->where('id', '=', [$id])->update(array('Address' => $input['street']));

            }
            if(!empty($input['city']))
                DB::table('users')->where('id', '=', [$id])->update(array('City' => $input['city']));
            if(!empty($input['State']))
                DB::table('users')->where('id', '=', [$id])->update(array('State' => $input['State']));
            if(!empty($input['country']))
                DB::table('users')->where('id', '=', [$id])->update(array('Country' => $input['country']));
            if(!empty($input['postal_code']))
                DB::table('users')->where('id', '=', [$id])->update(array('Postalcode' => $input['postal_code']));
            if(!empty($input['userole']))
                DB::table('users')->where('id', '=', [$id])->update(array('UserRoleID' => $input['userole']));
            if(!empty($input['AssignedTo']))
            {
                if($input['AssignedTo']!= null && $input['AssignedTo']!= 'null' )
                    DB::table('users')->where('id', '=', [$id])->update(array('AssignedTo' => $input['AssignedTo']));
            }
            if(!empty($input['Description']))
                DB::table('users')->where('id', '=', [$id])->update(array('Description' => $input['Description']));
            if(!empty($input['jobtitle']))
                DB::table('users')->where('id', '=', [$id])->update(array('jobtitle' => $input['jobtitle']));

            if (isset($input['child_id']) && $input['child_id'] != "null") {
                $ids = explode(',', Input::get('child_id'));
                foreach ($ids as $index => $value) {
                    $ids[$index] = (int)$value; 
                }
                $childId = DB::table('users')->where('users.AssignedTo','=',$id)->lists('id');
                $childIdsToDelete = array_diff($childId, $ids);
                $childIdsToAdd = array_diff($ids, $childId);

                if (!empty($childIdsToDelete)) {
                    foreach ($childIdsToDelete as $cid) {
                        DB::table('users')->where('id', '=', $cid)->update(array('AssignedTo' => null));
                    }
                }
                if (!empty($childIdsToAdd)) {
                    foreach ($childIdsToAdd as $cid) {
                        DB::table('users')->where('id', '=', $cid)->update(array('AssignedTo' => $id));
                    }
                }
            } else {
                $childId = DB::table('users')->where('users.AssignedTo','=',$id)->lists('id');
                foreach ($childId as $cid) {
                    DB::table('users')->where('id', '=', $cid)->update(array('AssignedTo' => null));
                }
            }

            if(\Request::hasFile('image')) {
                $file = \Request::file('image');
                $originalfilename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $filename = $id.'_'.$originalfilename;
                $destinationPath = public_path('images');
                $path = $file->move($destinationPath, $filename);
                $filePath = asset('images/'.$filename);
                //dd($filePath);
                DB::table('users')->where('id', '=', [$id])->update(array('image' => $filePath));
            }
           if(isset($input['signature']))
           {
               $signaturedata = $input['signature'];

               if(!empty($signaturedata)) {
                   if ($signaturedata != 'undefined') {
                       $fileSourceId = Filesource::where('FileSource', '=', 'Signature')->pluck('FileSourceID');
                       $destinationpath = public_path('images');
                       $output_file = $destinationpath . '/' . $id . "_" . time() . '.png';
                       $image = file_put_contents($output_file, base64_decode(explode(',', $signaturedata)[1]));
                       if (isset($signaturedata)) {
                           $filepath = asset('images/' . $id . "_" . time() . '.png');
                           $fileattachment = Fileattachment::create([
                               'FileType' => 'image/png',
                               'FilePath' => $filepath,
                               'FileSourceID' => $fileSourceId[0],
                               'SourceID' => $id,
                               'CreatedBy' => $id,
                           ]);
                           $filetype = $fileattachment->id;
                       } else
                           $filetype = null;

                       DB::table('users')->where('id', '=', [$id])->update(array('signaturepad' => $filetype));
                   }
               }

           }


            $response['data']= "User Record updated successfully";
            $response['code']= 200;
            return $response;
        }
        else
        {
            $response['data'] = $error;
            $response['code'] = 400;
            return $response;
        }
    }
    public function get_userroles()
    {
        //Sales Representative
        $results = DB::table('user_role')
            ->select('UserRoleID','UserRole')
            ->where('UserRole','<>','SuperAdmin')
            ->where('UserRole','<>','Sales Representative')
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "User role does not exists";
            $response['code']=400;

        }
        return $response;
    }
    public function get_userrole_by_id($id){
        $results = DB::table('user_role')
            ->select('UserRoleID','UserRole')
            ->where('UserRoleID','=',$id)
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "User role does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_userrole_by_name($userrole)
    {
        $results = DB::table('user_role')
            ->select('UserRoleID','UserRole')
            ->where('UserRole','=',$userrole)
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "User role does not exists";
            $response['code']=400;

        }
        return $response;
    }
    public function get_supervisors_list()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();

        $results = DB::table('users')
            ->select('id as UserID', 'FirstName', 'LastName', 'email as EmailID', 'PhoneNo', 'Gender', 'Address', 'City', 'Country', 'Postalcode', 'users.CreatedBy')
            ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
            ->where('user_role.UserRoleID','=','2')
            ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Supervisor does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_employees_list()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();
        //dd($org_id);
        $results = DB::table('users')
            ->select('users.id as UserID', 'users.OrganisationID','users.FirstName', 'users.LastName', 'users.email as EmailID', 'users.PhoneNo', 'users.Gender', 'users.Address',
                'users.City', 'users.Country', 'users.Postalcode', 'users.CreatedBy','users.AssignedTo',  'a.name as AssignedToName' )
            ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
            ->leftjoin('users as a','a.id','=','users.AssignedTo')
            ->where('user_role.UserRoleID','=','3')
            ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Employee does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_employees_list_bysupervisorid($id)
    {
       // $userId = Authorizer::getResourceOwnerId();
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$id)
            ->get();

        $results = DB::table('users')
            ->select('users.id as UserID', 'users.FirstName', 'users.LastName', 'users.email as EmailID', 'users.PhoneNo', 'users.Gender', 'users.Address',
                'users.City', 'users.Country', 'users.Postalcode', 'users.CreatedBy','users.AssignedTo',  'a.name as AssignedToName'  )
            ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
            ->where('user_role.UserRoleID','=','3')
            ->leftjoin('users as a','a.id','=','users.AssignedTo')
            ->where('users.AssignedTo','=',$id)
            ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Employee does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_workers_list()
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();
        if($userrole == '1')
        {
            $results = DB::table('users')
                ->select('id as UserID', 'FirstName', 'LastName', 'email as EmailID', 'PhoneNo', 'Gender', 'Address', 'City', 'Country', 'Postalcode', 'users.CreatedBy', 'AssignedTo')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->where('user_role.UserRoleID','=','4')
                ->where('user_role.UserRoleID','=','2')
                ->where('user_role.UserRoleID','=','3')
                ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
                ->get();
        }
        if($userrole == '2')
        {
            $results = DB::table('users')
                ->select('id as UserID', 'FirstName', 'LastName', 'email as EmailID', 'PhoneNo', 'Gender', 'Address', 'City', 'Country', 'Postalcode', 'users.CreatedBy', 'AssignedTo')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->where('user_role.UserRoleID','=','4')
                ->where('user_role.UserRoleID','=','3')
                ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
                ->where(function($query){
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('users.CreatedBy', '=', $userId)
                        ->orWhere('users.AssignedTo', '=', $userId);
                })
                ->get();
        }
        if($userrole == '3')
        {
            $results = DB::table('users')
                ->select('id as UserID', 'FirstName', 'LastName', 'email as EmailID', 'PhoneNo', 'Gender', 'Address', 'City', 'Country', 'Postalcode', 'users.CreatedBy', 'AssignedTo')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->where('user_role.UserRoleID','=','4')
                ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
                ->where(function($query){
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('users.CreatedBy', '=', $userId)
                        ->orWhere('users.AssignedTo', '=', $userId);
                })
                ->get();
        }
        if($userrole == '4')
        {
            $results = DB::table('users')
                ->select('id as UserID', 'FirstName', 'LastName', 'email as EmailID', 'PhoneNo', 'Gender', 'Address', 'City', 'Country', 'Postalcode', 'users.CreatedBy', 'AssignedTo')
                ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                ->where('user_role.UserRoleID','=','4')
                ->where('users.OrganisationID','=',$org_id['0']->OrganisationID)
                ->where(function($query){
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('users.CreatedBy', '=', $userId)
                        ->orWhere('users.AssignedTo', '=', $userId);
                })
                ->get();
        }
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Contractor does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_userdetails_by_id($id)
    {
        //id, UserRoleID, OrganisationID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode,
        // AssignedTo, UserImage, CreatedBy, updated_by, remember_token, created_at, updated_at, provider, provider_id, status, IsDeleted
        $results = DB::table('users')
            ->select('users.id as UserID',
                      'user_role.UserRoleID',
                      'user_role.UserRole',
                      'users.name',
                      'users.username',
                      'users.FirstName',
                       'users.LastName',
                       'users.email',
                       'users.PhoneNo',
                       'users.Gender',
                       'users.Address',
                       'users.City',
                       'users.State',
                       'users.Country',
                       'users.Postalcode',
                       'users.CreatedBy',
                       'users.AssignedTo',
                       'users.Description',
                       'users.jobtitle',
                       'users.image',
                       'a.name as AssignedToName' )
            ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
            ->leftjoin('users as a','a.id','=','users.AssignedTo')
            ->where('users.id','=',$id)
            ->get();
        if(!empty($results))
        {
            $childId = DB::table('users')->select('users.id as ChildId','users.name as ChildName')
                    ->where('users.AssignedTo','=',$id)
                    ->get();

            $results[0]->child = $childId;
            $address = explode('?',$results[0]->Address);
            if(!empty($address))
            {
                if(isset($address[0]))  $results[0]->Address = $address[0];
                else  $results[0]->Address = "";
                if(isset($address[1]))  $results[0]->Street = $address[1];
                else  $results[0]->Street = "";
                //$results[0]->Street = $address[1];
            }
            else
            {
                $results[0]->Address = "";
                $results[0]->Street = "";
            }

            $fileSourceId = Filesource::where('FileSource', '=', 'Signature')->pluck('FileSourceID');
            $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $id)
                    ->where('file_attachment.FileSourceID',$fileSourceId[0])
                    ->get();

            $results[0]->signature = $files;        

            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "User does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function deleteuser($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        }
        else
        {
            $response['data'] = "Select User to delete" ;
            $response['code']=400;
            return $response;
        }
        foreach ($ids as $id) {
            $user = $this->findNoFailUser($id);
            if(!empty($user))
            {
                $email = $this->GenerateString();
                $username = $this->GenerateString();

                DB::table('users')->where('id', '=', $id)->update(array('IsDeleted' => '1','email' => $email,'username' => $username ));
                $response['data'] = "User details deleted successfully";
                $response['code']=200;
            }
            else
            {
                $response['data'] = "User does not exists" ;
                $response['code']=400;
                return $response;
            }
        }
        return $response;
    }
    public function get_notification_by_userid($id)
    {
        $results = DB::table('notifications')
            ->select('notifications.*')
              ->where('notifications.NotificationFor','=',$id)
            ->where('notifications.Status','=','0')
            ->where('notifications.IsRead','=','0')
            ->orderBy('notifications.created_at')
            ->get();
        if(!empty($results))
        {
            $count = count($results);
            $data['count'] = $count;
            $data['data'] = $results;
            $response['data'] = $data;
            $response['code']=200;
            return $response;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
            return $response;
        }
    }
    public function get_hidden_notifications_by_userid($id)
    {
        $results = DB::table('notifications')
            ->select('notifications.*')
            ->where('notifications.NotificationFor','=',$id)
            ->where('notifications.Status','=','1')
            ->orderBy('notifications.created_at')
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
            return $response;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
            return $response;
        }
    }
    public function get_unread_notifications_count($id)
    {
        $results = DB::table('notifications')
            ->select('notifications.*')
            ->where('NotificationFor','=',$id)
            ->where('IsRead','=',0)
            ->where('notifications.Status','=','0')
            ->orderBy('notifications.created_at')
            ->count();
        if(!empty($results))
        {
            //$count =
            $response['data'] = $results;
            $response['code']=200;
            return $response;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
            return $response;
        }
    }
    public function change_notification_status($id,$status)
    {
        $results = DB::table('notifications')
            ->select('NotificationID','Notification','IsRead')
            ->where('NotificationID','=',$id)
            ->get();
        if(!empty($results))
        {
            if($results[0]->IsRead != $status)
            {
                DB::table('notifications')->where('NotificationID', '=', [$id])->update(array('IsRead' => $status));
                $data = "Record Updated Successfully";
            }
            else
                $data = "Nothing to Update!! Record is Up-to-date";

            $response['data'] = $data;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
        }
        return $response;
    }
    public function hide_notification($id)
    {
        $results = DB::table('notifications')
            ->select('NotificationID','Notification','Status')
            ->where('NotificationID','=',$id)
            ->get();
        if(!empty($results))
        {
            if($results[0]->Status != 1)
            {
                DB::table('notifications')->where('NotificationID', '=', [$id])->update(array('Status' => '1'));
                $data = "Record Updated Successfully";
            }
            else
                $data = "Nothing to Update!! Record is Up-to-date";

            $response['data'] = $data;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
        }
        return $response;
    }
    public function delete_notification($id)
    {
        $notifications =DB::table('notifications')
            ->select('NotificationID','Notification')
            ->where('notifications.NotificationID','=',$id)
            ->get();
        if(!empty($notifications))
        {

            DB::delete('delete from notifications where NotificationID = ?',[$id]);
            $response['data'] = "Notification details deleted successfully";
            $response['code']=200;
            return $response;
        }
        else
        {
            $response['data'] = "Notification Details does not exists";
            $response['code']=400;
            return $response;
        }

    }
    public function forgotpassword($input)
    {
        try {

            if (Input::has('email'))
                {

                    $password = $this->GenerateString();
                    $email = Input::get('email');
                    if (User::where('email', '=', $email)->exists()) {
                        $user = DB::table('users')->where('email', $email)->pluck('id');
                        $user = User::find($user)->first();
                        $user->password = Hash::make($password);
                        $user->save();
                    $var = "<p>Hi, " . $user->name . "!</p>" .
                        "<p>Password Recovery mail .</p><p></p>
                            <p> Username: " . $user->username . "</p>
                             <p>Password: " . $password . "</p><p></p>
                             <p>Thank you for registering.</p>
                             <p>Regards,</p>
                             <p>Team CEM</p>";
                        $emails_table_input = array(
                            'Name' => $user->name,
                            'EmailID' => $user->email,
                            'Subject' => "Password Recovery!",
                            'Message' => $var,
                            'Status' => 1,
                            'HasAttachment'=>0,
                            'UserID' => $user->id,
                            'MailFor' => 'Password',
                            'updated_by'=>$user->id,
                            'updated_at'=> Carbon::now(),
                            'CreatedBy'=>$user->id,
                            'created_at'=> Carbon::now()
                        );
                        $email = Sendmails::create($emails_table_input);
                        Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                            $message->to( $emails_table_input['EmailID'], $emails_table_input['Name'])
                                ->subject($emails_table_input['Subject']);
                        });
                       $response['data'] = "Please Check your registered email to recover Password";
                        $response['code']=200;
                    }
                    else
                    {
                        $response['data'] = "Invalid Email Id";
                        $response['code']=400;
                    }
//                $response['data'] = "Please Check your registered email to recover Password";
//                $response['code']=200;
                }
            else
            {
                $response['data'] = "Email Id is required";
                $response['code']=400;
            }
        } catch (\Exception $e)
        {
            $response['data'] = $e->getMessage();
            $response['code']=400;
        }
        return $response;
    }
    public function changePassword($input)
    {
        DB::beginTransaction();
        $input = Input::all();
        $id = Authorizer::getResourceOwnerId();
        $user = User::find($id);
        $validation = Validator::make(Input::all(), [

            // Here's how our new validation rule is used.
            'password' => 'hash:' . $user->password,
            'new_password' => 'required|different:password|confirmed'
        ]);
        if ($validation->fails()) {
            return $validation->errors();
        }
        $user->password = Hash::make($input['new_password']);
        $user->save();
    }
    public function GenerateString()
    {
        $unambiguousCharacters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
        $temporaryPassword = "";
        // build a temporary password consisting of 8 unambiguous characters
        for ($i = 0; $i < 8; $i++) {
            $temporaryPassword .= $unambiguousCharacters[rand(0, strlen($unambiguousCharacters) - 1)];
        }
        return $temporaryPassword;
    }
    public function create_customer($input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        //Input::get('ChildCustId');
        $rules = array(
            'EmailID' => 'required|email|unique:customers,EmailID',
            //'userrole' => 'required',
            //'phoneno' => 'required|numeric|unique:users,PhoneNo|regex:/[0-9]{10}/',
            'FirstName' => 'required',
            //'OrganisationID' =>'exists:organisation,OrganisationID',
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            DB::beginTransaction();
            $customer_list_count = DB::table('customers')
                ->select('*')
                ->count();

            if ($customer_list_count == "0")
                $customer_list_id = "9000001";
            else
                $customer_list_id = '';
            $id = Authorizer::getResourceOwnerId();
            $users = User::find($id);
            $error = "";
            $customername = Input::get('Prefix')." ".Input::get('FirstName')." ".Input::get('MiddleName')." ".Input::get('LastName');
            $cust_address = CustomerAddress::create([
                'Address1' => Input::get('Address1'),
                'Address2' => Input::get('Address2'),
                'City' => Input::get('City'),
                'State' => Input::get('State'),
                'Country' => Input::get('Country'),
                'Zipcode' =>Input::get('Zipcode'),
                'created_by' => $id,
                'updated_by' => $id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            $user=User::create([
                'name'=> $customername,//$customer->CustomerName,
                'email'=> Input::get('EmailID'),//$customer->EmailID,
                'UserRoleID' => '6',
                'PhoneNo' => Input::get('phoneno'),//$customer->Phoneno,
                'OrganisationID'=> $orgid,//Input::get('OrganisationID'),
                'username' => $this->GenerateString(),//$customer->CustomerName,
                'CreatedBy' => $users->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            $customer= Customers::create([
                'CustomerID' => $customer_list_id,
                'CustomerName' => $customername,
                'FirstName' => Input::get('FirstName'),
                'MiddleName' => Input::get('MiddleName'),
                'LastName' => Input::get('LastName'),
                'Prefix' => Input::get('Prefix'),
                'CompanyName'=> Input::get('CompanyName'),
                'Phoneno' => Input::get('phoneno'),
                'Phoneno2' => Input::get('phoneno2'),
                'EmailID'=> Input::get('EmailID'),
                'website'=> Input::get('website'),
                'UserID'=> $user->id,//Input::get('UserID'),
                'CustomerTypeID'=>Input::get('CustomerTypeID'),
                'SalesRep'=>Input::get('ParentSalesRep'),
                'PrincipalAddressID'=>$cust_address->id,
                'BillToAddressID'=>$cust_address->id,
                'ShipToAddressID'=>$cust_address->id,
                'Description'=>Input::get('Description'),
                'TransactionLink'=>Input::get('TransactionLink'),
                'ParentCustId'=>Input::get('ParentCustId'),
                'ChildCustId'=>Input::get('ChildCustId'),
                'NoOfSubCustomer'=>Input::get('NoOfSubCustomer'),
                'PrincipalSocialMediaID'=>Input::get('PrincipalSocialMediaID'),
                'PrincipalSocialMedias'=>Input::get('PrincipalSocialMedias'),
                'SecondarySocialMediaID'=>Input::get('SecondarySocialMediaID'),
                'SecondarySocialMedias'=>Input::get('SecondarySocialMedias'),
                'InitialDealValue'=>Input::get('InitialDealValue'),
                'ClosedDealValue'=>Input::get('ClosedDealValue'),
                'CustomerStageID' =>Input::get('CustomerStageID'),
                'Group' =>Input::get('Group'),
                'cycle_type' => Input::get('cycle_type'),
                'cycle_value' => Input::get('cycle_value'),
                'jobtitle' => Input::get('jobtitle'),
                'units' => Input::get('units'),
                'Created_By' => $id,
                'Updated_By' => $id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                //'ShipToAddressID'=>$cust_address->CustomerAddressID,
                //'Description'=>Input::get('Description'),
                //'TransactionLink'=>Input::get('TransactionLink'),
                //'SalesRep'=>'',//Input::get('SalesRep'),
                //'ParentSalesRep'=>Input::get('ParentSalesRep'),
                //'PrincipleAddressID'=>$cust_address->CustomerAddressID,
            ]);


            if($customer->ParentCustId != ""){

//                $update_parent_cust=DB::select(DB::raw("
//                                    update customers
//                                    set customers.HasSubCustomer = '1'
//                                    where customers.CustomerID = $customer->ParentCustId "));

                $sub_customers = SubCustomers::create([
                    'ParentCustomerID' => $customer->ParentCustId,
                    'CustomerID' => $customer->id,
                    'created_by' => $user->id,
                    'Updated_By' => $user->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                $cust_count = DB::select(DB::raw("
                                    select count(sub_customers.CustomerID) as cust_id
                                    from sub_customers
                                    where sub_customers.ParentCustomerID = $sub_customers->ParentCustomerID"));

                $count= $cust_count[0]->cust_id;
                $cust_count =DB::select(DB::raw("
                                     update customers
                                     set customers.NoOfSubCustomer = $count
                                     where customers.CustomerID = $customer->ParentCustId "));
        }
            if($customer->ChildCustId != ""){

               // $update_parent_cust=DB::select(DB::raw(" update customers set customers.HasSubCustomer = '1'  where customers.CustomerID = $customer->id "));

                $sub_customers = SubCustomers::create([
                    'ParentCustomerID' => $customer->id,
                    'CustomerID' => $customer->ChildCustId,
                    'created_by' => $user->id,
                    'Updated_By' => $user->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                $cust_count = DB::select(DB::raw("
                                    select count(sub_customers.CustomerID) as cust_id
                                    from sub_customers
                                    where sub_customers.ParentCustomerID = $customer->id"));

                $count= $cust_count[0]->cust_id;
                $cust_count =DB::select(DB::raw("
                                     update customers
                                     set customers.NoOfSubCustomer = $count
                                     where customers.CustomerID = $customer->id "));
            }
            $organization = OrganisationCustomers::create([
                'OrganisationID'=>$user->OrganisationID,
                'CustomerID' => $customer->id
            ]);
            $salesrep = Input::get('SalesRep');
            if(!empty($salesrep))
            {
                $salesrep_count =  count($salesrep);
                for($i=0;$i<$salesrep_count;$i++)
                {
                   if(isset($salesrep[$i]['isprimary']))
                   {
                       if($salesrep[$i]['isprimary'] == '1')
                           $isprimary = '1';
                       else
                           $isprimary = '0';
                   }
                    else
                        $isprimary = 0;
                    CustomerSalesRepresentative::create([
                        'CustomerID'=>$customer->id,
                        'SalesRepresentativeID'=>$salesrep[$i]['id'],
                        'OrganizationID'=>$orgid,
                        'IsPrimary'=>$isprimary,
                        'CreatedBy'=>$userId,
                        'created_at'=>Carbon::now(),
                    ]);
                }
            }
            else
            {
                CustomerSalesRepresentative::create([
                    'CustomerID'=>$customer->id,
                    'SalesRepresentativeID'=>$userId,
                    'OrganizationID'=>$orgid,
                    'IsPrimary'=>'1',
                    'CreatedBy'=>$userId,
                    'created_at'=>Carbon::now(),
                ]);

            }

            //CHANGED ACCORDING TO REQUIREMENT

            //                $social_media = CustomersSocialMediaDetails::create([
            //                    'SocialMediaTypeID' => Input::get('SocialMediaTypeID'),
            //                    'CustomerID' => $customer->id,
            //                    'SocialMediaID' => Input::get('SocialMediaID'),
            //                    'created_by' => $users->id,
            //                    'updated_by' => $users->id,]);
            //
            //            if(!empty($social_media->SocialMediaID)){
            //                $media_flag =DB::select(DB::raw("
            //                                     update customers
            //                                     set customers.HasSocialMedia = 1
            //                                     where customers.CustomerID = $customer->id "));
            //            }
            $sourceid = $customer->id;
            $orderid = "";
            $customerid = $customer->id;
            $transactionsourceid = '4';

            $transactions = \App\Helpers\Helpers::transactiondetails($sourceid,$orderid,$customerid,$orgid,$transactionsourceid);

            DB::commit();
            $response['data'] = "Customer Registered Successfully";
            $response['code']=200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code']=400;
            return $response;
        }
    }
    public function CreateLeadCustomer($input, $organizationId)
    {
        $rules = array(
            'EmailID' => 'required|email|unique:customers,EmailID',
            'CustomerName' => 'required',
        );

        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            DB::beginTransaction();
            $error = "";

            $customer_list_count = DB::table('customers')
                ->select('*')
                ->count();
            if($customer_list_count == "0")
                $customerlistid = "9000001";
            else
                $customerlistid = '';

            $cust_address = CustomerAddress::create([
                'Address1' => Input::get('Address1'),
                'Address2' => Input::get('Address2'),
                'City' => Input::get('City'),
                'State' => Input::get('State'),
                'Country' => Input::get('Country'),
                'Zipcode' =>Input::get('Zipcode'),
                'created_by' => 0,
                'updated_by' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            $customer= Customers::create([
                'CustomerID'=> $customerlistid,
                'CustomerName' => Input::get('CustomerName'),
                'CompanyName'=> Input::get('CompanyName'),
                'Phoneno' => Input::get('phoneno'),
                'EmailID'=> Input::get('EmailID'),
                'CustomerTypeID'=>7,
                'SalesRep'=>Input::get('SalesRep'),
                'ParentSalesRep'=>Input::get('SalesRep'),
                'PrincipalAddressID'=>$cust_address->id,
                'BillToAddressID'=>$cust_address->id,
                'ShipToAddressID'=>$cust_address->id,
                'InitialDealValue'=>Input::get('InitialDealValue'),
                'ClosedDealValue'=>Input::get('ClosedDealValue'),
                'CustomerStageID' =>"1",
                'Description'=>Input::get('Description'),
                'Created_By' => 0,
                'Updated_By' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            $organization = OrganisationCustomers::create([
                'OrganisationID'=>$organizationId,
                'CustomerID' => $customer->id
            ]);

            $sourceid = $customer->id;
            $orderid = Input::get('OrderID');
            $customerid = $customer->id;
            $transactionsourceid = '4';
            $userId = Input::get('SalesRep');
            $notification = \App\Helpers\Helpers::create_notification($userId,$sourceid,$organizationId,'customer');
            DB::commit();
            $response['data'] = "Registered Successfully";
            $response['code']=200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code']=400;
            return $response;
        }
    }
    public function update_customers_by_id($id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        DB::beginTransaction();
        $ids = Authorizer::getResourceOwnerId();
        $users = User::find($ids);
        $input = Input::all();
        $error = "";
        $results = DB::table('customers')
            ->select('customers.*')
            ->where('CustomerID','=',$id)
            ->get();

        if(isset($input['EmailID'])&&($input['EmailID']!="")) {
            $cust_email = $input['EmailID'];
            $check_email = DB::table('customers')
                ->select('CustomerID', 'EmailID')
                ->where('customers.EmailID', '=', $cust_email)
                ->get();

            if (!empty($check_email)){
                if(($check_email[0]->CustomerID == $id))
                    $error .= "";
                else
                    $error .= "Email Id already exists";
            }
            else{
                $cust_emails = DB::table('customers')->where('CustomerID',$id)->update(['EmailID'=> $cust_email]);
                $user_email =  DB::table('users')->where('id',$results[0]->UserID)->update(['email'=> $cust_email]);
            }
        }
        if(isset($input['Phoneno'])&&($input['Phoneno']!="")) {
            $cust_phone = $input['Phoneno'];
            $check_phone = DB::table('customers')
                ->select('CustomerID', 'Phoneno')
                ->where('customers.Phoneno', '=', $cust_phone)
                ->get();

            if (!empty($check_phone)){
                if(($check_phone[0]->CustomerID == $id))
                    $error .= "";
                else
                    $error .= "Phone number already exists";
            }
            else{
                $cust_phones = DB::table('customers')->where('CustomerID',$id)->update(['Phoneno'=> $cust_phone]);
                $user_phones =  DB::table('users')->where('id',$results[0]->UserID)->update(['PhoneNo'=> $cust_phone]);
            }
        }


        if($error=="" && !empty($results))
        {
            $address = DB::table('customer_address')
                ->where('CustomerAddressID',$results[0]->BillToAddressID)
                ->update([
                    'Address1' => $input['BillToAddress1'],
                    'Address2' => $input['BillToAddress2'],
                    'City' => $input['BillToCity'],
                    'State' => $input['BillToState'],
                    'Country' => $input['BillToCountry'],
                    'Zipcode' =>$input['BillToZipcode'],
                    'updated_by' => $users->id,
                    'updated_at' => Carbon::now()
                ]);

               $customername = Input::get('Prefix')." ".Input::get('FirstName')." ".Input::get('MiddleName')." ".Input::get('LastName');
            $cust = DB::table('customers')
                ->where('CustomerID',$id)
                ->update([
                    'CustomerName' => $customername,
                    'CompanyName'=> Input::get('CompanyName'),
                    'Prefix'=>Input::get('Prefix'),
                    'FirstName'=>Input::get('FirstName'),
                    'MiddleName'=>Input::get('MiddleName'),
                    'LastName'=>Input::get('LastName'),
                    'Phoneno2' => Input::get('phoneno2'),
                    'website' => Input::get('website'),
                    //'EmailID'=> $input['EmailID'],
                    'CustomerTypeID'=> Input::get('CustomerTypeID'),
                    //'PrincipalAddressID'=>$address->id,
                    'BillToAddressID'=>$results[0]->BillToAddressID,
                    //'ShipToAddressID'=>$address->id,
                    'Description'=>Input::get('Description'),
                    //'PrincipalSocialMediaID'=>$input['PrincipalSocialMediaID'],
                    'PrincipalSocialMedias'=>Input::get('PrincipalSocialMedias'),
                    //'SecondarySocialMediaID'=>$input['SecondarySocialMediaID'],
                    'SecondarySocialMedias'=>Input::get('SecondarySocialMedias'),
                    'CustomerStageID' => Input::get('CustomerStageID'),
                    'ParentCustId'=>Input::get('ParentCustId'),
                    'ChildCustId'=>Input::get('ChildCustId'),
                    'SalesRep'=>$input['SalesRep'],
                    'InitialDealValue'=>Input::get('InitialDealValue'),
                    'ClosedDealValue'=>Input::get('ClosedDealValue'),
                    'NoOfSubCustomer'=>Input::get('NoOfSubCustomer'),
                    'cycle_type' => Input::get('cycle_type'),
                    'cycle_value' => Input::get('cycle_value'),
                    'jobtitle' => Input::get('jobtitle'),
                    'units' =>Input::get('units'),
                    'Group' =>Input::get('Group'),
                    'Updated_By' => $users->id,
                    'updated_at' => Carbon::now()
                ]);

            $user =  DB::table('users')
                ->where('id',$results[0]->UserID)
                ->update([
                    'name'=> $customername,
                    //'email'=> $input['EmailID'],
                    'UserRoleID' => '6',
                   // 'PhoneNo' => $input['phoneno'],
                    //'OrganisationID'=> Input::get('OrganisationID'),
                    //'username' => $input['CustomerName'],
                    'updated_By' => $users->id,
                    'updated_at' => Carbon::now()
                ]);

            $social_media = DB::table('customers_socialmedia_details')
                ->where('CustomerID',$id)
                ->where('SocialMediaTypeID',Input::get('SocialMediaTypeID'))
                ->update([
                    'SocialMediaID' => Input::get('SocialMediaID'),
                    'updated_by' => $users->id,
                ]);

           //--------------------------------------------Logic for multiple sales rep selection ---------------------------------------
              $salesrep = Input::get('SalesRep');

            if (!empty($salesrep)) {
                $database_salesrep = array();
                $update_salesrep_array = array();
                $delete_salesrep_array = array();
                $update_array = array();

                $salesrep_array = Input::get('SalesRep');
                $salesrepcount = count($salesrep_array);
                for ($i = 0; $i < $salesrepcount; $i++)
                    $update_salesrep_array[$i] = $salesrep_array[$i]['id'];

                $salesrep_db = DB::table('customer_sales_representative')->where('CustomerID', '=', $id)->select('SalesRepresentativeID')->get();

                $database_salesrep_count = count($salesrep_db);

                for ($i = 0; $i < $database_salesrep_count; $i++)
                    $database_salesrep[$i] = $salesrep_db[$i]->SalesRepresentativeID;
                $k = 0;

                for ($i = 0; $i < $database_salesrep_count; $i++) {
                    if (!in_array($database_salesrep[$i], $update_salesrep_array)) {
                        $delete_salesrep_array[$k] = $database_salesrep[$i];
                        $salesrep_id = $database_salesrep[$i];
                        $k++;
                        DB::select(DB::raw("DELETE FROM `customer_sales_representative` WHERE  CustomerID = $id AND SalesRepresentativeID = $salesrep_id ;"));
                    }
                }
                $j = 0;
                for ($i = 0; $i < $salesrepcount; $i++) {
                    if (!in_array($update_salesrep_array[$i], $database_salesrep)) {
                        $update_array[$j] = $update_salesrep_array[$i];
                        $j++;
                        if(isset($salesrep_array[$i]['isprimary']))
                        {
                            if($salesrep_array[$i]['isprimary'] == '1') {
                                $isprimary = '1';
                                $salesrep_primary =  DB::table('customer_sales_representative')
                                    ->where('CustomerID',$id)
                                    ->update([
                                        'IsPrimary'=> 0,
                                        'updated_By' => $userId,
                                        'updated_at' => Carbon::now()
                                    ]);

                            }
                            else
                                $isprimary = '0';
                        }
                        else
                            $isprimary = 0;

                        CustomerSalesRepresentative::create([
                            'CustomerID'=>$id,
                            'SalesRepresentativeID'=>$salesrep_array[$i]['id'],
                            'OrganizationID'=>$orgid,
                            'IsPrimary'=>$isprimary,
                            'CreatedBy'=>$userId,
                            'created_at'=>Carbon::now(),
                        ]);
                    }
                }
//
            }
            //-------------------------------------End of sales Rep multiple selection logic----------------

            DB::commit();
            $data = "Customer Record Updated Successfully";
            $response['data'] = $data;
            $response['code']=200;
        }
        else
        {
            $response['data'] = $error;
            $response['code']=400;
        }
        return $response;
    }
    public function dashboard_details($year,$graphfor)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        $graph_name = "";
        $tablename = "";
        $listarray = array('Tasks','Order','Invoices','Customers');

         if(empty($year)|| $year == '0'){
             $year = date_format(date_create('now'),"Y");
         }
        if($graphfor=="1")
        {
            $tablename = 'orders';
            $graph_name = 'Orders';

        }
        if($graphfor=="2")
        {
            $tablename = 'tasklist';
            $graph_name = 'Tasks';
        }

        if($graphfor=="3")
        {
            $tablename = 'inventory';
            $graph_name = 'Inventory';
        }

        if($graphfor=="4")
        {
            $tablename = 'invoices';
            $graph_name = 'Invoices';
        }
        if($graphfor=="5")
        {
            $tablename = 'customers';
            $graph_name = 'Customers';
        }
        $graph_query = "SELECT
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 1 THEN 1 ELSE 0 END) January,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 2 THEN 1 ELSE 0 END) Feburary,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 3 THEN 1 ELSE 0 END) March,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 4 THEN 1 ELSE 0 END) April,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 5 THEN 1 ELSE 0 END) May,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 6 THEN 1 ELSE 0 END) June,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 7 THEN 1 ELSE 0 END) July,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 8 THEN 1 ELSE 0 END) August,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 9 THEN 1 ELSE 0 END) September,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 10 THEN 1 ELSE 0 END) October,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 11 THEN 1 ELSE 0 END) November,
                                                SUM(CASE WHEN  MONTH($tablename.created_at) = 12 THEN 1 ELSE 0 END) December
                                                FROM $tablename";

        $order_query = "SELECT
                                                      (SELECT count(*) from orders where OrderTypeID = 1) AS RMA,
                                                      (SELECT count(*) from orders where OrderTypeID = 2) AS Sales,
                                                     (SELECT count(*) from orders where OrderTypeID = 3) AS Service,
                                                     (SELECT count(*) from orders where OrderTypeID = 4) AS Purchase,
                                                     (SELECT count(*) from orders where OrderTypeID = 5) AS BackOrder,
                                                     (SELECT count(*) from orders where OrderTypeID = 6) AS Internal
                                    FROM orders
                                    where  orders.OrganisationID = $orgid Limit 1";

        if($userrole =="1")
        {
            $total_customers = DB::table('organisation_customers')->select('organisation_customers.*')
                               ->join('customers', 'customers.CustomerID', '=', 'organisation_customers.CustomerID')
                               ->where('OrganisationID','=',$org_id)->where('customers.IsDeleted','=','0')->count();
            $total_inventory = DB::table('inventory')->select('inventory.*')->where('OrganisationID','=',$org_id)->where('IsDeleted','=','0')->count();
            $total_orders = DB::table('orders')->select('orders.*')->where('OrganisationID','=',$org_id)->where('IsDeleted','=','0')->count();
            $total_invoices = DB::table('invoices')->select('invoices.*')->where('OrganisationID','=',$org_id)->where('IsDeleted','=','0')->count();
            $total_tasks = DB::table('tasklist')->select('tasklist.*')->where('OrganisationID','=',$org_id)->where('IsDeleted','=','0')->count();
            $order_list = DB::select(DB::raw("SELECT orders.*,DATE(orders.OrderShipDate) as DueDate,order_type.OrderTypeID,order_status.OrderStatus
                          FROM orders
                          LEFT JOIN  users ON users.id = orders.CreatedBy
                          LEFT JOIN  order_type ON order_type.OrderTypeID = orders.OrderTypeID
                          LEFT JOIN  order_status ON order_status.OrderStatusID = orders.OrderStatusID
                          where  orders.OrganisationID = $orgid AND orders.IsDeleted = '0'  ORDER BY orders.CreatedBy limit 10 "));
            $customers_list = DB::select(DB::raw("SELECT customers.*,customer_types.CustomerType
                          FROM customers
                          left JOIN organisation_customers ON  organisation_customers.CustomerID = customers.CustomerID
                          left JOIN customer_types ON  customer_types.CustomerTypeID = customers.CustomerTypeID
                          left JOIN  users ON users.id = customers.Created_By
                          where  organisation_customers.OrganisationID = $orgid AND customers.IsDeleted = '0' ORDER BY customers.Created_By limit 10 "));
            $invoices_list = DB::select(DB::raw("SELECT invoices.*,invoice_type.InvoiceType
                          FROM invoices
                          left JOIN invoice_type ON  invoice_type.InvoiceTypeID = invoices.InvoiceTypeID
                          left JOIN  users ON users.id = invoices.CreatedBy
                          where  invoices.OrganisationID = $orgid AND invoices.IsDeleted = '0' ORDER BY invoices.Amount limit 10 "));
            $task_list = DB::select(DB::raw("SELECT tasklist.*,task_type.TaskType
                          FROM tasklist
                          left JOIN task_type ON  task_type.TaskTypeID = tasklist.TaskTypeID
                          left JOIN  users ON users.id = tasklist.CreatedBy
                          where  tasklist.OrganisationID = $orgid AND tasklist.IsDeleted = '0' ORDER BY tasklist.CreatedBy limit 10 "));
            $inventory_list = DB::select(DB::raw("SELECT inventory.*,inventory_type.InventoryType
                          FROM inventory
                           left JOIN inventory_type ON  inventory_type.InventoryTypeID = inventory.InventoryTypeID
                           left JOIN  users ON users.id = inventory.CreatedBy
                          where  inventory.OrganisationID = $orgid AND inventory.IsDeleted = '0' ORDER BY inventory.CreatedBy limit 10 "));
            $orders_type_count = DB::select(DB::raw($order_query));
            if($graphfor != "5")
            {
                $graph_query = $graph_query." where  YEAR($tablename.created_at)=$year  and $tablename.OrganisationID = $orgid and $tablename.IsDeleted = '0'";
                $graph_details = DB::select(DB::raw($graph_query));
            }
            else
            {
                $graph_query = $graph_query." left JOIN  organisation_customers ON organisation_customers.CustomerID = $tablename.CustomerID
                                                where  YEAR($tablename.created_at)=$year  and organisation_customers.OrganisationID = $orgid  and $tablename.IsDeleted = '0'";
                $graph_details = DB::select(DB::raw($graph_query));
            }
        }
        elseif($userrole =="3"||$userrole =="4"||$userrole =="5")
        {
            $total_customers = DB::table('organisation_customers')->select('organisation_customers.*')
                               ->join('customers', 'customers.CustomerID', '=', 'organisation_customers.CustomerID')
                               ->where('OrganisationID','=',$org_id)->where('created_by','=',$userId)->where('customers.IsDeleted','=','0')->count();
            $total_inventory = DB::table('inventory')->select('inventory.*')->where('OrganisationID','=',$org_id)->where('CreatedBy','=',$userId)->where('IsDeleted','=','0')->count();
            $total_orders = DB::table('orders')->select('orders.*')->where('OrganisationID','=',$org_id)->where('CreatedBy','=',$userId)->where('IsDeleted','=','0')->count();
            $total_invoices = DB::table('invoices')->select('invoices.*')->where('OrganisationID','=',$org_id)->where('CreatedBy','=',$userId)->where('IsDeleted','=','0')->count();
            $total_tasks = DB::table('tasklist')->select('tasklist.*')->where('OrganisationID','=',$org_id)->where('CreatedBy','=',$userId)->where('IsDeleted','=','0')->count();
            $order_list = DB::select(DB::raw("SELECT orders.*,DATE(orders.OrderShipDate) as DueDate,order_type.OrderTypeID,order_status.OrderStatus
                          FROM orders
                          LEFT JOIN  users ON users.id = orders.CreatedBy
                          LEFT JOIN  order_type ON order_type.OrderTypeID = orders.OrderTypeID
                          LEFT JOIN  order_status ON order_status.OrderStatusID = orders.OrderStatusID
                          where  orders.OrganisationID = $orgid AND orders.CreatedBy = $userId AND orders.IsDeleted = '0' ORDER BY orders.CreatedBy limit 10  "));
            $customers_list = DB::select(DB::raw("SELECT customers.*,customer_types.CustomerType
                          FROM customers
                          LEFT JOIN organisation_customers ON  organisation_customers.CustomerID = customers.CustomerID
                          LEFT JOIN customer_types ON  customer_types.CustomerTypeID = customers.CustomerTypeID
                          LEFT JOIN  users ON users.id = customers.Created_By
                          where  organisation_customers.OrganisationID = $orgid AND customers.Created_By = $userId AND customers.IsDeleted = '0'
                          ORDER BY customers.Created_By limit 10 "));
            $invoices_list = DB::select(DB::raw("SELECT invoices.*,invoice_type.InvoiceType
                          FROM invoices
                          LEFT JOIN invoice_type ON  invoice_type.InvoiceTypeID = invoices.InvoiceTypeID
                          LEFT JOIN  users ON users.id = invoices.CreatedBy
                          where  invoices.OrganisationID = $orgid AND invoices.CreatedBy = $userId AND invoices.IsDeleted = '0' ORDER BY invoices.Amount limit 10 "));
            $task_list = DB::select(DB::raw("SELECT tasklist.*,task_type.TaskType
                          FROM tasklist
                          LEFT JOIN task_type ON  task_type.TaskTypeID = tasklist.TaskTypeID
                          LEFT JOIN  users ON users.id = tasklist.CreatedBy
                          where  tasklist.OrganisationID = $orgid AND tasklist.CreatedBy = $userId AND tasklist.IsDeleted = '0' ORDER BY tasklist.CreatedBy limit 10 "));
            $inventory_list = DB::select(DB::raw("SELECT inventory.*,inventory_type.InventoryType
                          FROM inventory
                          LEFT JOIN inventory_type ON  inventory_type.InventoryTypeID = inventory.InventoryTypeID
                          LEFT JOIN  users ON users.id = inventory.CreatedBy
                          where  inventory.OrganisationID = $orgid AND inventory.CreatedBy = $userId AND inventory.IsDeleted = '0' ORDER BY inventory.CreatedBy limit 10 "));
            $orders_type_count = DB::select(DB::raw($order_query));
            if($graphfor != "5")
            {
                $graph_query = $graph_query." where  YEAR($tablename.created_at)=$year  and $tablename.OrganisationID = $orgid and $tablename.IsDeleted = 0";
                $graph_details = DB::select(DB::raw($graph_query));
            }
            else
            {
                $graph_query = $graph_query." LEFT JOIN  organisation_customers ON organisation_customers.CustomerID = $tablename.CustomerID
                                                where  YEAR($tablename.created_at)=$year  and organisation_customers.OrganisationID = $orgid and $tablename.IsDeleted = 0 ";
                $graph_details = DB::select(DB::raw($graph_query));
            }
        }
        elseif($userrole =="2")
        {
            $total_customers = DB::table('organisation_customers')->select('organisation_customers.*')
                               ->leftjoin('users as a','a.id','=','organisation_customers.created_by')
                               ->join('customers', 'customers.CustomerID', '=', 'organisation_customers.CustomerID')
                               ->where('organisation_customers.OrganisationID','=',$org_id)->where('customers.IsDeleted','=','0')->where('customers.Created_By','=',$userId)->orwhere('a.AssignedTo','=',$userId)->count();

            $total_inventory = DB::table('inventory')->select('inventory.*')->leftjoin('users as a','a.id','=','inventory.CreatedBy')
                               ->where('inventory.OrganisationID','=',$org_id)->where('inventory.IsDeleted','=','0')->where('inventory.CreatedBy','=',$userId)->orwhere('a.AssignedTo','=',$userId)->count();

            $total_orders = DB::table('orders')->select('orders.*')->leftjoin('users as a','a.id','=','orders.CreatedBy')
                              ->where('orders.OrganisationID','=',$org_id)->where('orders.IsDeleted','=','0')->where('orders.CreatedBy','=',$userId)->orwhere('a.AssignedTo','=',$userId)->count();

            $total_invoices = DB::table('invoices')->select('invoices.*')->leftjoin('users as a','a.id','=','invoices.CreatedBy')
                               ->where('invoices.OrganisationID','=',$org_id)->where('invoices.IsDeleted','=','0')->where('invoices.CreatedBy','=',$userId)->orwhere('a.AssignedTo','=',$userId)->count();
            $total_tasks = DB::table('tasklist')->select('tasklist.*')->leftjoin('users as a','a.id','=','tasklist.CreatedBy')
                ->where('tasklist.OrganisationID','=',$org_id)->where('tasklist.IsDeleted','=','0')->where('tasklist.CreatedBy','=',$userId)->orwhere('a.AssignedTo','=',$userId)->count();

            $order_list = DB::select(DB::raw("SELECT orders.*,DATE(orders.OrderShipDate) as DueDate,order_type.OrderTypeID,order_status.OrderStatus
                           FROM orders
                           LEFT JOIN  users ON users.id = orders.CreatedBy
                           LEFT JOIN  order_type ON order_type.OrderTypeID = orders.OrderTypeID
                           LEFT JOIN  order_status ON order_status.OrderStatusID = orders.OrderStatusID
                           where  orders.OrganisationID = $orgid AND orders.IsDeleted = '0'  AND (orders.CreatedBy = $userId OR users.AssignedTo = $userId)  "));
            $customers_list = DB::select(DB::raw("SELECT customers.*,customer_types.CustomerType
                          FROM customers
                          JOIN organisation_customers ON  organisation_customers.CustomerID = customers.CustomerID
                          LEFT JOIN customer_types ON  customer_types.CustomerTypeID = customers.CustomerTypeID
                          LEFT JOIN  users ON users.id = customers.Created_By
                          where  organisation_customers.OrganisationID = $orgid AND customers.IsDeleted = '0' AND (customers.Created_By = $userId OR users.AssignedTo = $userId)
                          ORDER BY customers.Created_By limit 10 "));
            $invoices_list = DB::select(DB::raw("SELECT invoices.*,invoice_type.InvoiceType
                          FROM invoices
                          LEFT JOIN invoice_type ON  invoice_type.InvoiceTypeID = invoices.InvoiceTypeID
                          LEFT JOIN  users ON users.id = invoices.CreatedBy
                          where  invoices.OrganisationID = $orgid AND invoices.IsDeleted = '0' AND (invoices.CreatedBy = $userId OR users.AssignedTo = $userId) ORDER BY invoices.Amount limit 10 "));
            $task_list = DB::select(DB::raw("SELECT tasklist.*,task_type.TaskType
                          FROM tasklist
                          LEFT JOIN task_type ON  task_type.TaskTypeID = tasklist.TaskTypeID
                          LEFT JOIN  users ON users.id = tasklist.CreatedBy
                          where  tasklist.OrganisationID = $orgid AND tasklist.IsDeleted = '0' AND (tasklist.CreatedBy = $userId OR users.AssignedTo = $userId) ORDER BY tasklist.CreatedBy limit 10 "));
            $inventory_list = DB::select(DB::raw("SELECT inventory.*,inventory_type.InventoryType
                          FROM inventory
                          LEFT JOIN inventory_type ON  inventory_type.InventoryTypeID = inventory.InventoryTypeID
                          JOIN  users ON users.id = inventory.CreatedBy
                          where  inventory.OrganisationID = $orgid AND inventory.IsDeleted = '0' AND (inventory.CreatedBy = $userId OR users.AssignedTo = $userId) ORDER BY inventory.CreatedBy limit 10 "));
            $orders_type_count = DB::select(DB::raw($order_query));
            if($graphfor != "5")
            {
                $graph_query = $graph_query." LEFT JOIN  users ON users.id = $tablename.CreatedBy where  YEAR($tablename.created_at)=$year  and $tablename.OrganisationID = $org_id[0] AND $tablename.IsDeleted = '0'
                 AND($tablename.CreatedBy = $userId OR users.AssignedTo = $userId )";
                $graph_details = DB::select(DB::raw($graph_query));
            }
            else
            {
                $graph_query = $graph_query." JOIN  organisation_customers ON organisation_customers.CustomerID = $tablename.CustomerID
                                             LEFT JOIN  users ON users.id = $tablename.Created_By where  YEAR($tablename.created_at)=$year
                                             and organisation_customers.OrganisationID = $orgid  AND $tablename.IsDeleted = '0'  AND($tablename.Created_By = $userId OR users.AssignedTo = $userId ) ";
                $graph_details = DB::select(DB::raw($graph_query));
            }
        }
        $results['total_customers'] = $total_customers;
        $results['total_inventory'] = $total_inventory;
        $results['total_invoices'] = $total_invoices;
        $results['total_orders'] = $total_orders;
        $results['total_tasks'] = $total_tasks;
        $results['graph_for'] = $graph_name;
        $results['graph_details'] = $graph_details;
        $results['orders_type_count'] =  $orders_type_count;
        $results['customer_list'] = $customers_list;
        $results['invoices_list'] = $invoices_list;
        $results['inventory_list'] = $inventory_list;
        $results['task_list'] = $task_list;
        $results['order_list'] =  $order_list;
        $results['listarray'] =  $listarray;
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_all_user_details_of_organisation($usertype,$orgid)
    {

//        if($user_role[0]=="1")
//        {
            if($usertype == "0")
            {
                $results = DB::table('users')
                    ->select('users.*','user_role.UserRole','u1.FirstName as assigned_tofname','u1.LastName as assigned_tolname','u1.id as assigned_id')
                    ->leftjoin('user_role','user_role.UserRoleID','=','users.UserRoleID')
                    ->leftjoin('users as u1','users.AssignedTo','=','users.id')
                    ->where('users.OrganisationID','=',$orgid)
                    ->where('users.IsDeleted','=',null)
                    ->where('users.UserRoleID','!=',6)
                    ->get();
            }
            else
            {
                $results = DB::table('users')
                    ->select('users.*','user_role.UserRole','u1.FirstName as assigned_tofname','u1.LastName as assigned_tolname','u1.id as assigned_id')
                    ->leftjoin('user_role','user_role.UserRoleID','=','users.UserRoleID')
                    ->leftjoin('users as u1','users.AssignedTo','=','users.id')
                    ->where('users.OrganisationID','=',$orgid)
                    ->where('users.UserRoleID', '=', $usertype)
                    ->where('users.IsDeleted','=',null)
                    ->where('users.UserRoleID','!=',6)
                    ->get();
            }
            if(!empty($results))
            {
                $response['data'] = $results;
                $response['code']=200;
            }
            else
            {
                $response['data'] = "User does not exists";
                $response['code']=400;
            }
//        }
//        else
//        {
//            $response['data'] = "Access denied";
//            $response['code']=400;
//        }
        return $response;
    }
    public function get_customers()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        $query = "select customers.*,organisation.OrganisationName, customer_types.CustomerTypeID,customer_types.CustomerType,ca1.CustomerAddressID as PrincipalAddressID,ca1.Address1 as PrincipalAddress1,ca1.Address2 as PrincipalAddress2,ca1.City as PrincipalCity,ca1.State as PrincipalState,ca1.Country as PrincipalCountry,ca1.Zipcode as PrincipalZipcode,ca2.CustomerAddressID as BillToAddressID,ca2.Address1 as BillToAddress1,ca2.Address2 as BillToAddress2,ca2.City as BillToCity,ca2.State as BillToState,ca2.Country as BillToCountry,ca2.Zipcode as BillToZipcode,
                     ca3.CustomerAddressID as ShipToAddressID,ca3.Address1 as ShipToAddress1,ca3.Address2 as ShipToAddress2,
                  ca3.City as ShipToCity,ca3.State as ShipToState,ca3.Country as ShipToCountry,ca3.Zipcode as ShipToZipcode,customer_stages.CustomerStage
                    from customers
                    left join customer_types on customer_types.CustomerTypeID = customers.CustomerTypeID
                    join organisation_customers on organisation_customers.CustomerID = customers.CustomerID
                    left join customer_address as ca1 on ca1.CustomerAddressID = customers.PrincipalAddressID
                    left join customer_address as ca2 on ca2.CustomerAddressID = customers.BillToAddressID
                    left join customer_address as ca3 on ca3.CustomerAddressID = customers.ShipToAddressID
                    left join customer_stages  on customer_stages.CustomerStageID = customers.CustomerStageID
                    join organisation on organisation.OrganisationID = organisation_customers.OrganisationID";

        if($userrole == "1")
                    $results = DB::select(DB::raw($query."   WHERE  organisation_customers.OrganisationID = $orgid AND customers.IsDeleted = 0 "));

        if($userrole == "3" || $userrole == "4" ||  $userrole == "5"  )
                $results = DB::select(DB::raw($query." WHERE  organisation_customers.OrganisationID = $orgid
                                  AND customers.IsDeleted = 0 and customers.Created_By = $userId "));

        if($userrole == "2")

            $results = DB::select(DB::raw($query . " left join users on users.id = customers.Created_By
                    WHERE  organisation_customers.OrganisationID = $orgid AND customers.IsDeleted = 0
                    AND (customers.Created_By = $userId OR users.AssignedTo = $userId)"));

        if(!empty($results))
        {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                if($results[$i]->created_at == null)
                    $results[$i]->created_at = '0000-00-00 00:00:00';

                $salesrep_array = DB::table('customer_sales_representative')
                    ->select('customer_sales_representative.CustomerSRID','customer_sales_representative.SalesRepresentativeID','customer_sales_representative.OrganizationID','users.name as SalesRepresentativeName','customer_sales_representative.IsPrimary')
                    ->where('customer_sales_representative.CustomerID', '=', $results[$i]->CustomerID)
                    ->leftjoin('users','users.id','=','customer_sales_representative.SalesRepresentativeID')
                    ->get();
                $results[$i]->salesrep = $salesrep_array;
            }
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Customer does not exist";
            $response['code']=400;
        }
        return $response;
    }
    public function get_customers_stages()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        $results = DB::table('customer_stages')
            ->select('CustomerStageID', 'CustomerStage')
            ->get();


        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Customer Stages does not exist";
            $response['code']=400;
        }
        return $response;
    }
    public  function get_Parent_Organisation_Customers()
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();
        $response = $this->get_customerslist($userrole,$org_id[0]->OrganisationID,$userId);
        return $response;
    }
    public  function get_Sub_Organisation_Customers($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        //$org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $response = $this->get_customerslist($userrole,$org_id,$userId);
        return $response;
    }
    public function get_customerslist($userrole,$orgid,$userId)
    {
        if($userrole=="1")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName, organisation.OrganisationID,organisation.OrganisationName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    JOIN organisation ON organisation.OrganisationID =  organisation_customers.OrganisationID
                    WHERE organisation_customers.OrganisationID = $orgid "));
        }
        if($userrole=="2")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName, organisation.OrganisationID,organisation.OrganisationName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    JOIN organisation ON organisation.OrganisationID =  organisation_customers.OrganisationID
                    LEFT JOIN  users ON users.id = customers.Created_By
                    WHERE organisation_customers.OrganisationID = $orgid AND(customers.Created_By = $userId OR users.AssignedTo = $userId ) " ));
        }
        if($userrole=="3"||$userrole=="4" ||$userrole=="5")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName,organisation.OrganisationID,organisation.OrganisationName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    JOIN organisation ON organisation.OrganisationID =  organisation_customers.OrganisationID
                    LEFT JOIN  users ON users.id = customers.CreatedBy
                    WHERE organisation_customers.OrganisationID = $orgid AND customers.Created_By = $userId"));
        }
        if(!empty($results))
        {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                $salesrep_array = DB::table('customer_sales_representative')
                    ->select('customer_sales_representative.*','users.name as SalesRepresentativeName')
                    ->where('customer_sales_representative.CustomerID', '=', $results[$i]->CustomerID)
                    ->leftjoin('users','users.id','=','customer_sales_representative.SalesRepresentativeID')
                    ->get();
                $results[$i]->salesrep = $salesrep_array;
            }
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Customer does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_customerlist_for_child_customer($id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if($userrole=="1")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    WHERE organisation_customers.OrganisationID = $orgid
                    AND customers.CustomerID <> $id"));
        }
        if($userrole=="2")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    LEFT JOIN  users ON users.id = customers.CreatedBy
                    WHERE organisation_customers.OrganisationID = $orgid AND(customers.CreatedBy = $userId OR users.AssignedTo = $userId ) AND customers.CustomerID <> $id " ));
        }
        if($userrole=="3"||$userrole=="4" || $userrole == "5")
        {
            $results = DB::select(DB::raw("
                  select customers.CustomerID,customers.CustomerName
                    from customers
                    JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    LEFT JOIN  users ON users.id = customers.CreatedBy
                    WHERE organisation_customers.OrganisationID = $orgid AND customers.CreatedBy = $userId  AND customers.CustomerID <> $id" ));
        }
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Customer does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_customers_by_id($id)
    {
        $results = DB::select(DB::raw("select customers.*,customer_types.*,
                  ca1.CustomerAddressID as PrincipalAddressID,ca1.Address1 as PrincipalAddress1,ca1.Address2 as PrincipalAddress2,
                  ca1.City as PrincipalCity,ca1.State as PrincipalState,ca1.Country as PrincipalCountry,ca1.Zipcode as PrincipalZipcode,
                  ca2.CustomerAddressID as BillToAddressID,ca2.Address1 as BillToAddress1,ca2.Address2 as BillToAddress2,
                  ca2.City as BillToCity,ca2.State as BillToState,ca2.Country as BillToCountry,ca2.Zipcode as BillToZipcode,
                  ca3.CustomerAddressID as ShipToAddressID,ca3.Address1 as ShipToAddress1,ca3.Address2 as ShipToAddress2,
                  ca3.City as ShipToCity,ca3.State as ShipToState,ca3.Country as ShipToCountry,ca3.Zipcode as ShipToZipcode,
                  customer_stages.CustomerStage
                    from customers
                    left join customer_types on customer_types.CustomerTypeID = customers.CustomerTypeID
                    join organisation_customers on organisation_customers.CustomerID = customers.CustomerID
                    left join customer_address as ca1 on ca1.CustomerAddressID = customers.PrincipalAddressID
                    left join customer_address as ca2 on ca2.CustomerAddressID = customers.BillToAddressID
                    left join customer_address as ca3 on ca3.CustomerAddressID = customers.ShipToAddressID
                    left join customer_stages  on customer_stages.CustomerStageID = customers.CustomerStageID
                    where customers.CustomerID = $id "));

        if(!empty($results))
        {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                $salesrep_array = DB::table('customer_sales_representative')
                    ->select('customer_sales_representative.*','users.name as SalesRepresentativeName')
                    ->where('customer_sales_representative.CustomerID', '=', $results[$i]->CustomerID)
                    ->leftjoin('users','users.id','=','customer_sales_representative.SalesRepresentativeID')
                    ->get();
                $results[$i]->salesrep = $salesrep_array;
            }
            $response['data'] = $results;
            $response['code']=200;
            return $response;
        }
        else
        {
            $response['data'] = "Customer does not exists";
            $response['code']=400;
            return $response;
        }
    }
    public function delete_customers_by_id($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        }
        else
        {
            $response['data'] = "Select Customer to delete" ;
            $response['code']=400;
            return $response;
        }
        foreach ($ids as $id) {
            $customers = DB::table('customers')
                ->select('customers.*')
                ->where('customers.CustomerID','=',$id)
                ->get();
            if(!empty($customers))
            {
                DB::table('customers')->where('CustomerID', '=', $id)->update(array('IsDeleted' => '1'));
                $response['data'] = "Customer Details deleted successfully";
                $response['code']=200;
            }
            else
            {
                $response['data'] = "Customer does not exists" ;
                $response['code']=400;
                return $response;
            }
        }
        return $response;
//        $task =DB::table('customers')
//            ->select('customers.*')
//            ->where('customers.CustomerID','=',$id)
//            ->get();
//        if(!empty($task))
//        {
//
//            DB::delete('delete from customers where CustomerID = ?',[$id]);
//            $response['data'] = "Customers details deleted successfully";
//            $response['code']=200;
//            return $response;
//        }
//        else
//        {
//            $response['data'] = "Customers details does not exists";
//            $response['code']=400;
//            return $response;
//        }
    }
//    public function update_customers_by_id($id)
//    {
//        $input = Input::all();
//        $error = "";
//        $number = "";
//        if (!empty($input['EmailID'])) $email = $input['EmailID'];
//        if (!empty($input['Phoneno'])) $number = $input['Phoneno'];
//        if (!empty($input['CustomerName'])) $customer_name = $input['CustomerName'];
//
//        if (!empty($input['EmailID'])) {
//            $check_emails = DB::table('customers')
//                ->select('customers.*')//('CustomerID', 'EmailID')
//                ->where('customers.EmailID', '=', $input['EmailID'])
//                ->get();
//            $email = DB::table('users')->where('id','=',$check_emails[0]->UserID)->update(array('email' => $check_emails[0]->EmailID));
//            if (!empty($check_emails)) {
//                if (($check_emails[0]->CustomerID == $id))
//                    $error .= "";
//                else
//                    $error .= "Email Id already exists";
//            } else {
//                    $emails = DB::table('customers')->where('CustomerID','=',$id)->update(array('EmailID' => $input['EmailID']));
//                    $email = DB::table('users')->where('id','=',$emails[0]->UserID)->update(array('email' => $emails[0]->EmailID));
//            }
//        }
//        if (!empty($input['Phoneno'])) {
//            $check_phone = DB::table('customers')
//                ->select('customers.*')
//                ->where('customers.PhoneNo', '=', $input['Phoneno'])
//                ->get();
//            $phones = DB::table('users')->where('id','=',$check_phone[0]->UserID)->update(array('PhoneNo' => $check_phone[0]->Phoneno));
//            if (!empty($check_phone)) {
//                if (($check_phone[0]->CustomerID == $id))
//                    $error .= "";
//                else
//                    $error .= "Phone number already exists";
//
//            } else {
//                $phone = DB::table('customers')->where('CustomerID','=',$id)->update(array('Phoneno' => $input['Phoneno'])) and DB::table('user')->where('id','=',$phone->UserID)->update(array('PhoneNo' => $phone->Phoneno));
//                $phones = DB::table('users')->where('id','=',$phone->UserID)->update(array('PhoneNo' => $phone->Phoneno));
//            }
//        }
//
//        if (!empty($input['CustomerName'])) {
//            $check_name = DB::table('customers')
//                ->select('customers.*')
//                ->where('customers.CustomerName', '=', $input['CustomerName'])
//                ->get();
//            $customer = DB::table('users')->where('id','=',$check_name[0]->UserID)->update(array('name' => $check_name[0]->CustomerName));
//            if (!empty($check_name)) {
//                if (($check_name[0]->CustomerID == $id))
//                    $error .= "";
//                else
//                    $error .= "Customer name already exists";
//            } else {
//                $customers = DB::table('customers')->where('CustomerID','=',$id)->update(array('CustomerName' => $input['CustomerName']));
//                $customer = DB::table('users')->where('id','=',$customers->UserID)->update(array('name' => $customers->CustomerName));
//            }
//        }
//
//        if ($error == "") {
//            if (!empty($input['userrole']))
//                DB::table('users')->where('id', '=', [$id])->update(array('UserRoleID' => $input['userrole']));
//
//            if (!empty($input['CustomerTypeID']) || $input['CustomerTypeID']== null)
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('CustomerTypeID' => $input['CustomerTypeID']));
//            if (!empty($input['HasSocialMedia']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('HasSocialMedia' => $input['HasSocialMedia']));
//            if (!empty($input['SalesRep']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('SalesRep' => $input['SalesRep']));
//            if (!empty($input['ParentSalesRep']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('ParentSalesRep' => $input['ParentSalesRep']));
//            if (!empty($input['PrincipalAddressID']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('PrincipalAddressID' => $input['PrincipalAddressID']));
//            if (!empty($input['BillToAddressID']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('BillToAddressID' => $input['BillToAddressID']));
//            if (!empty($input['ShipToAddressID']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('ShipToAddressID' => $input['ShipToAddressID']));
//            if (!empty($input['Description']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('Description' => $input['Description']));
//            if (!empty($input['TransactionLink']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('TransactionLink' => $input['TransactionLink']));
//            if (!empty($input['HasSubCustomer']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('HasSubCustomer' => $input['HasSubCustomer']));
//            if (!empty($input['NoOfSubCustomer']))
//                DB::table('customers')->where('UserID', '=', [$id])->update(array('NoOfSubCustomer' => $input['NoOfSubCustomer']));
//
//            if (!empty($input['Adress1']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('Address1' => $input['Address1']));
//
//            if (!empty($input['Adress2']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('Address2' => $input['Address2']));
//
//            if (!empty($input['City']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('City' => $input['City']));
//
//            if (!empty($input['State']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('State' => $input['State']));
//
//            if (!empty($input['Country']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('Country' => $input['Country']));
//
//            if (!empty($input['Zipcode']))
//                DB::table('customer_address')->where('CustomerAddressID', '=', [$check_emails[0]->PrincipalAddressID] || [$check_emails[0]->BillToAddressId] || [$check_emails[0]->ShipToAddressId])->update(array('Zipcode' => $input['Zipcode']));
//
//
//            $response['data'] = "Record updated successfully";
//            $response['code'] = 200;
//            return $response;
//        } else {
//            $response['data'] = $error;
//            $response['code'] = 400;
//            return $response;
//        }
//    }
    public function get_all_customer_type(){

        $results = DB::table('customer_types')
            ->select('customer_types.*')
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Customer type does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_social_media_types(){
        $results = DB::table('social_media_type')
            ->select('social_media_type.*')
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Social Media type does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_dashboard_category_list()
    {
        $results = array(
            "1" => 'Orders',
            "2" => 'Tasks',
            "3" => 'Inventory',
            "4" => 'Invoices',
            "5" => 'Customers');
        if (!empty($results)) {
            $response['data'] = $results;
        }
    }
    public function GlobalSearch($input)
            {
                $userId = Authorizer::getResourceOwnerId();
                $org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
                $user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
                $userrole = $user_role[0];
                $orgid = $org_id[0];
                //dd(Input::all());
                if($userrole == "1")
                {
                    $users = DB::table('users')
                        ->join('user_role', 'users.UserRoleID', '=', 'user_role.UserRoleID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where('users.IsDeleted','=',null)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('users.email', 'Like', '%' . $input . '%')
                                ->orWhere('users.FirstName', 'Like', '%' . $input . '%')
                                ->orWhere('users.LastName', 'Like', '%' . $input . '%')
                                ->orWhere('users.id', 'Like', '%' . $input . '%')
                                ->orWhere('users.name', 'Like', '%' . $input . '%');
                        })
                        ->select("users.*")
                        ->get();

                    $invoices = DB::table('invoices')
                        ->leftjoin('invoice_type', 'invoices.InvoiceTypeID', '=', 'invoice_type.InvoiceTypeID')
                        ->where('invoices.OrganisationID', '=', $orgid)
                        ->where('invoices.IsDeleted', '<>', 1)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('invoices.InvoiceName', 'Like', '%' . $input . '%')
                                ->orWhere('invoices.InvoiceID', 'Like', '%' . $input . '%')
                                ->orWhere('invoice_type.InvoiceType', 'Like', '%' . $input . '%');
                        })
                        ->select('invoices.*', 'invoices.InvoiceName')
                        ->get();

                    $organisation = DB::table('users')
                        ->leftjoin('organisation', 'users.OrganisationID', '=', 'organisation.OrganisationID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('organisation.OrganisationName', 'Like', '%' . $input . '%')
                                ->orWhere('organisation.OrganisationID', 'Like', '%' . $input . '%');
                        })

                        ->select('organisation.*')
                        ->get();
//JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    $customers = DB::table('users')
                        ->leftjoin('customers', 'users.id', '=', 'customers.UserID')
                        ->leftjoin('customer_types', 'customers.CustomerTypeID', '=', 'customer_types.CustomerTypeID')
                        ->join('organisation_customers', 'organisation_customers.CustomerID', '=', 'customers.CustomerID')
                        ->where('organisation_customers.OrganisationID', '=', $orgid)
                        ->where('customers.IsDeleted', '<>', 1)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('customers.CustomerName', 'Like', '%' . $input . '%')
                                ->orWhere('customers.CustomerID', 'Like', '%' . $input . '%')
                                ->orWhere('customer_types.CustomerType', 'Like', '%' . $input . '%');
                        })
                        ->select('customers.*', 'customer_types.CustomerType')
                        ->get();

                    $inventory = DB::table('inventory')
                        ->leftjoin('inventory_type', 'inventory.InventoryTypeID', '=', 'inventory_type.InventoryTypeID')
                        ->where('inventory.OrganisationID', '=', $orgid)
                        ->where('inventory.IsDeleted', '<>', 1)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('inventory.InventoryName', 'Like', '%' . $input . '%')
                                ->orWhere('inventory.InventoryID', 'Like', '%' . $input . '%')
                                ->orWhere('inventory_type.InventoryType', 'Like', '%' . $input . '%');
                        })
                        ->select('inventory.*', 'inventory_type.InventoryType')
                        ->get();

                    $orders = DB::table('orders')
                        ->leftjoin('order_type', 'orders.OrderTypeID', '=', 'order_type.OrderTypeID')
                        ->leftjoin('order_status', 'orders.OrderStatusID', '=', 'order_status.OrderStatusID')
                        ->where('orders.OrganisationID', '=', $orgid)
                        ->where('orders.IsDeleted', '<>', 1)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('orders.OrderName', 'Like', '%' . $input . '%')
                                ->orWhere('orders.OrderID', 'Like', '%' . $input . '%')
                                ->orWhere('order_type.OrderType', 'Like', '%' . $input . '%')
                                ->orWhere('order_status.OrderStatus', 'Like', '%' . $input . '%');
                        })
                        ->select('orders.*', 'order_type.OrderType', 'order_status.OrderStatus')
                        ->get();

                    $tasklist = DB::table('tasklist')
                        ->leftjoin('task_type', 'tasklist.TaskTypeID', '=', 'task_type.TaskTypeID')
                        ->leftjoin('task_status', 'tasklist.StatusID', '=', 'task_status.TaskStatusID')
                        ->leftjoin('task_severities', 'tasklist.TaskSeverityID', '=', 'task_severities.TaskSeverityID')
                        ->leftjoin('priorities', 'tasklist.PriorityID', '=', 'priorities.PriorityID')
                        ->where('tasklist.OrganisationID', '=', $orgid)
                        ->where('tasklist.IsDeleted', '<>', 1)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('tasklist.TasklistID', 'Like', '%' . $input . '%')
                                ->orWhere('task_type.TaskType', 'Like', '%' . $input . '%')
                                ->orWhere('task_status.TaskStatus', 'Like', '%' . $input . '%')
                                ->orWhere('task_severities.TaskSeverity', 'Like', '%' . $input . '%')
                                ->orWhere('priorities.Priority', 'Like', '%' . $input . '%');
                        })
                        ->select('tasklist.*', 'task_type.TaskType', 'task_status.TaskStatus', 'task_severities.TaskSeverity', 'priorities.Priority')
                        ->get();

                }
                if($userrole == "2")
                {
                    $users = DB::table('users')
                        ->leftjoin('user_role', 'users.UserRoleID', '=', 'user_role.UserRoleID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where('users.IsDeleted','=',null)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('users.CreatedBy','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('users.email', 'Like', '%' . $input . '%')
                                ->orWhere('users.FirstName', 'Like', '%' . $input . '%')
                                ->orWhere('users.LastName', 'Like', '%' . $input . '%')
                                ->orWhere('users.id', 'Like', '%' . $input . '%')
                                ->orWhere('users.name', 'Like', '%' . $input . '%');
                        })
                        ->select("users.*")
                        ->get();

                    $invoices = DB::table('invoices')
                        ->leftjoin('invoice_type', 'invoices.InvoiceTypeID', '=', 'invoice_type.InvoiceTypeID')
                        ->leftjoin('users', 'users.id', '=', 'invoices.CreatedBy')
                        ->where('invoices.OrganisationID', '=', $orgid)
                        ->where('invoices.IsDeleted', '<>', 1)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('invoices.CreatedBy','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('invoices.InvoiceName', 'Like', '%' . $input . '%')
                                ->orWhere('invoices.InvoiceID', 'Like', '%' . $input . '%')
                                ->orWhere('invoice_type.InvoiceType', 'Like', '%' . $input . '%');
                        })
                        ->select('invoices.*', 'invoices.InvoiceName')
                        ->get();

                    $organisation = DB::table('users')
                        ->leftjoin('organisation', 'users.OrganisationID', '=', 'organisation.OrganisationID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('organisation.Created_By','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('organisation.OrganisationName', 'Like', '%' . $input . '%')
                                ->orWhere('organisation.OrganisationID', 'Like', '%' . $input . '%');
                        })

                        ->select('organisation.*')
                        ->get();
//JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    $customers = DB::table('users')
                        ->leftjoin('customers', 'users.id', '=', 'customers.UserID')
                        ->leftjoin('customer_types', 'customers.CustomerTypeID', '=', 'customer_types.CustomerTypeID')
                        ->leftjoin('organisation_customers', 'organisation_customers.CustomerID', '=', 'customers.CustomerID')
                        ->leftjoin('customers as c1', 'users.id', '=', 'customers.Created_By')
                        ->where('organisation_customers.OrganisationID', '=', $orgid)
                        ->where('customers.IsDeleted', '<>', 1)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('c1.Created_By','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('customers.CustomerName', 'Like', '%' . $input . '%')
                                ->orWhere('customers.CustomerID', 'Like', '%' . $input . '%')
                                ->orWhere('customer_types.CustomerType', 'Like', '%' . $input . '%');
                        })
                        ->select('customers.*', 'customer_types.CustomerType')
                        ->get();

                    $inventory = DB::table('inventory')
                        ->leftjoin('inventory_type', 'inventory.InventoryTypeID', '=', 'inventory_type.InventoryTypeID')
                        ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                        ->where('inventory.OrganisationID', '=', $orgid)
                        ->where('inventory.IsDeleted', '<>', 1)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('inventory.CreatedBy','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('inventory.InventoryName', 'Like', '%' . $input . '%')
                                ->orWhere('inventory.InventoryID', 'Like', '%' . $input . '%')
                                ->orWhere('inventory_type.InventoryType', 'Like', '%' . $input . '%');
                        })
                        ->select('inventory.*', 'inventory_type.InventoryType')
                        ->get();

                    $orders = DB::table('orders')
                        ->leftjoin('order_type', 'orders.OrderTypeID', '=', 'order_type.OrderTypeID')
                        ->leftjoin('order_status', 'orders.OrderStatusID', '=', 'order_status.OrderStatusID')
                        ->leftjoin('users', 'users.id', '=', 'orders.CreatedBy')
                        ->where('orders.OrganisationID', '=', $orgid)
                        ->where('orders.IsDeleted', '<>', 1)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('orders.CreatedBy','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('orders.OrderName', 'Like', '%' . $input . '%')
                                ->orWhere('orders.OrderID', 'Like', '%' . $input . '%')
                                ->orWhere('order_type.OrderType', 'Like', '%' . $input . '%')
                                ->orWhere('order_status.OrderStatus', 'Like', '%' . $input . '%');
                        })
                        ->select('orders.*', 'order_type.OrderType', 'order_status.OrderStatus')
                        ->get();

                    $tasklist = DB::table('tasklist')
                        ->leftjoin('task_type', 'tasklist.TaskTypeID', '=', 'task_type.TaskTypeID')
                        ->leftjoin('task_status', 'tasklist.StatusID', '=', 'task_status.TaskStatusID')
                        ->leftjoin('task_severities', 'tasklist.TaskSeverityID', '=', 'task_severities.TaskSeverityID')
                        ->leftjoin('priorities', 'tasklist.PriorityID', '=', 'priorities.PriorityID')
                        ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                        ->where('tasklist.OrganisationID', '=', $orgid)
                        ->where('tasklist.IsDeleted', '<>', 1)
                        ->where(function($query)use($userId){
                            return $query
                                ->where('tasklist.CreatedBy','=',$userId)
                                ->orwhere('users.AssignedTo','=',$userId);
                        })
                        ->where(function($query)use($input){
                            return $query
                                ->Where('tasklist.TasklistID', 'Like', '%' . $input . '%')
                                ->orWhere('task_type.TaskType', 'Like', '%' . $input . '%')
                                ->orWhere('task_status.TaskStatus', 'Like', '%' . $input . '%')
                                ->orWhere('task_severities.TaskSeverity', 'Like', '%' . $input . '%')
                                ->orWhere('priorities.Priority', 'Like', '%' . $input . '%');
                        })
                        ->select('tasklist.*', 'task_type.TaskType', 'task_status.TaskStatus', 'task_severities.TaskSeverity', 'priorities.Priority')
                        ->get();
                }
                if($userrole == "3" || $userrole == "4" || $userrole == "5")
                {
                    $users = DB::table('users')
                        ->leftjoin('user_role', 'users.UserRoleID', '=', 'user_role.UserRoleID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where('users.IsDeleted','=',null)
                        ->where('users.id','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('users.email', 'Like', '%' . $input . '%')
                                ->orWhere('users.FirstName', 'Like', '%' . $input . '%')
                                ->orWhere('users.LastName', 'Like', '%' . $input . '%')
                                ->orWhere('users.id', 'Like', '%' . $input . '%')
                                ->orWhere('users.name', 'Like', '%' . $input . '%');
                        })
                        ->select("users.*")
                        ->get();

                    $invoices = DB::table('invoices')
                        ->leftjoin('invoice_type', 'invoices.InvoiceTypeID', '=', 'invoice_type.InvoiceTypeID')
                        ->where('invoices.OrganisationID', '=', $orgid)
                        ->where('invoices.IsDeleted', '<>', 1)
                        ->where('invoices.CreatedBy','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('invoices.InvoiceName', 'Like', '%' . $input . '%')
                                ->orWhere('invoices.InvoiceID', 'Like', '%' . $input . '%')
                                ->orWhere('invoice_type.InvoiceType', 'Like', '%' . $input . '%');
                        })
                        ->select('invoices.*', 'invoice_type.InvoiceType')
                        ->get();

                    $organisation = DB::table('users')
                        ->leftjoin('organisation', 'users.OrganisationID', '=', 'organisation.OrganisationID')
                        ->where('users.OrganisationID', '=', $orgid)
                        ->where('users.id','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('organisation.OrganisationName', 'Like', '%' . $input . '%')
                                ->orWhere('organisation.OrganisationID', 'Like', '%' . $input . '%');
                        })

                        ->select('organisation.*')
                        ->get();
//JOIN organisation_customers ON organisation_customers.CustomerID = customers.CustomerID
                    $customers = DB::table('users')
                        ->leftjoin('customers', 'users.id', '=', 'customers.UserID')
                        ->leftjoin('customer_types', 'customers.CustomerTypeID', '=', 'customer_types.CustomerTypeID')
                        ->leftjoin('organisation_customers', 'organisation_customers.CustomerID', '=', 'customers.CustomerID')
                        ->where('organisation_customers.OrganisationID', '=', $orgid)
                        ->where('customers.IsDeleted', '<>', 1)
                        ->where('customers.Created_By', '=', $userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('customers.CustomerName', 'Like', '%' . $input . '%')
                                ->orWhere('customers.CustomerID', 'Like', '%' . $input . '%')
                                ->orWhere('customer_types.CustomerType', 'Like', '%' . $input . '%');
                        })
                        ->select('customers.*', 'customer_types.CustomerType')
                        ->get();

                    $inventory = DB::table('inventory')
                        ->leftjoin('inventory_type', 'inventory.InventoryTypeID', '=', 'inventory_type.InventoryTypeID')
                        ->where('inventory.OrganisationID', '=', $orgid)
                        ->where('inventory.IsDeleted', '<>', 1)
                        ->where('inventory.CreatedBy','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('inventory.InventoryName', 'Like', '%' . $input . '%')
                                ->orWhere('inventory.InventoryID', 'Like', '%' . $input . '%')
                                ->orWhere('inventory_type.InventoryType', 'Like', '%' . $input . '%');
                        })
                        ->select('inventory.*', 'inventory_type.InventoryType')
                        ->get();

                    $orders = DB::table('orders')
                        ->leftjoin('order_type', 'orders.OrderTypeID', '=', 'order_type.OrderTypeID')
                        ->leftjoin('order_status', 'orders.OrderStatusID', '=', 'order_status.OrderStatusID')
                        ->where('orders.OrganisationID', '=', $orgid)
                        ->where('orders.IsDeleted', '<>', 1)
                        ->where('orders.CreatedBy','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('orders.OrderName', 'Like', '%' . $input . '%')
                                ->orWhere('orders.OrderID', 'Like', '%' . $input . '%')
                                ->orWhere('order_type.OrderType', 'Like', '%' . $input . '%')
                                ->orWhere('order_status.OrderStatus', 'Like', '%' . $input . '%');
                        })
                        ->select('orders.*', 'order_type.OrderType', 'order_status.OrderStatus')
                        ->get();

                    $tasklist = DB::table('tasklist')
                        ->leftjoin('task_type', 'tasklist.TaskTypeID', '=', 'task_type.TaskTypeID')
                        ->leftjoin('task_status', 'tasklist.StatusID', '=', 'task_status.TaskStatusID')
                        ->leftjoin('task_severities', 'tasklist.TaskSeverityID', '=', 'task_severities.TaskSeverityID')
                        ->leftjoin('priorities', 'tasklist.PriorityID', '=', 'priorities.PriorityID')
                        ->where('tasklist.OrganisationID', '=', $orgid)
                        ->where('tasklist.IsDeleted', '<>', 1)
                        ->where('tasklist.CreatedBy','=',$userId)
                        ->where(function($query)use($input){
                            return $query
                                ->Where('tasklist.TasklistID', 'Like', '%' . $input . '%')
                                ->orWhere('task_type.TaskType', 'Like', '%' . $input . '%')
                                ->orWhere('task_status.TaskStatus', 'Like', '%' . $input . '%')
                                ->orWhere('task_severities.TaskSeverity', 'Like', '%' . $input . '%')
                                ->orWhere('priorities.Priority', 'Like', '%' . $input . '%');
                        })
                        ->select('tasklist.*', 'task_type.TaskType', 'task_status.TaskStatus', 'task_severities.TaskSeverity', 'priorities.Priority')
                        ->get();
                }



                $data = array(
                    'users' => $users,
                    'invoices' => $invoices,
                    'organisation' => $organisation,
                    'customers' => $customers,
                    'orders' => $orders,
                    'tasklist' => $tasklist,
                    'inventory' => $inventory,
                );
                if (!empty($data)) {
                    $response['data'] = $data;
                    $response['code'] = 200;
                } else {
                    $response['data'] = "No record exists";
                    $response['code'] = 400;
                }
                return $response;
            }
    public function getDetails()
            {

                $customers = DB::select(DB::raw("SELECT

                                              customers.CustomerID AS id,customers.CustomerName As name,
                                              users.status,users.Country,organisation_type.OrganisationType,
                                              organisation.OrganisationID AS OrganisationID,customers.Phoneno,
                                              customers.EmailID,user_role.UserRole as user_type,DATE(customers.created_at) as date,users.id as User_id

                                      FROM
                                             users
                                      join

                                                user_role on users.UserRoleID = user_role.UserRoleID

                                                left join customers on users.id = customers.UserID
                                                left join organisation on organisation.OrganisationID = users.OrganisationID
                                                left join organisation_type on organisation.OrganisationTypeID= organisation_type.OrganisationTypeID
                                                 where users.UserRoleID =6
                                                "));

                $organisation = DB::select(DB::raw("SELECT

                                              organisation.OrganisationID AS id,organisation.OrganisationName As name,
                                              users.status,users.Country,organisation_type.OrganisationType,
                                              organisation.OrganisationID AS OrganisationID,organisation.Phoneno,
                                              organisation.EmailID,user_role.UserRole as user_type,DATE(users.created_at) as date,users.id as User_id
                                              FROM    users
                                              left join  user_role on users.UserRoleID = user_role.UserRoleID
                                              left join organisation on organisation.OrganisationID = users.OrganisationID
                                              left join organisation_type on organisation.OrganisationTypeID= organisation_type.OrganisationTypeID
                                                 where users.UserRoleID =1
                                                "));

                $users = DB::select(DB::raw("SELECT

                                              users.id,users.name,
                                              users.status,users.Country,organisation_type.OrganisationType,
                                              organisation.OrganisationID AS OrganisationID,users.PhoneNo As Phoneno,
                                              users.email As EmailID,user_role.UserRole as user_type,DATE(users.created_at) as date,users.id as User_id
                                              FROM users
                                              left join user_role on users.UserRoleID = user_role.UserRoleID
                                              left join organisation on organisation.OrganisationID = users.OrganisationID
                                              left join organisation_type on organisation.OrganisationTypeID= organisation_type.OrganisationTypeID
                                              where users.UserRoleID =3 OR  users.UserRoleID =4 OR  users.UserRoleID =5
                                                "));


                $result = array_merge($customers, $organisation, $users);

                if (!empty($result)) {
                    $response['data'] = $result;

                    $response['code'] = 200;
                } else {
                    $response['data'] = "No record for userdetails exists";
                    $response['code'] = 400;
                }
                return $response;

            }
    public function Change_User_Password($users_id)
    {
        $input = Input::all();
        $input_new_password=trim($input['new_password']);
        $input_confirm_password=trim($input['confirm_password']);
        if ($input_new_password != $input_confirm_password) {
            throw new NewPasswordMismatchException;
        }
        $usersdetails = DB::table('users')
            ->select('*' )
            ->where('id','=',$users_id)
            ->get();
        if(!empty($users_id))
        {

            DB::table('users')->where('id', '=', [$users_id])->update(array('password' => Hash::make($input_confirm_password)));
            $name = $usersdetails[0]->name;
            $username = $usersdetails[0]->email;
            $var = "<h1>Hi, " . $name . "!</h1>" .
                "<p>Your password is changed successfully. Your new credentials are:- </p>
                             <p>Username: " . $username . "</p>
                             <p>Password: " .$input_confirm_password. "</p>
                             <p>Thank you for registering.</p>
                             <p>Regards,</p>
                             <p>Team CEM</p>";

            $emails_table_input = array(
                'Message' => $var,
                'EmailID' =>$username,
                'Name' =>$name,
                'Subject'=>"Password Changed Successfully",
            );

            Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
                    ->subject($emails_table_input['Subject']);
            });

        }

        return "Password Changed Successfully";


    }
    public function Change_User_Status()
    {
        $rules = array(
            'user_id' => 'required',
            'status' => 'required',
            //'usertype'=>'required'
        );
        //$message = '';
        $input=Input::all();
        $v = Validator::make($input, $rules);
        if ($v->passes())
        {
            $status = Input::get('status');
            $user_id = Input::get('user_id');
           // $usertype = Input::get('usertype');

            if(trim(strtolower($status)) == 'active') {
                DB::table('users')->where('id', '=', [$user_id])->update(array('status' => 1));
                $message = "User account is  Activated";

            }
            elseif(trim(strtolower($status)) == 'suspended')
            {
                DB::table('users')->where('id', '=', [$user_id])->update(array('status' => -1));
                $message = " User account is Suspended";

                $usersdetails = DB::table('users')
                    ->select('*' )
                    ->where('id','=',$user_id)
                    ->get();
                if(!empty($usersdetails)) {
                    $name = $usersdetails[0]->name;
                    $username = $usersdetails[0]->email;
                    $var = "<h1>Dear, " . $name . "!</h1>" .
                        "<p>Due to administrative reasons , your services have been temporarily suspended. </p>
                         <p>Please reach out to our administrative team for service resolution.</p>
                        <p>Thanks</p>
                        <p>Compliance Team,</p>
                        <p>Nitai Partners Systems Pvt Ltd</p>";

                    $emails_table_input = array(
                        'Message' => $var,
                        'EmailID' => $username,
                        'Name' => $name,
                        'Subject' => "Mobi-Comm services has been suspended",
                    );

                    Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                        $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
                            ->subject($emails_table_input['Subject']);
                    });


                }


            }
            elseif(trim(strtolower($status)) == 'inactive')
            {
                DB::table('users')->where('id', '=', [$user_id])->update(array('status' => 0));
                $message = "User account is  Inactive";
            }

            return Response::json([
                'collection' => [
                    'version' => '1.0',
                    //'href' => $request->url(),
                    'data' => $message,
                ]
            ], 200);
        }
        else
        {
            return Response::json([
                'collection' => [
                    'version' => '1.0',
                   // 'href' => $request->url(),
                    'data' => $v->messages()->all()
                ]
            ], 400);
        }



        }
    public function get_all_organisation($OrganisationID)
    {
        $results = DB::table('organisation')
            ->select('organisation.*')
            ->where('OrganisationID','=',$OrganisationID)
            ->get();

        if (!empty($results)) {
            $response['data'] = $results;

            $response['code'] = 200;
        } else {
            $response['data'] = "Organization does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    public function get_all_organisation_list()
    {
        $results = DB::table('organisation')
            ->leftjoin('organisation_type','organisation.OrganisationTypeID','=','organisation_type.OrganisationTypeID')
            ->join('users','users.OrganisationID','=','organisation.OrganisationID')
            ->select('organisation.*','organisation_type.OrganisationType','users.id as userid','users.status')
            ->where('users.UserRoleID','=', '1')
            ->get();
          //dd($results);
        if (!empty($results)) {
            $count = count($results);
            for($i=0;$i<$count;$i++)
            {
                $total_customers = DB::table('organisation_customers')->select('organisation_customers.*')
                    ->join('customers', 'customers.CustomerID', '=', 'organisation_customers.CustomerID')
                    ->where('OrganisationID','=',$results[$i]->OrganisationID)->where('customers.IsDeleted','=','0')->count();
                $results[$i]->Customer_count = $total_customers;
                $results[$i]->created_at = date_format(date_create($results[$i]->created_at),"Y-m-d");
                $total_users =     DB::table('users')->select('users.*')
                               ->where('OrganisationID','=',$results[$i]->OrganisationID)
                               ->where('users.UserRoleID','<>','6')
                               ->where('users.IsDeleted','=',null)
                               ->count();
                $results[$i]->user_count = $total_users;

            }



            $response['data'] = $results;

            $response['code'] = 200;
        } else {
            $response['data'] = "Organization does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    public function get_all_organisation_details()
    {
        $results = DB::table('organisation')
             ->select('organisation.OrganisationID','organisation.OrganisationName')
            ->get();
        //dd($results);
        if (!empty($results)) {

            $count = count($results);
            $i = $count++;
            $results[$i]['OrganisationID'] = '';
            $results[$i]['OrganisationName'] = 'None';

            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Organization does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    public function UpdatePassword($input){
        $logged_user=  Authorizer::getResourceOwnerId();
        $input_old_password=trim($input['old_password']);
        $input_new_password=trim($input['new_password']);
        $input_confirm_password=trim($input['confirm_password']);
        $db_password=User::where('id', '=', $logged_user)-> pluck('password');
        //dd($db_password);
        if (!Hash::check($input_old_password, $db_password[0]))
        {
            $response['data']= "Password is Invalid";
            $response['code']=400;
            return $response;
        }
        if ($input_new_password != $input_confirm_password) {
            $response['data']= "Passwords do not match.";
            $response['code']=400;
            return $response;
        }
        $user = User::find($logged_user);
        $user->password=bcrypt($input_confirm_password);
        $user->save();
        $var = "<p>Hi, " . $user->name . "!</p>" .
            "<p>Password Recovery mail .</p><p></p>
                            <p> Username: " . $user->username . "</p>
                             <p>Password: " . $input_confirm_password . "</p><p></p>
                             <p>Thank you for registering.</p>
                             <p>Regards,</p>
                             <p>Team CEM</p>";
        $emails_table_input = array(
            'Name' => $user->name,
            'EmailID' => $user->email,
            'Subject' => "Password Updated Successfully!",
            'Message' => $var,
            'Status' => 1,
            'HasAttachment'=>0,
            'UserID' => $user->id,
            'MailFor' => 'Password',
            'updated_by'=>$user->id,
            'updated_at'=> Carbon::now(),
            'CreatedBy'=>$user->id,
            'created_at'=> Carbon::now()
        );
        $email = Sendmails::create($emails_table_input);
        Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
            $message->to( $emails_table_input['EmailID'], $emails_table_input['Name'])
                ->subject($emails_table_input['Subject']);
        });
        $response['data']= "Password Updated successfully";
        $response['code']=200;
        return $response;
    }






}