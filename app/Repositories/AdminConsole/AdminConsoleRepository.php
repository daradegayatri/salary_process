<?php
/**
 * Created by PhpStorm.
 * User: onwer
 * Date: 21-Jun-17
 * Time: 7:37 PM
 */

namespace App\Repositories\AdminConsole;
use App\Models\Users\Customers;
use Response;
use Illuminate\Support\Facades\Input;
use App\Models\Transactions\TransactionCharges;
use App\Models\Users\User;
use Validator;
use DB;

class AdminConsoleRepository implements AdminConsoleInterface
{


    protected $transactionChar;

    public function __construct(TransactionCharges $transactionCharges)
    {
        $this->transactionChar = $transactionCharges;

    }

    public function getAllData()
    {
        $result_in = DB::table('transaction_charges')
            ->select('transaction_charges.TransactionChargeID',
                'transaction_charges.TransactionSourceID',
                'transaction_source.TransactionFor',
                'transaction_charges.name',
                'transaction_charges.description',
                'transaction_charges.TransactionNo_RangeFrom',
                'transaction_charges.TransactionNo_RangeTo',
                'transaction_charges.Min_Monthly_Amt',
                'transaction_charges.TransactionCharge',
                'transaction_charges.Country',
                'transaction_charges.created_at',
                'transaction_charges.updated_at',
                'transaction_charges.IsDeleted'
                ,'organisation.OrganisationName','organisation.OrganisationID'
                )
            ->join('transaction_source', 'transaction_source.TransactionSourceID', '=', 'transaction_charges.TransactionSourceID')
            ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'transaction_charges.OrganisationID')
            ->where('transaction_charges.IsDeleted', '=', 0)
            ->orderby('transaction_charges.OrganisationID', 'asc')
            ->get();
        if (!empty($result_in)) {
            $response['data'] = $result_in;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }
    }
    public function transaction_charges_By_id($id)
    {
        $transactiondetails = DB::table('transaction_charges')
            ->select('transaction_charges.TransactionChargeID',
                'transaction_charges.TransactionSourceID',
                'transaction_source.TransactionFor',
                'transaction_charges.name',
                'transaction_charges.description',
                'transaction_charges.TransactionNo_RangeFrom',
                'transaction_charges.TransactionNo_RangeTo',
                'transaction_charges.Min_Monthly_Amt',
                'transaction_charges.TransactionCharge',
                'transaction_charges.Country',
                'transaction_charges.created_at',
                'transaction_charges.updated_at',
                'transaction_charges.IsDeleted',
                'organisation.OrganisationID',
                'organisation.OrganisationName'
            )
            ->join('transaction_source', 'transaction_source.TransactionSourceID', '=', 'transaction_charges.TransactionSourceID')
            ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'transaction_charges.OrganisationID')
            ->Where('transaction_charges.TransactionChargeID', '=', $id)
            ->where('transaction_charges.IsDeleted', '=', 0)
            ->orderby('transaction_charges.TransactionChargeID', 'asc')
            ->get();

        if (!empty($transactiondetails)) {
            $response['data'] = $transactiondetails;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;
        }


    }

    public function delete_transaction_charges($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        } else {
            $response['data'] = "Select Set Rules to delete";
            $response['code'] = 400;
            return $response;
        }

        foreach ($ids as $id) {
             $transaction_charges = DB::table('transaction_charges')
                ->select('transaction_charges.*')
                ->where('transaction_charges.TransactionChargeID', '=', $id)
                ->get();
            if (!empty($transaction_charges)) {
                DB::table('transaction_charges')->where('TransactionChargeID', '=', $id)->update(array('IsDeleted' => '1'));
                $response['data'] = 'Your Record has been successfully deleted ';
                $response['code'] = 200;

            } else {
                $response['data'] = "No record exists";
                $response['code'] = 400;

            }
        }
        return $response;
    }


    public function update_transaction_charges($id)
    {
        $set_rules = DB::table('transaction_charges')
            ->where('TransactionChargeID', $id)
            ->update([
                'name' => Input::get('name'),
                'description' => Input::get('description'),
                'TransactionCharge' => Input::get('TransactionCharge'),
                'TransactionSourceID' => Input::get('TransactionSourceID'),
                'TransactionNo_RangeFrom' => Input::get('TransactionNo_RangeFrom'),
                'TransactionNo_RangeTo' => Input::get('TransactionNo_RangeTo'),
                'Min_Monthly_Amt' => Input::get('Min_Monthly_Amt')
            ]);

        if (!empty($set_rules)) {
            $response['data'] = $set_rules;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
            return $response;

        }

    }

    public function update_transaction_log_status($organisation_id,$status)
    {

        $transaction_log = DB::update(DB::raw("update `transaction_log_monthly` set `IsPaid` = 1  where MONTH(created_at) < MONTH(CURRENT_DATE) and YEAR(created_at) = YEAR(CURRENT_DATE) and SourceID = $organisation_id  and  `IsPaid` = 0 "));
        $transaction_details = DB::update(DB::raw("update `transaction_details` set `IsPaid` = 1  where `OrganisationID` = $organisation_id and YEAR(created_at) = YEAR(CURRENT_DATE) and MONTH(created_at) < MONTH(CURRENT_DATE)"));

            $response['data'] = "Status updated Successfully";
            $response['code'] = 200;
            return $response;

    }


    public function AdminConsoleGlobalSearch($input)
    {
        $users = DB::table('users')
            ->join('user_role', 'users.UserRoleID', '=', 'user_role.UserRoleID')
            ->Where('users.email', 'Like', '%' . $input . '%')
            ->orWhere('users.FirstName', 'Like', '%' . $input . '%')
            ->orWhere('users.LastName', 'Like', '%' . $input . '%')
            ->orWhere('users.id', 'Like', '%' . $input . '%')
            ->orWhere('users.name', 'Like', '%' . $input . '%')
            ->select("users.*")
            ->get();

        $organisation = DB::table('users')
            ->join('organisation', 'users.OrganisationID', '=', 'organisation.OrganisationID')
            ->Where('organisation.OrganisationName', 'Like', '%' . $input . '%')
            ->orWhere('organisation.OrganisationID', 'Like', '%' . $input . '%')
            ->select('organisation.*')
            ->get();


        $customers = DB::table('users')
            ->join('customers', 'users.id', '=', 'customers.UserID')
            ->join('customer_types', 'customers.CustomerTypeID', '=', 'customer_types.CustomerTypeID')
            ->Where('customers.CustomerName', 'Like', '%' . $input . '%')
            ->orWhere('customers.CustomerID', 'Like', '%' . $input . '%')
            ->orWhere('customer_types.CustomerType', 'Like', '%' . $input . '%')
            ->select('customers.*', 'customer_types.CustomerType')
            ->get();

        $transaction = DB::table('transaction_details')
            ->join('transaction_source', 'transaction_details.TransactionSourceID', '=', 'transaction_source.TransactionSourceID')
            ->join('transaction_charges', 'transaction_charges.TransactionSourceID', '=', 'transaction_source.TransactionSourceID')
            ->join('transaction_log_monthly', 'transaction_log_monthly.TransactionSourceID', '=', 'transaction_source.TransactionSourceID')
            ->Where('transaction_source.TransactionSourceID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_charges.TransactionChargeID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_details.TransactionDetailsID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_log_monthly.TransactionLogMonthlyID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_details.OrganisationID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_details.CustomerID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_details.TransactionSourceID', 'Like', '%' . $input . '%')
            ->orWhere('transaction_details.SourceID', 'Like', '%' . $input . '%')
            ->select('transaction_details.*')
            ->get();

        $result = array(
            'users' => $users,
            'organisation' => $organisation,
            'customers' => $customers,
            'transaction' => $transaction

        );

        if (!empty($result)) {
            $response['data'] = $result;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;

    }

    public function gettransactiondetails_byid($userid, $duration, $month)
    {
        $results = array();
       if ($duration == 'monthly') {
                $results['transactions'] = DB::select(DB::raw("SELECT
                                              transaction_log_monthly.TransactionLogMonthlyID,transaction_log_monthly.PerTransactionCharge,
                                              transaction_log_monthly.TransactionMonth,transaction_log_monthly.TransactionYear,transaction_log_monthly.TransactionSourceID,
                                              transaction_log_monthly.Amount,transaction_log_monthly.SourceID,transaction_log_monthly.NoOfTransaction,transaction_log_monthly.IsPaid,
                                              DATE(transaction_log_monthly.created_at) as date,
                                              transaction_log_monthly.created_at,transaction_log_monthly.updated_at
                                      FROM
                                             transaction_log_monthly
                                                 where transaction_log_monthly.SourceID=$userid"));
           $results['statistics']= DB::select(DB::raw("SELECT SUM(transaction_log_monthly.Amount) as Amount,transaction_log_monthly.SourceID as OrganisationID,
                                                                   SUM(transaction_log_monthly.NoOfTransaction) as NoOfTransaction

                                      FROM
                                             transaction_log_monthly
                                                 where transaction_log_monthly.SourceID=$userid"));
        } else if ($duration == 'daily') {
            if ($month == 'all') {
                $results['transactions'] = DB::select(DB::raw("SELECT
                                              transaction_details.OrganisationID,transaction_details.TransactionDetailsID as transactionid,
                                              organisation.OrganisationName,transaction_details.CustomerID,customers.CustomerName,
                                              transaction_details.OrderID,transaction_charges.TransactionCharge,transaction_charges.Min_Monthly_Amt,
                                              transaction_charges.TransactionNo_RangeFrom,transaction_source.TransactionFor as Transaction_Type,
                                              DATE(transaction_details.created_at) as date,
                                              transaction_charges.TransactionNo_RangeTo,transaction_details.IsPaid,transaction_details.created_at,transaction_details.updated_at
                                      FROM
                                             transaction_details
                                       left join
                                               organisation on organisation.OrganisationID = transaction_details.OrganisationID
                                               left join  customers on customers.CustomerID = transaction_details.CustomerID
                                               left join orders on orders.OrderID = transaction_details.OrderID
                                               left join  transaction_source on transaction_source.TransactionSourceID = transaction_details.TransactionSourceID
                                               left join transaction_charges on transaction_charges.TransactionChargeID = transaction_details.TransactionChargesID
                                               where transaction_details.OrganisationID=$userid ORDER BY transaction_details.created_at desc "));
                $results['statistics']= DB::select(DB::raw("SELECT SUM(transaction_log_monthly.Amount) as Amount,transaction_log_monthly.SourceID as OrganisationID,
                                                                   SUM(transaction_log_monthly.NoOfTransaction) as NoOfTransaction

                                      FROM
                                             transaction_log_monthly
                                                 where transaction_log_monthly.SourceID=$userid"));
            } else {
                $results['transactions'] = DB::select(DB::raw("SELECT
                                              transaction_details.OrganisationID,transaction_details.TransactionDetailsID as transactionid,
                                              organisation.OrganisationName,transaction_details.CustomerID,customers.CustomerName,
                                              transaction_details.OrderID,transaction_charges.TransactionCharge,transaction_charges.Min_Monthly_Amt,
                                              transaction_charges.TransactionNo_RangeFrom,transaction_source.TransactionFor as Transaction_Type,
                                              DATE(transaction_details.created_at) as date,
                                              transaction_charges.TransactionNo_RangeTo,transaction_details.IsPaid,transaction_details.created_at,transaction_details.updated_at
                                      FROM
                                             transaction_details
                                       left join
                                               organisation on organisation.OrganisationID = transaction_details.OrganisationID
                                               left join  customers on customers.CustomerID = transaction_details.CustomerID
                                               left join orders on orders.OrderID = transaction_details.OrderID
                                               left join  transaction_source on transaction_source.TransactionSourceID = transaction_details.TransactionSourceID
                                               left join transaction_charges on transaction_charges.TransactionChargeID = transaction_details.TransactionChargesID
                                               where transaction_details.OrganisationID=$userid"));




                $results['statistics']= DB::select(DB::raw("SELECT SUM(transaction_log_monthly.Amount) as Amount,transaction_log_monthly.SourceID as OrganisationID,
                                                                   SUM(transaction_log_monthly.NoOfTransaction) as NoOfTransaction

                                      FROM
                                             transaction_log_monthly
                                                 where transaction_log_monthly.SourceID=$userid"));
            }

           $month = date('M');
           $year = date('Y');
           $results['current_month_statistics']= DB::select(DB::raw("SELECT SUM(transaction_log_monthly.Amount) as Amount,transaction_log_monthly.SourceID as OrganisationID,
                                                                   SUM(transaction_log_monthly.NoOfTransaction) as NoOfTransaction
                                            FROM  transaction_log_monthly
                                            where transaction_log_monthly.SourceID=$userid
                                            AND TransactionMonth = '$month'
                                            AND TransactionYear = '$year' "));
           $prevmonth = date("M", strtotime("first day of previous month"));
           $prevyear = date("Y", strtotime("first day of previous month"));

           $results['prev_month_statistics']= DB::select(DB::raw("SELECT SUM(transaction_log_monthly.Amount) as Amount,transaction_log_monthly.SourceID as OrganisationID,
                                                                   SUM(transaction_log_monthly.NoOfTransaction) as NoOfTransaction
                                            FROM  transaction_log_monthly
                                            where transaction_log_monthly.SourceID=$userid
                                            AND TransactionMonth = '$prevmonth'
                                            AND TransactionYear = '$prevyear' "));



        }

        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;

        }



}