<?php
/**
 * Created by PhpStorm.
 * User: onwer
 * Date: 21-Jun-17
 * Time: 7:37 PM
 */

namespace App\Repositories\AdminConsole;


interface AdminConsoleInterface {


    public function getAllData();
    public function transaction_charges_By_id($id);
    public function delete_transaction_charges($id);
    public function update_transaction_charges($id);
    public function gettransactiondetails_byid($userid, $duration,$month);

}