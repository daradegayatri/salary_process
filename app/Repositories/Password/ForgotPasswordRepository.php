<?php
namespace App\Repositories\Password;

use App\Models\Emails\ContactEmails;
use App\Models\Person\Email\PersonEmails;
use App\Models\Person\PersonPresons;
use App\Models\Users\User;
use Input;
use Response;
use DB;
use App\Models\Password\RandomPassword;

class ForgotPasswordRepository implements ForgotPasswordRepositoryInterface {

//    protected $supervisor;
//
//    public function __construct(Supervisor $supervisor)
//    {
//        $this->supervisor = $supervisor;
//    }

    public function ForgotPassword(){
        //try {
            $password=new RandomPassword();
            $temp=$password->randomPassword();
            $input = \Input::all();
            //dd($input);
            $email = \Input::get('email');
            if (PersonEmails::where('email', '=', $email)->exists()) {
                $people = DB::table('person_emails')->where('email', $email)->pluck('person_id');
                $user_name = DB::table('people')->where('id', $people)->pluck('nick_name');
                $user_id = DB::table('people')->where('id', $people)->pluck('id');
                //$user = DB::table('users')-> where('userable_id',$user_id);
                $user = User::find($user_id);
                $user->password = bcrypt($temp);
                $user->save();

                $var1=   "<h1>Hi, ".$user_name."!</h1>

					<p>Please Login to DreamCat using Following Details.</p>
                    <p>Username: ".\Input::get('email')."</p>
					<p>Password: ".$temp."</p>
					<p>Thank you for Using DreamCat!</p>
					<p>Regards,</p><br>
					<p>Team DreamCat</p>"
                ;
                $contact_emails_table_input=array(
                    'name' => $user_name,
                    'email' => \Input::get('email'),
                    'subject' => "Forgot Password!",
                    'body' => $var1,
                    'is_sent' =>0,
                    'type'=>'ForgotPassword',
                    'attachment_path' =>null,
                    'mime'=> null
                );
                $email = ContactEmails::create($contact_emails_table_input);
                return Response::json([
                    'collection' => [
                        'version' => '1.0',
                        //'href' => $request->url(),
                        'data' => "Please Check Your Email...!! Thank You"
                    ]
                ], 200);
            } else {
                return Response::json([
                    'collection' => [
                        'version' => '1.0',
                       // 'href' => $request->url(),
                        'data' => "Database Doesn't Contain Written Email-Id..!!"
                    ]
                ], 200);

            }
        //}
//        catch(QueryException $qe) {
//            return Response::json([
//                'collection' => [
//                    'version' => '1.0',
//                    'href' => $request->url(),
//                    'data' => "Email Id Already Exist"
//                ]
//            ], 400);
//        }
//        catch(\Exception $e){
//            return Response::json([
//                'collection' => [
//                    'version' => '1.0',
//                    'href' => $request->url(),
//                    'data' => "".$e->getMessage()
//                ]
//            ], 400);
//        }

    }
}