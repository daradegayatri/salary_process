<?php
/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 5/8/2017
 * Time: 4:03 PM
 */

namespace App\Repositories\Organisation;
//use App\Repositories\Users;
//use App\Repositories\Organisation;
use App\Models\Users\Organisation;
use App\Models\Users\User;
use App\Models\General\Sendmails;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;
use Hash;
use App\Events;
use App\Events\SendMail;
use App\Models\Users\SubOrganisations;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class OrganisationRepository implements OrganisationRepositoryInterface
{
    public function createorganisation($input,$url){
        $rules = array(
            'email' => 'required|email|unique:users,email',
            /*'phoneno' => 'required|numeric|unique:users,PhoneNo|regex:/[0-9]{10}/',*/
            'companyname'=>'required|unique:organisation,OrganisationName'
        );
        $v = Validator::make($input, $rules);
        if( $v->passes() ) {
            DB::beginTransaction();
            $organisation_list_count = DB::table('organisation')
                ->select('*')
                ->count();

            if ($organisation_list_count == "0")
                $organisation_list_id = "8000001";
            else
                $organisation_list_id = '';
            $organizationData = [
                'OrganisationID' => $organisation_list_id,
                'OrganisationName' => Input::get('companyname'),
                'OrganisationTypeID' => Input::get('organisationtype'),
                'ContactPerson' => Input::get('contactperson'),
                'EmailID'=>Input::get('email'),
                'Phoneno' => Input::get('phoneno'),
                'Address'=> Input::get('address'),
                'City' => Input::get('city'),
                'State'=>Input::get('state'),
                'Country'=>Input::get('country'),
                'ZipCode'=>Input::get('pincode'),
                'HasSocialMediaDetails' => Input::get('OrgSocialMediaType'),
                'OrgSocialMediaAccount' => Input::get('OrgSocialMediaAccount'),
                'HasSubOrganisation' => '0',
                'IsSubOrganisation' => '0',
            ];

            if(isset($input['description'])) $organizationData['Description'] = $input['description'];
            if(isset($input['ParentOrganisationID'])) $organizationData['ParentOrgID'] = $input['ParentOrganisationID'];
            if(isset($input['webaddress'])) $organizationData['OrgWebAddress'] = $input['webaddress'];
            if(isset($input['OrgHierarchy'])) $organizationData['OrgHierarchy'] = $input['OrgHierarchy'];

            $organisation = Organisation::create($organizationData);

            $password = $this->GenerateString();
            $users_list_count = DB::table('users')
                ->select('*')
                ->count();

            if ($users_list_count == "0")
                $users_list_id = "8000001";
            else
                $users_list_id = '';
            $person=User::create([
                'id'=> $users_list_id,
                'FirstName' => Input::get('companyname'),
                'LastName' => "",
                'OrganisationID' => $organisation->id,
                'name'=> Input::get('companyname'),
                'Gender' => "",
                'email'=>Input::get('email'),
                'username' =>Input::get('email'),
                'password' => Hash::make($password),
                'UserRoleID' => '1',
                'City' => Input::get('city'),
                'Country' => Input::get('country'),
                'Postalcode' =>Input::get('postal_code'),
                'PhoneNo' => Input::get('phoneno'),
                'AssignedTo' =>Input::get('assignto'),
                'Address' =>Input::get('address'),
                'CreatedBy' => Input::get('createdby'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if(isset($input['ParentOrganisationID'])) {
                $subOrganisation = SubOrganisations::create([
                    'ParentOrganisationID' => $input['ParentOrganisationID'],
                    'OrganisationID' => $organisation->id,
                ]);
            }

            DB::commit();

            $firstname = Input::get('companyname');
            $x = Input::get('email');
            $var = "<h1>Hi, " .$firstname. "!</h1>" .
                "<p>Welcome to MobiComm CEM.</p>
                   <p>Your Company is successfully registered. Please Login to our Application using Following Details.</p><p></p>
                             <p>Username: $x</p>
                             <p>Password: " .$password."</p>
                             <p>Link: " .$url."</p><p></p>
                             <p>Thank you for registering.</p>
                             <p>Regards,</p>
                             <p>Team CEM</p>";

            $emails_table_input = array(
                'Name' => $firstname,
                'EmailID' => Input::get('email'),
                'Subject' => "Welcome to MobiComm CEM!",
                'Message' => $var,
                'Status' => 1,
                'HasAttachment'=>0,
                'UserID' => $person->id,
                'MailFor' => 'Registration',
                'updated_by'=>Input::get('createdby'),
                'updated_at'=> Carbon::now(),
                'CreatedBy'=>Input::get('createdby'),
                'created_at'=> Carbon::now()
            );
            $email = Sendmails::create($emails_table_input);
                // \Event::fire(new SendMail($emails_table_input));
             Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                $message->to( $emails_table_input['EmailID'], $emails_table_input['Name'])
                    ->subject($emails_table_input['Subject']);
            });

            $response['data'] = Input::get('companyname')." Registered Successfully. Your ID is ".$organisation->id.".";
            $response['code']=200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code']=400;
            return $response;
        }
    }

    public function findNoFailOrganisation($id)
    {
        return Organisation::where('OrganisationID','=',$id)->first();
    }

    public function getAllSubOrganisation()
    {
        $userId = Authorizer::getResourceOwnerId();
        $organisationId = DB::table('users')->where('users.id','=',$userId)->pluck('OrganisationID');

        if(!empty($organisationId))
        {
            $org_ids = SubOrganisations::where('sub_organisations.ParentOrganisationID','=',$organisationId)->lists('OrganisationID');
            $subOrganization = DB::table('organisation')->select('organisation.*')->whereIn('organisation.OrganisationID', $org_ids)->get();

            if (!empty($subOrganization)) {
                $response['data'] = $subOrganization;
                $response['code']=200;
            } else {
                $response['data'] = "Suborganisation does not exists";
                $response['code']=400;
            }    
        }
        else
        {
            $response['data'] = "Organisation does not exists";
            $response['code']=400;
        }
        return $response;
    }
    public function getSubOrganisationList()
    {
        $userId = Authorizer::getResourceOwnerId();
        $organisationId = DB::table('users')->where('users.id','=',$userId)->pluck('OrganisationID');

        if(!empty($organisationId))
        {
            $org_ids = SubOrganisations::where('sub_organisations.ParentOrganisationID','=',$organisationId)->lists('OrganisationID');
            $count = count($org_ids);
            $org_ids[$count] = $organisationId[0];
            $subOrganization = DB::table('organisation')->select('organisation.*')->whereIn('organisation.OrganisationID', $org_ids)->get();
             if (!empty($subOrganization)) {
                $response['data'] = $subOrganization;
                $response['code']=200;
            } else {
                $response['data'] = "Suborganisation does not exists";
                $response['code']=400;
            }
        }
        else
        {
            $response['data'] = "Organisation does not exists";
            $response['code']=400;
        }
        return $response;
    }

    public function GetOrganisationById($id)
    {
        $organisation = $this->findNoFailOrganisation($id);
        
        if(!empty($organisation))
        {
            $results = DB::table('organisation')
                ->select('organisation.*')
                ->where('organisation.OrganisationID','=',$id)
                ->get();

            $users = DB::table('users')->select('users.id','users.name','users.email','users.username','users.PhoneNo')
                    ->where('users.email','=',$results[0]->EmailID)
                    ->get();

            $org_ids = SubOrganisations::where('sub_organisations.ParentOrganisationID','=',$id)->lists('OrganisationID');
            $subOrganization = DB::table('organisation')->select('organisation.*')->whereIn('organisation.OrganisationID', $org_ids)->get();

            $results[0]->users = $users;
            $results[0]->subOrganization = $subOrganization;

            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Organisation does not exists";
            $response['code']=400;
        }
        return $response;
    }

    public function UpdateOrganisation($id,$input)
    {
        $organisation = $this->findNoFailOrganisation($id);
        $update_user_table_array = array();

        if(!empty($organisation))
        {

            if(isset($input['username']))
            {
                $update_user_table_array['username'] = $input['username'];
                unset($input['username']);
            }

            if(isset($input['OrganisationName'])) $update_user_table_array['name'] = $input['OrganisationName'];
            if(isset($input['OrganisationName'])) $update_user_table_array['FirstName'] = $input['OrganisationName'];
            if(isset($input['EmailID'])) $update_user_table_array['email'] = $input['EmailID'];
            if(isset($input['Phoneno'])) $update_user_table_array['PhoneNo'] = $input['Phoneno'];
            if(isset($input['city'])) $update_user_table_array['City'] = $input['city'];
            if(isset($input['State'])) $update_user_table_array['State'] = $input['State'];
            if(isset($input['country'])) $update_user_table_array['Country'] = $input['country'];
            if(isset($input['ZipCode'])) $update_user_table_array['Postalcode'] = $input['ZipCode'];
            $organisation= Organisation::where('OrganisationID','=',$id)->update($input);

            $user= User::where('OrganisationID','=',$id)->where('UserRoleID','=','1')->update($update_user_table_array);

            $data = "Organisation Updated Successfully";
            $response['data'] = $data;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Organisation does not exists";
            $response['code']=400;
        }
        return $response;
    }

    public function get_all_organisation_type()
    {
        $results = DB::table('organisation_type')
            ->select('organisation_type.*')
            ->get();
        if(!empty($results))
        {
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Organisation type does not exists";
            $response['code']=400;

        }
        return $response;


    }

    public function GenerateString()
    {
        $unambiguousCharacters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
        $temporaryPassword = "";
        // build a temporary password consisting of 8 unambiguous characters
        for ($i = 0; $i < 8; $i++) {
            $temporaryPassword .= $unambiguousCharacters[rand(0, strlen($unambiguousCharacters) - 1)];
        }
        return $temporaryPassword;
    }

    public function deleteOrganisation($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        }
        else
        {
            $response['data'] = "Select Organisation to delete" ;
            $response['code']=400;
            return $response;
        }
        foreach ($ids as $id) {
            $organisation =DB::table('organisation')
                ->select('OrganisationID','OrganisationName')
                ->where('organisation.OrganisationID','=',$id)
                ->get();
            if(!empty($organisation))
            {
                /*DB::delete('delete from sub_organisations where OrganisationID = ?',[$id]);
                DB::delete('delete from sub_organisations where OrganisationID = ?',[$id]);
                DB::delete('delete from sub_organisations where OrganisationID = ?',[$id]);
                DB::delete('delete from orders where OrganisationID = ?',[$id]);
                DB::delete('delete from organisation_customers where OrganisationID = ?',[$id]);
                DB::delete('delete from organisation_socialmedia_details where OrganisationID = ?',[$id]);
                DB::delete('delete from sub_organisations where OrganisationID = ?',[$id]);
                DB::delete('delete from tasklist where SourceID = ?',[$id]);
                DB::delete('delete from transaction_details where OrganisationID = ?',[$id]);*/
                DB::delete('delete from transaction_log_monthly where SourceID = ?',[$id]);
                //DB::delete('delete from users where OrganisationID = ?',[$id]);
                DB::delete('delete from organisation where OrganisationID = ?',[$id]);
                $response['data'] = "Organisation deleted successfully";
                $response['code']=200;
            }
            else
            {
                $response['data'] = "Organization does not exists" ;
                $response['code']=400;
                return $response;
            }
        }
        return $response;
    }
}