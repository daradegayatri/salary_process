<?php
/**
 * Created by PhpStorm.
 * User: admin-nitai
 * Date: 5/4/2017
 * Time: 11:02 AM
 */
namespace App\Repositories\BackgroundColor;



use App\Models\BackgroundColor\BackgroundColors;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
class BackgroundColorRepository implements BackgroundColorInterface {

    protected $backgroundColors;

    public function __construct(BackgroundColors $backgroundColors)
    {
        $this->backgroundColors = $backgroundColors;
    }

    public function all()
    {
        return $this->backgroundColors->all();
    }

    public function find($id)
    {
        return BackgroundColors::findOrFail($id);
    }

    public function update($id, array $input)
    {
        $color = DB::table('theme_color')
                 ->where('updated_by',$id)
                 ->update([
                   'header' => Input::get('header'),
                   'footer' => Input::get('footer'),
                   'side_menu' => Input::get('side_menu'),
                 ]);
    }

    public function show($id)
    {
        $result = DB::table('theme_color')
                  ->select('theme_color.*')
                  ->where('theme_color.created_by',$id)
                  ->get();;
        return $result;
    }
}