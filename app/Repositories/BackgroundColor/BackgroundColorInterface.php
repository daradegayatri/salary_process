<?php
/**
 * Created by PhpStorm.
 * User: admin-nitai
 * Date: 5/4/2017
 * Time: 11:01 AM
 */
namespace App\Repositories\BackgroundColor;

interface BackgroundColorInterface
{
    public function all();
    public function find($id);
    public function update($id, array $input);
    public function show($id);
   // public function upload_image();
}
