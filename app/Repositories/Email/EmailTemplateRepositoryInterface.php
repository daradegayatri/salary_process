<?php 

namespace App\Repositories\Email;

interface EmailTemplateRepositoryInterface {

    /**
     * Returns all Email Template
     *
     * @return \App\Models\Email\EmailTemplate
     */
    public function all();

    /**
     * Creates a new Email Template resource
     *
     * @param array $input
     * @return \App\Models\Email\EmailTemplate
     */
    public function create(array $input);

    /**
     * Returns the specified Email Template
     *
     * @param $id
     * @return \App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find($id);

    /**
     * Updates the specified Email Template
     *
     * @param $id
     * @param array $input
     * @return \App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update($id, array $input);

    /**
     * Deletes the specified Email Template
     *
     * @param $id
     * @return void
     */
    public function delete($id);
}