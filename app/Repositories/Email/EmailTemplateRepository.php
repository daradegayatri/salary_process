<?php

namespace App\Repositories\Email;

use App\Models\Email\EmailTemplate;
use Validator;
use App\Models\Files\Fileattachment;
use App\Models\Files\Filesource;
use DB;
use App\Models\Users\User;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Models\Tasks\Tasklist;


class EmailTemplateRepository implements EmailTemplateRepositoryInterface 
{
    public function __construct(EmailTemplate $Emailtemplate, Fileattachment $fileattachment, Filesource $filesource)
    {
        $this->Emailtemplate = $Emailtemplate;
        $this->fileattachment = $fileattachment;
        $this->filesource = $filesource;
    }

    /**
     * Returns all Email Template
     *
     * @return Email Template
     */
    public function all()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $emailTemplate = $this->Emailtemplate->where('organisation_id','=',$org_id)->get();
        if(count($emailTemplate))
        {
            $response['data'] = $emailTemplate;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Email Template does not exists";
            $response['code']=400;
        }
        return $response;
    }

    /**
     * Creates a new Email Template
     *
     * @param array $input
     * @return \App\Models\Email\EmailTemplate
     */
    public function create(array $input)
    {
        $rules = array(
            'Name' => 'required|unique:email_template,name',
            'subject' => 'required',
            'Html' => 'required',
            'Organisation_id' => 'required|exists:organisation,OrganisationID',
        );

        $v = Validator::make($input, $rules);

        if ($v->passes()) 
        {
            $emailTemplateValue = [
                'name' => $input['Name'],
                'html' => $input['Html'],
                'subject' => $input['subject'],
                'organisation_id' => $input['Organisation_id'],
            ];

            $emailTemplate = $this->Emailtemplate->create($emailTemplateValue);

            $response['data'] = "Email Template created successfully";
            $response['code']=200;
            return $response;
        }
        else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }

    /**
     * Returns the specified Email Template
     *
     * @param $id
     * @return App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById($id)
    {
        $emailTemplate = $this->findNoFail($id);
        if(count($emailTemplate))
        {
            $response['data'] = $emailTemplate;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "Email Template does not exists";
            $response['code']=400;
        }
        return $response;
    }

    /**
     * Returns the specified Email Template
     *
     * @param $id
     * @return App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findNoFail($id)
    {
        return $this->Emailtemplate->find($id);
    }

    /**
     * Returns the specified Email Template
     *
     * @param $id
     * @return App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find($id)
    {
        return $this->Emailtemplate->findOrFail($id);
    }

    /**
     * Updates the specified Email Template
     *
     * @param $id
     * @param array $input
     * @return App\Models\Email\EmailTemplate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update($id, array $input)
    {
        $exsistEmailTemplate = $this->findNoFail($id);
        if ($exsistEmailTemplate)
        {
            $emailTemplate = $this->find($id);
            $emailTemplate->fill($input);
            $emailTemplate->save();

            $response['data'] = $emailTemplate;
            $response['code']=200;
            return $response;
        } 
        else 
        {
            $response['data'] = "Email Template Not Found" ;
            $response['code']=400;
            return $response;
        }
    }

    /**
     * Deletes the specified Email Template
     *
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $exsistEmailTemplate = $this->findNoFail($id);
        if ($exsistEmailTemplate)
        {
            $this->Emailtemplate->destroy($id);

            $response['data'] = "Email Template deleted successfully";
            $response['code']=200;
            return $response;
        } 
        else 
        {
            $response['data'] = "Email Template Not Found" ;
            $response['code']=400;
            return $response;
        }
    }

        /**
     * get all Template Images
     *
     * @param array $input
     * @return App\Models\Files\Fileattachment
     */
    public function getTemplateImages()
    {
        $fileSourceId = $this->filesource->where('FileSource','=','Template')->pluck('FileSourceID');

        $images = $this->fileattachment->where('FileSourceID','=',$fileSourceId)->get();

        if(count($images))
        {
            $imageresult = array();

            foreach ($images as $key => $value) {
                $data = array(
                    "name" => $value['FilePath'],
                );

                array_push($imageresult,$data);
            }

            $response['files'] = $imageresult;
            $response['code']=200;
        }
        else
        {
            $response['files'] = "Images does not exists";
            $response['code']=400;
        }
        return $response;
    }

    /**
     * Creates a new File Attachment
     *
     * @param array $input
     * @return App\Models\Files\Fileattachment
     */
    public function storeImages()
    {
        $fileSourceId = $this->filesource->where('FileSource','=','Template')->pluck('FileSourceID');

        $file = \Request::file('upload');

        $destinationPath = public_path('images/emailtemplate');
        $originalfilename = $file->getClientOriginalName();
        $mime = $file->getClientmimeType();
        $filename = date("Y_m_d").'_'.$fileSourceId[0].'_'.$originalfilename;
        $path = $file->move($destinationPath, $filename);

        $filepath = asset('images/emailtemplate/'.date("Y_m_d").'_'.$fileSourceId[0].'_'.$originalfilename);

        $fileattachment = Fileattachment::create([
           'FileType' => $mime,
           'FilePath' => $filepath,
           'FileSourceID' => $fileSourceId[0],
       ]);

        $response['data'] = "Image uploaded successfully";
        $response['code']=200;
        return $response;
    }

    public function sendCustomeEmail(array $input)
    {
        if(isset($input['email']) && !(empty($input['email'])))
        {
            $createEmail = new \App\Helpers\CreateEmail();
            $email = $createEmail->execute($input);

            $response['data'] = "Mail Send Successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "Error In Sending Mail";
            $response['code'] = 400;
            return $response;
        }
    }

    public function EmailResponse(array $input)
    {
        if(isset($input['taskid'])) $taskid = $input['taskid']; else $taskid = '';
        if(isset($input['description'])) $description = $input['description']; else $description = '';
        $task_list_details = DB::table('tasklist')
                             ->select('*')
                             ->where('TasklistID','=',$taskid)
                             ->where('IsDeleted','=','0')
                             ->get();
        if(!empty($task_list_details))
        {
            $task_list_count = Tasklist::count();

            if($task_list_count == "0")
                $tasklistid = "5000001";
            else
                $tasklistid = '';

            $task_subject = "Re: ".$task_list_details[0]->TasklistID." :- ".$task_list_details[0]->name;
            if(empty($task_list_details[0]->name))
            {
                if(isset($input['title']))
                {
                    if(!empty($input['title']))
                    $task_subject =    $input['title'];
                }
            }

            $task_list = [
                'TasklistID' => $tasklistid,
                'OrganisationID' => $task_list_details[0]->OrganisationID,
                'name' => $task_subject,
                'Description' =>$description,
                'HasAttachment' => 0,
                'TaskTypeID' => 1,
                'CustomerID' => $task_list_details[0]->CustomerID,
                'CreatedBy' =>$task_list_details[0]->CreatedBy
            ];
            $task = Tasklist::create($task_list);

             $taskid = $task->id;
             $userId = $task_list_details[0]->CreatedBy;
             $orgid = $task_list_details[0]->OrganisationID;

            $notification = \App\Helpers\Helpers::create_notification($userId, $taskid, $orgid, 'task');

            $sourceid = $taskid;
            $orderid = '';
            $customerid = '';
            $transactionsourceid = '3';

            $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);

            $email['data']= "Task created successfully";
            $email['code']=200;
            return $email;
        }
        $email['data']= "Cannot create task";
        $email['code']=400;
        return $email;
    }
}