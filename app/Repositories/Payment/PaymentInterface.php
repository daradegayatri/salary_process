<?php
/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 7/11/2017
 * Time: 2:46 PM
 */

namespace App\Repositories\Payment;


interface PaymentInterface {
    public function get_authorise_payment_status($input);

}