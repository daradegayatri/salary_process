<?php
/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 7/11/2017
 * Time: 2:46 PM
 */

namespace App\Repositories\Payment;
use App\Models\Payments\Payment;
use App\Models\Transactions\TransactionLog;
use DB;



class PaymentRepository implements  PaymentInterface
{

    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment=$payment;
    }

    public function get_authorise_payment_status($input)
    {

        $payment_input_array = array();
        if(isset($input['x_trans_id']))$payment_input_array['TransactionID'] = $input['x_trans_id'];
        if(isset($input['x_response_reason_text']))$payment_input_array['Status'] = $input['x_response_reason_text'];
        if(isset($input['x_response_code']))$payment_input_array['StatusCode'] = $input['x_response_code'];
        if(isset($input['x_card_type']))$payment_input_array['CardType'] = $input['x_card_type'];
        if(isset($input['x_account_number']))$payment_input_array['AccountNumber'] = $input['x_account_number'];
        if(isset($input['x_amount']))$payment_input_array['Amount'] = $input['x_amount'];
        if(isset($input['x_tax']))$payment_input_array['Tax'] = $input['x_tax'];
        if(isset($input['x_auth_code']))$payment_input_array['AuthCode'] = $input['x_auth_code'];
        if(isset($input['x_organisation_id']))$payment_input_array['OrganisationID'] = $input['x_organisation_id'];
        if(isset($input['x_transactionlog_id']))$payment_input_array['TransactionLogMonthlyID'] = $input['x_transactionlog_id'];

        if(!empty($payment_input_array))
        {
            $payment = Payment::create($payment_input_array);
            if(!empty($payment))
            {
                if(isset($input['x_response_code']))
                    $status = $input['x_response_code'];
                else
                    $status = 0;

                if($status=="1")
                {
                    $month = date('M', strtotime('-1 month', strtotime(date('Y-m-01'))));
                    $year = date('Y', strtotime('-1 month', strtotime(date('Y-m-01'))));
                    $organisation_id = $payment_input_array['OrganisationID'];

                    $transaction_log = DB::select(DB::raw("update `transaction_log_monthly`
                                       set `IsPaid` = 1
                                       where MONTH(created_at) < MONTH(CURRENT_DATE)
                                       and YEAR(created_at) = YEAR(CURRENT_DATE)
                                       and SourceID = $organisation_id
                                       and  `IsPaid` = 0 "));



                    $transaction_details = DB::select(DB::raw("update `transaction_details`
                                       set `IsPaid` = 1
                                       where `OrganisationID` = $organisation_id
                                       and YEAR(created_at) = YEAR(CURRENT_DATE)
                                       and MONTH(created_at) < MONTH(CURRENT_DATE)"));


                    $response['data']="1";
                    $response['code']=200;
                }
                else
                {
                    $response['data']="0";
                    $response['code']=400;
                }

            }

        }
        else
        {
            $response['data']="0";
            $response['code']=400;
        }


        return $response;


    }

}