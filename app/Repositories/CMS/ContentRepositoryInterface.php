<?php 

namespace App\Repositories\CMS;

interface ContentRepositoryInterface {

    /**
     * Returns Category content
     *
     * @return App\Models\CMS\CMSContent
     */
    public function getContentByValue($categoryName);

    /**
     * Returns the specified Category content
     *
     * @param $id
     * @return App\Models\CMS\CMSContent
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find($id);

    /**
     * Updates the specified Category content
     *
     * @param $id
     * @param array $input
     * @return App\Models\CMS\CMSContent
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update($id, array $input);
}