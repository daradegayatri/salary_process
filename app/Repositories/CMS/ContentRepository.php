<?php

namespace App\Repositories\CMS;

use Illuminate\Support\Facades\DB;
use App\Models\CMS\CMSCategory;
use App\Models\CMS\CMSContent;
use Auth;

class ContentRepository implements ContentRepositoryInterface
{
    public function __construct(CMSCategory $CMScategory, CMSContent $CMScontent)
    {
        $this->CMScategory = $CMScategory;
        $this->CMScontent = $CMScontent;
    }

    /**
     * Returns get Category content by category name and organization
     *
     * @return Category content
     */
    public function getContentByValue($categoryName)
    {
        $organizationId = app('Dingo\Api\Auth\Auth')->user()->OrganisationID;

        $categoryId = $this->CMScategory->where('value', '=', $categoryName)->pluck('id');

        if ($categoryId) {
            $content = $this->findByOrganization($categoryId, $organizationId);
            if ($content) {
                $response['data'] = $content;
                $response['code']=200;
                return $response;
            } else {
                $response['data'] = "Content Not Found" ;
                $response['code']=400;
                return $response;
            }   
        } else {
            $response['data'] = "Category Not Found" ;
            $response['code']=400;
            return $response;
        }
    }

    public function updateContent($id, array $input)
    {
       $content = $this->findNoFail($id);
         if ($content) {
           $content = $this->findByOrganization($input['cms_category_id'], $input['organization_id']);
            if ($content) {
                $category = $this->CMScategory->where('id', '=', $input['cms_category_id'])->pluck('name');
                if($category[0] == 'Logo') {
                    if(\Request::hasFile('content')) {
                        $file = \Request::file('content');
                        $originalfilename = $file->getClientOriginalName();
                        $filename = date("Y_m_d").'_'.$originalfilename;
                        $destinationPath = public_path() . '/images/';
                        if (!is_dir($destinationPath)) {
                            mkdir(public_path() . '/images/', 0777);
                        }
                        $path = $file->move($destinationPath, $filename);
                        $input['content'] = strstr($path->getPathName(), '/images', false);
                    }
                }
                $content = $this->update($id, $input);
                $response['data'] = $content;
                $response['code']=200;
                return $response;
            } else {
                $response['data'] = "Content Not Found" ;
                $response['code']=400;
                return $response;
            }
        } else {
            $content = $this->findByOrganization($input['cms_category_id'], $input['organization_id']);
            if (($content)) {
                $category = $this->CMScategory->where('id', '=', $input['cms_category_id'])->pluck('name');
                if($category[0] == 'Logo') {
                    if(\Request::hasFile('content')) {
                        $file = \Request::file('content');
                        $originalfilename = $file->getClientOriginalName();
                        $filename = date("Y_m_d").'_'.$originalfilename;
                        $destinationPath = public_path() . '/images/';
                        if (!is_dir($destinationPath)) {
                            mkdir(public_path() . '/images/', 0777);
                        }
                        $path = $file->move($destinationPath, $filename);
                        $input['content'] = strstr($path->getPathName(), '/images', false);
                    }
                }
                $content = $this->create($input);
                $response['data'] = $content;
                $response['code']=200;
                return $response;
            } else {
                $response['data'] = "Content Not Found" ;
                $response['code']=400;
                return $response;
            }
        }
    }

    /**
     * Returns the specified Category content
     *
     * @param $id
     * @return App\Models\CMS\CMSContent
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findNoFail($id)
    {
        return $this->CMScontent->find($id);
    }

    /**
     * Returns the specified Category content
     *
     * @param $id
     * @return App\Models\CMS\CMSContent

     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find($id)
    {
        return $this->CMScontent->findOrFail($id);
    }

    /**
     * Updates the specified Category content
     *
     * @param $id
     * @param array $input
     * @return App\Models\CMS\CMSContent
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update($id, array $input)
    {
        $content = $this->find($id);
        $content->fill($input);
        $content->save();

        return $content;
    }
    public function create(array $input)
    {
        $content = CMSContent::create([
        'content' => $input['content'],//$input['firstname'],
        'cms_category_id' => $input['cms_category_id'],//$input['firstname'],
        'organization_id' => $input['organization_id'],//$input['lastname'],
    ]);
        return $content;
    }

    /**
     * Returns the specified Category content
     *
     * @param $id
     * @return App\Models\CMS\CMSContent
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findByOrganization($id, $organizationId)
    {
        return $this->CMScontent->where('cms_category_id','=',$id)->where('organization_id','=',$organizationId)->get();
    }
}