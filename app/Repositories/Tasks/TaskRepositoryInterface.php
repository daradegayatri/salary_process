<?php
/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 2/14/2017
 * Time: 11:28 AM
 */

namespace App\Repositories\Tasks;


interface TaskRepositoryInterface {
    public function get_all_taskstatus();
    public function get_all_tasktype();
    public function get_taskstatus_by_id($id);
    public function get_taskstatus_by_name($task);
    public function get_all_worktype();
    public function get_worktype_by_id($id);
    public function get_worktype_by_name($worktype);
    public function get_all_priorities();
    public function get_priorities_by_id($id);
    public function get_priorities_by_name($priorities);
    public function get_all_filesource();
    public function get_filesource_by_id($id);
    public function get_filesource_by_name($priorities);
    public function get_orderdetails_by_id($orderid);
    public function get_all_order_details($userrole,$orgid,$supervisorid);
    public function get_all_inventorydetails($userrole,$orgid,$userId);
    public function get_all_available_inventorydetails();
    public function get_inventorydetails_by_id($inventoryid);
    public function get_all_inventorydetails_bytype($type);
    //public function get_all_invoicedetails($supervisorid);
    //public function get_invoicedetails_by_id($invoiceid);
    public function get_all_tasklist($userrole,$orgid,$userId);
    public function gettaskby_userid($id);
    public function gettaskby_status($statusid,$userid);
    public function get_all_locationsdetails();
    public function get_all_jobtype();
    public function createtask($input);
    public function  updatetaskstatus($id,$status);
    public function postcomments($input);
    public function get_commentslistby_taskid($id);
    public function get_commentscountfor_task($id);
    public function assign_user_task($input);
    public function deleteorder($input);
    public function createorder($input);
    //public function raiseinvoices($input);
    //public function get_invoicedetails_by_orderid($orderid);
    public function get_order_listby_userid($id);
    public function get_taskdetails_by_id($id);
    public function Update($TasklistID, array $input);
}