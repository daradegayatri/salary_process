<?php

/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 2/14/2017
 * Time: 11:30 AM
 */
namespace App\Repositories\Tasks;

use App\Models\General\Eventdetails;
use App\Models\Users\Customers;
use Illuminate\Support\Facades\Storage;
use App\Models\Orders\Invoices;
use Validator;
use Hash;
use App\Models\Tasks\Taskstatus;
use App\Models\Orders\TaskOrder;
use App\Models\Tasks\Tasklist;
use App\Models\General\Comments;
use App\Models\Orders\Order;
use App\Models\Orders\OrderFrom;
use App\Models\Orders\InvoiceInventory;
use App\Models\Orders\OrderInventory;
use App\Models\Users\UserInventory;
use App\Models\General\Notifications;
use App\Models\Files\Fileattachment;
use App\Models\Inventory\Inventory;
use App\Models\Tasks\Assignusertask;
use App\Models\General\Sendmails;
use App\Models\Users\User;
use App\Repositories\Users\UserRepositoryInterface;
use Dingo\Blueprint\Annotation\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\RandomPassword;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Models\Transactions\TransactionLog;
use App\Models\Transactions\TransactionCharges;
use App\Models\Transactions\TransactionDetails;
use  App\Models\Transactions\TransactionSource;
use Illuminate\Support\Facades\Mail;
use App\Models\Files\Filesource;

class TaskRepository implements TaskRepositoryInterface
{
//-------------------------------------------Routes for Task-----------------------------------
//------------------------------------------GET Route-------------------------------------------
    public function get_all_taskstatus()
    {
        try {
            $results = DB::table('task_status')
                ->select('TaskStatusID', 'TaskStatus')
                ->get();
            if (!empty($results)) {
                $response['data'] = $results;
                $response['code'] = 200;
            } else {
                $response['data'] = "Task status does not exists";
                $response['code'] = 400;

            }
            return $response;
        } catch (Exceptions $ex) {


        }


    }

    public function get_all_tasktype()
    {
        try {
            $results = DB::table('task_type')
                ->select('TaskTypeID', 'TaskType')
                ->get();
            if (!empty($results)) {
                $response['data'] = $results;
                $response['code'] = 200;
            } else {
                $response['data'] = "Task Type does not exists";
                $response['code'] = 400;

            }
            return $response;
        } catch (Exceptions $ex) {


        }


    }

    public function get_taskstatus_by_id($id)
    {

        $results = DB::table('task_status')
            ->select('TaskStatusID', 'TaskStatus')
            ->where('TaskStatusID', '=', $id)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task status does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_taskstatus_by_name($task)
    {

        $results = DB::table('task_status')
            ->select('TaskStatusID', 'TaskStatus')
            ->where('TaskStatus', '=', $task)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task status does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_commentslistby_taskid($id)
    {

        $results = DB::table('comments')
            ->select('comments.CommentID', 'comments.TasklistID', 'comments.UserID',
                'comments.HasAttachment', 'comments.Description', 'comments.created_at as postedon',
                'users.name as postedby', 'tasklist.TaskID')
            ->leftjoin('users', 'users.id', '=', 'comments.UserID')
            ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'comments.TasklistID')
            ->where('comments.TasklistID', '=', $id)
            ->get();
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->CommentID)
                    ->where('file_attachment.FileSourceID', '=', '2')
                    ->get();
                $filecount = count($files);
                //dd($filecount);
                for ($a = 0; $a < $filecount; $a++) {
                    $files[$a]->FileName = $files[$a]->FilePath;
                    $files[$a]->FilePath = asset('images/' . $files[$a]->FilePath);

                }
                $results[$i]->filedetails = $files;
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_commentscountfor_task($id)
    {
        $results = DB::table('comments')
            ->select('comments.CommentID', 'comments.TasklistID', 'comments.UserID', 'comments.HasAttachment', 'comments.Description', 'users.name as postedby', 'tasklist.TaskID')
            ->leftjoin('users', 'users.id', '=', 'comments.UserID')
            ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'comments.TasklistID')
            ->where('comments.TasklistID', '=', $id)
            ->count();
        if (!empty($results)) {
            $response['data']['count'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }
    public  function get_Parent_Organisation_tasklist()
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $org_id = DB::table('users')
            ->select('users.id as UserID','users.OrganisationID')
            ->where('users.id','=',$userId)
            ->get();
        $response = $this->get_all_tasklist($userrole,$org_id[0]->OrganisationID,$userId);
        return $response;
    }
    public  function get_Sub_Organisation_tasklist($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        //$org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $response = $this->get_all_tasklist($userrole,$org_id,$userId);
        return $response;
    }
    public function get_all_tasklist($userrole,$orgid,$userId)
    {

        if ($userrole == "1") {
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus', 'organisation.OrganisationName',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'tasklist.OrganisationID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.IsDeleted', '=', '0')
                ->get();
        }
        if ($userrole == "2") {
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus','organisation.OrganisationName',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'tasklist.OrganisationID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.IsDeleted', '=', '0')
                ->where('tasklist.CreatedBy', '=', $userId)
                ->orwhere('users.AssignedTo', '=', $userId)
                ->get();
        }
        if ($userrole == "3" || $userrole == "4") {
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus','organisation.OrganisationName',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'tasklist.OrganisationID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.IsDeleted', '=', '0')
                ->where('tasklist.CreatedBy', '=', $userId)
                ->get();
        }

        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                if (!empty($results[$i]->AssignedTo)) {
                    $assignedto = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedTo)
                        ->get();
//                //dd($assignedto);
                    $results[$i]->Assignedtoname = $assignedto[0]->name;
                } else
                    $results[$i]->Assignedtoname = null;
                if (!empty($results[$i]->AssignedBy)) {
                    $assignedby = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedBy)
                        ->get();
//
                    $results[$i]->AssignedByname = $assignedby[0]->name;
                } else {
                    $results[$i]->AssignedByname = null;
                }
//
//
                //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->TasklistID)
                    ->where('file_attachment.FileSourceID', '=', '1')
                    ->get();
                $filecount = count($files);
                //dd($filecount);
                for ($a = 0; $a < $filecount; $a++) {
                    $files[$a]->FileName = $files[$a]->FilePath;
                    $files[$a]->FilePath = asset('images/' . $files[$a]->FilePath);

                }
                $results[$i]->filedetails = $files;
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function gettaskby_userid($id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        if ($userrole == '1') {
            // dd('2');
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.CreatedBy', '=', $id)
                ->get();
        }
        if ($userrole == '3' || $userrole == '4') {
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.CreatedBy', '=', $id)
                ->get();

        }
        if ($userrole == '2') {
            $results = DB::table('tasklist')
                ->select('tasklist.*', 'task_status.TaskStatus',
                    'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                    'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                    'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                    'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                ->where('tasklist.OrganisationID', '=', $orgid)
                ->where('tasklist.CreatedBy', '=', $id)
                //->orwhere('users.AssignedTo','=',$id)
                ->get();
        }

        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                if (!empty($results[$i]->AssignedTo)) {
                    $assignedto = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedTo)
                        ->get();
//                //dd($assignedto);
                    $results[$i]->Assignedtoname = $assignedto[0]->name;

                } else
                    $results[$i]->Assignedtoname = null;
                if (!empty($results[$i]->AssignedBy)) {
                    $assignedby = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedBy)
                        ->get();
//
                    $results[$i]->AssignedByname = $assignedby[0]->name;
                } else {
                    $results[$i]->AssignedByname = null;
                }
//
                //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->TasklistID)
                    ->where('file_attachment.FileSourceID', '=', '1')
                    ->get();
                $filecount = count($files);
                //dd($filecount);
                for ($a = 0; $a < $filecount; $a++) {
                    $files[$a]->FileName = $files[$a]->FilePath;
                    $files[$a]->FilePath = asset('images/' . $files[$a]->FilePath);

                }
                $results[$i]->filedetails = $files;
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function gettask_for_order()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];

        if (!empty($userrole)) {
            if ($userrole == '1') {
                $results = DB::table('tasklist')
                    ->select('tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.JobID',
                        'tasklist.PriorityID', 'tasklist.LocationID', 'tasklist.Description',
                        'tasklist.StatusID', 'tasklist.HasAttachment', 'task_status.TaskStatus',
                        'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                        'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                        'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                        'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                    ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                    ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                    ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                    ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                    ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                    ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                    ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                    ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                    ->where('tasklist.CreatedBy', '=', $userId)
                    ->where('tasklist.OrganisationID', '=', $orgid)
                    ->where('tasklist.StatusID', '<>', '3')
                    ->get();

            } elseif ($userrole == '2') {
                // dd('2');
                $results = DB::table('tasklist')
                    ->select('tasklist.TasklistID', 'tasklist.TaskID')
                    ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                    ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                    ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                    ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                    ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                    ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                    ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                    ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                    ->where('users.AssignedTo', '=', $userId)
                    ->orwhere('tasklist.CreatedBy', '=', $userId)
                    ->where('tasklist.OrganisationID', '=', $orgid)
                    ->where('tasklist.StatusID', '<>', '3')
                    ->get();
                //dd($results);
            } elseif ($userrole == '3') {
                // dd('2');
                $results = DB::table('tasklist')
                    ->select('tasklist.TasklistID', 'tasklist.TaskID')
                    ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                    ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                    ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                    ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                    ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                    ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                    ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                    ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                    ->where('tasklist.OrganisationID', '=', $orgid)
                    ->where('tasklist.StatusID', '<>', '3')
                    ->get();
                //dd($results);
            }
        }


        if (!empty($results)) {
//            $count = count($results);
//            for ($i = 0; $i < $count; $i++) {
//
//                if(!empty($results[$i]->AssignedTo))
//                {
//                    $assignedto = DB::table('users')
//                        ->select('name')
//                        ->where('users.id', '=', $results[$i]->AssignedTo)
//                        ->get();
////                //dd($assignedto);
//                    $results[$i]->Assignedtoname = $assignedto[0]->name;
//
//                }
//                else
//                    $results[$i]->Assignedtoname = null;
//                if(!empty($results[$i]->AssignedBy))
//                {
//                    $assignedby = DB::table('users')
//                        ->select('name')
//                        ->where('users.id', '=', $results[$i]->AssignedBy)
//                        ->get();
////
//                    $results[$i]->AssignedByname = $assignedby[0]->name;
//                }
//                else
//                {
//                    $results[$i]->AssignedByname = null;
//                }
////
//                //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
//                $files = DB::table('file_attachment')
//                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
//                    ->where('file_attachment.SourceID', '=', $results[$i]->TasklistID)
//                    ->where('file_attachment.FileSourceID', '=', '1')
//                    ->get();
//                $filecount = count($files);
//                //dd($filecount);
//                for($a=0;$a<$filecount;$a++)
//                {
//                    $files[$a]->FileName = $files[$a]->FilePath;
//                    $files[$a]->FilePath = asset('images/'.$files[$a]->FilePath);
//
//                }
//                $results[$i]->filedetails = $files;
//            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_taskdetails_by_id($id)
    {
        //TasklistID, TaskID, JobID, PriorityID, LocationID, Description, StatusID, HasAttachment, CreatedBy, updated_by, created_at, updated_at
        //UserID, UserRoleID, UserName, Password, FirstName, LastName, EmailID, PhoneNo, Gender, Address, City, Country, Postalcode
        //JobID, JobName, JobDescription, CreatedBy, updated_by, created_at, updated_at
        //TaskStatusID, TaskStatus, CreatedBy, updated_by, created_at, updated_at
        //PriorityID, Priority, CreatedBy, updated_by, created_at, updated_at
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        //AssignUserTaskID, UserTaskStatusID, TasklistID, AssignedTo, AssignedBy, CreatedBy, updated_by, created_at, updated_at

        $results = DB::table('tasklist')
            ->select('tasklist.*', 'task_status.TaskStatus',
                'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
            ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
            ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
            ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
            ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
            ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
            ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
            ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
            ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
            ->where('tasklist.TasklistID', '=', $id)
            ->get();
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                if (!empty($results[$i]->AssignedTo)) {
                    $assignedto = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedTo)
                        ->get();
//                //dd($assignedto);
                    $results[$i]->Assignedtoname = $assignedto[0]->name;

                } else
                    $results[$i]->Assignedtoname = null;
                if (!empty($results[$i]->AssignedBy)) {
                    $assignedby = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedBy)
                        ->get();
//
                    $results[$i]->AssignedByname = $assignedby[0]->name;
                } else {
                    $results[$i]->AssignedByname = null;
                }//
                //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->TasklistID)
                    ->where('file_attachment.FileSourceID', '=', '1')
                    ->get();
                $filecount = count($files);
                //dd($filecount);
                for ($a = 0; $a < $filecount; $a++) {
                    $files[$a]->FileName = $files[$a]->FilePath;
                    $files[$a]->FilePath = asset('images/' . $files[$a]->FilePath);

                }
                $results[$i]->filedetails = $files;
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function Update($TasklistID, array $input)
    {
        $task = DB::table('tasklist')->where('TasklistID', '=', $TasklistID)->get();

        if (!empty($task)) {
            $results = DB::table('tasklist')
                ->where('TasklistID', '=', $TasklistID)
                ->update([
                    'TaskID' => Input::get('TaskID'),
                    'TaskTypeID' => Input::get('TaskTypeID'),
                    'PriorityID' => Input::get('PriorityID'),
                    'LocationID' => Input::get('LocationID'),
                    'TaskSeverityID' => Input::get('TaskSeverityID'),
                    'Description' => Input::get('Description'),
                    'StatusID' => Input::get('StatusID'),
                    'DueDate' => Input::get('DueDate'),
                    'name' => Input::get('Name'),
                    'OrderID' => Input::get('OrderID'),
                    'CustomerID' => Input::get('CustomerID'),
                    'cycle_type' => Input::get('cycle_type'),
                    'cycle_value' => Input::get('cycle_value'),
                    'units' => Input::get('units'),
                ]);
            $response['data'] = "Task Updated Successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "Failed to update record. Try again later";
            $response['code'] = 400;
            return $response;
        }
    }

    public function gettaskdetails_for_order($id)
    {
        //TasklistID, TaskID, JobID, PriorityID, LocationID, Description, StatusID, HasAttachment, CreatedBy, updated_by, created_at, updated_at
        //UserID, UserRoleID, UserName, Password, FirstName, LastName, EmailID, PhoneNo, Gender, Address, City, Country, Postalcode
        //JobID, JobName, JobDescription, CreatedBy, updated_by, created_at, updated_at
        //TaskStatusID, TaskStatus, CreatedBy, updated_by, created_at, updated_at
        //PriorityID, Priority, CreatedBy, updated_by, created_at, updated_at
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        //AssignUserTaskID, UserTaskStatusID, TasklistID, AssignedTo, AssignedBy, CreatedBy, updated_by, created_at, updated_at

        $results = DB::table('tasklist')
            ->select('tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.LocationID',
                'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
            ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
            ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
            ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
            ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
            ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
            ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
            ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
            ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
            ->where('tasklist.TasklistID', '=', $id)
            ->get();
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                if (!empty($results[$i]->AssignedTo)) {
                    $assignedto = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedTo)
                        ->get();
//                //dd($assignedto);
                    $results[$i]->Assignedtoname = $assignedto[0]->name;

                } else {
                    $response['data'] = "Can not create order of task as user is not assigned to it";
                    $response['code'] = 400;
                    return $response;
                }
                //$results[$i]->Assignedtoname = null;


            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function gettaskby_status($statusid, $userid)
    {
        $userrole = DB::table('users')
            ->select('users.id', 'users.UserRoleID')
            ->where('users.id', '=', $userid)
            ->get();
        if (!empty($userrole)) {
            if ($userrole['0']->UserRoleID == '3') {
                $results = DB::table('tasklist')
                    ->select('tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.JobID',
                        'tasklist.PriorityID', 'tasklist.LocationID', 'tasklist.Description',
                        'tasklist.StatusID', 'tasklist.HasAttachment', 'task_status.TaskStatus',
                        'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                        'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                        'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                        'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                    ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                    ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                    ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                    ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                    ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                    ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                    ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                    ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                    ->where('tasklist.CreatedBy', '=', $userid)
                    ->where('tasklist.StatusID', '=', $statusid)
                    ->get();

            } elseif ($userrole['0']->UserRoleID == '2') {
                $results = DB::table('tasklist')
                    ->select('tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.JobID',
                        'tasklist.PriorityID', 'tasklist.LocationID', 'tasklist.Description',
                        'tasklist.StatusID', 'tasklist.HasAttachment', 'task_status.TaskStatus',
                        'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                        'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'users.name as createdbyname',
                        'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                        'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
                    ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                    ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
                    ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
                    ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
                    ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
                    ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
                    ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
                    ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
                    ->where('users.AssignedTo', '=', $userid)
                    ->where('tasklist.StatusID', '=', $statusid)
                    ->get();
            }
        }
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {

                if (!empty($results[$i]->AssignedTo)) {
                    $assignedto = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedTo)
                        ->get();
                    $results[$i]->Assignedtoname = $assignedto[0]->name;

                } else
                    $results[$i]->Assignedtoname = null;
                if (!empty($results[$i]->AssignedBy)) {
                    $assignedby = DB::table('users')
                        ->select('name')
                        ->where('users.id', '=', $results[$i]->AssignedBy)
                        ->get();//
                    $results[$i]->AssignedByname = $assignedby[0]->name;
                } else {
                    $results[$i]->AssignedByname = null;
                }
//
                //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->TasklistID)
                    ->where('file_attachment.FileSourceID', '=', '1')
                    ->get();
                $filecount = count($files);
                //dd($filecount);
                for ($a = 0; $a < $filecount; $a++) {
                    $files[$a]->FileName = $files[$a]->FilePath;
                    $files[$a]->FilePath = asset('images/' . $files[$a]->FilePath);

                }
                $results[$i]->filedetails = $files;
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_orderlist()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if ($userrole == "1") {
            $results = DB::select(DB::raw("
                  select orders.OrderID,orders.OrderName
                   from orders
                   WHERE orders.OrganisationID = $orgid "));
        }
        if ($userrole == "2") {
            $results = DB::select(DB::raw("
                   select orders.OrderID,orders.OrderName
                   from orders
                    LEFT JOIN  users ON users.id = orders.CreatedBy
                    WHERE orders.OrganisationID = $orgid AND(orders.CreatedBy = $userId OR users.AssignedTo = $userId ) "));
        }
        if ($userrole == "3" || $userrole == "4") {
            $results = DB::select(DB::raw("
                  select orders.OrderID,orders.OrderName
                   from orders
                    WHERE orders.OrganisationID = $orgid AND orders.CreatedBy = $userId"));
        }


        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    //-----------------------------------------End of Get Route--------------------------------------
    //------------------------------------------Post Route-------------------------------------------
    public function createtask($input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        $org_email = User::where('id', '=', $userId)->pluck('email');
        $org_name = User::where('id', '=', $userId)->pluck('name');
        $rules = array(
            'name' => 'required',
        );
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            if (!empty($input['fileattachment'])) $count = count($input['fileattachment']);
            else  $count = 0;
            if ($count == '0') $hasattachment = '0';
            else    $hasattachment = '1';
            $task_list_count = DB::table('tasklist')
                ->select('TaskStatusID', 'TaskStatus')
                ->count();
            if ($task_list_count == "0") $tasklistid = "5000001";
            else   $tasklistid = '';


            DB::beginTransaction();
            $task = Tasklist::create([
                'TasklistID' => $tasklistid,
                'OrganisationID' => $orgid,
                'TaskSeverityID' => Input::get('Severity'),
                'PriorityID' => Input::get('Priority'),
                'LocationID' => Input::get('Location'),
                'name' => Input::get('name'),
                'Description' => Input::get('Description'),
                'StatusID' => Input::get('StatusID'),
                'TaskTypeID' => Input::get('TaskTypeID'),
                'OrderID' => Input::get('OrderID'),
                'CustomerID' => Input::get('CustomerID'),
                'DueDate' => date_format(date_create(Input::get('DueBy')), "Y-m-d"),
                'EndDate' => date_format(date_create(Input::get('EndDate')), "Y-m-d"),
                'HasAttachment' => $hasattachment,
                'cycle_type' => Input::get('cycle_type'),
                'cycle_value' => Input::get('cycle_value'),
                'units' => Input::get('units'),
                'CreatedBy' => $userId,
                'created_at' => Carbon::now()
            ]);
            $taskid = $task->id;
            $order_id = Input::get('OrderID');
            if (!empty($order_id)) {
                if (!empty($order_details)) {
                    $order = TaskOrder::create([
                        'OrderID' => $order_id,
                        'TasklistID' => $taskid,
                        'OrderStatusID' => "",
                        'CreatedBy' => $userId,
                        'created_at' => Carbon::now()
                    ]);
                }
            }

            $notification = \App\Helpers\Helpers::create_notification($userId, $taskid, $orgid, 'task');

            if ($count > '0') {
                $filedata = Input::file('fileattachment');
                $filesourceid = '1';
                $fileattachment = \App\Helpers\Helpers::fileattachment($filedata, $taskid, $userId, $filesourceid);
            }
            $sourceid = $taskid;
            $orderid = Input::get('OrderID');
            $customerid = Input::get('CustomerID');
            $transactionsourceid = '3';
            $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);
            $task_type = DB::table('task_type')->select('TaskTypeID', 'TaskType')->where('TaskType', '=', 'Appointment')->orwhere('TaskType', '=', 'Reminder')->get();

            if (Input::get('TaskTypeID') == $task_type[0]->TaskTypeID || Input::get('TaskTypeID') == $task_type[1]->TaskTypeID) {
                $event_details = array(
                    "start_date" => $task->DueDate,
                    "end_date" => $task->EndDate,
                    "start_time" => "00:00:00",
                    "end_time" => "00:00:00",
                    "event_description" => $task->Description,
                    "event_title" => $task->name,
                    "event_location" => "",
                    "event_organizer" => $org_name[0],
                    "organizer_mail" => $org_email[0],
                    "orgid" => $orgid,
                    "userid" => $userId,
                    "timezone" => "Asia/Kolkata",
                    "event_id" => "0",
                    'alert_type_id' => '',
                    'repeat_type_id' => '',
                    "taskid" => $task->id
                );
                if (!empty($customerid)) {
                    $event_details['customerid'] = $customerid;
                    $customeremail = Customers::where('CustomerID', '=', $customerid)->pluck('EmailID');
                    $customername = Customers::where('CustomerID', '=', $customerid)->pluck('CustomerName');
                    $event_details['receipent_mail'] = $customeremail[0];
                    $event_details['receipent_name'] = $customername[0];

                } else
                    $event_details['customerid'] = '0';

                $event_details['message'] = "<p>Appointment schedule for " . $event_details['event_title'] . "</p>.
                             <p>Start Time:" . $event_details['start_time'] . "</p>
                             <p>End Time:" . $event_details['end_time'] . "</p>
                             <p>Start Date:" . $event_details['start_date'] . " </p>
                             <p>End Date:" . $event_details['end_date'] . " </p>
                             <p>Location:" . $event_details['event_location'] . "</p>
                             <p>Description:" . $event_details['event_description'] . "</p>
                             <p>Organizer:" . $event_details['event_organizer'] . "</p>
                             <p></p>
                             <p> Regards,</p>
                             <p> Team CEM </p>";

                $create_event = \App\Helpers\Helpers::create_ical_event($event_details);

                $notification = \App\Helpers\Helpers::create_notification($userId, $taskid, $orgid, 'task');
//                $sourceid = $taskid;
//                $orderid = Input::get('OrderID');
//                $customerid = Input::get('CustomerID');
//                $transactionsourceid = '3';
//
//                $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);

            }
            DB::commit();
            $response['data'] = "Task Created Successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }

    public function  updatetaskstatus($id, $status)
    {
        $results = DB::table('tasklist')
            ->select('TasklistID', 'StatusID')
            ->where('TasklistID', '=', $id)
            ->get();
        if (!empty($results)) {
            if ($results[0]->StatusID != $status) {
                if ($status == "1") {
                    DB::table('tasklist')->where('TasklistID', '=', [$id])->update(array('StatusID' => $status));
                    DB::table('assign_user_task')->where('TasklistID', '=', [$id])->update(array('UserTaskStatusID' => "1"));

                } else {
                    DB::table('tasklist')->where('TasklistID', '=', [$id])->update(array('StatusID' => $status));
                }

                $data = "Record Updated Successfully";
            } else
                $data = "Nothing to Update!! Record is Up-to-date";

            $response['data'] = $data;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists for task";
            $response['code'] = 400;
        }
        return $response;
    }

    public function postcomments($input)
    {
        $rules = array(
            'Description' => 'required',
            'User' => 'required|exists:users,id',
            'Task' => 'required|exists:tasklist,TasklistID',
        );

//CommentID, TasklistID, UserID, Description, HasAttachment, CreatedBy, updated_by, created_at, updated_at
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $count = count(Input::file('fileattachment'));
            if ($count == '0')
                $hasattachment = '0';
            else
                $hasattachment = '1';
            // dd(Input::all());
            DB::beginTransaction();
            $comment = Comments::create([
                'TasklistID' => Input::get('Task'),
                'UserID' => Input::get('User'),
                'Description' => Input::get('Description'),
                'HasAttachment' => $hasattachment,
                'CreatedBy' => Input::get('User'),
                'created_at' => Carbon::now()
            ]);
            $commentid = $comment->id;
            //$task_id = "TI".$taskid;
            //DB::table('tasklist')->where('TasklistID', '=', $task->id)->update(array('TaskID' => $task_id));
            //dd($taskid);
            $createdby = DB::table('tasklist')
                ->select('name', 'id', 'AssignedTo', 'tasklist.TaskID', 'tasklist.LocationID')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->where('tasklist.TasklistID', '=', Input::get('Task'))
                ->get();
            $notificationname = DB::table('users')
                ->select('name', 'id', 'AssignedTo')
                ->where('users.id', '=', Input::get('User'))
                ->get();
            if (!empty($createdby)) {
                if (!empty($notificationdata)) {
                    $notificationdata = $notificationname[0]->name . " posted comment on task " . $createdby[0]->TaskID;
                    //NotificationID, Notification, NotificationFor, TasklistID, LocationID, IsRead, Status, CreatedBy, updated_by, created_at, updated_at
                    $notification = Notifications::create([
                        'Notification' => $notificationdata,
                        'NotificationFor' => $createdby[0]->id,
                        'TasklistID' => Input::get('Task'),
                        'LocationID' => $createdby[0]->LocationID,
                        'IsRead' => '0',
                        'Status' => '0',
                        'CreatedBy' => Input::get('createdby'),
                    ]);
                }
            }
            if ($count > '0') {
                $filedata = Input::file('fileattachment');
                $destinationpath = public_path('uploads');
                for ($i = 0; $i < $count; $i++) {

                    if (isset($filedata[$i])) {
                        $filetype[$i]['originalname'] = $filedata[$i]->getClientOriginalName();
                        $filetype[$i]['realpath'] = $filedata[$i]->getRealPath();
                        $filetype[$i]['originalextension'] = $filedata[$i]->getClientOriginalExtension();
                        $filetype[$i]['mimetype'] = $filedata[$i]->getMimeType();
                        $filetype[$i]['filesize'] = $filedata[$i]->getSize();
                        $filetype[$i]['name'] = trim($filetype[$i]['originalname']);
                        $filepath = $filedata[$i]->move($destinationpath, $filetype[$i]['name']);
                        $filetype[$i]['filepath'] = $filepath->getRealPath();
                        $filepath = asset('images/' . $filetype[$i]['name']);
                        //$filepath = $destinationpath."/".$filetype[$i]['originalname'];
                        //$filetype[$i]['filepath'] = $filepath->getRealPath();
                        //dd($filepath);
                        //dd(Storage::url( $filetype[$i]['name']));
                        //dd(  Storage::disk('local')->get($filetype[$i]['name']));

                    } else
                        $filetype[$i] = "null";
                    $fileattachment = Fileattachment::create([
                        'FileType' => $filetype[$i]['mimetype'],
                        'FilePath' => $filetype[$i]['name'],
                        'FileSourceID' => '2',
                        'SourceID' => $commentid,
                        'CreatedBy' => Input::get('createdby'),
                    ]);
                }
            }

            DB::commit();
            $response['data'] = "Comment Posted Successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }

    public function assign_user_task($input)
    {
        $rules = array(
            'taskid' => 'required|exists:tasklist,TasklistID',
            'assignedTo' => 'required|exists:users,id',
            'assignedBy' => 'required|exists:users,id',
        );
//CommentID, TasklistID, UserID, Description, HasAttachment, CreatedBy, updated_by, created_at, updated_at
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $notification = "";
            $count = count(Input::get('fileattachment'));
            if ($count == '0')
                $hasattachment = '0';
            else
                $hasattachment = '1';
            DB::beginTransaction();
            //AssignUserTaskID, UserTaskStatusID, TasklistID, AssignedTo, AssignedBy, CreatedBy, updated_by, created_at, updated_at
            $comment = Assignusertask::create([
                'TasklistID' => Input::get('taskid'),
                'AssignedTo' => Input::get('assignedTo'),
                'AssignedBy' => Input::get('assignedBy'),
                'UserTaskStatusID' => '2',
                'CreatedBy' => Input::get('assignedBy'),
                'created_at' => Carbon::now()
            ]);
            $commentid = $comment->id;
            //$task_id = "TI".$taskid;
            //DB::table('tasklist')->where('TasklistID', '=', $task->id)->update(array('TaskID' => $task_id));
            $createdby = DB::table('tasklist')
                ->select('name', 'id', 'AssignedTo', 'tasklist.TaskID', 'tasklist.LocationID')
                ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
                ->where('tasklist.TasklistID', '=', Input::get('taskid'))
                ->get();
            $notificationname = DB::table('users')
                ->select('name', 'id', 'AssignedTo')
                ->where('users.id', '=', Input::get('assignedTo'))
                ->get();
            if (!empty($notificationname)) {
                $notificationdetails = $notificationname[0]->name;
            }
            $AssignedBy = DB::table('users')
                ->select('name', 'id', 'AssignedTo')
                ->where('users.id', '=', Input::get('assignedBy'))
                ->get();

            if (!empty($createdby)) {
                if (!empty($notificationname)) {
                    $notificationdata = $AssignedBy[0]->name . " has assigned task " . $createdby[0]->TaskID . " to " . $notificationname[0]->name;
                    //NotificationID, Notification, NotificationFor, TasklistID, LocationID, IsRead, Status, CreatedBy, updated_by, created_at, updated_at
                    $notification = Notifications::create([
                        'Notification' => $notificationdata,
                        'NotificationFor' => $createdby[0]->id,
                        'TasklistID' => Input::get('taskid'),
                        'LocationID' => $createdby[0]->LocationID,
                        'IsRead' => '0',
                        'Status' => '0',
                        'CreatedBy' => Input::get('assignedTo'),
                    ]);
                }
            }
            DB::commit();

            $response['data'] = "Task " . $createdby[0]->TaskID . " assigned sucessfully to " . $notificationdetails;
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }

    public function deletetask($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        } else {
            $response['data'] = "Select Task to delete";
            $response['code'] = 400;
            return $response;
        }
        foreach ($ids as $id) {
            $task = DB::table('tasklist')
                ->select('TasklistID', 'TaskID')
                ->where('tasklist.TasklistID', '=', $id)
                ->get();
            if (!empty($task)) {
                DB::table('tasklist')->where('TasklistID', '=', $id)->update(array('IsDeleted' => '1'));
                $response['data'] = "Task Details deleted successfully";
                $response['code'] = 200;
            } else {
                $response['data'] = "Task does not exists";
                $response['code'] = 400;
            }
        }
        return $response;

    }

    public function deletecomment($id)
    {
        $task = DB::table('comments')
            ->select('CommentID', 'TasklistID')
            ->where('comments.CommentID', '=', $id)
            ->get();
        if (!empty($task)) {

            DB::delete('delete from comments where CommentID = ?', [$id]);
            $response['data'] = "Comment deleted successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = "Comment does not exists";
            $response['code'] = 400;
            return $response;
        }
    }
    //--------------------------------------End of Post Route---------------------------------------
    //------------------------------------------End of Task Routes-----------------------------------
    ////---------------------------------Start of Priorities route----------------------------------------------------------
//-------------------------------GET Routes--------------------------------------------------
    public function get_all_priorities()
    {

        $results = DB::table('priorities')
            ->select('PriorityID', 'Priority')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Priority does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_priorities_by_id($id)
    {

        $results = DB::table('priorities')
            ->select('PriorityID', 'Priority')
            ->where('PriorityID', '=', $id)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Priority does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_priorities_by_name($priorities)
    {
        $results = DB::table('priorities')
            ->select('PriorityID', 'Priority')
            ->where('Priority', '=', $priorities)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Priority does not exists ";
            $response['code'] = 400;

        }
        return $response;
    }
    //--------------------------------------------------End of GET Routes-------------------------
    //------------------------------------------------Start of Post Routes----------------------
    //----------------------------------------------End of Post Routes------------------------
    //--------------------------------------End of Priorities Route--------------------------------
    //---------------------------------------Start of Order Routes---------------------------------
    //--------------------------------------Start of GET Routes----------------------------------
    public function get_orderdetails_by_id($orderid)
    {
        $results = $results = DB::table('orders')
            ->select('orders.*', 'order_status.OrderStatus', 'order_type.OrderType', 'orders.OrderTo as OrderToID', 'u2.CustomerName as CustomerName', 'u2.Phoneno as CustomerPhoneno', 'u2.EmailID as CustomerEmailID',
                'priorities.Priority',
                'task_severities.TaskSeverity', 'locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName',
                'u1.FirstName', 'u1.LastName', 'u1.email', 'u1.PhoneNo', 'u1.Address', 'u1.Gender', 'u1.City', 'u1.Country', 'u1.Postalcode')
            ->leftjoin('users as u1', 'u1.id', '=', 'orders.UserID')
            ->leftjoin('customers as u2', 'u2.CustomerID', '=', 'orders.OrderTo')
            ->leftjoin('order_status', 'order_status.OrderStatusID', '=', 'orders.OrderStatusID')
            ->leftjoin('order_type', 'order_type.OrderTypeID', '=', 'orders.OrderTypeID')
            ->leftjoin('locations', 'locations.LocationID', '=', 'orders.LocationID')
            ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'orders.SeverityID')
            ->leftjoin('priorities', 'priorities.PriorityID', '=', 'orders.PriorityID')
            ->where('orders.OrderID', '=', $orderid)
            ->get();
        if (!empty($results)) {
            $orderfrom = DB::select(DB::raw("select order_from.OrderRaisedBy,users.name from
                                            order_from
                                            left join customers On customers.CustomerID = order_from.OrderRaisedBy
                                            left join users   On users.id = customers.UserID
                                            OR   users.id = order_from.OrderRaisedBy
                                            where order_from.OrderID  = $orderid "));
            $results[0]->orderfrom = $orderfrom;
            $results[0]->OrderTo = intval($results[0]->OrderTo);

            $inventory = DB::table('inventory')
                ->select('inventory.InventoryID', 'inventory.InventoryName', 'inventory_type.InventoryTypeID', 'inventory_type.InventoryType', 'inventory.InventorydisplayID', 'inventory.Description',
                    'inventory.Price', 'inventory.MinInventoryCount', 'inventory.MaxInventoryCount', 'inventory.AvaliableInventoryCount',
                    'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('order_inventory', 'order_inventory.InventoryID', '=', 'inventory.InventoryID')
                ->where('order_inventory.OrderID', '=', $orderid)
                ->get();
            $results[0]->inventory = $inventory;

            $task = DB::table('task_order')
                ->select('task_order.*')
                ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'task_order.TasklistID')
                ->where('task_order.OrderID', '=', $orderid)
                ->get();

            $results[0]->task = $task;

            $files = DB::table('file_attachment')
                ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                ->where('file_attachment.SourceID', '=', $orderid)
                ->where('file_attachment.FileSourceID', '=', '4')
                ->get();
            $filecount = count($files);
            if ($filecount != '0')
                $results[0]->filedetails = $files;
            else
                $results[0]->filedetails = [];

            $customer_signature = DB::table('file_attachment')
                ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                ->where('file_attachment.FileAttachmentID', '=', $results[0]->SignaturePad)
                ->get();

            $customer_signature_count = count($customer_signature);

            if ($customer_signature_count != '0')
                $results[0]->customersignature = $customer_signature;
            else
                $results[0]->customersignature = [];


            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public  function get_Parent_Organisation_orders($supervisorid)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $supervisorid)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $supervisorid)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        $response = $this->get_all_order_details($userrole,$orgid,$supervisorid);
        return $response;
    }

    public  function get_Sub_Organisation_Orders_List($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $response = $this->get_all_order_details($userrole,$org_id,$userId);
        return $response;
    }

    public function get_all_order_details($userrole,$orgid,$supervisorid)
    {

        if ($userrole == "1") {
            $results = DB::table('orders')
                ->select('orders.*','organisation.OrganisationName', 'order_status.OrderStatus', 'order_type.OrderType', 'u2.CustomerID as OrderToID', 'u2.CustomerName as OrderToName',
                    'u3.name as OrderFromName', 'u3.id as OrderFromID', 'priorities.Priority',
                    'task_severities.TaskSeverity', 'locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName',
                    'u1.FirstName', 'u1.LastName', 'u1.email', 'u1.PhoneNo', 'u1.Address', 'u1.Gender', 'u1.City', 'u1.Country', 'u1.Postalcode')
                ->leftjoin('users as u1', 'u1.id', '=', 'orders.UserID')
                ->leftjoin('customers as u2', 'u2.CustomerID', '=', 'orders.OrderTo')
                ->leftjoin('users as u3', 'u3.id', '=', 'orders.OrderFrom')
                ->leftjoin('order_status', 'order_status.OrderStatusID', '=', 'orders.OrderStatusID')
                ->leftjoin('order_type', 'order_type.OrderTypeID', '=', 'orders.OrderTypeID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'orders.LocationID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'orders.SeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'orders.PriorityID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'orders.OrganisationID')
                ->where('orders.OrganisationID', '=', $orgid)
                ->where('orders.IsDeleted', '=', '0')
                ->get();
        }
        if ($userrole == "3" || $userrole == "4" || $userrole == "5") {
            $results = DB::table('orders')
                ->select('orders.*','organisation.OrganisationName', 'order_status.OrderStatus', 'order_type.OrderType', 'u2.CustomerID as OrderToID', 'u2.CustomerName as OrderToName',
                    'u3.name as OrderFromName', 'u3.id as OrderFromID', 'priorities.Priority',
                    'task_severities.TaskSeverity', 'locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName',
                    'u1.FirstName', 'u1.LastName', 'u1.email', 'u1.PhoneNo', 'u1.Address', 'u1.Gender', 'u1.City', 'u1.Country', 'u1.Postalcode')
                ->leftjoin('users as u1', 'u1.id', '=', 'orders.UserID')
                ->leftjoin('customers as u2', 'u2.CustomerID', '=', 'orders.OrderTo')
                ->leftjoin('users as u3', 'u3.id', '=', 'orders.OrderFrom')
                ->leftjoin('order_status', 'order_status.OrderStatusID', '=', 'orders.OrderStatusID')
                ->leftjoin('order_type', 'order_type.OrderTypeID', '=', 'orders.OrderTypeID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'orders.LocationID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'orders.SeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'orders.PriorityID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'orders.OrganisationID')
                ->where('orders.CreatedBy', '=', $supervisorid)
                ->where('orders.IsDeleted', '=', '0')
                ->get();
        }
        if ($userrole == "2") {
            $results = DB::table('orders')
                ->select('orders.*','organisation.OrganisationName', 'order_status.OrderStatus', 'order_type.OrderType', 'u2.CustomerID as OrderToID', 'u2.CustomerName as OrderToName',
                    'u3.name as OrderFromName', 'u3.id as OrderFromID', 'priorities.Priority',
                    'task_severities.TaskSeverity', 'locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName',
                    'u1.FirstName', 'u1.LastName', 'u1.email', 'u1.PhoneNo', 'u1.Address', 'u1.Gender', 'u1.City', 'u1.Country', 'u1.Postalcode')
                ->leftjoin('users as u1', 'u1.id', '=', 'orders.UserID')
                ->leftjoin('customers as u2', 'u2.CustomerID', '=', 'orders.OrderTo')
                ->leftjoin('users as u3', 'u3.id', '=', 'orders.OrderFrom')
                ->leftjoin('order_status', 'order_status.OrderStatusID', '=', 'orders.OrderStatusID')
                ->leftjoin('order_type', 'order_type.OrderTypeID', '=', 'orders.OrderTypeID')
                ->leftjoin('locations', 'locations.LocationID', '=', 'orders.LocationID')
                ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'orders.SeverityID')
                ->leftjoin('priorities', 'priorities.PriorityID', '=', 'orders.PriorityID')
                ->leftjoin('users as u4', 'u4.id', '=', 'orders.CreatedBy')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'orders.OrganisationID')
                ->where('orders.IsDeleted', '=', '0')
                ->where(function ($query) {
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('orders.CreatedBy', '=', $userId)
                        ->orwhere('u4.AssignedTo', '=', $userId);
                })->get();
        }

        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                $inventory = DB::table('inventory')
                    ->select('inventory.InventoryID', 'inventory.InventoryName', 'inventory.InventorydisplayID', 'inventory.Description',
                        'inventory.Price', 'inventory.MinInventoryCount', 'inventory.MaxInventoryCount', 'inventory.AvaliableInventoryCount', 'inventory.CreatedBy',
                        'users.FirstName', 'users.LastName', 'users.email')
                    ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                    ->leftjoin('order_inventory', 'order_inventory.InventoryID', '=', 'inventory.InventoryID')
                    ->where('order_inventory.OrderID', '=', $results[$i]->OrderID)
                    ->get();
                $results[$i]->inventory = $inventory;

                $task = DB::table('task_order')
                    ->select('task_order.*')
                    ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'task_order.TasklistID')
                    ->where('task_order.OrderID', '=', $results[$i]->OrderID)
                    ->get();

                $results[$i]->task = $task;

                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->OrderID)
                    ->where('file_attachment.FileSourceID', '=', '4')
                    ->get();
                $filecount = count($files);
                if ($filecount != '0')
                    $results[$i]->filedetails = $files;
                else
                    $results[$i]->filedetails = [];

                $customer_signature = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.FileAttachmentID', '=', $results[$i]->SignaturePad)
                    //->where('file_attachment.FileSourceID', '=', '5')
                    ->get();

                $customer_signature_count = count($customer_signature);
                if ($customer_signature_count != '0')
                    $results[$i]->customersignature = $customer_signature;
                else
                    $results[$i]->customersignature = [];

            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_orderdetails_by_task($taskid)
    {
        //OrderID, WorkOrderID, UserID, LocationID, WorkTypeID, InventoryID, InvoiceID, OrderStatusID, NoOfInventory, CreatedBy, updated_by, created_at, updated_at
        //InventoryID, InventoryName, InventorydisplayID, Description, Price, MinInventoryCount, MaxInventoryCount, AvaliableInventoryCount, CreatedBy, updated_by, created_at, updated_at
        //InvoiceID, Rate, Amount, Description, ToCompany, ForCompany, BillTo, ShipTo, CreatedBy, updated_by, created_at, updated_at
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        //OrderStatusID, OrderStatus, CreatedBy, updated_by, created_at, updated_at
        //WorkTypeID, WorkType, CreatedBy, updated_by, created_at, updated_at
        //CompanyDetailsID, CompanyName, Address, Phoneno, EmailID, CreatedBy, updated_by, created_at, updated_at
        //UserID, UserRoleID, UserName, Password, FirstName, LastName, EmailID, PhoneNo, Gender, Address, City, Country, Postalcode
        $results = DB::table('orders')
            ->select('orders.OrderID', 'orders.WorkOrderID', 'orders.NoOfInventory', 'orders.created_at',
                'order_status.OrderStatus', 'locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName',
                'invoices.Rate', 'invoices.Amount', 'invoices.Description', 'tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.Description',
                'users.FirstName', 'users.LastName', 'users.email',
                'users.PhoneNo', 'users.Address', 'users.Gender', 'users.City', 'users.Country', 'users.Postalcode')
            ->leftjoin('users', 'users.id', '=', 'orders.UserID')
            ->leftjoin('order_status', 'order_status.OrderStatusID', '=', 'orders.OrderStatusID')
            ->leftjoin('locations', 'locations.LocationID', '=', 'orders.LocationID')
            ->leftjoin('task_order', 'task_order.OrderID', '=', 'orders.OrderID')
            ->leftjoin('tasklist', 'tasklist.TasklistID', '=', 'task_order.TasklistID')
            ->leftjoin('invoices', 'invoices.InvoiceID', '=', 'orders.InvoiceID')
            //->leftjoin('inventory','inventory.InventoryID','=','orders.InventoryID')
            ->where('task_order.TasklistID', '=', $taskid)
            //->leftjoin('companydetails','companydetails.CompanyDetailsID','orders.UserID')
            //->leftjoin('inventory')
            ->get();
        if (!empty($results)) {
            //dd($results);
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                //InventoryID, InventoryName, InventorydisplayID, Description, Price, MinInventoryCount, MaxInventoryCount, AvaliableInventoryCount, CreatedBy, updated_by, created_at, updated_at
                $inventory = DB::table('inventory')
                    ->select('inventory.InventoryID', 'inventory.InventoryName', 'inventory.InventorydisplayID', 'inventory.Description',
                        'inventory.Price', 'inventory.MinInventoryCount', 'inventory.MaxInventoryCount', 'inventory.AvaliableInventoryCount', 'inventory.CreatedBy',
                        'users.FirstName', 'users.LastName', 'users.email')
                    ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                    ->leftjoin('order_inventory', 'order_inventory.InventoryID', '=', 'inventory.InventoryID')
                    ->where('order_inventory.OrderID', '=', $results[$i]->OrderID)
                    //->leftjoin('companydetails','companydetails.CompanyDetailsID','orders.UserID')
                    //->leftjoin('inventory')
                    ->get();
                $results[$i]->inventory = $inventory;
            }


            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order  does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_ordertypes()
    {
        $results = DB::table('order_type')
            ->select('OrderTypeID', 'OrderType')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order Type does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_orderstatus()
    {
        $results = DB::table('order_status')
            ->select('OrderStatusID', 'OrderStatus')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order status does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    //--------------------------------------End of GET ROUTES-----------------------
    //------------------------------------------------Start of Post Routes----------------------
    public function createorder($input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $orgid = $org_id[0];
        $rules = array(
            'ordername' => 'required',
            /*'location' => 'required|exists:locations,LocationID',*/
        );
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $order_repeat_date = '';
            $duration = '';
            $EstimatedShippingTime = Input::get('estimatedshippingtime');
            $OrderShipDate = Input::get('Ordershipdate');

            if ((isset($EstimatedShippingTime) && $EstimatedShippingTime != 'undefined') && ($EstimatedShippingTime != 'null' && $EstimatedShippingTime != null))
                $EstimatedShippingTime = date_format(date_create($EstimatedShippingTime), "Y-m-d");
            else
                $EstimatedShippingTime = "";

            if ((isset($OrderShipDate) && $OrderShipDate != 'undefined') && ($OrderShipDate != 'null' && $OrderShipDate != null))
                $OrderShipDate = date_format(date_create($OrderShipDate), "Y-m-d");
            else
                $OrderShipDate = "";

            $inventorycount = count(Input::get('inventory'));
            $file_count = count(Input::file('fileattachment'));
            if ($file_count == '0') $hasattachment = '0';
            else   $hasattachment = '1';

            $order_list_count = DB::table('orders')
                ->select('*')
                ->count();
            if ($order_list_count == "0")
                $orderlistid = "7000001";
            else
                $orderlistid = '';
            if (Input::get('ordertypeid') == '6')
                $isinternal = '1';
            else
                $isinternal = '0';
            $cycle_value = Input::get('cycle_value');
            $cycle_type = Input::get('cycle_type');
            if (!empty($cycle_type) && !empty($cycle_value)) {
                if ($cycle_type == 'Days') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " day";
                    else
                        $duration = " +" . $cycle_value . " days";


                }
                if ($cycle_type == 'Weeks') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " week";
                    else
                        $duration = " +" . $cycle_value . " weeks";
                }
                if ($cycle_type == 'Months') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " month";
                    else
                        $duration = " +" . $cycle_value . " months";
                }


                $date = strtotime(date('Y-m-d'));
                //echo "duration:- ".$duration;
                $order_repeat_date = date('Y-m-d', strtotime($duration, $date));
                //dd($order_repeat_date);
            }

            DB::beginTransaction();
            $order = Order::create([
                'OrderID' => $orderlistid,
                'OrderName' => Input::get('ordername'),
                'UserID' => $userId,
                'OrderDescription' => Input::get('orderdescription'),
                'OrderTypeID' => Input::get('ordertypeid'),
                'OrderFrom' => Input::get('orderfrom'),
                'OrderTo' => Input::get('customerid'),
                'HasAttachment' => $hasattachment,
                'SeverityID' => Input::get('severity'),
                'PriorityID' => Input::get('priority'),
                'EstimatedShippingTime' => $EstimatedShippingTime,
                'OrderShipDate' => $OrderShipDate,
                'OrderRepeatDate' => $order_repeat_date,
                'OrganisationID' => $orgid,
                'JobID' => 1,
                'LocationID' => Input::get('location'),
                'BillToAddress' => Input::get('billtoaddress'),
                'ShipToAddress' => Input::get('shiptoaddress'),
                'OrderStatusID' => Input::get('orderstatusid'),
                'OrderValueTotal' => Input::get('ordervalue'),
                'OrderTaxTotal' => Input::get('ordertax'),
                'Disclaimer' => Input::get('disclaimer'),
                'ShipmentTrackingLink' => Input::get('shipmentlink'),
                'NoOfInventory' => $inventorycount,
                'CreatedBy' => $userId,
                'IsDeleted' => '0',
                'IsInternal' => $isinternal,
                'created_at' => Carbon::now(),
                'cycle_type' => Input::get('cycle_type'),
                'cycle_value' => Input::get('cycle_value'),
                'units' => Input::get('units'),
            ]);
            $orderid = $order->OrderID;
            $order_id = "WOI" . $orderid;
            $sourceid = $orderid;
            $customerid = Input::get('customerid');
            DB::table('orders')->where('OrderID', '=', $order->OrderID)->update(array('WorkOrderID' => $order_id));
            //if($order->OrderTypeID == '2' || $order->OrderTypeID == '3')
            //{
            $organisation_name = DB::table('organisation')->where('OrganisationID', $order->OrganisationID)->pluck('OrganisationName');
            $customer_name = DB::table('customers')->select('CustomerName', 'EmailID')->where('CustomerID', $order->OrderTo)->get();
            $ordertype = DB::table('order_type')->where('OrderTypeID', $order->OrderTypeID)->pluck('OrderType');
            $signaturedata = Input::get('signature');
            if (!empty($signaturedata)) {
                if ($signaturedata != 'undefined') {
                    //$signaturedata =Input::get('signature');//"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAA8CAYAAADPLpCHAAAHXklEQVR4Xu2dR+glRRDGf6sYwYgXD2IEc0IUD2ZBjKiIgVVQD+KK6KooejJcxAQmUAQjGFFQMYGKWVARjCiK+SAK5pxQ+WBG2qbnzUxPz3s9M9WXXf6vu6br6+/1q66uqlmCNUNgRAgsWZAuGwFbAWsAXwBrA38Wc9Hffiz+/znw+oLmaI8dIALzJrSIew+wXwusROqngeNajLGuE0VgnoS+A1jaAef3gC06jLehE0BgXoR+C9gmAZ4XARcmkGMiRorAPAh9FbA8EX7fAeskkmViRohA34TeDnijArcHgbuAL4tD4R+BQ+FtwHre+POBawGR25oh8D8E+ib09cAyD/OzgZsaElLekI8r1uwF4LQEXpDDgLUAfcHsSzLwL0ifhN4BeBlY2cHoOWDPlpidAVxZMUa7v54T204AbikGHw48ECvIxuWBQJ+EfgrY21HzB2B74JMI1Q8GLgO29Mb+DawYIa8cYoTuAF6OQ/sitH7G73cU1qXJxQk8FAcCj3hA3gqcGWkuyC++V2HDS461gSPQF6G1C2/oYLNjAltX4qps6neArQe+Fjb9BAj0QWj3Z1xTvBqQHZyqyc49NCBM9vquqR5icoaJQB+EVuyFbOWybRxpN89C1H9Gn88a5spOdNapCe3bzlW7s/rpwKhd9c4I7GX7vlgEOLnDDwIejZBnQ0aCQGpCi6C71OzOvkkSc/tXtUMboUdCzFg1UhJau+a3zkQUIbdPYGLXFBci7kcnAk29DP4XwpXTh3kTi62NWwACKQmtK2kFD5XtCuCcgE7HArd7f9ctncyQJu0zYINAxzYymjzH+gwQgRSElh9XN3n+jd2smzeZGbpuLttPRbB/HYSXAOd6nSRLz3qmbrB9Pn4EuhJaZoZ8wOt7UH1a+IyrEPQj8H4FVp8B9zGAzJJQYsAhwMMJlkq/EP8UMR0JxJmIRSDQldC+V0M6fAUcWbNj6oLkXWBVR+kq+7fK76yhqXzcPztfqJhD6iLWzp4ZQKArof2dVmGdpzdEuskFjMwZHS5D7W1g24bPmtUtdEi1QKUEwC5CRBdCK2DoXm+XbXPF7XtFdF2uXdpt8nwcHwBGvmsdLlO0EKEtMyYFsguQEUvoENHq7OaQer4c3TC+6XT0P/8IOCJRXEj5mP2Bx7zJyTsjL421gSEQQ+hQStVvwCktfMklTP4ufSlwnoOhTA6RWoFOin2WzV4XfipPyKnAX4WNXZeDGMqqOar49RnYctp0Ywjtu9yE4knAjZFwXg4oi6VsewDPe7JE/CbZJL7N3cQd6H+p9OgnWpZaiFTdhqVGoC2hQ16NrlkjIpRqb6xWKKc8w9hyB76JojhsN2OmCj/fk/K7dzZIjbvJ6wmBFIRO4RFwYzOeLYLuY1T2fz2ahpTuDig9rGzyR68QMwEbs1gE2hLaD7BvugPWafkqsFPR6X1g87oBgc9Dwf9tYkREYre1xSZiyjYkNQIxi+bupil2Z+n0AbBpodyHwGYRiu4WsL218yo7vElzCW07dBPEMuwTQ2jXy9FmB5yl/n2FO059mpoJvrzQDt00+s4/GIrQmzTwqGS4pNOeUgyh3bICqXZo9zAXe/Uc8laoylIT70hobKpr9WkzbM7axxC6j9R/+YovcHSfNS+RT1feKrsr74j80iJtyORQTMkvRb+6sry+h0QytcM3+ULMednscVUIxBC6j9R/3x0ocirFym83F1F3MStalxmu8NfXPMG6oLku5mE2ZjEIxBC6r5mqtt1KhXC50JTZ7e6OqsehuhxdWlUWTSnTvwVNGTPSZd42tiECORFaya0HOPN+BTi5iNuYVeOuoar/das7KMoPrttKNbOj26K74P45EVrxGqoz55YPU/VR2exPAvsmwqoukVYmVRn/oX/Nhk4E/DzE5ERo6SsyPQ7sXCiv3VIhqWt6YCjwSCaJeygMJc/qIOinhp01o/jjPDC3Z/SIQG6ElqqycxVkpPYNsG5Afz8qT11CZolePiRviNtS+c57XBYTHYtAjoR2bw1Deil/UHmEoVaVEW6EjmXIwMblSGj31tCHUyV53Wxx//NQNKDfp+lly8CW0qYrBHIktGzel4BVnCVSrqKuxPUmrbqmcgZVRdVvCLxRoE6efT4gBHIktOD72rOd2+6qoVJhXeO2B7Ss051qroSWF6OszB8b+aadvnQB6qBZd/U9XRaMSPMcCd0lam5ES2OqxCCQI6FDr52ouwyJ0d3GjBCBHAl9NHC3h7VKgekd4dYMgZkI5EjoULUk2cJWjNHIXIuAEboWIuswJARyJLQdCofEoMzmmiOhBZEbG50qszwz6G06fSCQI6H9dCyVGSuL0PSBgckcEQK5EVpkXl6EkZYwNynnNaIlMVW6IJAToVWe96GAMnrtsVKjrBkCtQjkRGj/dcrfF0Suqx5aq6R1mA4CuRDaD/tUscRlEeV5p7NypmkQgVwJnaqAjS37xBDIhdCCXXayIuQUFZfyZfcTW9Jpq5sToae9EqZ9EgSM0ElgNCG5IPAvARM3TGeJKssAAAAASUVORK5CYII=";

                    $fileSourceId = Filesource::where('FileSource', '=', 'Signature')->pluck('FileSourceID');
                    $destinationpath = public_path('images');
                    $output_file = $destinationpath . '/' . $orderid . "_" . time() . '.png';
                    $image = file_put_contents($output_file, base64_decode(explode(',', $signaturedata)[1]));
                    if (isset($signaturedata)) {
                        $filepath = asset('images/' . $orderid . "_" . time() . '.png');
                        $fileattachment = Fileattachment::create([
                            'FileType' => 'image/png',
                            'FilePath' => $filepath,//$filetype[$i]['name'],
                            'FileSourceID' => $fileSourceId,
                            'SourceID' => $orderid,
                            'CreatedBy' => $userId,
                        ]);
                        $filetype = $fileattachment->id;
                    } else
                        $filetype = null;

                    DB::table('orders')->where('OrderID', '=', $order->OrderID)->update(array('SignaturePad' => $filetype));
                }
            }

            If (!empty($customer_name)) {
                $firstname = $customer_name[0]->CustomerName;
                $x = $customer_name[0]->EmailID;
                $var = "<h1>Dear, " . $firstname . "!</h1>" .
                    "<p>" . $ordertype[0] . " Order Confirmation Letter</p>
                   <p>This is a confirmation that your order " . $orderid . " has been successfully placed and is currently under process.</p><p></p>
                   <p>" . $organisation_name[0] . " values your business and is continuously looking for ways to better satisfy their customers. Also Please Acknowledge receipt of this Order.</p>
                   <p>Best Regards,</p>
                   <p>" . $organisation_name[0] . "</p>";

                $emails_table_input = array(
                    'Name' => $firstname,
                    'EmailID' => $x,
                    'Subject' => $ordertype[0] . " Order Confirmation Letter",
                    'Message' => $var,
                    'Status' => 1,
                    'HasAttachment' => 0,
                    'UserID' => $order->OrderTo,
                    'MailFor' => 'order_confirmation',
                    'updated_by' => Input::get('createdby'),
                    'updated_at' => Carbon::now(),
                    'CreatedBy' => $userId,
                    'created_at' => Carbon::now()
                );
            }
            //}
            $task_array = Input::get('taskid');
            $task_count = count($task_array);
            for ($i = 0; $i < $task_count; $i++) {
                $task_details = DB::table('task_order')
                    ->select('OrderID', 'TasklistID', 'TaskOrderID')
                    ->where('TasklistID', '=', $task_array[$i]['id'])
                    ->where('OrderID', '=', $orderid)
                    ->get();
                if (empty($task_details)) {
                    $order = TaskOrder::create([
                        'OrderID' => $orderid,
                        'TasklistID' => $task_array[$i]['id'],
                        'OrderStatusID' => Input::get('orderstatusid'),
                        'CreatedBy' => $userId,
                        'created_at' => Carbon::now()
                    ]);

                }


            }
//                $orderfrom_array = Input::get('orderfrom');
//                $orderfrom_count = count($orderfrom_array);
//                if($orderfrom_count!= "0")
//                {
//                    for($i=0;$i<$orderfrom_count;$i++)
//                    {
//                        if($userId == $orderfrom_array[$i]['id'])
//                            $userroleid =  $user_role[0];
//                        else
//                            $userroleid = '6';
//
//                        $orderfrom = OrderFrom::create([
//                        'OrderID' => $orderid,
//                        'OrderRaisedBy' => $orderfrom_array[$i]['id'],
//                        'UserRoleID' => $userroleid,
//                        'CreatedBy' => $userId,
//                        'created_at' => Carbon::now()
//                    ]);
//                  }
//                }
            $inventoryarray = Input::get('inventory');
            if ($inventorycount != '0') {
                $sum = 0;
                //echo "Inventory count:-".$inventory_count;
                for ($i = 0; $i < $inventorycount; $i++) {
                    //echo "Inventory count:-".$inventorycount."---->i".$i;
                    $inventorydetails = DB::table('inventory')->select('AvaliableInventoryCount', 'Price')->where('InventoryID', $inventoryarray[$i]['id'])->get();
                    if (!empty($inventorydetails)) {
                        $avaliable_inventory_count = $inventorydetails[0]->AvaliableInventoryCount;
                        $inventory_price = $inventorydetails[0]->Price;
                        $avaliable_inventory = $avaliable_inventory_count - $inventoryarray[$i]['no'];
                        if ($avaliable_inventory < 0)
                            $avaliable_inventory = '0';
                        DB::table('inventory')->where('InventoryID', '=', $inventoryarray[$i]['id'])->update(array('AvaliableInventoryCount' => $avaliable_inventory));

                        $orderinventory = OrderInventory::create([
                            'OrderID' => $orderid,
                            'InventoryID' => $inventoryarray[$i]['id'],
                            'NoOfInventory' => $inventoryarray[$i]['no'],
                            'CreatedBy' => $userId,
                            'created_at' => Carbon::now()
                        ]);
                        $amount = $inventoryarray[$i]['no'] * $inventory_price;
                        $sum = $sum + $amount;

                        $orderinventory = InvoiceInventory::create([
                            'OrderID' => $orderid,
                            'InventoryID' => $inventoryarray[$i]['id'],
                            'NoOfInventory' => $inventoryarray[$i]['no'],
                            'InventoryPrice' => $amount,
                            'CreatedBy' => $userId,
                            'created_at' => Carbon::now()
                        ]);

                        $userinventorydetails = DB::table('user_inventory')
                            ->select('UserInventoryID', 'UserID', 'InventoryID', 'NoOfInventory', 'IsReturned', 'ReturnedInventoryCount', 'AvaliableInventoryCount')
                            ->where('UserID', '=', Input::get('requesterid'))
                            ->where('InventoryID', '=', $inventoryarray[$i]['id'])
                            ->get();
                        if (!empty($userinventorydetails)) {
                            $avaliable_count = intval($userinventorydetails[0]->AvaliableInventoryCount) + $inventoryarray[$i]['no'];
                            $inventory_count = intval($userinventorydetails[0]->NoOfInventory) + $inventoryarray[$i]['no'];
                            DB::table('user_inventory')->where('UserInventoryID', '=', [$userinventorydetails[0]->UserInventoryID])->update(array('NoOfInventory' => $inventory_count, 'AvaliableInventoryCount' => $avaliable_count, 'updated_by' => Input::get('createdby'), 'updated_at' => Carbon::now()));
                        } else {
                            $userinventory = UserInventory::create([
                                'UserID' => Input::get('requesterid'),
                                'InventoryID' => $inventoryarray[$i]['id'],
                                'NoOfInventory' => $inventoryarray[$i]['no'],
                                'IsReturned' => '0',
                                'ReturnedInventoryCount' => '0',
                                'AvaliableInventoryCount' => $inventoryarray[$i]['no'],
                                'CreatedBy' => $userId,
                                'created_at' => Carbon::now()
                            ]);
                        }

                    }

                }
            }
            if ($file_count != 0) {
                $filetype = array();
                $filedata = Input::file('fileattachment');
                //dd($filedata);
                $filesourceid = '4';
                $fileattachment = \App\Helpers\Helpers::fileattachment($filedata, $orderid, $userId, $filesourceid);
            }
            $notification = \App\Helpers\Helpers::create_notification($userId, $orderid, $orgid, 'order');
            $transactionsourceid = '1';
            $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);
            DB::commit();
            if (!empty($emails_table_input)) {

                $email = Sendmails::create($emails_table_input);
                $user_details = User::where('id', '=', $userId)->select('email','name')->get();
                $emails_table_input['SenderID'] = $user_details[0]['email'];
                $emails_table_input['SenderName'] = $user_details[0]['name'];

                Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
                    $message->setFrom($emails_table_input['SenderID'], $emails_table_input['SenderName']);
                    $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
                        ->subject($emails_table_input['Subject']);
                });
            }
            $response['data'] = "Order " . $orderid . " raised Sucessfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
            return $response;
        }
    }

    public function updateOrder($id, array $input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $orgid = $org_id[0];
        $signature_value = Input::get('signature');
        $inventory_details = Input::get('inventory');
        $task_details = Input::get('taskid');
        $fileattachment_details = Input::file('fileattachment');
        $delete_file_details = Input::get('deletefile');
        if (isset($input['deletefile'])) unset($input['deletefile']);
        if (isset($input['fileattachment'])) unset($input['fileattachment']);
        if (isset($input['inventory'])) unset($input['inventory']);
        if (isset($input['taskid'])) unset($input['taskid']);
        if (empty($signature_value)) {
            unset($input['signature']);
            unset($input['SignaturePad']);
        } else
            unset($input['signature']);
        $order = $this->findNoFailOrder($id);
        if (!empty($order)) {
            //--------------Logic to update Order Table------------------
            $EstimatedShippingTime = Input::get('estimatedshippingtime');

            if ((isset($EstimatedShippingTime) && $EstimatedShippingTime != 'undefined') && ($EstimatedShippingTime != 'null' && $EstimatedShippingTime != null)) {
                $EstimatedShippingTime = date_format(date_create($EstimatedShippingTime), "Y-m-d");
            } else {
                $EstimatedShippingTime = "";
            }

            $cycle_value = Input::get('cycle_value');
            $cycle_type = Input::get('cycle_type');
            if (!empty($cycle_type) && !empty($cycle_value)) {
                if ($cycle_type == 'Days') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " day";
                    else
                        $duration = " +" . $cycle_value . " days";


                }
                if ($cycle_type == 'Weeks') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " week";
                    else
                        $duration = " +" . $cycle_value . " weeks";
                }
                if ($cycle_type == 'Months') {
                    if ($cycle_value == "1")
                        $duration = " +" . $cycle_value . " month";
                    else
                        $duration = " +" . $cycle_value . " months";
                }


                $date = strtotime(date('Y-m-d'));
                //echo "duration:- ".$duration;
               $input['OrderRepeatDate'] = date('Y-m-d', strtotime($duration, $date));
                //dd($order_repeat_date);
            }

            $order->fill($input);
            $order->save();
            $orderid = $order->OrderID;
            //-----------End of Update order Table logic -----------
            //-------------Logic to update order inventory details---------------------------------
            if (!empty($inventory_details)) {

                $database_inventory = array();

                $inventory_array = Input::get('inventory');
                $inventorycount = count($inventory_array);
                for ($i = 0; $i < $inventorycount; $i++)
                    $update_inventory_array[$i] = $inventory_array[$i]['id'];
                $inventory_db = DB::table('order_inventory')->where('OrderID', '=', $order->OrderID)->select('InventoryID')->get();
                $database_inventory_count = count($inventory_db);

                for ($i = 0; $i < $database_inventory_count; $i++)
                    $database_inventory[$i] = $inventory_db[$i]->InventoryID;

                $k = 0;
                $delete_inventory_array = array();
                for ($i = 0; $i < $database_inventory_count; $i++) {
                    if (!in_array($database_inventory[$i], $update_inventory_array)) {
                        $delete_inventory_array[$k] = $database_inventory[$i];
                        $inventory_id = $database_inventory[$i];
                        $k++;
                        DB::select(DB::raw("DELETE FROM `order_inventory` WHERE  InventoryID = $inventory_id AND OrderID = $orderid ;"));
                    } else {
                        DB::table('order_inventory')->where('OrderID', '=', $order->OrderID)
                            ->where('InventoryID', '=', $database_inventory[$i])
                            ->update(array('NoOfInventory' => $inventory_array[$i]['no']));
                    }
                }
                $j = 0;
                for ($i = 0; $i < $inventorycount; $i++) {
                    if (!in_array($update_inventory_array[$i], $database_inventory)) {
                        $update_array[$j] = $update_inventory_array[$i];
                        $j++;
                        $order = OrderInventory::create([
                            'OrderID' => $orderid,
                            'InventoryID' => $update_inventory_array[$i],
                            'NoOfInventory' => $inventory_array[$i]['no'],
                            'CreatedBy' => $userId,
                            'created_at' => Carbon::now()
                        ]);
                    }
                }
            }
            //-----------End of update order inventory Table logic -----------
            //-------------Logic to update order task details---------------------------------

            if (!empty($task_details)) {
                $database_tasklist = array();
                $update_task_array = array();
                $delete_task_array = array();
                //$task_db = array();
                $update_array = array();
                $delete_task_array = array();

                $task_array = Input::get('taskid');
                $taskcount = count($task_array);
                for ($i = 0; $i < $taskcount; $i++)
                    $update_task_array[$i] = $task_array[$i]['id'];
                $task_db = DB::table('task_order')->where('OrderID', '=', $order->OrderID)->select('TasklistID')->get();
                $database_tasklist_count = count($task_db);

                for ($i = 0; $i < $database_tasklist_count; $i++)
                    $database_tasklist[$i] = $task_db[$i]->TasklistID;
                $k = 0;

                for ($i = 0; $i < $database_tasklist_count; $i++) {
                    if (!in_array($database_tasklist[$i], $update_task_array)) {
                        $delete_task_array[$k] = $database_tasklist[$i];
                        $task_id = $database_tasklist[$i];
                        $k++;
                        DB::select(DB::raw("DELETE FROM `task_order` WHERE  TaskListID = $task_id AND OrderID = $orderid ;"));
                    }
                }
                $j = 0;
                for ($i = 0; $i < $taskcount; $i++) {
                    if (!in_array($update_task_array[$i], $database_tasklist)) {
                        $update_array[$j] = $update_task_array[$i];
                        $j++;
                        $order = TaskOrder::create([
                            'OrderID' => $orderid,
                            'TasklistID' => $update_task_array[$i],
                            'OrderStatusID' => $order->OrderStatusID,
                            'CreatedBy' => $userId,
                            'created_at' => Carbon::now()
                        ]);
                    }
                }
            }
            //-----------End of update order task Table logic -----------
            //-------------Logic to update signature details---------------------------------
            if (!empty($signature_value) || $signature_value != 'undefined') {
                //if($order->OrderTypeID == '2' || $order->OrderTypeID == '3')
                //{
                $organisation_name = DB::table('organisation')->where('OrganisationID', $order->OrganisationID)->pluck('OrganisationName');
                $customer_name = DB::table('customers')->select('CustomerName', 'EmailID')->where('CustomerID', $order->OrderTo)->get();
                $ordertype = DB::table('order_type')->where('OrderTypeID', $order->OrderTypeID)->pluck('OrderType');
                $signature_count = count($signature_value);
                if ($signature_count > 0) {
                    $signaturedata = $signature_value;
                    $destinationpath = public_path('images');
                    $i = 0;
                    if (isset($signaturedata)) {
                        $destinationpath = public_path('images');
                        $output_file = $destinationpath . '/' . $orderid . "_" . time() . '.png';
                        $image = file_put_contents($output_file, base64_decode(explode(',', $signaturedata)[1]));
                        $filepath = asset('images/' . $orderid . "_" . time() . '.png');
                        if (!empty($order->SignaturePad)) {

                            $fileattachment = DB::table('file_attachment')->where('FileAttachmentID', '=', $order->SignaturePad)->update(array(
                                'FileType' => 'image/png',
                                'FilePath' => $filepath,//$filetype[$i]['name'],
                                'FileSourceID' => '5',
                                'SourceID' => $orderid,
                                'CreatedBy' => $userId,
                            ));
                        } else {
                            $fileattachment = Fileattachment::create([
                                'FileType' => 'image/png',
                                'FilePath' => $filepath,//$filetype[$i]['name'],
                                'FileSourceID' => '5',
                                'SourceID' => $orderid,
                                'CreatedBy' => $userId,
                            ]);
                            DB::table('orders')->where('OrderID', '=', $order->OrderID)->update(array('SignaturePad' => $fileattachment->id));
                        }
                    }
                }
//                if(!empty($customer_name))
//                {
//                    $firstname = $customer_name[0]->CustomerName;
//                    $x = $customer_name[0]->EmailID;
//                    $var = "<h1>Dear, " .$firstname. "!</h1>" .
//                        "<p>".$ordertype[0]." Order Confirmation Letter</p>
//                   <p>This is a confirmation that your order ".$orderid." has been successfully placed and is currently under process.</p><p></p>
//                   <p>".$organisation_name[0]." values your business and is continuously looking for ways to better satisfy their customers. Also Please Acknowledge receipt of this Order.</p>
//                   <p>Best Regards,</p>
//                   <p>".$organisation_name[0]."</p>";
//
//                    $emails_table_input = array(
//                        'Name' => $firstname,
//                        'EmailID' => $x,
//                        'Subject' => $ordertype[0]." Order Confirmation Letter",
//                        'Message' => $var,
//                        'Status' => 1,
//                        'HasAttachment'=>0,
//                        'UserID' => $order->OrderTo,
//                        'MailFor' => 'order_confirmation',
//                        'updated_by'=>Input::get('createdby'),
//                        'updated_at'=> Carbon::now(),
//                        'CreatedBy'=>Input::get('createdby'),
//                        'created_at'=> Carbon::now()
//                    );
//                    $email = Sendmails::create($emails_table_input);
//                    Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
//                        $message->to( $emails_table_input['EmailID'], $emails_table_input['Name'])
//                            ->subject($emails_table_input['Subject']);
//                    });
//
//                }

                //}
            }
            //-----------End of update signature logic -----------
            if (!empty($fileattachment_details)) {
                $filesourceid = '4';
                $file_details = \App\Helpers\Helpers::fileattachment($fileattachment_details, $orderid, $userId, $filesourceid);
            }

            if (!empty($delete_file_details))
                $file = \App\Helpers\Helpers::delete_file($delete_file_details);


            $data = "Order Updated Successfully";
            $response['data'] = $data;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order does not exists";
            $response['code'] = 400;
        }

        return $response;
    }

    public function findNoFailOrder($id)
    {
        return Order::find($id);
    }

    public function deleteorder($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        } else {
            $response['data'] = "Select order to delete";
            $response['code'] = 400;
            return $response;
        }
        foreach ($ids as $id) {
            $order = $this->findNoFailOrder($id);
            if (!empty($order)) {
                DB::table('orders')->where('OrderID', '=', $id)->update(array('IsDeleted' => '1'));
                $response['data'] = "Order Details deleted successfully";
                $response['code'] = 200;
            } else {
                $response['data'] = "Order does not exists";
                $response['code'] = 400;

            }
        }
        return $response;
    }
    //----------------------------------------------End of Post Routes------------------------
    //--------------------------------------End of Order Route--------------------------------
    //---------------------------------------Start of Inventory Routes---------------------------------
    //--------------------------------------Start of GET Routes----------------------------------
    public function get_all_inventorytypes()
    {
        //InventoryID, InventoryName, InventorydisplayID, Description, Price, MinInventoryCount, MaxInventoryCount, AvaliableInventoryCount, CreatedBy, updated_by, created_at, updated_at
        //InvoiceID, Rate, Amount, Description, ToCompany, ForCompany, BillTo, ShipTo, CreatedBy, updated_by, created_at, updated_at
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        //OrderStatusID, OrderStatus, CreatedBy, updated_by, created_at, updated_at
        //WorkTypeID, WorkType, CreatedBy, updated_by, created_at, updated_at
        //CompanyDetailsID, CompanyName, Address, Phoneno, EmailID, CreatedBy, updated_by, created_at, updated_at
        //UserID, UserRoleID, UserName, Password, FirstName, LastName, EmailID, PhoneNo, Gender, Address, City, Country, Postalcode

        $results = DB::table('inventory_type')
            ->select('inventory_type.InventoryTypeID', 'inventory_type.InventoryType')
            //->where('inventory.IsDeleted','=','0')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory Type does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_inventorydetails($userrole,$orgid,$userId)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if ($userrole == "1")
            $results = DB::table('inventory')
                ->select('inventory.*','organisation.OrganisationName','inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'inventory.OrganisationID')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->get();
        if ($userrole == "2")
            $results = DB::table('inventory')
                ->select('inventory.*','organisation.OrganisationName', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'inventory.OrganisationID')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->where(function ($query) {
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('inventory.CreatedBy', '=', $userId)
                        ->orwhere('users.AssignedTo', '=', $userId);
                })->get();
        if ($userrole == "3" || $userrole == "4" || $userrole == "5")
            $results = DB::table('inventory')
                ->select('inventory.*','organisation.OrganisationName', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->leftjoin('organisation', 'organisation.OrganisationID', '=', 'inventory.OrganisationID')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->where('inventory.CreatedBy', '=', $userId)
                ->get();


        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->InventoryID)
                    ->where('file_attachment.FileSourceID', '=', '7')
                    ->get();
                $filecount = count($files);
                if ($filecount != '0')
                    $results[$i]->filedetails = $files;
                else
                    $results[$i]->filedetails = [];
            }

            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_available_inventorydetails()
    {

        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if ($userrole == "1")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->where('inventory.AvaliableInventoryCount', '>', '0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->get();
        if ($userrole == "2")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->where('inventory.AvaliableInventoryCount', '>', '0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->where(function ($query) {
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('inventory.CreatedBy', '=', $userId)
                        ->orwhere('users.AssignedTo', '=', $userId);
                })->get();
        if ($userrole == "3" || $userrole == "4" || $userrole == "5")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                ->where('inventory.AvaliableInventoryCount', '>', '0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('users.OrganisationID', '=', $orgid)
                ->where('inventory.CreatedBy', '=', $userId)
                ->get();
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->InventoryID)
                    ->where('file_attachment.FileSourceID', '=', '7')
                    ->get();
                $filecount = count($files);
                if ($filecount != '0')
                    $results[$i]->filedetails = $files;
                else
                    $results[$i]->filedetails = [];
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_inventorydetails_bytype($type)
    {

        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        if ($userrole == "1")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                //->where('inventory.AvaliableInventoryCount','>','0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('inventory.InventoryTypeID', '=', $type)
                ->where('users.OrganisationID', '=', $orgid)
                ->get();
        if ($userrole == "2")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                //->where('inventory.AvaliableInventoryCount','>','0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('inventory.InventoryTypeID', '=', $type)
                ->where('users.OrganisationID', '=', $orgid)
                ->where(function ($query) {
                    $userId = Authorizer::getResourceOwnerId();
                    return $query
                        ->where('inventory.CreatedBy', '=', $userId)
                        ->orwhere('users.AssignedTo', '=', $userId);
                })->get();
        if ($userrole == "3" || $userrole == "4" || $userrole == "5")
            $results = DB::table('inventory')
                ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
                ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
                ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
                //->where('inventory.AvaliableInventoryCount','>','0')
                ->where('inventory.IsDeleted', '=', '0')
                ->where('inventory.InventoryTypeID', '=', $type)
                ->where('users.OrganisationID', '=', $orgid)
                ->where('inventory.CreatedBy', '=', $userId)
                ->get();
        if (!empty($results)) {
            $count = count($results);
            for ($i = 0; $i < $count; $i++) {
                $files = DB::table('file_attachment')
                    ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                    ->where('file_attachment.SourceID', '=', $results[$i]->InventoryID)
                    ->where('file_attachment.FileSourceID', '=', '7')
                    ->get();
                $filecount = count($files);
                if ($filecount != '0')
                    $results[$i]->filedetails = $files;
                else
                    $results[$i]->filedetails = [];
            }
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public  function get_Parent_Organisation_inventory()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $orgid = $org_id[0];
        $response = $this->get_all_inventorydetails($userrole,$orgid,$userId);
        return $response;
    }

    public  function get_Sub_Organisation_Inventory_List($org_id)
    {
        $userId = Authorizer::getResourceOwnerId();
        //$org_id=User::where('id', '=', $userId)-> pluck('OrganisationID');
        $user_role = User::where('id', '=', $userId)->pluck('UserRoleID');
        $userrole = $user_role[0];
        $response = $this->get_all_inventorydetails($userrole,$org_id,$userId);
        return $response;
    }

    public function get_inventorydetails_by_id($inventoryid)
    {
        //InventoryID, InventoryName, InventorydisplayID, Description, Price, MinInventoryCount, MaxInventoryCount, AvaliableInventoryCount, CreatedBy, updated_by, created_at, updated_at
        //InvoiceID, Rate, Amount, Description, ToCompany, ForCompany, BillTo, ShipTo, CreatedBy, updated_by, created_at, updated_at
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        //OrderStatusID, OrderStatus, CreatedBy, updated_by, created_at, updated_at
        //WorkTypeID, WorkType, CreatedBy, updated_by, created_at, updated_at
        //CompanyDetailsID, CompanyName, Address, Phoneno, EmailID, CreatedBy, updated_by, created_at, updated_at
        //UserID, UserRoleID, UserName, Password, FirstName, LastName, EmailID, PhoneNo, Gender, Address, City, Country, Postalcode

        $results = DB::table('inventory')
            ->select('inventory.*', 'inventory_type.InventoryType as InventoryTypeName', 'users.FirstName as Createdbyfname', 'users.LastName as Createdbylname')
            ->leftjoin('users', 'users.id', '=', 'inventory.CreatedBy')
            ->leftjoin('inventory_type', 'inventory_type.InventoryTypeID', '=', 'inventory.InventoryTypeID')
            ->where('inventory.InventoryID', '=', $inventoryid)
            ->where('inventory.IsDeleted', '=', '0')
            ->get();
        if (!empty($results)) {
            $files = DB::table('file_attachment')
                ->select('file_attachment.FileAttachmentID', 'file_attachment.FileType', 'file_attachment.FilePath')
                ->where('file_attachment.SourceID', '=', $inventoryid)
                ->where('file_attachment.FileSourceID', '=', '7')
                ->get();
            $filecount = count($files);
            if ($filecount != '0')
                $results[0]->filedetails = $files;
            else
                $results[0]->filedetails = [];
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_return_inventory_data($inventoryid)
    {

        $results = DB::table('inventory')
            ->select('inventory.*', 'users.FirstName', 'users.LastName', 'user_inventory.*')
            ->leftjoin('user_inventory', 'user_inventory.InventoryID', '=', 'inventory.InventoryID')
            ->leftjoin('users', 'users.id', '=', 'user_inventory.UserID')
            ->where('inventory.InventoryID', '=', $inventoryid)
            ->where('inventory.IsDeleted', '=', '0')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Inventory does not exists";
            $response['code'] = 400;
        }
        return $response;
    }
    //--------------------------------------End of GET ROUTES-----------------------
    //------------------------------------------------Start of Post Routes----------------------
    public function createinventory($input)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        //$user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $orgid = $org_id[0];
        $rules = array(
            'Inventory' => 'required',
            //'MaxCount' => 'required',
            //'Minlevel' => 'required',
            //'Value' => 'required'
        );
        $v = Validator::make($input, $rules);
        $percenttax = 0;
        if ($v->passes()) {
            $cost = Input::get('Value');
            if ($cost == null || $cost == '0')
                $cost = '0';

            if ($cost != '0') {
                $percenttax = (((Input::get('CentralTaxPercent') / 100) * $cost)
                    + ((Input::get('StateTaxPercent') / 100) * $cost) + + ((Input::get('ThresholdPercent') / 100) * $cost)
                    + ((Input::get('OtherTaxPercent') / 100) * $cost));

            }
           //dd($percenttax);
            $price = (Input::get('Value') + Input::get('Margin') + $percenttax + Input::get('OtherCost'));
            //dd($price);
            $parentassetid = Input::get('ParentAssetID');
            if (!empty($parentassetid))
                $is_subasset = 1;
            else
                $is_subasset = 0;
            $expiry_date = Input::get('ExpiryDate');
            if (!empty($expiry_date))
                $expiry_date = date_format(date_create($expiry_date), "Y-m-d");

            $file_count = count(Input::file('fileattachment'));
            if ($file_count == '0')
                $hasattachment = '0';
            else
                $hasattachment = '1';
            $inventory_list_count = DB::table('inventory')
                ->select('*')
                ->count();
            if ($inventory_list_count == "0")
                $inventorylistid = "4000001";
            else
                $inventorylistid = '';

            $check_inventory = DB::table('inventory')
                ->select('InventoryID', 'InventoryName', 'IsDeleted')
                ->where('inventory.InventoryName', '=', Input::get('Inventory'))
                ->where('inventory.OrganisationID', '=', $orgid)
                ->get();

            if (empty($check_inventory)) {
                DB::beginTransaction();
                $inventory = Inventory::create([
                    'InventoryID' => $inventorylistid,
                    'InventoryName' => Input::get('Inventory'),
                    'InventoryTypeID' => Input::get('InventoryType'),
                    'OrganisationID' => $orgid,
                    'SKU' => Input::get('SKU'),
                    'SkuCode' => Input::get('SkuCode'),
                    'ParentAssetID' => $parentassetid,
                    'IsSubAsset' => $is_subasset,
                    'ExpiryDate' => $expiry_date,
                    'Margin' => Input::get('Margin'),
                    'ThresholdPercent' => Input::get('ThresholdPercent'),
                    'CentralTaxPercent' => Input::get('CentralTaxPercent'),
                    'StateTaxPercent' => Input::get('StateTaxPercent'),
                    'OtherTaxPercent' => Input::get('OtherTaxPercent'),
                    'OtherCost' => Input::get('OtherCost'),
                    'Cost' => Input::get('Value'),
                    'Price' => $price,
                    'Tax' => Input::get('Tax'),
                    'Bucket' => Input::get('Bucket'),
                    'Shelf' => Input::get('Shelf'),
                    'RFIDTag' => Input::get('RFIDTag'),
                    'BarCode' => Input::get('BarCode'),
                    'MinInventoryCount' => Input::get('Minlevel'),
                    'Description' => Input::get('Description'),
                    'MaxInventoryCount' => Input::get('MaxCount'),
                    'AvaliableInventoryCount' => Input::get('AvaliableCount'),
                    'IsDeleted' => '0',
                    'CreatedBy' => Input::get('createdby'),
                    'created_at' => Carbon::now(),
                    'description2' => Input::get('description2'),
                    'description3' => Input::get('description3'),
                    'description4' => Input::get('description4'),
                ]);
                $inventoryid = $inventory->id;
                $inventory_id = "INV" . $inventoryid;
                DB::table('inventory')->where('InventoryID', '=', $inventoryid)->update(array('InventorydisplayID' => $inventory_id));
                if ($file_count != 0) {
                    $filetype = array();
                    $filedata = Input::file('fileattachment');

                    $filesourceid = '7';
                    \App\Helpers\Helpers::fileattachment($filedata, $inventoryid, $userId, $filesourceid);

                }
                $sourceid = $inventoryid;
                $orderid = "";
                $customerid = "";
                $transactionsourceid = '6';
                $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);
                DB::commit();
                $response['data'] = "Inventory added  Successfully";
                $response['code'] = 200;
            } else {
                if ($check_inventory[0]->IsDeleted == "1") {

                    $id = $check_inventory[0]->InventoryID;
                    $input = Input::all();
                    if (!empty($input['Inventory']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('InventoryName' => $input['Inventory']));
                    if (!empty($input['MaxCount']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('MaxInventoryCount' => $input['MaxCount']));
                    //DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('AvaliableInventoryCount' => $input['Count']));
                    if (!empty($input['AvaliableCount']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('AvaliableInventoryCount' => $input['AvaliableCount']));
                    if (!empty($input['Minlevel']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('MinInventoryCount' => $input['Minlevel']));
                    if (!empty($input['Value']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Cost' => $input['Value']));
                    if (!empty($input['Tax']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Tax' => $input['Tax']));
                    if (!empty($input['Description']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Description' => $input['Description']));
                    if (!empty($input['createdby']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('CreatedBy' => $input['createdby']));
                    if (!empty($input['SKU']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('SKU' => $input['SKU']));
                    if (!empty($input['SkuCode']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('SkuCode' => $input['SkuCode']));
                    if (!empty($input['IsSubAsset']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('IsSubAsset' => $is_subasset));
                    if (!empty($input['ParentAssetID']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ParentAssetID' => $parentassetid));
                    if (!empty($input['ExpiryDate']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ExpiryDate' => $expiry_date));
                    if (!empty($input['Margin']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Margin' => $input['Margin']));
                    if (!empty($input['ThresholdPercent']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ThresholdPercent' => $input['ThresholdPercent']));
                    if (!empty($input['CentralTaxPercent']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('CentralTaxPercent' => $input['CentralTaxPercent']));
                    if (!empty($input['StateTaxPercent']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('StateTaxPercent' => $input['StateTaxPercent']));
                    if (!empty($input['OtherTaxPercent']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('OtherTaxPercent' => $input['OtherTaxPercent']));
                    if (!empty($input['OtherCost']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('OtherCost' => $input['OtherCost']));
                    if (!empty($input['Bucket']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Bucket' => $input['Bucket']));
                    if (!empty($input['Shelf']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Shelf' => $input['Shelf']));
                    if (!empty($input['RFIDTag']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('RFIDTag' => $input['RFIDTag']));
                    if (!empty($input['BarCode']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('BarCode' => $input['BarCode']));
                    if (!empty($input['description2']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description2' => $input['description2']));
                    if (!empty($input['description3']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description3' => $input['description3']));
                    if (!empty($input['description4']))
                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description4' => $input['description4']));


                    $get_inventory_data = DB::table('inventory')
                        ->select('*')
                        ->where('inventory.InventoryID', '=', [$id])
                        ->get();
                    if (!empty($get_inventory_data)) {
                        $cost = $get_inventory_data[0]->Cost;
                        if (!empty($cost) || $cost != '0')
                            $percenttax = ((($get_inventory_data[0]->CentralTaxPercent / $cost) * 100)
                                + (($get_inventory_data[0]->StateTaxPercent / $cost) * 100)
                                + (($get_inventory_data[0]->OtherTaxPercent / $cost) * 100));
                        else
                            $percenttax = 0;

                    }
                    $price = ($get_inventory_data[0]->Cost + $get_inventory_data[0]->Margin + $percenttax + $get_inventory_data[0]->OtherCost);
                    DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Price' => $price));

                    DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('IsDeleted' => '0'));


                    if ($file_count != 0) {
                        $filetype = array();
                        $filedata = Input::file('fileattachment');
                        $filesourceid = '7';
                        \App\Helpers\Helpers::fileattachment($filedata, $id, $userId, $filesourceid);

                    }
                    $sourceid = $id;
                    $orderid = "";
                    $customerid = "";
                    $transactionsourceid = '6';
                    $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);

                    $response['data'] = "Inventory added  Successfully";
                    $response['code'] = 200;
                } else {
                    $response['data'][] = "The inventory has already been taken";
                    $response['code'] = 400;
                }
            }
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
        }
        return $response;
    }

    public function updateinventory($id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        //$user_role = User::where('id', '=', $userId)-> pluck('UserRoleID');
        $orgid = $org_id[0];
        $input = Input::all();
        $error = "";
        $file_count = count(Input::file('fileattachment'));
        $fileattachment_details = Input::file('fileattachment');
        if ($file_count == '0')
            $hasattachment = '0';
        else
            $hasattachment = '1';
        if (!empty($input['Inventory'])) {
            $check_username = DB::table('inventory')
                ->select('InventoryID', 'InventoryName')
                ->where('inventory.InventoryName', '=', $input['Inventory'])
                ->where('inventory.OrganisationID', '=', $orgid)
                ->get();
            if (!empty($check_username)) {
                if (($check_username[0]->InventoryID == $id))
                    $error .= "";
                else
                    $error .= "The inventory has already been taken";
            }
        }
        if ($error == "") {
            $percenttax = 0;
            $cost = Input::get('Value');
            if ($cost == null || $cost == 'null')
                $cost = '0';

            if ($cost != '0') {
                $percenttax = (((Input::get('CentralTaxPercent') / 100) * $cost)
                    + ((Input::get('StateTaxPercent') / 100) * $cost) + + ((Input::get('ThresholdPercent') / 100) * $cost)
                    + ((Input::get('OtherTaxPercent') / 100) * $cost));
            }
            $price = (Input::get('Value') + Input::get('Margin') + $percenttax + Input::get('OtherCost'));
            $parentassetid = Input::get('ParentAssetID');
            if (!empty($parentassetid))
                $is_subasset = 1;
            else
                $is_subasset = 0;
            $expiry_date = Input::get('ExpiryDate');
            //dd($expiry_date);
            if ($expiry_date != 'null')
                $expiry_date = date_format(date_create($expiry_date), "Y-m-d");


            if (!empty($input['Inventory']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('InventoryName' => $input['Inventory']));

            if (!empty($input['InventoryType']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('InventoryTypeID' => $input['InventoryType']));
            if (!empty($input['MaxCount']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('MaxInventoryCount' => $input['MaxCount']));
            if (!empty($input['AvaliableCount']))

                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('AvaliableInventoryCount' => $input['AvaliableCount']));
            if (!empty($input['Minlevel']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('MinInventoryCount' => $input['Minlevel']));
            if (!empty($input['Value']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Cost' => $input['Value']));
//                    if(!empty($input['Cost']))
//                        DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Cost' => $input['Cost']));
            if (!empty($input['Tax']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Tax' => $input['Tax']));
            if (!empty($input['Description']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Description' => $input['Description']));
            if (!empty($input['createdby']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('CreatedBy' => $input['createdby']));
            if (!empty($input['SKU']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('SKU' => $input['SKU']));
            if (!empty($input['SkuCode']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('SkuCode' => $input['SkuCode']));

            DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('IsSubAsset' => $is_subasset));
            if (!empty($input['ParentAssetID']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ParentAssetID' => $parentassetid));
            if (!empty($input['ExpiryDate']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ExpiryDate' => $expiry_date));
            if (!empty($input['Margin']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Margin' => $input['Margin']));
            if (!empty($input['ThresholdPercent']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('ThresholdPercent' => $input['ThresholdPercent']));
            if (!empty($input['CentralTaxPercent']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('CentralTaxPercent' => $input['CentralTaxPercent']));
            if (!empty($input['StateTaxPercent']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('StateTaxPercent' => $input['StateTaxPercent']));
            if (!empty($input['OtherTaxPercent']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('OtherTaxPercent' => $input['OtherTaxPercent']));
            if (!empty($input['OtherCost']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('OtherCost' => $input['OtherCost']));
            if (!empty($input['Bucket']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Bucket' => $input['Bucket']));
            if (!empty($input['Shelf']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Shelf' => $input['Shelf']));
            if (!empty($input['RFIDTag']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('RFIDTag' => $input['RFIDTag']));
            if (!empty($input['BarCode']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('BarCode' => $input['BarCode']));
            if (!empty($input['description2']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description2' => $input['description2']));
            if (!empty($input['description3']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description3' => $input['description3']));
            if (!empty($input['description4']))
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('description4' => $input['description4']));

//              dd($cost);
//                if($cost != '0' ||$cost != 'null' || $cost != null ||$cost != '0.0' )
//                    $percenttax =  ((($get_inventory_data[0]->CentralTaxPercent/$cost)*100)
//                        + (($get_inventory_data[0]->StateTaxPercent/$cost)*100)
//                        + (($get_inventory_data[0]->OtherTaxPercent/$cost)*100) );
//                else
//                    $percenttax = 0;
//
//            }
//            $price = ($get_inventory_data[0]->Cost+ $get_inventory_data[0]->Margin+$percenttax+ $get_inventory_data[0]->OtherCost);
            DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('Price' => $price));

            DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('IsDeleted' => '0'));
            if (!empty($fileattachment_details)) {
                $filesourceid = '7';
                $file_details = \App\Helpers\Helpers::fileattachment($fileattachment_details, $id, $userId, $filesourceid);
            }

            if (!empty($delete_file_details))
                $file = \App\Helpers\Helpers::delete_file($delete_file_details);

            $response['data'] = "Inventory Record updated successfully";
            $response['code'] = 200;
            return $response;
        } else {
            $response['data'] = $error;
            $response['code'] = 400;
            return $response;
        }
    }

    public function deleteinventory($input)
    {
        if (!empty($input)) {
            $ids = explode(",", $input);
        } else {
            $response['data'] = "Select Inventory to delete";
            $response['code'] = 400;
            return $response;
        }
        foreach ($ids as $id) {
            $customers = DB::table('inventory')
                ->select('inventory.*')
                ->where('inventory.InventoryID', '=', $id)
                ->get();
            if (!empty($customers)) {
                DB::table('inventory')->where('InventoryID', '=', $id)->update(array('IsDeleted' => '1'));
                $response['data'] = "Inventory Details deleted successfully";
                $response['code'] = 200;
            } else {
                $response['data'] = "Inventory does not exists";
                $response['code'] = 400;
                return $response;
            }
        }
        return $response;
    }

    public function returninventorytools($input)
    {
        $error = "";
        $rules = array(
            'InventoryId' => 'required',
            'Count' => 'required',
            'UserID' => 'required'
            //'Value' => 'required'
        );
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            $inventory = DB::table('user_inventory')
                ->select('UserInventoryID', 'UserID', 'InventoryID', 'NoOfInventory', 'IsReturned', 'ReturnedInventoryCount', 'AvaliableInventoryCount')
                ->where('user_inventory.InventoryID', '=', Input::get('InventoryId'))
                ->where('user_inventory.UserID', '=', Input::get('UserID'))
                ->get();
            if (!empty($inventory)) {
                $userinventoryid = $inventory['0']->UserInventoryID;
                $inventoryid = $inventory['0']->InventoryID;
                $avaliable_count = intval($inventory['0']->AvaliableInventoryCount) - Input::get('Count');

                $inventorydetails = DB::table('inventory')
                    ->select('InventoryID', 'MinInventoryCount', 'MaxInventoryCount', 'AvaliableInventoryCount')
                    ->where('inventory.InventoryID', '=', $inventoryid)
                    ->get();
                if (!empty($inventorydetails)) {
                    $avaliableinventorycount = intval($inventorydetails[0]->AvaliableInventoryCount) + Input::get('Count');
                    if ($avaliableinventorycount > $inventorydetails[0]->MaxInventoryCount) {
                        $error = "Avaliable count cannot exceeds total number of inventory tools avaliable";
                        $response['data'] = $error;
                        $response['code'] = 400;
                    } else {
                        if ($avaliable_count > $inventory['0']->NoOfInventory) {
                            $error = "Avaliable count cannot exceeds total number of inventory tools avaliable with user";
                            $response['data'] = $error;
                            $response['code'] = 400;
                        } elseif ($avaliable_count < 0) {
                            $error = "No tools avaliable to return";
                            $response['data'] = $error;
                            $response['code'] = 400;
                        } else {
                            DB::table('user_inventory')->where('UserInventoryID', '=', $userinventoryid)->update(array('IsReturned' => '1', 'ReturnedInventoryCount' => Input::get('Count'), 'AvaliableInventoryCount' => $avaliable_count));
                            DB::table('inventory')->where('InventoryID', '=', $inventoryid)->update(array('AvaliableInventoryCount' => $avaliableinventorycount));
                            $response['data'] = "Inventory returned  Successfully";
                            $response['code'] = 200;
                        }
                    }
                } else {
                    $response['data'] = "Invalid Inventory details";
                    $response['code'] = 400;
                }
            } else {
                $response['data'] = "Invalid Inventory details";
                $response['code'] = 400;
            }
        } else {
            $response['data'] = $v->messages()->all();
            $response['code'] = 400;
        }
        return $response;
    }
    //----------------------------------------------End of Post Routes------------------------
    //--------------------------------------End of Inventory Route--------------------------------
    //---------------------------------------Start of Invoice Routes---------------------------------
    //--------------------------------------Start of GET Routes----------------------------------
    /* public function get_all_invoicedetails($supervisorid)
    {
       $results = DB::table('invoices')
            ->select('invoices.InvoiceID','invoices.Amount','invoices.InvoicedisplayID','users.id','users.name','invoices.created_at')
            ->leftjoin('users','users.id','=','invoices.InvoiceTo')
           ->where('invoices.CreatedBy','=',$supervisorid)
            ->get();
        if(!empty($results))
        {
          $count = count($results);
            for($i=0;$i<$count;$i++)
            {
                $orderdetails = DB::table('orders')
                    ->select('users.name')
                    ->leftjoin('users','users.id','=','orders.UserID')
                    ->where('orders.InvoiceID','=',$results[$i]->InvoiceID)
                    ->get();
                if(!empty($orderdetails))
                {
                    $results[$i]->Invoiceto_name = $orderdetails[0]->name;
                }
                else
                {
                    $results[$i]->Invoiceto_name = null;

                }


            }
            $response['data'] = $results;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
        }
        return $response;
    }
    public function get_invoicedetails_by_id($invoiceid)
    {
        $error = "";
        $results = DB::table('orders')
            ->select('orders.OrderID','orders.WorkOrderID','orders.InvoiceID')
             ->where('orders.InvoiceID','=',$invoiceid)
            ->get();
        $count_result = count($results);
        for($i=0;$i<$count_result;$i++)
        {
            $input['orderarray'][$i]= $results[$i]->OrderID;
            $input['orderids'][$i] = $results[$i]->WorkOrderID;
        }
            $count = count($input['orderarray']);
            $j=0;
            $l=0;
            $inventoryid_array = array();
            for($i=0;$i<$count;$i++)
            {
                $invoicedetails[$j]['inventorydetails'] = DB::table('inventory')
                            ->select('inventory.InventoryID','inventory.InventoryName','invoice_inventory.NoOfInventory',
                                'invoice_inventory.InventoryPrice','invoice_inventory.CreatedBy','invoice_inventory.OrderID' )
                            ->leftjoin('invoice_inventory','invoice_inventory.InventoryID','=','inventory.InventoryID')
                            ->leftjoin('users','users.id','=','invoice_inventory.CreatedBy')
                            ->where('invoice_inventory.OrderID','=',$input['orderarray'][$i])
                            ->get();
                        if(!empty($invoicedetails[$j]['inventorydetails']))
                        {
                            $count_array = count($invoicedetails[$j]['inventorydetails']);

                            for($k=0;$k<$count_array;$k++)
                            {
                                $inventoryid_array[$l]= $invoicedetails[$j]['inventorydetails'][$k]->InventoryID;
                                $invoicedetails[$j]['orderid']= $input['orderarray'][$i];
                                $l++;
                            }
                            $j++;
                        }
                else
                    $error .= "Order id ".$input['orderarray'][$i]." does not exists";
            }
        if($error=="") {
            $unique_inventory = array_values(array_unique($inventoryid_array));
            $count_uniquearray = count($unique_inventory);
            $count_invoicedtailsarray = count($invoicedetails);
            $k = 0;
            $sum = 0;
            $invoicedata[$k]['Amount'] = 0;
            $invoicedata[$k]['NoofInventory'] = 0;
            for ($p = 0; $p < $count_uniquearray; $p++)
            {
                for ($q = 0; $q < $count_invoicedtailsarray; $q++)
                {
                    $count_inventorydetails = count($invoicedetails[$q]['inventorydetails']);
                    for ($r = 0; $r < $count_inventorydetails; $r++) {
                        if ($unique_inventory[$p] == $invoicedetails[$q]['inventorydetails'][$r]->InventoryID) {
                            $inventoryarray[$k] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryName;

                            $invoicedata[$k]['InventoryID'] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryID;
                            $invoicedata[$k]['Amount'] = $invoicedata[$k]['Amount'] + $invoicedetails[$q]['inventorydetails'][$r]->InventoryPrice;
                            $invoicedata[$k]['NoofInventory'] = $invoicedata[$k]['NoofInventory'] + $invoicedetails[$q]['inventorydetails'][$r]->NoOfInventory;
                            $invoicedata[$k]['InventoryName'] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryName;
                            $sum = $sum + $invoicedata[$k]['Amount'];
                        }
                    }
                }
                $k++;
                if ($k < $count_uniquearray) {
                    $invoicedata[$k]['Amount'] = 0;
                    $invoicedata[$k]['NoofInventory'] = 0;
                }
            }
        }
        if(!empty($invoicedata))
        {
            $userdetails = DB::table('invoices')
                ->select('invoices.created_at','invoices.InvoiceID','invoices.InvoicedisplayID','users.name')
                ->leftjoin('users','users.id','=','invoices.CreatedBy')
                ->where('invoices.InvoiceID','=',$invoiceid)
                ->get();
            $invoicetoname = DB::table('orders')
                ->select('users.name')
                ->leftjoin('users','users.id','=','orders.UserID')
                ->where('orders.InvoiceID','=',$invoiceid)
                ->get();
            if(!empty($invoicetoname))
            {
                $invoiceto_name = $invoicetoname[0]->name;
            }
            else
            {
                $invoiceto_name = null;

            }

            $return_data['inventorydata'] = $invoicedata;
            $return_data['totalamount']= $sum;
            $return_data['invoiceto'] = $invoiceto_name;
            $return_data['invoicedisplayid'] = $userdetails[0]->InvoicedisplayID;
            //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
            $return_data['amountinwords'] = $sum;//$f->format($sum);
            $return_data['note'] = "Invoice generated for following Orders IDs:-";
            $return_data['orderid'] =  $input['orderids'];
            $response['data'] = $return_data;
            $response['code']=200;
        }
        else
        {
            $response['data'] = "No record exists";
            $response['code']=400;
        }
        return $response;
    }*/
    public function get_requestor_list($id)
    {

        //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
        //OrderID, WorkOrderID, UserID, LocationID, InventoryID, InvoiceID, JobID, OrderStatusID, NoOfInventory, CreatedBy, updated_by, created_at, updated_at
        /************will require changes*********************/
        $results = DB::table('tasklist')
            ->distinct()
            ->select('users.id', 'users.name')
            ->leftjoin('users', 'users.id', '=', 'tasklist.CreatedBy')
            ->where('users.AssignedTo', '=', $id)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Requestor does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_order_listby_userid($id)
    {

        //id, UserRoleID, name, email, username, password, FirstName, LastName, PhoneNo, Gender, Address, City, Country, Postalcode, AssignedTo, CreatedBy, updated_by, remember_token, created_at, updated_at
        //OrderID, WorkOrderID, UserID, LocationID, InventoryID, InvoiceID, JobID, OrderStatusID, NoOfInventory, CreatedBy, updated_by, created_at, updated_at
        /************will require changes*********************/
        $results = DB::table('orders')
            ->distinct()
            ->select('orders.OrderID', 'orders.WorkOrderID')
            ->where('orders.UserID', '=', $id)
            ->whereNull('orders.InvoiceID')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Order does not exists ";
            $response['code'] = 400;
        }
        return $response;
    }
    //--------------------------------------End of GET ROUTES-----------------------
    //------------------------------------------------Start of Post Routes----------------------
    /* public function raiseinvoices($input)
    {
        //dd($input);
        $error = "";
        if(empty($input['orderarray']))
          $error .= "Select order to raise invoice";
         elseif(empty($input['requestorid']))
          $error .= "Select Requestor id to raise invoice";
         else
         {
            $count = count($input['orderarray']);
            $j=0;
            $l=0;
            $inventoryid_array = array();
            for($i=0;$i<$count;$i++)
            {
                $results =  DB::table('orders')
                    ->select('orders.OrderID','orders.WorkOrderID','orders.InvoiceID')
                    ->where('orders.OrderID','=',$input['orderarray'][$i])
                    ->get();
                if(!empty($results))
                {
                    if($results[0]->InvoiceID==null)
                    {
                        $invoicedetails[$j]['inventorydetails'] = DB::table('inventory')
                            ->select('inventory.InventoryID','inventory.InventoryName','invoice_inventory.NoOfInventory',
                                'invoice_inventory.InventoryPrice','invoice_inventory.CreatedBy','invoice_inventory.OrderID','orders.WorkOrderID' )
                            ->leftjoin('invoice_inventory','invoice_inventory.InventoryID','=','inventory.InventoryID')
                            ->leftjoin('users','users.id','=','invoice_inventory.CreatedBy')
                            ->leftjoin('orders','orders.OrderID','=','invoice_inventory.OrderID')
                            ->where('invoice_inventory.OrderID','=',$input['orderarray'][$i])
                            ->get();
                        if(!empty($invoicedetails[$j]['inventorydetails']))
                        {
                            $count_array = count($invoicedetails[$j]['inventorydetails']);

                            for($k=0;$k<$count_array;$k++)
                            {
                                $inventoryid_array[$l]= $invoicedetails[$j]['inventorydetails'][$k]->InventoryID;
                                $invoicedetails[$j]['orderid']= $input['orderarray'][$i];
                                $input['orderids'][$j] = $invoicedetails[$j]['inventorydetails'][$k]->WorkOrderID;

                                $l++;
                            }
                            $j++;
                        }
                        else
                            $error .= "Something went wrong while creating  Order id ".$input['orderarray'][$i]."!! Create order again!!! ";
                    }
                    else
                        $error .= "Invoice for order id ".$input['orderarray'][$i]." is raised ";
                }
                else
                    $error .= "Order id ".$input['orderarray'][$i]." does not exists";
            }
        }
        if($error=="")
        {
            $unique_inventory = array_values(array_unique($inventoryid_array));
            $count_uniquearray=count($unique_inventory);
            $count_invoicedtailsarray = count($invoicedetails);
            $k=0;
            $sum=0;
            $invoicedata[$k]['Amount']=0;
            $invoicedata[$k]['NoofInventory']=0;
            for($p =0;$p<$count_uniquearray;$p++ )
            {
                for($q =0;$q<$count_invoicedtailsarray;$q++)
                {
                    $count_inventorydetails = count($invoicedetails[$q]['inventorydetails']);
                    for($r=0;$r<$count_inventorydetails;$r++)
                    {
                        if($unique_inventory[$p]==$invoicedetails[$q]['inventorydetails'][$r]->InventoryID)
                        {
                            $inventoryarray[$k]=$invoicedetails[$q]['inventorydetails'][$r]->InventoryName;

                            $invoicedata[$k]['InventoryID'] = $invoicedetails[$q]['inventorydetails'][$r]->InventoryID;
                            $invoicedata[$k]['Amount'] = $invoicedata[$k]['Amount'] + $invoicedetails[$q]['inventorydetails'][$r]->InventoryPrice;
                            $invoicedata[$k]['NoofInventory'] = $invoicedata[$k]['NoofInventory'] + $invoicedetails[$q]['inventorydetails'][$r]->NoOfInventory;
                            $invoicedata[$k]['InventoryName'] =  $invoicedetails[$q]['inventorydetails'][$r]->InventoryName;

                            $sum = $sum+$invoicedata[$k]['Amount'];
                        }
                    }
                }
                $k++;
                if($k<$count_uniquearray)
                {
                    $invoicedata[$k]['Amount']=0;
                    $invoicedata[$k]['NoofInventory']=0;
                }
            }
            $invoice = Invoices::create([
                'Amount' => $sum,
                'InvoiceTo' => Input::get('requestorid'),
                'RaisedInvoice' => '1',
                'CreatedBy' => Input::get('createdby'),
                'created_at' => Carbon::now()
            ]);
            $invoiceid = "INVO".$invoice->id;
            DB::table('invoices')->where('InvoiceID', '=', $invoice->id)->update(array('InvoicedisplayID' => $invoiceid));
            DB::table('orders')->whereIn('OrderID', $input['orderarray'])->update(array('InvoiceID' => $invoice->id));
            $userdetails = DB::table('invoices')
                ->select('invoices.created_at','invoices.InvoiceID','invoices.InvoicedisplayID','users.name','users.email','users.id')
                ->leftjoin('users','users.id','=','invoices.CreatedBy')
                ->where('invoices.InvoiceID','=',$invoice->id)
                ->get();
            $invoiceto_name = DB::table('invoices')
                ->select('invoices.created_at','invoices.InvoiceID','invoices.InvoicedisplayID','users.name','users.email','users.id')
                ->leftjoin('users','users.id','=','invoices.InvoiceTo')
                ->where('invoices.InvoiceID','=',$invoice->id)
                ->get();
            $orderid_array = implode(",",$input['orderids']);
            $var = "Hi, " .$invoiceto_name[0]->name. "!" .
                "Welcome to Work Order System.
                Invoice for Order ID ".$orderid_array." is generated. Kindly find it in attachment.
                Regards,
                Team WOS";
            $emails_table_input = array(
                'Name' =>$invoiceto_name[0]->name,
                'EmailID' => $invoiceto_name[0]->email,
                'Subject' => "Invoice generated for Order ID:- ".$orderid_array,
                'Message' => $var,
                'Status' => 0,
                'HasAttachment'=>1,
                'UserID' => $invoiceto_name[0]->id,
                'MailFor' => 'Invoice',
                'MailSourceID'=>$invoice->id,
                'updated_by'=>Input::get('createdby'),
                'updated_at'=> Carbon::now(),
                'CreatedBy'=>Input::get('createdby'),
                'created_at'=> Carbon::now()
            );
            $email = Sendmails::create($emails_table_input);

            $return_data['inventorydata'] = $invoicedata;
            $return_data['totalamount']= $sum;
            $return_data['invoiceto'] = $invoiceto_name[0]->name;
            $return_data['invoicedisplayid'] = $userdetails[0]->InvoicedisplayID;
            //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
            $return_data['amountinwords'] = $sum;//$f->format($sum);
            $return_data['note'] = "Invoice generated for following Orders IDs:-";
            $return_data['orderid'] =     $input['orderids'];
            $response['data'] = $return_data;
            $response['code'] = 200;
            return $response;
        }
        else
        {
            $response['data'] = $error;
            $response['code'] = 400;
            return $response;
        }
    }*/
    //----------------------------------------------End of Post Routes------------------------
    //--------------------------------------End of Invoice Route--------------------------------
    //-------------------------------------------Other Routes-----------------------------------
    public function get_all_worktype()
    {
        //JobID, JobName, JobDescription, CreatedBy, updated_by, created_at, updated_at
        $results = DB::table('jobs')
            ->select('JobID', 'JobName', 'JobDescription')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Jobs does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_worktype_by_id($id)
    {

        $results = DB::table('work_type')
            ->select('WorkTypeID', 'WorkType')
            ->where('WorkTypeID', '=', $id)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Work Type does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_worktype_by_name($worktype)
    {

        $results = DB::table('work_type')
            ->select('WorkTypeID', 'WorkType')
            ->where('WorkType', '=', $worktype)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Work Type does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_all_filesource()
    {

        $results = DB::table('file_source')
            ->select('FileSourceID', 'FileSource')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "File source does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_filesource_by_id($id)
    {

        $results = DB::table('file_source')
            ->select('FileSourceID', 'FileSource')
            ->where('FileSourceID', '=', $id)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "File source does not exists";
            $response['code'] = 400;

        }
        return $response;
    }

    public function get_filesource_by_name($priorities)
    {

        $results = DB::table('file_source')
            ->select('FileSourceID', 'FileSource')
            ->where('FileSource', '=', $priorities)
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "File source does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_locationsdetails()
    {
        //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
        $results = DB::table('locations')
            ->select('locations.LocationID', 'locations.WorkLocationID', 'locations.LocationName')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Location does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_all_jobtype()
    {

        $results = DB::table('task_severities')
            ->select('TaskSeverityID', 'TaskSeverity')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "Task severity does not exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function search($input)
    {
        $results = DB::table('tasklist', 'orders', 'invoices', 'customers')
            ->select('tasklist.TasklistID', 'tasklist.TaskID', 'tasklist.JobID',
                'tasklist.PriorityID', 'tasklist.LocationID', 'tasklist.Description',
                'tasklist.StatusID', 'tasklist.HasAttachment', 'task_status.TaskStatus',
                'jobs.JobName', 'jobs.JobDescription', 'priorities.Priority', 'task_severities.TaskSeverity', 'tasklist.TaskSeverityID',
                'locations.WorkLocationID', 'locations.LocationName', 'tasklist.CreatedBy', 'u1.name as createdbyname',
                'assign_user_task.AssignedTo', 'assign_user_task.AssignedBy',
                'assign_user_task.UserTaskStatusID', 'user_task_status.UserTaskStatus')
            ->leftjoin('jobs', 'jobs.JobID', '=', 'tasklist.JobID')
            ->leftjoin('task_status', 'task_status.TaskStatusID', '=', 'tasklist.StatusID')
            ->leftjoin('task_severities', 'task_severities.TaskSeverityID', '=', 'tasklist.TaskSeverityID')
            ->leftjoin('priorities', 'priorities.PriorityID', '=', 'tasklist.PriorityID')
            ->leftjoin('locations', 'locations.LocationID', '=', 'tasklist.LocationID')
            ->leftjoin('assign_user_task', 'assign_user_task.TasklistID', '=', 'tasklist.TasklistID')
            ->leftjoin('user_task_status', 'user_task_status.UserTaskStatusID', '=', 'assign_user_task.UserTaskStatusID')
            ->leftjoin('users as u1', 'u1.id', '=', 'tasklist.CreatedBy')
            ->leftjoin('users as u2', 'u2.id', '=', 'assign_user_task.AssignedBy')
            ->leftjoin('users as u3', 'u3.id', '=', 'assign_user_task.AssignedTo')
            ->Where('u1.name', 'Like', '%' . $input . '%')
            ->orWhere('u1.email', 'Like', '%' . $input . '%')
            ->orWhere('u2.name', 'Like', '%' . $input . '%')
            ->orWhere('u2.email', 'Like', '%' . $input . '%')
            ->Where('u3.name', 'Like', '%' . $input . '%')
            ->orWhere('u3.email', 'Like', '%' . $input . '%')
            ->orWhere('task_severities.TaskSeverity', 'Like', '%' . $input . '%')
            ->orWhere('task_status.TaskStatus', 'Like', '%' . $input . '%')
            ->orWhere('tasklist.TaskID', 'Like', '%' . $input . '%')
            ->orWhere('tasklist.Description', 'Like', '%' . $input . '%')
            ->orWhere('tasklist.StatusID', 'Like', '%' . $input . '%')
            ->orWhere('locations.LocationName', 'Like', '%' . $input . '%')
            ->orWhere('locations.WorkLocationID', 'Like', '%' . $input . '%')
            ->orWhere('user_task_status.UserTaskStatus', 'Like', '%' . $input . '%')
            ->orWhere('priorities.Priority', 'Like', '%' . $input . '%')
            ->orWhere('tasklist.PriorityID', 'Like', '%' . $input . '%')
            ->orWhere('tasklist.CreatedBy', 'Like', '%' . $input . '%')
            ->orWhere('assign_user_task.AssignedBy', 'Like', '%' . $input . '%')
            ->orWhere('assign_user_task.AssignedTo', 'Like', '%' . $input . '%')
            ->get();
        if (!empty($results)) {
            $response['data'] = $results;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function base64ToImage($base64_string, $output_file)
    {
        $file = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($file, base64_decode($data[1]));
        fclose($file);

        return $output_file;
    }

    public function create_event()
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $org_email = User::where('id', '=', $userId)->pluck('email');
        $org_name = User::where('id', '=', $userId)->pluck('name');
        //dd($org_email);
        $task_list_count = Tasklist::count();

        if ($task_list_count == "0")
            $tasklistid = "5000001";
        else
            $tasklistid = '';
        $event_details = array(
            'start_date' => date_format(date_create(Input::get('start_date')), "Y-m-d"),
            'end_date' => date_format(date_create(Input::get('end_date')), "Y-m-d"),
            'start_time' => date_format(date_create(Input::get('start_time')), "H:i:s"),
            'end_time' => date_format(date_create(Input::get('end_time')), "H:i:s"),
            'event_description' => Input::get('event_description'),
            'event_title' => Input::get('event_title'),
            'event_location' => Input::get('event_location'),
            'event_organizer' => $org_name[0],
            'organizer_mail' => $org_email[0],
            'orgid' => $org_id[0],
            'userid' => $userId,
            'timezone' => Input::get('timezone'),
            'alert_type_id' => Input::get('alert_type_id'),
            'repeat_type_id' => Input::get('repeat_type_id'),
            'event_id' => '0'
        );
        if (!empty($event_details['event_description']))
            $description = "Location:- " . Input::get('event_location') . "Description:- " . Input::get('event_description') . " Start Date:- " . $event_details['start_date'] . " End Date:- " . $event_details['end_date']
                . " Start Time:- " . $event_details['start_time'] . "  End Time:- " . $event_details['end_time'];
        else
            $description = "";

        $task_list = [
            'TasklistID' => $tasklistid,
            'OrganisationID' => $org_id[0],
            'name' => $event_details['event_title'],
            'Description' => $description,
            'HasAttachment' => 0,
            'DueDate' => date_format(date_create($event_details['start_date']), "Y-m-d"),
            'EndDate' => date_format(date_create($event_details['end_date']), "Y-m-d"),
            'TaskTypeID' => '3',
            'CreatedBy' => $userId
        ];
        $customerid = Input::get('CustomerID');
        if (!empty($customerid)) {
            $task_list['CustomerID'] = $customerid;
            $event_details['customerid'] = $customerid;
            $customeremail = Customers::where('CustomerID', '=', $customerid)->pluck('EmailID');
            $customername = Customers::where('CustomerID', '=', $customerid)->pluck('CustomerName');
        } else
            $event_details['customerid'] = '0';

        $task = Tasklist::create($task_list);
        if (!empty($task)) {

            $taskid = $task->id;
            $event_details['taskid'] = $taskid;

            if (!empty($customeremail)) {
                $event_details['receipent_mail'] = $customeremail[0];
                $event_details['receipent_name'] = $customername[0];
            }
            $event_details['message'] = "<p>Appointment schedule for " . $event_details['event_title'] . "</p>.
                             <p>Start Time:" . $event_details['start_time'] . "</p>
                             <p>End Time:" . $event_details['end_time'] . "</p>
                             <p>Start Date:" . $event_details['start_date'] . " </p>
                             <p>End Date:" . $event_details['end_date'] . " </p>
                             <p>Location:" . $event_details['event_location'] . "</p>
                             <p>Description:" . $event_details['event_description'] . "</p>
                             <p>Organizer:" . $event_details['event_organizer'] . "</p>
                             <p></p>
                             <p> Regards,</p>
                             <p> Team CEM </p>";
            //dd($event_details);
            $create_event = \App\Helpers\Helpers::create_ical_event($event_details);
            if(!empty($create_event))
            {
                if($create_event[0]== "1")
                {
                    $data['msg'] = "You have another meeting on ".$event_details['start_date']." at ".$event_details['start_time'];
                    $data['filedetails'] = "No file data";
                    $response['data'] = $data;
                    $response['code'] = 200;
                }
                else
                {
                    $orgid = $org_id[0];
                    $notification = \App\Helpers\Helpers::create_notification($userId, $taskid, $orgid, 'task');
                    $sourceid = $taskid;
                    $orderid = '';
                    $transactionsourceid = '3';
                    $transactions = \App\Helpers\Helpers::transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid);

                    $data['msg'] = "Event Created Successfully";
                    $data['filedetails'] = $create_event;
                    $response['data'] = $data;
                    $response['code'] = 200;
                }
            }

        } else {
            $response['data'] = "Error in event creation";
            $response['code'] = 400;
        }
        return $response;
    }

    public function update_event($id)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $org_email = User::where('id', '=', $userId)->pluck('email');
        $org_name = User::where('id', '=', $userId)->pluck('name');
        $check_eventdetails = DB::table('event_details')
            ->select('*')
            ->where('event_details.EventDetailsID', '=', $id)
            ->get();
        if (!empty($check_eventdetails)) {
            $event_details = array(
                'start_date' => date_format(date_create(Input::get('start_date')), "Y-m-d"),
                'end_date' => date_format(date_create(Input::get('end_date')), "Y-m-d"),
                'start_time' => date_format(date_create(Input::get('start_time')), "H:i:s"),
                'end_time' => date_format(date_create(Input::get('end_time')), "H:i:s"),
                'event_description' => Input::get('event_description'),
                'event_title' => Input::get('event_title'),
                'event_location' => Input::get('event_location'),
                'event_organizer' => $org_name[0],
                'organizer_mail' => $org_email[0],
                'orgid' => $org_id[0],
                'userid' => $userId,
                'timezone' => Input::get('timezone'),
                'alert_type_id' => Input::get('alert_type_id'),
                'repeat_type_id' => Input::get('repeat_type_id'),
                'event_id' => $id
            );
            if (!empty($event_details['event_description']))
                $description = Input::get('event_description') . " Start Date:- " . $event_details['start_date'] . " End Date:- " . $event_details['end_date']
                    . " Start Time:- " . $event_details['start_time'] . "  End Time:-" . $event_details['end_time'];
            else
                $description = "";

            $task_list = [
                'name' => $event_details['event_title'],
                'Description' => $description,
                'HasAttachment' => 0,
                'TaskTypeID' => '3',
                'CreatedBy' => $userId
            ];

            $customerid = Input::get('CustomerID');
            if (!empty($customerid)) {
                $task_list['CustomerID'] = $customerid;
                $event_details['customerid'] = $customerid;
                $customeremail = Customers::where('CustomerID', '=', $customerid)->pluck('EmailID');
                $customername = Customers::where('CustomerID', '=', $customerid)->pluck('CustomerName');
            } else
                $event_details['customerid'] = '0';
            $taskid = $check_eventdetails[0]->TaskID;
            $check_taskdetails = DB::table('Tasklist')
                ->select('*')
                ->where('Tasklist.TasklistID', '=', $taskid)
                ->get();
            if (!empty($check_taskdetails)) {
                if (($check_taskdetails[0]->IsDeleted != '1') && ($check_taskdetails[0]->TaskTypeID == '3')
                    && ($check_taskdetails[0]->CustomerID == $check_eventdetails[0]->CustomerID)
                )
                    $task = DB::table('tasklist')->where('TasklistID', '=', [$id])->update($task_list);
            }

            if (!empty($customeremail)) {
                $event_details['receipent_mail'] = $customeremail[0];
                $event_details['receipent_name'] = $customername[0];
            }

            $event_details['taskid'] = $taskid;

            $event_details['message'] = "<p>Appointment schedule for " . $event_details['event_title'] . "</p>.
                             <p>Start Time:" . $event_details['start_time'] . "</p>
                             <p>End Time:" . $event_details['end_time'] . "</p>
                             <p>Start Date:" . $event_details['start_date'] . " </p>
                             <p>End Date:" . $event_details['end_date'] . " </p>
                             <p>Location:" . $event_details['event_location'] . "</p>
                             <p>Description:" . $event_details['event_description'] . "</p>
                             <p>Organizer:" . $event_details['event_organizer'] . "</p>
                             <p></p>
                             <p> Regards,</p>
                             <p> Team CEM </p>";
            //dd($event_details);
            $create_event = \App\Helpers\Helpers::create_ical_event($event_details);

            if (!empty($create_event)) {
                $data['msg'] = "Event Updated Successfully";
                $data['filedetails'] = $create_event;
                $response['data'] = $data;
                $response['code'] = 200;
            } else {
                $response['data'] = "Error in event creation";
                $response['code'] = 400;
            }

        }
        return $response;

    }

    public function delete_event($input) {
        if (!empty($input)) {
            $ids = explode(",", $input);
        } else {
            $response['data'] = "Select Event to delete";
            $response['code'] = 400;
            return $response;
        }
        foreach ($ids as $id) {
            $task = DB::table('event_details')
                ->select('EventDetailsID', 'TaskID')
                ->where('event_details.EventDetailsID', '=', $id)
                ->get();
            if (!empty($task)) {
                DB::delete('delete from event_details where EventDetailsID = ?', [$id]);
                $response['data'] = "Events deleted successfully";
                $response['code'] = 200;
            } else {
                $response['data'] = "Events does not exists";
                $response['code'] = 400;
            }
        }
        return $response;
    }

    public function get_event_details($customerid)
    {
        $userId = Authorizer::getResourceOwnerId();
        $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
        $org_email = User::where('id', '=', $userId)->pluck('email');
        $org_name = User::where('id', '=', $userId)->pluck('name');
        $eventdetails = Eventdetails::Where('CustomerID', '=', $customerid)->where('OrganisationID', '=', $org_id[0])->get();


        if (!empty($eventdetails)) {
            foreach($eventdetails as $event)
            {
                $filedetails = DB::table('file_attachment')
                    ->select('FilePath', 'FileType')
                    ->where('file_attachment.SourceID', '=', $event->EventDetailsID)
                    ->get();
                  $event->filedetails = $filedetails;
            }

            $response['data'] = $eventdetails;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }

        return $response;
    }

    public function get_time_zone()
    {
        $timezoneIdentifiers = \DateTimeZone::listIdentifiers();
        $utcTime = new \DateTime('now', new \DateTimeZone('UTC'));

        $tempTimezones = array();
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new \DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = array(
                'offset' => (int)$currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            );
        }// Sort the array by offset,identifier ascending
        usort($tempTimezones, function ($a, $b) {
            return ($a['offset'] == $b['offset'])
                ? strcmp($a['identifier'], $b['identifier'])
                : $a['offset'] - $b['offset'];
        });

        $timezoneList = array();
        $i = 0;
        foreach ($tempTimezones as $tz) {
            $sign = ($tz['offset'] > 0) ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$i]['identifier'] = $tz['identifier'];
            $timezoneList[$i]['UTC'] = '(UTC ' . $sign . $offset . ') ' . $tz['identifier'];
            $i++;
        }
        if (!empty($timezoneList)) {
            $response['data'] = $timezoneList;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_event_alerts()
    {
        $event_alert = DB::table('event_alert_types')->select('AlertTypeID', 'AlertType')->get();

        if (!empty($event_alert)) {
            $response['data'] = $event_alert;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }

    public function get_event_repeats()
    {
        $event_repeat = DB::table('event_repeat_types')->select('RepeatTypeID', 'RepeatType')->get();
        if (!empty($event_repeat)) {
            $response['data'] = $event_repeat;
            $response['code'] = 200;
        } else {
            $response['data'] = "No record exists";
            $response['code'] = 400;
        }
        return $response;
    }



}