<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Users\Customers;
use App\Repositories\Users\UserRepository;
use Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Users\User;
use App\Models\Password\RandomPassword;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;
use Validator;
use Hash;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\Users\CustomerAddress;
use Authorizer;
use Carbon\Carbon;
use App\Models\Users\CustomerType;
use App\Models\Users\Salary_Structure;
use App\Models\Users\Monthly_Salary;
use App\Models\Users\Salary_Users;

class FileController extends BaseController
{

    protected $userRepository;

    public function __construct(Request $request, UserRepository $userRepository)
    {
        parent::__construct($request);

        $this->userRepository = $userRepository;
        $this->request = $request;
    }



    public function importExportExcelORCSV(){
        return view('file_import_export');
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = array('id','total','salary_user_id','pay_days','month','year') ;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if ($header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
           }
            fclose($handle);
        }

        return $data;
    }

    public function importFileIntoDB(Request $request){

        if($request->hasFile('LoadCustomersData')){
            $path = $request->file('LoadCustomersData')->getRealPath();
            $data = Excel::load($path)->get();
            $totalRows = $data->count();
            $customerArr = $this->csvToArray(Input::file('LoadCustomersData'));
            $record_count = 0;
            $fail_count=null;
//            foreach ($customerArr as $memu) {
//                $result[]= $memu['EmailID'];


//            $length = count($result);
//            for ($k = 0; $k < $length; $k++) {
//                $rse = DB::table('customers')
//                    ->where('EmailID', '=', $result[$k])
//                    ->get();

//                if (!empty($rse)) {
//                    $j = '';
//                    $total[]=$array[$j]['EmailID'] = $rse;
//                    $country=$array[$j]['error'] = 'Email Id Already Exist';
//                    $fail_count++;
//                    $j++;

//                } else {
//
//                    $cust_address= CustomerAddress::Create($customerArr[$k]);
//                    $customers_table_inputs = array(
//                        'PrincipalAddressID'=>$cust_address->id,
//                        'BillToAddressID'=>$cust_address->id,
//                        'ShipToAddressID'=>$cust_address->id,
//                    );

//                    $customer_table_inputs = array_merge($customerArr[$k],$customers_table_inputs);
                    $tem = CustomerAddress::Create($customerArr);
                        $record_count++;
                    }


//           if($record_count==0)
//            {
//               // dd($fail_count.' Records fail to Insert due to Email Id Already Exist.');
//                return Response::json([
//                    'collection' => [
//                        'version' => '1.0',
//                        'href' => $this->request->url(),
//                        'data' => $fail_count.' Records fail to Insert due to Email Id Already Exist.'
//                    ]
//                ], 400);
//
//            }
           // else{
               // dd($record_count.' Record Inserted successfully.');

                return Response::json([
                    'collection' => [
                        'version' => '1.0',
                        'href' => $this->request->url(),
                        'data' => $record_count.' Record Inserted successfully.'
                    ]
                ], 200);

            }



        //dd('Request data does not have any files to import.');
//        return Response::json([
//            'collection' => [
//                'version' => '1.0',
//                'href' => $this->request->url(),
//                'data' => 'Request data does not have any files to import.'
//            ]
//        ], 400);




    public function createUser(){

        $input=Input::all();
        $salary_users=Salary_Users::create([
            'email'=>$input['email'],
            'username'=>$input['username'],
            'password'=>$input['password'],
            'name'=>$input['name'],
        ]);

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $salary_users.' Record Inserted successfully.'
            ]
        ], 200);

    }


    public function excuteCharges(){

        $input=Input::all();
        $charges=Salary_Structure::create([
            'salary_user_id'=>$input['salary_user_id'],
            'basics'=>$input['basics'],
            'HRA'=>$input['HRA'],
            'DA'=>$input['DA']
        ]);

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $charges.' Record Inserted successfully.'
            ]
        ], 200);

    }

    public function processChanrges(){

        $salary_structure=DB::table('salary_structure')
            ->select('salary_structure.id','salary_structure.salary_user_id','salary_structure.basics',
                'salary_structure.HRA','salary_structure.DA','salary_users.email')
            ->join('salary_users','salary_users.id','=','salary_structure.user_id')
            ->get();

        $salary_process=DB::table('salary_process')
            ->select('salary_process.*')
            ->get();

        //get data from salary structure table
        $basic=$salary_structure->basics;
        $hra=$salary_structure->HRA;
        $da=$salary_structure->DA;

        //get data from salary_process table
        $month=$salary_process->month;
        $year=$salary_process->year;
        $pay_days =$salary_process->pay_days;

        //calculate salary basis on below formulaa
        $calcBasics = $basic*$pay_days/31;
        $calcHRA = $hra*$pay_days/31;
        $calcDA = $da*$pay_days/31;

        //create monthly calculation in this table
        $monthly_salary=Monthly_Salary::create([
            'basics'=>$calcBasics,
            'HRA'=>$calcHRA,
            'DA'=>$calcDA,
            'month'=>$month,
            'year'=>$year,
        ]);

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $monthly_salary.' Record Inserted successfully.'
            ]
        ], 200);
    }

    public function importFile(Request $request)
    {
        if ($request->hasFile('LoadEmpData')) {
            $path = $request->file('LoadEmpData')->getRealPath();
            $data = Excel::load($path)->get();
            $totalRows = $data->count();
            $customerArr = $this->csvToArray(Input::file('LoadEmpData'));
            $record_count = 0;
            $fail_count=null;

            $cust_address= CustomerAddress::Create($customerArr);
            $record_count++;
            return Response::json([
                'collection' => [
                    'version' => '1.0',
                    'href' => $this->request->url(),
                    'data' => $record_count.' Record Inserted successfully.'
                ]
            ], 200);
        }
    }

    public function GetEmployeeExcelSheetDemo()
    {
        $sheet = file_get_contents(base_path() . '/database/seeds/Files/LoadEmpData.csv');
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="LoadEmpData.csv"',
        );
        return Response::make(rtrim($sheet, "\n"), 200, $headers);
    }

    public function downloadEmpExcelFile($type){
        $products = Customers::get()->toArray();
        return Excel::create('LoadEmpData', function($excel) use ($products) {
            $excel->sheet('sheet name', function($sheet) use ($products)
            {
                $sheet->fromArray($products);
            });
        })->download($type);
    }

    public function downloadExcelFile($type){
        $products = Customers::get()->toArray();
        return Excel::create('LoadCustomersData', function($excel) use ($products) {
            $excel->sheet('sheet name', function($sheet) use ($products)
            {
                $sheet->fromArray($products);
            });
        })->download($type);
    }

    public function GetCustomersExcelSheetDemo()
    {
        $sheet = file_get_contents(base_path() . '/database/seeds/Files/LoadCustomersData.csv');
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="LoadCustomersData.csv"',
        );
        return Response::make(rtrim($sheet, "\n"), 200, $headers);
    }




}
