<?php

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;

use Authorizer;
use League\OAuth2\Server\Exception\InvalidCredentialsException;
use Mockery\CountValidator\Exception;
use Validator;
use Config;
use App\Models\Users\User;
use Response;
use AuthorizeNetSIM_Form;
use Illuminate\Support\Facades\View;
use net\authorize\api\contract\v1 as AnetAPI;

use Illuminate\Support\Facades\Password;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\OAuth\OAuthClient;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class OAuthController extends BaseController
{
    public function getLoginValidationRules()
    {
        return [
            'grant_type'    => 'required',
            'client_id'     => 'required',
            'client_secret' => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'scope'         => 'required',
        ];
    }

    public function login(Request $request)
    {
        $count = 0;
        $amount = 0;
        $payment_form ="";
        $clientData=DB::table('oauth_clients')->where('name', 'api')->first();

        $userScope=$this->checkUserScope(Input::get('username'));


        Input::merge([
            'client_id'     => "".$clientData->id,
            'client_secret' => "".$clientData->secret,
            'scope'         => $userScope
        ]);

        $credentials = $request->only(['grant_type', 'client_id', 'client_secret', 'username', 'password','scope']);


        $validationRules = $this->getLoginValidationRules();

        $credentials["client_id"]="".$clientData->id;
        $credentials["client_secret"]="".$clientData->secret;

        $this->validateOrFail($credentials, $validationRules);

        try {
            if (! $accessToken = Authorizer::issueAccessToken()) {
                return $this->response->errorUnauthorized();
            }
        }
        catch (\League\OAuth2\Server\Exception\OAuthException $e)
        {
            throw $e;
            return $this->response->error('could_not_create_token', 500);
        }

        $accessToken["groups"][]=$userScope;
        $request->headers->set('Authorization','Bearer '.$accessToken['access_token']);
        Authorizer::validateAccessToken();
        $userId = Authorizer::getResourceOwnerId();
        $userType=User::find($userId)->id;
        $name= DB::table('users')->where('id',$userType)->pluck('name');
        $organisation_id= DB::table('users')->where('id',$userType)->pluck('OrganisationID');
        $organisation_name= DB::table('organisation')->where('OrganisationID',$organisation_id)->pluck('OrganisationName');

        $userstatus= DB::table('users')->where('id',$userType)->pluck('status');
         if($userstatus[0] == 0 || $userstatus[0] == '-1' )
            return Response::json([
                'collection' => [
                    'version' => '1.0',
                    'href' => $request->url(),
                    'error' => [
                        'title' => 'Your account is blocked. Contact Support team to activate account.',
                        'code' => 400,
                        'message' => 'Your account is blocked. Contact Support team to activate account.',
                    ]
                ]
            ], 400);

        if($userScope == 'Administrator')
        {
            $organisation_details = DB::Select("SELECT * FROM organisation
                                    WHERE OrganisationID = ?", [$organisation_id[0]]);
            $count1 = DB::Select("SELECT  count(*) as count, SUM(Amount) as Amount, TransactionLogMonthlyID
                                  FROM transaction_log_monthly
                                  WHERE  MONTH(created_at) < MONTH(CURRENT_DATE)
                                  AND IsPaid = 0
                                  AND SourceID = ?", [$organisation_id[0]]);

            if(!empty($count1))
            {
                if($count1[0]->count==0)
                {
                    $count = 0;
                    $amount = 0;
                }
                else
                {

                    $transactionId = "200".$this->GenerateId(4);
                    $fp_timestamp = time();
                    $fp_sequence = "123" . time();
                    $url = $request->header('host');
                    $relay_Url = $url."/api/v1/payment/status";
                    $url_protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? 'https://' : 'http://';
                    $finalURL = $url_protocol.$relay_Url;
                    $fingerprint = AuthorizeNetSIM_Form::getFingerprint(env('AUTHORIZE_LOGIN_ID'), env('AUTHORIZE_TRANSACTION_KEY'), $count1[0]->Amount, $fp_sequence, $fp_timestamp);
                    $data = [
                        'api_login_id' => env('AUTHORIZE_LOGIN_ID'),
                        'transaction_key' => env('AUTHORIZE_TRANSACTION_KEY'),
                        'amount' => $count1[0]->Amount,
                        'transactionId' => $transactionId,
                        'fingerprint' => $fingerprint,
                        'fp_timestamp' => $fp_timestamp,
                        'fp_sequence' => $fp_sequence,
                        'relay_url' => $finalURL,
                        'token' => $accessToken['access_token'],
                        'user_id' => $userId,
                        'org_id' => $organisation_id[0],
                        'transaction_log_id' =>$count1[0]->TransactionLogMonthlyID,
                        'org_name' => trim($organisation_details[0]->OrganisationName),
                        'org_address' => trim($organisation_details[0]->Address),
                        'org_city' => trim($organisation_details[0]->City),
                        'org_state' => trim($organisation_details[0]->State),
                        'org_country' => trim($organisation_details[0]->Country),
                        'org_zipcode' => trim($organisation_details[0]->ZipCode),
                        'org_email' => trim($organisation_details[0]->EmailID),
                        'org_phone' => trim($organisation_details[0]->Phoneno),
                        'redirecturl'=>trim($request->header('origin')),
                    ];

                    $form = View::make('transaction')->with($data);
                    $payment_form = str_replace(array("\n\r", "\n", "\r"), '', (string)$form);
                    $count = 1;
                    $amount = $count1[0]->Amount;
                }

            }
        }
        $accessToken['userable_id']=$userType;
        $accessToken['userId']=$userId;
        $accessToken['name']=$name;
        $accessToken['organisation_id']=$organisation_id;
        $accessToken['organisation_name']=$organisation_name;
        $accessToken['requiretopay']=$count;
        $accessToken['Amount']=$amount;
        $accessToken['Payment_form']=$payment_form;
        //$accessToken['val'] = base64_decode("UGF5bWVudCB0cmFuc2FjdGlvbiBjb21wbGV0ZWQgc3VjY2Vzc2Z1bGx5");

        return response()->json(compact('accessToken'));
    }

    public function getUserIdByEmail($email)
    {
        try
        {
            $user=User::where('email',$email)->firstOrFail();
            return $user;
        }
        catch(ModelNotFoundException $mnfex)
        {
            return $this->response->error('User Does Not Exists !', 404);
        }
        catch(\Exception $ex)
        {
            return $this->response->error('Error Occurred !', 500);
        }
    }

    public function checkUserScope($username)
    {
        try
        {
            if((User::where('username', '=', $username)->exists()))
            {
               $userId=User::where('username', '=', $username)->pluck('id');
//                $user=User::find($userId);
//                $groups=$user->groups;
//
//                return $groups[0]->name;
                $user=DB::table('users')
                    ->select('UserRole')
                    ->join('user_role','user_role.UserRoleID','=','users.UserRoleID')
                    ->where('users.id','=',$userId)
                    ->get();

                //dd($user);
                return $user[0]->UserRole;
            }
            else
            {
                return "empty";
            }
        }
        catch(\Exception $ex)
        {
            return $this->response->error('Error Occurred : '.$ex->getMessage(), 404);
        }


    }

    public function GenerateId($n)
    {
        $unambiguousCharacters = "0123456789";
        $temporaryPassword = "";
        // build a temporary password consisting of 8 unambiguous characters
        for ($i = 0; $i < $n; $i++) {
            $temporaryPassword .= $unambiguousCharacters[rand(0, strlen($unambiguousCharacters) - 1)];
        }
        return $temporaryPassword;
    }


}
