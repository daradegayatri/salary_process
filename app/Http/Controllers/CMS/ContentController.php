<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BaseController;
use App\Repositories\CMS\ContentRepository;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;

class ContentController extends BaseController
{
	public function __construct(Request $request, ContentRepository $contentRepository)
    {
    	$this->request = $request;
        $this->contentRepository = $contentRepository;
    }

    public function getContentByValue($categoryName)
    {
    	$content = $this->contentRepository->getContentByValue($categoryName);
    	return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $content['data']
            ]
        ], $content['code']);
    }

    public function updateContent($ContentId)
    {
    	$input = Input::all();
    	$content = $this->contentRepository->updateContent($ContentId, $input);
    	return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $content['data']
            ]
        ], $content['code']);
    }
}
