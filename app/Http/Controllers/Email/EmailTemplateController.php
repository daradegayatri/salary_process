<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BaseController;
use App\Repositories\Email\EmailTemplateRepository;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;

class EmailTemplateController extends BaseController
{
    public function __construct(Request $request, EmailTemplateRepository $emailtemplaterepository)
    {
    	$this->request = $request;
        $this->emailtemplaterepository = $emailtemplaterepository;
    }

    public function all()
    {
        $emailTemplate = $this->emailtemplaterepository->all();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $emailTemplate['data']
            ]
        ], $emailTemplate['code']);
    }

    public function create()
    {
    	$input = Input::all();
        $emailTemplate = $this->emailtemplaterepository->create($input);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $emailTemplate['data']
            ]
        ], $emailTemplate['code']);
    }

    public function findById($emailTemplateId)
    {
        $emailTemplate = $this->emailtemplaterepository->findById($emailTemplateId);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $emailTemplate['data']
            ]
        ], $emailTemplate['code']);
    }

    public function update($emailTemplateId)
    {
    	$input = Input::all();
        $emailTemplate = $this->emailtemplaterepository->update($emailTemplateId, $input);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $emailTemplate['data']
            ]
        ], $emailTemplate['code']);
    }

    public function delete($emailTemplateId)
    {
        $emailTemplate = $this->emailtemplaterepository->delete($emailTemplateId);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $emailTemplate['data']
            ]
        ], $emailTemplate['code']);
    }

    // Get all email images
    /*public function getTemplateImages()
    {
        $image = $this->emailtemplaterepository->getTemplateImages();
        $serverName = $_SERVER['SERVER_NAME'];
        return Response::json([
            'collection' => [
                'resourceType' => 'Images',
                'currentFolder' => [
                	'url' => $serverName.'/images/',
                ],
                'files' => $image['files']
            ]
        ], $image['code']);
    }*/

    public function storeImages()
    {
        $image = $this->emailtemplaterepository->storeImages();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $image['data']
            ]
        ], $image['code']);
    }

    public function sendCustomeEmail()
    {
        $input = Input::all();
        //$input['url'] = $this->request->getHost();
        $email = $this->emailtemplaterepository->sendCustomeEmail($input);
        return $email;
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $email['data']
            ]
        ], $email['code']);
    }

    public function EmailResponse()
    {
        $input = Input::all();
        //$input['url'] = $this->request->getHost();
        $email = $this->emailtemplaterepository->EmailResponse($input);

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $email['data']
            ]
        ], $email['code']);
    }
}
