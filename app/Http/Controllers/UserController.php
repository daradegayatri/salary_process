<?php

namespace App\Http\Controllers;

use App\Repositories\Users\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Models\Users\User;
use Illuminate\Support\Facades\Input;
use App\Repositories\Users\UserRepositoryInterface;
use App\Http\Requests\CreateUserRequest;
class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $userRepository;

    public function __construct(Request $request, UserRepository $userRepository)
    {
        parent::__construct($request);

        $this->userRepository = $userRepository;
        $this->request = $request;
    }
    public function getAllUsers()
    {
        $users = $this->userRepository->get_Parent_Organisation_Users();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $users['data']
            ]
        ], $users['code']);
    }
    public function get_Sub_Organisation_Users($org_id)
    {
        $users = $this->userRepository->get_Sub_Organisation_Users($org_id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $users['data']
            ]
        ], $users['code']);
    }
    public function createuser(Request $request)
    {
        $input = Input::all();
        $url = $request->header('origin');
        $user = $this->userRepository->createuser($input, $url);
        //$this->code = 201;
        //return $user;

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function updateuser($id)
    {
        $input = Input::all();
        $user = $this->userRepository->updateuser($id);
        //$this->code = 201;
        //return $user;

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function get_userroles()
    {
        //$input = Input::all();
        $userrole = $this->userRepository->get_userroles();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userrole['data']
            ]
        ], $userrole['code']);
    }
    public function get_userrole_by_id($id)
    {

        $userrole = $this->userRepository->get_userrole_by_id($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userrole['data']
            ]
        ], $userrole['code']);
    }
    public function get_userrole_by_name($userrole)
    {

        $userrole = $this->userRepository->get_userrole_by_name($userrole);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userrole['data']
            ]
        ], $userrole['code']);
    }
    public function get_supervisors_list()
   {
    $supervisorslist = $this->userRepository->get_supervisors_list();
    return Response::json([
        'collection' => [
            'version' => '1.0',
            'href' => $this->request->url(),
            'data' => $supervisorslist['data']
        ]
    ], $supervisorslist['code']);
}
    public function get_employees_list()
    {

        $employeeslist = $this->userRepository->get_employees_list();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $employeeslist['data']
            ]
        ], $employeeslist['code']);
    }
    public function get_employees_list_bysupervisorid($id)
    {
        $employeeslist = $this->userRepository->get_employees_list_bysupervisorid($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $employeeslist['data']
            ]
        ], $employeeslist['code']);
    }
    public function get_workers_list()
    {
        $workerslist = $this->userRepository->get_workers_list();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $workerslist['data']
            ]
        ], $workerslist['code']);
    }
    public function get_userdetails_by_id($id)
    {

        $userdetails = $this->userRepository->get_userdetails_by_id($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function deleteuser()
    {
        $input = Input::all();
        $userdetails = $this->userRepository->deleteuser($input['ids']);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function get_notification_by_userid($id)
    {
        $userdetails = $this->userRepository->get_notification_by_userid($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function get_hidden_notifications_by_userid($id)
    {
        $userdetails = $this->userRepository->get_hidden_notifications_by_userid($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function get_unread_notifications_count($id)
    {
        $userdetails = $this->userRepository->get_unread_notifications_count($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function change_notification_status($id,$status)
    {
        $notificationdetails = $this->userRepository->change_notification_status($id,$status);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $notificationdetails['data']
            ]
        ], $notificationdetails['code']);
    }
    public function hide_notification($id)
    {
        $userdetails = $this->userRepository->hide_notification($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function delete_notification($id)
    {
        $userdetails = $this->userRepository->delete_notification($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function forgotpassword()
    {
        $input = Input::all();
        $userdetails = $this->userRepository->forgotpassword($input);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $userdetails['data']
            ]
        ], $userdetails['code']);
    }
    public function dashboard_details($year,$graphfor)
    {
        $dashboard_details = $this->userRepository->dashboard_details($year,$graphfor);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $dashboard_details['data']
            ]
        ], $dashboard_details['code']);
    }
    public function get_all_user_details_of_organisation($usertype,$organisationid)
    {
        $customer_list = $this->userRepository->get_all_user_details_of_organisation($usertype,$organisationid);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_customers()
    {
        $customer_list = $this->userRepository->get_customers();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_customers_stages()
    {
        $customers_stages = $this->userRepository->get_customers_stages();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customers_stages['data']
            ]
        ], $customers_stages['code']);
    }
    public function get_customerslist()
    {
        $customer_list = $this->userRepository->get_Parent_Organisation_Customers();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_Sub_Organisation_Customers($org_id)
    {
        $customer_list = $this->userRepository->get_Sub_Organisation_Customers($org_id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_customerlist_for_child_customer($id)
    {
        $customer_list = $this->userRepository->get_customerlist_for_child_customer($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_customers_by_id($id)
    {
        $customer_details = $this->userRepository->get_customers_by_id($id);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_details['data']
            ]
        ], $customer_details['code']);
    }
    public function create_customer()
    {
        $input = Input::all();
        $user = $this->userRepository->create_customer($input);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function create_lead_customer($organizationId)
    {
        $input = Input::all();
        $user = $this->userRepository->CreateLeadCustomer($input, $organizationId);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function delete_customers_by_id()
    {
        $input = Input::all();
        $customers = $this->userRepository->delete_customers_by_id($input['ids']);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customers['data']
            ]
        ], $customers['code']);
    }
    public function update_customers_by_id($id)
    {
        $input = Input::all();
        $user = $this->userRepository->update_customers_by_id($id);

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function get_all_customer_type()
    {
        $input = Input::all();
        $user = $this->userRepository->get_all_customer_type();

        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $user['data']
            ]
        ], $user['code']);
    }
    public function get_social_media_types()
    {
        $customer_list = $this->userRepository->get_social_media_types();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $customer_list['data']
            ]
        ], $customer_list['code']);
    }
    public function get_dashboard_category_list()
    {
        $dashboard_category_list = $this->userRepository->get_dashboard_category_list();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $dashboard_category_list['data']
            ]
        ], $dashboard_category_list['code']);
    }
    public function get_organisation($OrganisationID)
    {
        $organisation = $this->userRepository->get_all_organisation($OrganisationID);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $organisation['data']
            ]
        ], $organisation['code']);


    }
    public function get_all_organisation_list()
    {
        $organisation = $this->userRepository->get_all_organisation_list();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $organisation['data']
            ]
        ], $organisation['code']);


    }
    public function get_all_organisation_details()
    {
        $organisation = $this->userRepository->get_all_organisation_details();
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $organisation['data']
            ]
        ], $organisation['code']);


    }
    public function UpdatePassword(Request $request)
    {

        $input = Input::all();
        $updatepassword = $this->userRepository->UpdatePassword($input);
        return Response::json([
            'collection' => [
                'version' => '1.0',
                'href' => $this->request->url(),
                'data' => $updatepassword['data']
            ]
        ], $updatepassword['code']);


    }
}
