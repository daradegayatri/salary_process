<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    dd('workorder');
});
Route::get('qbo/connect','QuickBookController@connect');
Route::get('qbo/oauth','QuickBookController@qboOauth');
Route::get('qbo/success','QuickBookController@qboSuccess');
Route::get('qbo/disconnect','QuickBookController@qboDisconnect');
Route::get('qbo/createCustomer','QuickBookController@createCustomer');
Route::get('qbo/addItem','QuickBookController@addItem');
Route::get('qbo/addInvoice','QuickBookController@addInvoice');
Route::get('get/form','CustomersDataLoaderController@showForm');
Route::get('fbo/oauth','FreshBooksController@fobOath');
Route::get('fbo/refreshToken','FreshBooksController@refreshToken');
Route::get('fbo/createInvoice','FreshBooksController@createInvoice');
Route::get('fbo/createCustomer','FreshBooksController@createCustomer');
Route::get('fbo/createItems','FreshBooksController@createItems');
Route::get('fbo/disconnect','FreshBooksController@disconnect');
Route::get('tally/export','TallyController@export');
Route::get(
    '/chatfunction', ['as' => 'home', function () {
        return response()->view('index');
    }] );

Route::get(
    '/email_reply', ['as' => 'home', function () {
        return response()->view('email_reply');
    }] );








$api = app('Dingo\Api\Routing\Router');

$api->version('v1',['prefix' => 'api/v1'], function ($api) {
    //--------------------Post Routes------------------------------------//
    //login
    $api->post('login','App\Http\Controllers\OAuth\OAuthController@login');//1

    $api->post('forgotpassword','App\Http\Controllers\UserController@forgotpassword');//2



        $api->post('salary_users','App\Http\Controllers\FileController@createUser');
        $api->post('salary_structure','App\Http\Controllers\FileController@excuteCharges');
        $api->post('salary_process','App\Http\Controllers\FileController@processChanrges');
        $api->post('monthly_salary','App\Http\Controllers\FileController@salary_process');

    $api->post('/thankyoupage', ['as' => 'home', function () {
        return response()->view('contract');
    }

        ]);


    //------------------------------Get Excel File Upload---------------------------------------------//

     $api->get('download/customers/Sheet','App\Http\Controllers\FileController@GetCustomersExcelSheetDemo');  // Excel Sheet Download
     $api->get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'App\Http\Controllers\FileController@importExportExcelORCSV')); // form for uploading excel xls,excel xlsx,csv,
     $api->get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'App\Http\Controllers\FileController@downloadExcelFile'));  // Excel file Export
     $api->get('download/Employee/Sheet','App\Http\Controllers\FileController@GetEmployeeExcelSheetDemo');
     $api->get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'App\Http\Controllers\FileController@downloadEmpExcelFile'));



    //--------------------Get Routes------------------------------------//
});


$api->version('v1', ['prefix' => 'api/v1','middleware' => 'api.auth','providers' => ['oauth'],'scopes' => ['supervisor','administrator','employee','contractor','sales representative']],
    function ($api) {

        //--------------------------------Post Excel file Upload------------------------------------------------------//

        $api->post('import-csv-excel', array('as' => 'import-csv-excel', 'uses' => 'App\Http\Controllers\FileController@importFileIntoDB'));
        $api->post('import-excel', array('as' => 'import-excel', 'uses' => 'App\Http\Controllers\FileController@importFile'));


    });



