<?php
/**
 * Created by PhpStorm.
 * User: Deepika
 * Date: 7/26/2017
 * Time: 12:41 PM
 */

namespace App\Helpers;
use DB;
use App\Models\General\Notifications;
use App\Models\Transactions\TransactionLog;
use App\Models\Transactions\TransactionSource;
use App\Models\Transactions\TransactionDetails;
use App\Models\Transactions\TransactionCharges;
use App\Models\Files\Fileattachment;
use App\Models\Files\Filesource;
use Illuminate\Support\Facades\Storage;
use App\Models\General\Eventdetails;


class Helpers
{

    public static function create_notification($userId, $sourceid, $orgid, $transactionsource)
    {
        // Notifications
        $createdby = DB::table('users')
            ->select('name', 'id', 'AssignedTo', 'UserRoleID')
            ->where('id', '=', $userId)
            ->where('IsDeleted', '=', null)
            ->get();
        if (!empty($createdby)) {
            if ($transactionsource == 'order' || $transactionsource == 'task')
                $notificationdata = $createdby[0]->name . " has created new " . $transactionsource . " with  ID " . $sourceid;
            if ($transactionsource == 'customer')
                $notificationdata = "New customer has been added with type LEAD, Customer ID:  " . $sourceid;
            if ($transactionsource == 'invoice')
                $notificationdata = $createdby[0]->name . " has raised new " . $transactionsource . " Invoice ID: " . $sourceid;

            if ($createdby[0]->AssignedTo != '0') {
                $notification = Notifications::create([
                    'Notification' => $notificationdata,
                    'NotificationFor' => $createdby[0]->AssignedTo,
                    'SourceID' => $sourceid,
                    'SourceName' => $transactionsource,
                    'IsRead' => '0',
                    'Status' => '0',
                    'CreatedBy' => $userId,
                ]);
            }
            if ($createdby[0]->UserRoleID != '1') {
                $organisation_details = DB::table('users')
                    ->select('name', 'id', 'AssignedTo')
                    ->where('OrganisationID', '=', $orgid)
                    ->where('UserRoleID', '=', '1')
                    ->get();

                $notification = Notifications::create([
                    'Notification' => $notificationdata,
                    'NotificationFor' => $organisation_details[0]->id,
                    'SourceID' => $sourceid,
                    'SourceName' => $transactionsource,
                    'IsRead' => '0',
                    'Status' => '0',
                    'CreatedBy' => $userId,
                ]);
            }
        }
    }


    public static function transactiondetails($sourceid, $orderid, $customerid, $orgid, $transactionsourceid)
    {

        $month = date('M');
        $year = date('Y');
        $transactiondetails = Db::table('transaction_log_monthly')
            ->where('TransactionMonth', $month)
            ->where('TransactionYear', $year)
            ->where('SourceID', $orgid)
            ->get();


        if (!empty($transactiondetails)) {
            $no_of_transactions_update = ($transactiondetails[0]->NoOfTransaction) + 1;
            $no_of_transactions = ($transactiondetails[0]->NoOfTransaction);


            $transactiondetailsid = $transactiondetails[0]->TransactionLogMonthlyID;
            $amount = $transactiondetails[0]->Amount;

            $transaction_charges = Db::table('transaction_charges')
                ->where('TransactionSourceID', '=', $transactionsourceid)
                ->where('TransactionNo_RangeFrom', '<', ($no_of_transactions))
                ->where('TransactionNo_RangeTo', '>', ($no_of_transactions))
                ->where('OrganisationID', '=', $orgid)
                ->get();
            if(empty($transaction_charges))
            {
                $transaction_charges = Db::table('transaction_charges')
                    ->where('TransactionSourceID', '=', $transactionsourceid)
                    ->where('TransactionNo_RangeFrom', '<', ($no_of_transactions))
                    ->where('TransactionNo_RangeTo', '>', ($no_of_transactions))
                    ->where('OrganisationID', '=', 'null')
                    ->get();

                $orgid = "";
            }
            if (!empty($transaction_charges)) {
                $transactioncharges_id = $transaction_charges[0]->TransactionChargeID;
                $transactionpercharge = $transaction_charges[0]->TransactionCharge;
                $min_monthlyamount = $transaction_charges[0]->Min_Monthly_Amt;

                $result = DB::table('transaction_charges')
                    ->select('TransactionNo_RangeFrom', 'TransactionNo_RangeTo')
                    ->where('OrganisationID', '=', $orgid)
                    ->get();

                if (count($result) > 0) {
                    $total = DB::table('transaction_charges')
                        ->where('TransactionSourceID', '=', $transactionsourceid)
                        ->where('TransactionNo_RangeFrom', '<', ($no_of_transactions))
                        ->where('TransactionNo_RangeTo', '>', ($no_of_transactions))
                        ->where('OrganisationID', '=', $orgid)
                        ->select('TransactionNo_RangeFrom','TransactionNo_RangeTo')
                        ->get();
                    if($total[0]->TransactionNo_RangeTo >$no_of_transactions)
                    {
                        if($total[0]->TransactionNo_RangeFrom < $no_of_transactions) {
                            $no_of_transaction = $no_of_transactions - 500;

                            if ($no_of_transactions < 500) {
                                $amount = $min_monthlyamount;
                                $transactioncharges_id = '';

                            }
                            if ($no_of_transactions > 0) {
                                $query = DB::select(DB::raw("select * from transaction_charges where TransactionNo_RangeTo >=500 and OrganisationID = $orgid and TransactionSourceID = $transactionsourceid"));
                            }

                            if ($no_of_transactions > 500) {

                                $amt = $no_of_transaction - $query[0]->TransactionNo_RangeTo;
                                $transaction_log_monthly_table_amount = DB::select(DB::raw("select * from transaction_log_monthly where SourceID = $orgid"));

                                if ($amt > 0) {
                                    $pay = $amt * $query[0]->TransactionCharge;
                                    $amount = $pay + $transaction_log_monthly_table_amount[0]->Amount;

                                }
                                else{

                                    $res=$no_of_transaction+1;
                                    $pay = $res * $query[0]->TransactionCharge;
                                    $amount = $pay + $transaction_log_monthly_table_amount[0]->Amount;
                                }

                            }

                            if ($no_of_transactions > 0) {
                                $q = DB::select(DB::raw("select * from transaction_charges where TransactionNo_RangeFrom >=500 and OrganisationID = $orgid and TransactionSourceID = $transactionsourceid"));

                                if (!empty($q)) {
                                    if ($no_of_transactions> 500) {
                                        $amt = $no_of_transaction - $q[0]->TransactionNo_RangeFrom;
                                        $transaction_log_monthly_table_amount = DB::select(DB::raw("select * from transaction_log_monthly where SourceID = $orgid"));
                                        if ($amt > 0) {
                                            $pay = $amt * $q[0]->TransactionCharge;
                                            $amount = $pay + $transaction_log_monthly_table_amount[0]->Amount;
                                        }
                                        else{
                                            $res=$no_of_transaction+1;
                                            $pay = $res * $query[0]->TransactionCharge;

                                            $amount = $pay + $transaction_log_monthly_table_amount[0]->Amount;
                                        }
                                    }
                                }
                            }

                        }

                    }

                }
                DB::table('transaction_log_monthly')->where('TransactionLogMonthlyID', '=', [$transactiondetailsid])
                    ->update(array('Amount' => $amount,
                        'NoOfTransaction' => $no_of_transactions_update,
                        'PerTransactionCharge' => $transactionpercharge,
                        'TransactionMonth' => $month,
                        'TransactionYear' => $year,
                        'TransactionSourceID' => '2',
                        'SourceID' => $orgid,
                        'IsPaid' => '0',
                        'created_at' => date("Y-m-d H:i:s")));

                $transactiondetails = TransactionDetails::create([
                    'OrganisationID' => $orgid,
                    'CustomerID' => $customerid,
                    'OrderID' => $orderid,
                    'TransactionSourceID' => $transactionsourceid,
                    'TransactionChargesID' => $transactioncharges_id,
                    'SourceID' => $sourceid,
                    'IsPaid' => '0',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
            }
        } else {
            $transaction_charges = DB::table('transaction_charges')
                ->where('TransactionSourceID', '=', $transactionsourceid)
                ->where('TransactionNo_RangeFrom', '=', '0')
                ->where('OrganisationID', '=', $orgid)
                ->get();
            if(empty($transaction_charges))
            {
                $transaction_charges = Db::table('transaction_charges')
                    ->where('TransactionSourceID', '=', $transactionsourceid)
                    ->where('TransactionNo_RangeFrom', '=', '0')
                    ->where('OrganisationID', '=', 'null')
                    ->get();

                $orgid = "";
            }
            if (!empty($transaction_charges)) {
                $transactioncharges_id = $transaction_charges[0]->TransactionChargeID;
                $transactionpercharge = $transaction_charges[0]->TransactionCharge;
                $min_monthlyamount = $transaction_charges[0]->Min_Monthly_Amt;
                //id, pertransactioncharge, transactionmonth, transactionyear, transactionsourceid, amount,
                // source_id, no_of_transaction, ispaid, created_at, updated_at
                $no_of_transactions = '1';
                $amount = ($transactionpercharge * $no_of_transactions) + $min_monthlyamount;
                $transaction_monthly_log = TransactionLog::create([
                    'PerTransactionCharge' => $transactionpercharge,
                    'TransactionMonth' => $month,
                    'TransactionYear' => $year,
                    'TransactionSourceID' => $transactionsourceid,
                    'SourceID' => $orgid,
                    'IsPaid' => '0',
                    'Amount' => $amount,
                    'NoOfTransaction' => $no_of_transactions,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);

                $transactiondetails = TransactionDetails::create([
                    'OrganisationID' => $orgid,
                    'CustomerID' => $customerid,
                    'OrderID' => $orderid,
                    'TransactionSourceID' => $transactionsourceid,
                    'TransactionChargesID' => $transactioncharges_id,
                    'SourceID' => $sourceid,
                    'IsPaid' => '0',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
            }
        }
        return true;
    }


    public static function fileattachment($fileattachment_details,$sourceid,$createdby,$filesourceid)
    {
        $file_count = count($fileattachment_details);
        if ($file_count != 0) {
            $filetype = array();
            $filedata = $fileattachment_details;
            $destinationpath = public_path('images');
            for ($i = 0; $i < $file_count; $i++) {
                if (isset($filedata[$i])) {

                    $filetype[$i]['originalname'] = $filedata[$i]->getClientOriginalName();
                    //dd($filedata[$i]);
                    $filetype[$i]['realpath'] = $filedata[$i]->getRealPath();
                    $filetype[$i]['originalextension'] = $filedata[$i]->getClientOriginalExtension();
                    $filetype[$i]['mimetype'] = $filedata[$i]->getMimeType();
                    $filetype[$i]['filesize'] = $filedata[$i]->getSize();
                    $filetype[$i]['name'] = trim($filetype[$i]['originalname']);
                    $filepath = $filedata[$i]->move($destinationpath, time() . $filetype[$i]['name']);
                    //$filepath = $destinationpath."/".$filetype[$i]['originalname'];
                    $filetype[$i]['filepath'] = $filepath->getRealPath();
                    $filepath = asset('images/' . time() . $filetype[$i]['name']);
                } else
                    $filetype[$i] = "null";

                $fileattachment = Fileattachment::create([
                    'FileType' => $filetype[$i]['mimetype'],
                    'FilePath' => $filepath,//$filetype[$i]['name'],
                    'FileSourceID' => $filesourceid,
                    'SourceID' => $sourceid,
                    'CreatedBy' => $createdby,
                ]);
            }
        }
    }
    public static function delete_file($delete_file_details)
    {
        $delete_file_count = count($delete_file_details);
        for ($i = 0; $i < $delete_file_count; $i++) {
            $deletefile = DB::table('file_attachment')
                ->where('FileAttachmentID', '=', $delete_file_details[$i]['id'])
                ->select('*')
                ->get();
            if (!empty($deletefile)) {
                $filename = explode("images/", $deletefile[0]->FilePath);
                $link = public_path('images/');
                $filepath = $link . $filename[1];
                $fileattachmentid = $delete_file_details[$i]['id'];
                unlink($filepath);
                DB::select(DB::raw("DELETE FROM `file_attachment` WHERE  FileAttachmentID = $fileattachmentid "));
            }
        }
    }
    public static function create_ical_event($event_details)
   {
       $ical = "";
       $filename = preg_replace('/\s+/', '', $event_details['event_title']."_".$event_details['taskid']."_".time().".ics");
       $dtstart = gmdate('Ymd\THis\Z', strtotime( $event_details['start_date'].$event_details['start_time'] ));
       $dtend = gmdate('Ymd\THis\Z', strtotime( $event_details['end_date'].$event_details['end_time'] ));
       $location = $event_details['event_location'];
       $titulo_invite = $event_details['event_title'];
       $organizer = "CN=".$event_details['event_organizer']."name:".$event_details['organizer_mail'];
       $timezone = $event_details['timezone'];
       // ICS
       $mail[0]  = "BEGIN:VCALENDAR";
       $mail[1] = "PRODID:-//Google Inc//Google Calendar 70.9054//EN";
       $mail[2] = "VERSION:2.0";
       $mail[3] = "CALSCALE:GREGORIAN";
       $mail[4] = "METHOD:REQUEST";
       $mail[5] = "BEGIN:VEVENT";
       $mail[6] = "DTSTART;TZID=$timezone:" . $dtstart;
       $mail[7] = "DTEND;TZID=$timezone:" .  $dtend;
       $mail[8] = "DTSTAMP;TZID=$timezone:" . gmdate('Ymd\THis\Z');
       $mail[9] = "UID:" . date('Ymd').'T'.date('His').'-'.rand();
       $mail[10] = "ORGANIZER;" . $organizer;
       $mail[11] = "CREATED:" . gmdate('Ymd\THis\Z');
       $mail[12] = "DESCRIPTION:" . strip_tags($event_details['event_description']);
       $mail[13] = "LAST-MODIFIED:" . gmdate('Ymd\THis\Z');
       $mail[14] = "LOCATION:" . $location;
       $mail[15] = "SEQUENCE:0";
       $mail[16] = "STATUS:CONFIRMED";
       $mail[17] = "SUMMARY:" . $titulo_invite;
       $mail[18] = "TRANSP:OPAQUE";
       $mail[19] = "END:VEVENT";
       $mail[20] = "END:VCALENDAR";

       $mail = implode("\r\n", $mail);
       $ical = $mail;

       $destinationPath = public_path('images');
       //dd($destinationPath);
       if (!is_dir($destinationPath)) {
           mkdir(public_path('images'), 0777,true);
       }
       $filedes = public_path('images').'/'.$filename;

       header("text/calendar");
       file_put_contents($filedes, $ical);
       $filepath = asset('images/'.$filename);
       $filesourceid = '8';
       if($event_details['event_id'] == '0')
       {
           $event_exist_details = DB::table('event_details')
               ->where('StartDate', '=', $event_details['start_date'])
               ->where('StartTime', '=', $event_details['start_time'])
               ->where('EndDate', '=', $event_details['end_date'])
               ->where('EndTime', '=', $event_details['end_time'])
               ->where('CustomerID', '=', $event_details['customerid'])
               ->where('OrganisationID', '=', $event_details['orgid'])
               ->get();
           if(!empty($event_exist_details))
           {
               $fileattachment[] = "1";
               return $fileattachment;
           }


           $events = Eventdetails::create([
               'TaskID' =>$event_details['taskid'],
               'OrganisationID' =>$event_details['orgid'],
               'CustomerID' =>$event_details['customerid'],
               'StartDate' =>$event_details['start_date'],
               'StartTime' =>$event_details['start_time'],
               'EndDate' =>$event_details['end_date'],
               'EndTime' =>$event_details['end_time'],
               'Title' =>$event_details['event_title'],
               'Description' =>$event_details['event_description'],
               'Location' =>$event_details['event_location'],
               'Organizer' =>$event_details['event_organizer'],
               'AlertTypeID' =>$event_details['alert_type_id'],
               'RepeatTypeID' =>$event_details['repeat_type_id'],
               'TimeZone' =>$event_details['timezone'],
               'created_at' =>date("Y-m-d H:i:s"),
               'updated_at' =>date("Y-m-d H:i:s"),
           ]);
           $fileattachment = Fileattachment::create([
               'FileType' => "text/calendar",
               'FilePath' => $filepath,
               'FileSourceID' => $filesourceid,
               'SourceID' => $events->id,
               'CreatedBy' => $event_details['userid'],
           ]);
//dd($filename);
           if($event_details['customerid'] <> 0)
           {
               \Mail::send('emails_template', ['html_code' => $event_details['message']], function($message) use($event_details,$filedes,$ical,$titulo_invite,$filename)
               {
                   $message->subject($titulo_invite);
                   $message->to($event_details['receipent_mail']);
                   $message->attachData($ical,$filename,[
                             'mime' => 'text/calender',
                         ]);
               });
           }
       }
       else
       {
           $eventid = $event_details['event_id'];
           $event_update = DB::table('event_details')
               ->where('EventDetailsID', '=', $eventid)
               ->update(array(
                   'CustomerID' =>$event_details['customerid'],
                   'StartDate' =>$event_details['start_date'],
                   'StartTime' =>$event_details['start_time'],
                   'EndDate' =>$event_details['end_date'],
                   'EndTime' =>$event_details['end_time'],
                   'Title' =>$event_details['event_title'],
                   'Description' =>$event_details['event_description'],
                   'Location' =>$event_details['event_location'],
                   'Organizer' =>$event_details['event_organizer'],
                   'AlertTypeID' =>$event_details['alert_type_id'],
                   'RepeatTypeID' =>$event_details['repeat_type_id'],
                   'TimeZone' =>$event_details['timezone'],
                   'updated_at' =>date("Y-m-d H:i:s"),
               ));

      $event_file = DB::table('file_attachment')
          ->where('SourceID', '=', $eventid)
          ->get();
           if(!empty($event_file))
           {
               if(!empty($event_file[0]->FilePath))
               {
                   $filename = explode("images/",$event_file[0]->FilePath);
                   $link = public_path('images/');
                   $filepaths = $link.$filename[1];
                   unlink($filepaths);
               }
           }
           $fileattachment =  DB::table('file_attachment')
               ->where('SourceID', '=', $eventid)
               ->update(array(
               'FileType' => "text/calendar",
               'FilePath' => $filepath,//$filetype[$i]['name'],
               'FileSourceID' => $filesourceid,
               'SourceID' => $eventid,
               'CreatedBy' => $event_details['userid']
           ));
        $fileattachment = DB::table('file_attachment')
            ->where('SourceID', '=', $eventid)
            ->get();
       }

       $filenames = $filename[1];

       if($event_details['customerid'] <> 0)
       {
           \Mail::send('emails_template', ['html_code' => $event_details['message']], function($message) use($event_details,$filedes,$ical,$titulo_invite,$filenames)
           {
               $message->subject($titulo_invite);
               $message->to($event_details['receipent_mail']);
               $message->attachData($ical,$filenames,['mime' => 'text/calender' ]);
           });
       }
 return $fileattachment;
}






}