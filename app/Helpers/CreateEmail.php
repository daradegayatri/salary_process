<?php

namespace App\Helpers;
use App\Models\General\Sendmails;
use App\Models\Tasks\Tasklist;
use Carbon\Carbon;
use DB;
use App\Models\Users\User;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Illuminate\Support\Facades\Mail;

class CreateEmail {
	
	public function execute($input)
	{
		$emails = explode(",", $input['email']);
        foreach($emails as $email){
            if ($this->check_email($email)) {
                $user = DB::table('users')->select('id','FirstName','LastName','Description','jobtitle')->where('users.email','=',trim($email))->get();
                if (count($user)) {
					$customer = DB::table('customers')->where('UserID','=',$user[0]->id)->get();
                    $body = $input['html'];

                    if (count($customer)) {
                        $name = $customer[0]->FirstName. $customer[0]->LastName;
                        $customerId = $customer[0]->CustomerID;
                        $replacements = [
                            'First Name' => $customer[0]->FirstName,
                            'Middle Name' => $customer[0]->MiddleName,
                            'Last Name' => $customer[0]->LastName,
                            'Description' => $customer[0]->Description,
                            'Job Title' => $customer[0]->jobtitle,
                        ];
                    } else {
                        $name = $user[0]->FirstName. $user[0]->LastName;
                        $customerId = '';
                        $replacements = [
                            'First Name' => $user[0]->FirstName,
                            'Last Name' => $user[0]->LastName,
                            'Description' => $user[0]->Description,
                            'Job Title' => $user[0]->jobtitle,
                        ];
                    }

                    foreach ($replacements as $key => $value) {
                        $body = str_replace('%%'.$key.'%%', $value, $body);
                    }
                    $html = $this->removeHtmlTag($body);
                    
                    $userId = Authorizer::getResourceOwnerId();
                    $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
                    $task_list_count = Tasklist::count();

                    if($task_list_count == "0")
                        $tasklistid = "5000001";
                    else
                        $tasklistid = '';
                    $task_list = [
                        'TasklistID' => $tasklistid,
                        'OrganisationID' => $org_id[0],
                        'name' => $input['subject'],
                        'Description' => $html,
                        'HasAttachment' => 0,
                        'TaskTypeID' => 1,
                        'CustomerID' => $customerId,
                        'CreatedBy' => $userId
                    ];

                    $task = Tasklist::create($task_list);

                    $taskid  = $task->id;
                    $task_id = "TI".$taskid;
                    $task = Tasklist::where('TasklistID', '=', $task->id)->update(array('TaskID' => $task_id));
                    if(isset($input['url']))
                    {
                        $url = $input['url'];
                        $relay_Url = $url."?tid=".base64_encode($taskid);
                        $reply_link = '<p></p>  <a href='.$relay_Url. ' style="padding:05px 05px;font-size:14px;background:#057db9;color:#fff;text-decoration:none;border-radius:10px" target="_blank"> Click Here</a>  To Reply';
                        $body = $body.$reply_link;
                    }

                    $emails_table_input = array(
                        'Name' => $name,
                        'EmailID' => $email,
                        'Subject' => $input['subject'],
                        'Message' => $body,
                        'Status' => 0,
                        'HasAttachment' => 0,
                        'UserID' => $user[0]->id,
                        'MailFor' => 'Custom Email',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'CreatedBy' => $userId
                    );
                    $email = Sendmails::create($emails_table_input);
//                    Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
//                        $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
//                            ->subject($emails_table_input['Subject']);
//                    });
                } else {
                    $userId = Authorizer::getResourceOwnerId();
                    $org_id = User::where('id', '=', $userId)->pluck('OrganisationID');
                    $task_list_count = Tasklist::count();
                    $body = $input['html'];

                    $html = $this->removeHtmlTag($body);

                    if($task_list_count == "0")
                        $tasklistid = "5000001";
                    else
                        $tasklistid = '';
                    $task_list = [
                        'TasklistID' => $tasklistid,
                        'OrganisationID' => $org_id[0],
                        'name' => $input['subject'],
                        'Description' => $html,
                        'HasAttachment' => 0,
                        'TaskTypeID' => 1,
                        'CustomerID' => '',
                        'CreatedBy' => $userId
                    ];

                    $task = Tasklist::create($task_list);

                    $taskid  = $task->id;
                    $task_id = "TI".$taskid;
                    $task = Tasklist::where('TasklistID', '=', $task->id)->update(array('TaskID' => $task_id));
                    if(isset($input['url']))
                    {
                        $url = $input['url'];
                        $relay_Url = $url."?tid=".base64_encode($taskid);
                        $reply_link = '<p></p>  <a href='.$relay_Url. ' style="padding:05px 05px;font-size:14px;background:#057db9;color:#fff;text-decoration:none;border-radius:10px" target="_blank"> Click Here</a>  To Reply';
                        $body = $body.$reply_link;
                    }
                    // $url = $input['url'];
                    // $relay_Url = $url."?tid=".base64_encode($taskid);
                    // $body = $input['html'];
                    // $reply_link = '<p></p><a href='.$relay_Url.' style="padding:10px 20px;font-size:24px;background:#b90f37;color:#fff;text-decoration:none;border-radius:20px" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://localhost/referralsystem1/Invitation/accept/MTIz&amp;source=gmail&amp;ust=1503124300526000&amp;usg=AFQjCNGBvyLy6Nnl_TJUDtoNQjQR5OhWGQ">Click Here To Reply</a>';
                    // $body = $body.$reply_link;
                    $emails_table_input = array(
                        'Name' => $email,
                        'EmailID' => $email,
                        'Subject' => $input['subject'],
                        'Message' => $body,
                        'Status' => 0,
                        'HasAttachment' => 0,
                        'UserID' => null,
                        'MailFor' => 'Custom Email',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'CreatedBy' => $userId
                    );
                    $email = Sendmails::create($emails_table_input);
//                    Mail::send('emails_template', ['html_code' => $emails_table_input['Message']], function ($message) use ($emails_table_input) {
//                        $message->to($emails_table_input['EmailID'], $emails_table_input['Name'])
//                            ->subject($emails_table_input['Subject']);
//                    });
                }      
            } else {
                continue;
            }
        }
	}

	private function removeHtmlTag($str)
    {
        // remove HTML tags
        $str = preg_replace('/<[^>]*>/', ' ', $str);

        // remove control characters 
        $str = str_replace("\r", '', $str);
        $str = str_replace("\n", ' ', $str);
        $str = str_replace("\t", ' ', $str);
        $str = str_replace("&nbsp;", ' ', $str);

        // remove multiple spaces 
        $str = trim(preg_replace('/ {2,}/', ' ', $str));

        return $str;
    }

    private function check_email($email)
    {
        $email = trim(strtolower($email));

        //If less than 8 characters, not an email
        if(strlen($email) < 8) return false;

        //Regex validation
        $pattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        if (!preg_match($pattern, $email)) return false;

        //Check for filtered words
        $bad_words = array('nomail','spam','thank','dont','asd','sdf','dsf','abcd','bulk','sdf','test','none','noone','nothing','support','abuse','hell','aaa','bbb','ccc','ddd','eee','fff','ggg','hhh','iii','jjj','kkk','lll','mmm','nnn','ooo','ppp','qqq','rrr','sss','ttt','uuu','vvv','www','xxx','yyy','zzz','asd','qwe','zxc','xyz','sdf','jsd','jkl','djf','111','222','333','444','555','666','777','888','999','000','abc@','aa@','xyz@','need','tobeupdated');
        foreach($bad_words as $bad) {
            if (stripos($email, $bad) !== false) return false;
        }
        return TRUE;//email_verify($email); //TRUE;
    }
}