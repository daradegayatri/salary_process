<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * Table Name: sub_customers
SubCustomerID	int PK
ParentCustomerID	unsignedint
CustomerID	unsignedint
CreatedOn	datetime
CreatedBy	int
ModifiedOn	datetime
ModifiedBy	int
*/
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('sub_customers')) {
            Schema::create('sub_customers', function (Blueprint $table) {
                $table->increments('SubCustomerID');
                $table->unsignedInteger('ParentCustomerID')->nullable();
                $table->unsignedInteger('CustomerID')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('sub_customers');

                if(!in_array('sub_customers_parentcustomerid_foreign', $foreignKeys))
                    $table->foreign('ParentCustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('sub_customers_customerid_foreign', $foreignKeys))
                    $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_customers', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('sub_customers');

            if(in_array('sub_customers_parentcustomerid_foreign', $foreignKeys))      $table->dropForeign('sub_customers_parentcustomerid_foreign');
            if(in_array('sub_customers_customerid_foreign', $foreignKeys))      $table->dropForeign('sub_customers_customerid_foreign');

        });
        Schema::drop('sub_customers');
    }
}
