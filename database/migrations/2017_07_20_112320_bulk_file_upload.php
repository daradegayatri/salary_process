<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BulkFileUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bulk_file_upload')) {
            Schema::create('bulk_file_upload', function (Blueprint $table) {
              $table->increments('id');
               $table->string('sheet_name')->nullable();
               $table->integer('UserID')->unsigned();
               $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
               $table->integer('OrganisationID')->unsigned();
               $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
               $table->boolean('is_latest')->default(0);
               $table->integer('CreatedBy')->nullable();
               $table->integer('updated_by')->nullable();
               $table->timestamps();
           });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bulk_file_upload');
    }
}
