<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        if(!Schema::hasColumn('orders','OrganisationID')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('OrganisationID')->after('OrderID');
                $foreignKeys = $this->listTableForeignKeys('orders');
                if(!in_array('orders_organisationid_foreign', $foreignKeys))
                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('orders', function(Blueprint $table) {
//            $foreignKeys = $this->listTableForeignKeys('orders');
//            if(in_array('orders_organisationid_foreign', $foreignKeys))                   $table->dropForeign('orders_organisationid_foreign');
            $table->dropColumn('OrganisationID');
        });
    }
}
