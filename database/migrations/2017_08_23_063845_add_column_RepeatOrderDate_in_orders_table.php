<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRepeatOrderDateInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('orders', 'OrderRepeatDate')))
        {
            Schema::table('orders', function (Blueprint $table) {
                $table->date('OrderRepeatDate')->nullable()->after('EstimatedShippingTime');
            });
        }

        if ((Schema::hasColumn('orders', 'RelatedOrderId')))
        {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('RelatedOrderId');
            });
        }

        if (!(Schema::hasColumn('orders', 'RelatedOrderId')))
        {
            Schema::table('orders', function (Blueprint $table) {
                $table->integer('RelatedOrderId')->nullable()->after('EstimatedShippingTime')->default('0');
            });
        }

        if (!(Schema::hasColumn('orders', 'RelatedOrderCreatedOn')))
        {
            Schema::table('orders', function (Blueprint $table) {
                $table->date('RelatedOrderCreatedOn')->nullable()->after('OrderRepeatDate');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!(Schema::hasColumn('orders', 'OrderRepeatDate')))
        {
            Schema::table('orders', function(Blueprint $table) {
                $table->dropColumn('OrderRepeatDate');
            });
        }

        if(!Schema::hasColumn('orders','RelatedOrderId'))
        {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('RelatedOrderId')->after('EstimatedShippingTime')->nullable();
            });
        }

        if ((Schema::hasColumn('orders', 'RelatedOrderId')))
        {
            Schema::table('orders', function(Blueprint $table) {
                $table->dropColumn('RelatedOrderId');
            });
        }
        if (!(Schema::hasColumn('orders', 'RelatedOrderCreatedOn')))
        {
            Schema::table('orders', function(Blueprint $table) {
                $table->dropColumn('RelatedOrderCreatedOn');
            });
        }
    }
}
