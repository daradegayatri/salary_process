<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if(!Schema::hasColumn('invoices','OrderID')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->dropColumn('OrderID');
            });
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        if(!Schema::hasColumn('invoices','OrganisationID')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->unsignedInteger('OrganisationID')->after('InvoicedisplayID');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('invoices', function(Blueprint $table) {
            $table->dropColumn('OrganisationID');
        });

        if(!Schema::hasColumn('invoices','OrderID')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('OrderID')->after('InvoicedisplayID');
            });
        }
    }
}
