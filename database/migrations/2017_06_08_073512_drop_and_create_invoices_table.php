<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndCreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign('invoices_customerid_foreign');
            $table->dropForeign('invoices_invoiceto_foreign');
        });

        if(Schema::hasTable('invoices')) {
            Schema::dropIfexists('invoices');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        if (!Schema::hasTable('invoices')) {
            Schema::create('invoices', function (Blueprint $table) {
                $table->bigIncrements('InvoiceID');
                $table->String('InvoiceName')->nullable();
                $table->string('InvoiceTypeID')->nullable();
                $table->double('Amount')->nullable();
                $table->string('InvoicedisplayID')->nullable();
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->string('OrderID')->nullable();
                $table->integer('InvoiceTo')->nullable();
                $table->boolean('RaisedInvoice')->nullable();
                $table->dateTime('Raisedon')->nullable();
                $table->dateTime('DueOn')->nullable();
                $table->string('PastDue')->nullable();
                $table->boolean('IsPending')->nullable();
                $table->string('SpecialInstructions',1024)->nullable();
                $table->string('Description',1024)->nullable();
                $table->boolean('IsPaid')->nullable();
                $table->string('CompanyLogo')->nullable();
                $table->string('EmailTo')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->integer('CustomerID')->nullable();
                $table->string('TaskID')->nullable();
                $table->timestamps();
            });
            DB::update("ALTER TABLE invoices AUTO_INCREMENT = 6000001");
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
