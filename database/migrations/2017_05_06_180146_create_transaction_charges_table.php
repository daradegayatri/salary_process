<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionChargesTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('transaction_charges')) {
            Schema::create('transaction_charges', function (Blueprint $table) {
                $table->increments('TransactionChargeID');
                $table->float('TransactionCharge');
                $table->integer('TransactionSourceID');
                $table->float('TransactionNo_RangeFrom');
                $table->float('TransactionNo_RangeTo');
                $table->float('Min_Monthly_Amt');
                $table->string('Country');
                $table->string('CountryCode');
                $table->string('Currency');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::drop('TransactionCharges');
    }
}
