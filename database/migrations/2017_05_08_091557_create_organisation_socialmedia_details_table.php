<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationSocialmediaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('organisation_socialmedia_details')) {
            Schema::create('organisation_socialmedia_details', function (Blueprint $table) {
                $table->increments('OrganisationSocialMediaID');
                $table->unsignedInteger('SocialMediaTypeID')->nullable();
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->string('SocialMediaID')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('organisation_socialmedia_details');

                if(!in_array('organisation_socialmedia_details_socialmediatypeid_foreign', $foreignKeys))
                    $table->foreign('SocialMediaTypeID')->references('SocialMediaTypeID')->on('social_media_type')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('organisation_socialmedia_details_organisationid_foreign', $foreignKeys))
                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisation_socialmedia_details', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('organisation_socialmedia_details');

            if(in_array('organisation_socialmedia_details_socialmediatypeid_foreign', $foreignKeys))      $table->dropForeign('organisation_socialmedia_details_socialmediatypeid_foreign');
            if(in_array('organisation_socialmedia_details_organisationid_foreign', $foreignKeys))      $table->dropForeign('organisation_socialmedia_details_organisationid_foreign');

        });
        Schema::drop('organisation_socialmedia_details');
    }
}
