<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogMonthlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        if (!Schema::hasTable('transaction_log_monthly')) {
            Schema::create('transaction_log_monthly', function (Blueprint $table) {
                $table->increments('TransactionLogMonthlyID');
                $table->float('PerTransactionCharge');
                $table->string('TransactionMonth');
                $table->string('TransactionYear');
                $table->integer('TransactionSourceID')->nullable();
                $table->float('Amount');
                $table->integer('SourceID')->nullable();
                $table->float('NoOfTransaction');
                $table->boolean('IsPaid');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_log_monthly');
    }
}
