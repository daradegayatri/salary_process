<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * Table Name: customer
`CustomerID`,	int PK
 `CustomerName`,	varchar
  `Phoneno`,	varchar
 `EmailID`,	varchar
UserID	int
CustomerTypeID	int
SocialMediatypeID	Int
SocialMedia_id	varchar
SalesRep	varchar
ParentSalesRep	varchar
PrincipalAddressID	unsignedint
BillToAddressID	unsignedint
ShippToAddressID	unsignedint
Description	varchar
TransactionLink	varchar
HasSubCustomer	boolean
NoOfSubCustomer	int
 `CreatedBy`,	datetime
 `updated_by`,	int
 `created_at`,	datetime
 `updated_at`	int
*/
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->increments('CustomerID');
                $table->string('CustomerName')->nullable();
                $table->string('Phoneno')->nullable();
                $table->string('EmailID')->nullable();
                $table->unsignedInteger('UserID')->nullable();
                $table->unsignedInteger('CustomerTypeID')->nullable();
                $table->boolean('HasSocialMedia')->nullable();
                $table->unsignedInteger('SalesRep')->nullable();
                $table->unsignedInteger('ParentSalesRep')->nullable();
                $table->unsignedInteger('PrincipalAddressID')->nullable();
                $table->unsignedInteger('BillToAddressID')->nullable();
                $table->unsignedInteger('ShipToAddressID')->nullable();
                $table->string('Description')->nullable();
                $table->string('TransactionLink')->nullable();
                $table->boolean('HasSubCustomer')->nullable();
                $table->integer('NoOfSubCustomer')->nullable();
                $table->integer('Created_By')->nullable();
                $table->integer('Updated_By')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('customers');

                if(!in_array('customers_userid_foreign', $foreignKeys))
                    $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_customertypeid_foreign', $foreignKeys))
                    $table->foreign('CustomerTypeID')->references('CustomerTypeID')->on('customer_types')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_salesrep_foreign', $foreignKeys))
                    $table->foreign('SalesRep')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_parentsalesrep_foreign', $foreignKeys))
                    $table->foreign('ParentSalesRep')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_principaladdressid_foreign', $foreignKeys))
                    $table->foreign('PrincipalAddressID')->references('CustomerAddressID')->on('customer_address')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_billtoaddressid_foreign', $foreignKeys))
                    $table->foreign('BillToAddressID')->references('CustomerAddressID')->on('customer_address')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_shiptoaddressid_foreign', $foreignKeys))
                    $table->foreign('ShipToAddressID')->references('CustomerAddressID')->on('customer_address')->onUpdate('cascade')->onDelete('cascade');

            });
        }
    }
    public function down()
    {

        Schema::table('customers', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('customers');

            if(in_array('customers_userid_foreign', $foreignKeys))      $table->dropForeign('customers_userid_foreign');
            if(in_array('customers_customertypeid_foreign', $foreignKeys))      $table->dropForeign('customers_customertypeid_foreign');
            if(in_array('customers_salesrep_foreign', $foreignKeys))      $table->dropForeign('customers_salesrep_foreign');
            if(in_array('customers_parentsalesrep_foreign', $foreignKeys))      $table->dropForeign('customers_parentsalesrep_foreign');
            if(in_array('customers_principaladdressid_foreign', $foreignKeys))      $table->dropForeign('customers_principaladdressid_foreign');
            if(in_array('customers_billtoaddressid_foreign', $foreignKeys))      $table->dropForeign('customers_billtoaddressid_foreign');
            if(in_array('customers_shiptoaddressid_foreign', $foreignKeys))      $table->dropForeign('customers_shiptoaddressid_foreign');
        });
        Schema::dropIfExists('customers');
    }
}
