<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('salary_process')) {
            Schema::create('salary_process', function (Blueprint $table) {
                $table->Increments('id');

                $table->string('total')->nullable();
                $table->integer('salary_user_id')->unsigned();
                //$table->foreign('salary_user_id')->nullable;
                $table->string('pay_days')->nullable();
                $table->date('month')->nullable();
                $table->date('year')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_process');
    }
}
