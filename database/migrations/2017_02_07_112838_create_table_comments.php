<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('comments')) {
            Schema::create('comments', function (Blueprint $table) {
                $table->increments('CommentID');
                $table->integer('TasklistID')->nullable();
                $table->integer('UserID')->nullable();
                $table->string('Description')->nullable();
                $table->boolean('HasAttachment')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('TasklistID');
                $table->index('UserID');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
