<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsprimaryColumnToCustomerSalesrepresentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('customer_sales_representative', 'IsPrimary')))
        {
            Schema::table('customer_sales_representative', function (Blueprint $table)
            {
                $table->boolean('IsPrimary')->nullable()->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_sales_representative', function(Blueprint $table)
        {
            $table->dropcolumn('IsPrimary');
        });
    }
}
