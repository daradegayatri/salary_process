<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTasklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        if(!Schema::hasColumn('tasklist','OrganisationID')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->unsignedInteger('OrganisationID')->after('TasklistID')->nullable();
                $foreignKeys = $this->listTableForeignKeys('tasklist');
                if(!in_array('tasklist_organisationid_foreign', $foreignKeys))
                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tasklist', function(Blueprint $table) {
            $foreignKeys = $this->listTableForeignKeys('tasklist');
            if(in_array('tasklist_organisationid_foreign', $foreignKeys))                   $table->dropForeign('tasklist_organisationid_foreign');
            $table->dropColumn('OrganisationID');
        });
    }
}
