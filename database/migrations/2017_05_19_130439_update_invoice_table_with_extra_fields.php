<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoiceTableWithExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        if(!(Schema::hasColumn('invoices', 'PastDue') &&
            Schema::hasColumn('invoices', 'CompanyLogo')&&
            Schema::hasColumn('invoices', 'EmailTo')&&
            Schema::hasColumn('invoices', 'CustomerID')&&
            Schema::hasColumn('invoices', 'TaskID')))
            \Schema::table('invoices', function (Blueprint $table) {

                $table->string('PastDue')->after('DueOn')->nullable();
                $table->string('CompanyLogo')->after('IsPaid')->nullable();
                $table->string('EmailTo')->after('CompanyLogo')->nullable();
                $table->integer('CustomerID')->unsigned()->nullable();
                $table->string('TaskID')->after('CustomerID')->nullable();
                $foreignKeys = $this->listTableForeignKeys('invoices');

                if(!in_array('invoices_customerid_foreign', $foreignKeys))
                    $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('invoices');

            if(in_array('invoices_customerid_foreign', $foreignKeys))      $table->dropForeign('invoices_customerid_foreign');

        });
        Schema::drop('invoices');
    }
}
