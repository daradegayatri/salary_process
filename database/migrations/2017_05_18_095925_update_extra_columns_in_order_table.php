<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExtraColumnsInOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // //
    // Ship To Address (Customer Id , Self) , Bill To Address (Customer Id , Self),
    // Order Value Total ,
    // Order Tax Total , Task Id (multiple selection allowed) , Disclaimer ,
    // Shipment Tracking Link, Related Order Id, Inventory Id

    public function up()
    {
        if(!Schema::hasColumn('orders','ShipToAddress')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('ShipToAddress')->after('NoOfInventory')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','BillToAddress')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('BillToAddress')->after('ShipToAddress')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderValueTotal')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('OrderValueTotal')->after('BillToAddress')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderTaxTotal')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('OrderTaxTotal')->after('OrderValueTotal')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','TaskId')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('TaskId')->after('OrderTaxTotal')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','Disclaimer')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->string('Disclaimer')->after('TaskId')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','ShipmentTrackingLink')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->string('ShipmentTrackingLink')->after('Disclaimer')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','RelatedOrderId')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('RelatedOrderId')->after('ShipmentTrackingLink')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('ShipToAddress');
            $table->dropColumn('BillToAddress');
            $table->dropColumn('OrderValueTotal');
            $table->dropColumn('OrderTaxTotal');
            $table->dropColumn('TaskId');
            $table->dropColumn('Disclaimer');
            $table->dropColumn('ShipmentTrackingLink');
            $table->dropColumn('RelatedOrderId');
        });
    }
}
