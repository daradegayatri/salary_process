<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomerTableSocialMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('customers','PrincipalSocialMediaID')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->integer('PrincipalSocialMediaID')->after('updated_at')->nullable();
            });
        }
        if(!Schema::hasColumn('customers','SecondarySocialMediaID')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->integer('SecondarySocialMediaID')->after('PrincipalSocialMedia')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('PrincipalSocialMediaID');
            $table->dropColumn('SecondarySocialMediaID');
        });
    }
}
