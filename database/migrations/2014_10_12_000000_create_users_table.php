<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('UserRoleID');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('PhoneNo')->nullable()->unique();
            $table->string('Gender')->nullable();
            $table->string('Address')->nullable();
            $table->string('City')->nullable();
            $table->string('Country')->nullable();
            $table->string('Postalcode')->nullable();
            $table->integer('AssignedTo')->nullable();
            $table->integer('CreatedBy')->nullable();
            $table->integer('updated_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        $statement = "ALTER TABLE users AUTO_INCREMENT = 100001";
        DB::unprepared($statement);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
