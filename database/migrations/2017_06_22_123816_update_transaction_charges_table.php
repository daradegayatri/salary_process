<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('transaction_charges','status'))) {
            Schema::table('transaction_charges', function (Blueprint $table) {
                $table->string('description')->after('TransactionChargeID')->nullable();
                $table->string('name')->after('TransactionChargeID')->nullable();
                $table->boolean('IsDeleted')->default(0)->nullable();

            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_charges', function(Blueprint $table)
        {
            $table->dropColumn('description');
            $table->dropColumn('name');
            $table->dropColumn('IsDeleted');
        });
    }
}
