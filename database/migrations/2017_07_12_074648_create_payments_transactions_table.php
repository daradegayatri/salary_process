<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payment_transactions'))
        {
            Schema::create('payment_transactions', function (Blueprint $table) {
                $table->increments('PaymentTransactionID');
                $table->bigInteger('TransactionID')->unique()->nullable();
                $table->integer('Amount')->nullable();
                $table->integer('Tax')->nullable();
                $table->integer('StatusCode')->nullable();
                $table->string('Status')->nullable();
                $table->string('CardType')->nullable();
                $table->string('AccountNumber')->nullable();
                $table->string('AuthCode')->nullable();
                $table->unsignedInteger('TransactionLogMonthlyID')->nullable();
                $table->foreign('TransactionLogMonthlyID')->references('TransactionLogMonthlyID')->on('transaction_log_monthly')->onDelete('cascade');
                $table->unsignedInteger('OrganisationID')->unsigned()->nullable();
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }
    public function down()
    {
        Schema::drop('payment_transactions');
    }
}
