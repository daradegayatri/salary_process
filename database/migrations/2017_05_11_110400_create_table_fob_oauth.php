<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFobOauth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('fob_oauth'))
        {
            Schema::create('fob_oauth', function (Blueprint $table) {
                $table->increments('id');
                $table->String('access_token');
                $table->String('refresh_token');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('fob_oauth')) {
            Schema::drop('fob_oauth');
        }
    }
}
