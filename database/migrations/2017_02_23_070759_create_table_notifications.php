<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('notifications'))
        {
            Schema::create('notifications', function (Blueprint $table) {
                $table->bigIncrements('NotificationID');
                $table->string('Notification')->nullable();
                $table->string('NotificationFor')->nullable();
                $table->integer('TasklistID')->nullable();
                $table->integer('LocationID')->nullable();
                //$table->integer('TasklistID')->nullable();
                $table->boolean('IsRead')->nullable();
                $table->integer('Status')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('notifications');
    }
}
