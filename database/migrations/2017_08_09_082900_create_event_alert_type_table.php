<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAlertTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_alert_types')) {
            Schema::create('event_alert_types', function (Blueprint $table) {
                $table->increments('AlertTypeID');
                $table->string('AlertType')->nullable();
                $table->unsignedInteger('OrganizationID')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('AlertType');
                $table->foreign('OrganizationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_alert_types');
    }
}
