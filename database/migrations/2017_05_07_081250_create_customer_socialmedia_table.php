<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSocialmediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * Table Name: customers_socialmedia
CustomersSocialMediaID	int PK
SocialMediaTypeID	int
CustomerID	int
SocialMediaID	varchar
CreatedOn	datetime
CreatedBy	int
ModifiedOn	datetime
ModifiedBy	int
*/
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('customers_socialmedia_details')) {
            Schema::create('customers_socialmedia_details', function (Blueprint $table) {
                $table->increments('CustomersSocialMediaID');
                $table->unsignedInteger('SocialMediaTypeID')->nullable();
                $table->unsignedInteger('CustomerID')->nullable();
                $table->string('SocialMediaID')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('customers_socialmedia_details');

                if(!in_array('customers_socialmedia_details_socialmediatypeid_foreign', $foreignKeys))
                    $table->foreign('SocialMediaTypeID')->references('SocialMediaTypeID')->on('social_media_type')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('customers_socialmedia_details_customerid_foreign', $foreignKeys))
                    $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_socialmedia_details', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('customers_socialmedia_details');

            if(in_array('customers_socialmedia_details_socialmediatypeid_foreign', $foreignKeys))      $table->dropForeign('customers_socialmedia_details_socialmediatypeid_foreign');
            if(in_array('customers_socialmedia_details_customerid_foreign', $foreignKeys))      $table->dropForeign('customers_socialmedia_details_customerid_foreign');

        });
        Schema::drop('customers_socialmedia_details');
    }
}
