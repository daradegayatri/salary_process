<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderfromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (Schema::hasTable('order_from')) {
            Schema::drop('order_from');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        if (!Schema::hasTable('order_from')) {

            Schema::create('order_from', function (Blueprint $table) {
                $table->increments('OrderFromID');
                $table->unsignedInteger('OrderRaisedBy')->nullable();
                $table->unsignedBigInteger('OrderID')->nullable();
                $table->unsignedInteger('UserRoleID')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('order_from');
                if(!in_array('order_from_orderid_foreign', $foreignKeys))
                    $table->foreign('OrderID')->references('OrderID')->on('orders')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (Schema::hasTable('order_from')) {
            Schema::drop('order_from');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
