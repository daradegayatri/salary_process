<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('customers', 'InitialDealValue'))) {
            Schema::table('customers', function (Blueprint $table)
            {
                $table->double('InitialDealValue')->nullable();
            });
        }
        if (!(Schema::hasColumn('customers', 'ClosedDealValue'))) {
            Schema::table('customers', function (Blueprint $table)
            {
                $table->double('ClosedDealValue')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->dropcolumn('InitialDealValue');
            $table->dropcolumn('ClosedDealValue');
        });
    }
}
