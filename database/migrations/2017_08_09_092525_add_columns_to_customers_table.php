<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('customers', 'Prefix'))) {
            Schema::table('customers', function (Blueprint $table) {
                $table->string('Prefix')->nullable()->after('CustomerTypeID');
            });
        }
        if (!(Schema::hasColumn('customers', 'FirstName'))) {
            Schema::table('customers', function (Blueprint $table) {
                $table->string('FirstName')->nullable()->after('Prefix');
            });
        }

        if (!(Schema::hasColumn('customers', 'LastName'))) {
            Schema::table('customers', function (Blueprint $table) {
                $table->string('LastName')->nullable()->after('FirstName');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


   if (!(Schema::hasColumn('customers', 'Prefix'))) {
        Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('Prefix');
        });
}

        if (!(Schema::hasColumn('customers', 'FirstName'))) {
        Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('FirstName');
        });
        }

        if (!(Schema::hasColumn('customers', 'LastName'))) {
        Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('LastName');
        });
    }
    }
}
