<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysToorderstables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
       // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('invoice_inventory', function(Blueprint $table)
        {
            $table->unsignedBigInteger('InventoryID')->change();
            $table->unsignedBigInteger('OrderID')->nullable()->change();

            $foreignKeys = $this->listTableForeignKeys('invoice_inventory');
            //var_dump($foreignKeys);
            if(!in_array('invoice_inventory_inventoryid_foreign', $foreignKeys))
                $table->foreign('InventoryID')->references('InventoryID')->on('inventory')->onDelete('cascade');



            if(!in_array('invoice_inventory_orderid_foreign', $foreignKeys))
                $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
        });
       // DB::statement('SET FOREIGN_KEY_CHECKS=1;');

       // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('task_order', function(Blueprint $table)
        {
            $table->unsignedBigInteger('TasklistID')->change();
            $table->unsignedBigInteger('OrderID')->nullable()->change();
            $table->unsignedInteger('OrderStatusID')->change();

            $foreignKeys = $this->listTableForeignKeys('task_order');

            if(!in_array('task_order_tasklistid_foreign', $foreignKeys))
                $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
            if(!in_array('task_order_orderstatusid_foreign', $foreignKeys))
                $table->foreign('OrderStatusID')->references('OrderStatusID')->on('order_status')->onDelete('cascade');
            if(!in_array('task_order_orderid_foreign', $foreignKeys))
                $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
        });
       // DB::statement('SET FOREIGN_KEY_CHECKS=1;');
       // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('notifications', function(Blueprint $table)
        {
            $table->unsignedBigInteger('TasklistID')->change();
            $table->unsignedInteger('NotificationFor')->nullable()->change();
            $table->unsignedInteger('LocationID')->change();

            $foreignKeys = $this->listTableForeignKeys('notifications');

            if(!in_array('notifications_tasklistid_foreign', $foreignKeys))
                $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
            if(!in_array('notifications_notificationfor_foreign', $foreignKeys))
                $table->foreign('NotificationFor')->references('id')->on('users')->onDelete('cascade');
            if(!in_array('notifications_locationid_foreign', $foreignKeys))
                $table->foreign('LocationID')->references('LocationID')->on('locations')->onDelete('cascade');
        });
       // DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_order', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('task_order');

            if(in_array('task_order_orderid_foreign', $foreignKeys))         $table->dropForeign('task_order_orderid_foreign');
            if(in_array('task_order_tasklistid_foreign', $foreignKeys))      $table->dropForeign('task_order_tasklistid_foreign');
            if(in_array('task_order_orderdtatusid_foreign', $foreignKeys))      $table->dropForeign('task_order_orderdtatusid_foreign');

            $table->integer('OrderID')->nullable()->change();
            $table->bigInteger('TasklistID')->nullable()->change();
            $table->integer('OrderStatusID')->nullable()->change();
        });
        Schema::table('invoice_inventory', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('invoice_inventory');

            if(in_array('invoice_inventory_inventoryid_foreign', $foreignKeys))          $table->dropForeign('invoice_inventory_inventoryid_foreign');
            if(in_array('invoice_inventory_orderid_foreign', $foreignKeys))          $table->dropForeign('invoice_inventory_orderid_foreign');

            $table->integer('OrderID')->nullable()->change();
            $table->integer('InventoryID')->nullable()->change();
        });
    }

}