<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeytoordertables extends Migration
{
//    /**
//     * Run the migrations.
//     *
//     * @return void
//     */
//    public function listTableForeignKeys($table)
//    {
//        $conn = Schema::getConnection()->getDoctrineSchemaManager();
//
//        return array_map(function($key) {
//            return $key->getName();
//        }, $conn->listTableForeignKeys($table));
//    }
    public function up()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        Schema::table('invoice_inventory', function(Blueprint $table)
//        {
//            $table->unsignedBigInteger('InventoryID')->change();
//            $table->unsignedBigInteger('OrderID')->nullable()->change();
//
//            $foreignKeys = $this->listTableForeignKeys('invoice_inventory');
//
//            if(!in_array('invoice_inventory_InventoryID_foreign', $foreignKeys))
//            $table->foreign('InventoryID')->references('InventoryID')->on('inventory')->onDelete('cascade');
//            if(!in_array('invoice_inventory_OrderID_foreign', $foreignKeys))
//            $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
//        });
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        Schema::table('task_order', function(Blueprint $table)
//        {
//            $table->unsignedBigInteger('TasklistID')->change();
//            $table->unsignedBigInteger('OrderID')->nullable()->change();
//            $table->unsignedInteger('OrderStatusID')->change();
//
//            $foreignKeys = $this->listTableForeignKeys('task_order');
//
//            if(!in_array('task_order_TasklistID_foreign', $foreignKeys))
//            $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
//            if(!in_array('task_order_OrderStatusID_foreign', $foreignKeys))
//            $table->foreign('OrderStatusID')->references('OrderStatusID')->on('order_status')->onDelete('cascade');
//            if(!in_array('task_order_OrderID_foreign', $foreignKeys))
//            $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
//        });
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        Schema::table('notifications', function(Blueprint $table)
//        {
//            $table->unsignedBigInteger('TasklistID')->change();
//            $table->unsignedBigInteger('NotificationFor')->nullable()->change();
//            $table->unsignedInteger('LocationID')->change();
//
//            $foreignKeys = $this->listTableForeignKeys('notifications');
//
//            if(!in_array('notifications_TasklistID_foreign', $foreignKeys))
//            $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
//            if(!in_array('notifications_NotificationFor_foreign', $foreignKeys))
//            $table->foreign('NotificationFor')->references('id')->on('users')->onDelete('cascade');
//            if(!in_array('notifications_LocationID_foreign', $foreignKeys))
//            $table->foreign('LocationID')->references('LocationID')->on('locations')->onDelete('cascade');
//        });
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
   }
//    /**
//     * Reverse the migrations.
//     *
//     * @return void
//     */
    public function down()
    {
//        Schema::table('task_order', function(Blueprint $table)
//        {
//            $foreignKeys = $this->listTableForeignKeys('task_order');
//
//            if(in_array('task_order_OrderID_foreign', $foreignKeys))         $table->dropForeign('task_order_OrderID_foreign');
//            if(in_array('task_order_TasklistID_foreign', $foreignKeys))      $table->dropForeign('task_order_TasklistID_foreign');
//            if(in_array('task_order_OrderStatusID_foreign', $foreignKeys))      $table->dropForeign('task_order_OrderStatusID_foreign');
//
//            $table->integer('OrderID')->nullable()->change();
//            $table->bigInteger('TasklistID')->nullable()->change();
//            $table->integer('OrderStatusID')->nullable()->change();
//        });
//        Schema::table('invoice_inventory', function(Blueprint $table)
//        {
//            $foreignKeys = $this->listTableForeignKeys('invoice_inventory');
//
//            if(in_array('invoice_inventory_InventoryID_foreign', $foreignKeys))          $table->dropForeign('invoice_inventory_InventoryID_foreign');
//            if(in_array('invoice_inventory_OrderID_foreign', $foreignKeys))          $table->dropForeign('invoice_inventory_OrderID_foreign');
//
//            $table->integer('OrderID')->nullable()->change();
//            $table->integer('InventoryID')->nullable()->change();
//        });
    }
//
}