<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFilesource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('file_source'))
        {
            Schema::create('file_source', function (Blueprint $table) {
                $table->increments('FileSourceID');
                $table->string('FileSource')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                $table->integer('CreatedBy')->nullable();
               // $table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                       $table->index('FileSource');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_source');
    }
}
