<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobIDInOrderstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('orders','JobID')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('JobID')->after('InvoiceID');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('JobID');
        });
    }
}
