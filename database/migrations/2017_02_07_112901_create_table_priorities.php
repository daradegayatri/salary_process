<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePriorities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('priorities')) {
            Schema::create('priorities', function (Blueprint $table) {
                $table->increments('PriorityID');
                $table->string('Priority')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('Priority');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('priorities');
    }
}
