<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserWithProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!(Schema::hasColumn('users', 'provider') &&
            Schema::hasColumn('users', 'provider_id')))
            \Schema::table('users', function (Blueprint $table) {

                $table->string('provider')->nullable();
                $table->string('provider_id')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('provider');
            $table->dropColumn('provider_id');
        });
    }
}
