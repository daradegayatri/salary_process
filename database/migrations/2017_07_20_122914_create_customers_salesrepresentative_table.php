<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersSalesrepresentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customer_sales_representative')) {
            Schema::create('customer_sales_representative', function (Blueprint $table) {
                $table->increments('CustomerSRID');
                $table->unsignedInteger('CustomerID')->nullable();
                $table->unsignedInteger('SalesRepresentativeID')->nullable();
                $table->unsignedInteger('OrganizationID')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('CustomerID');
                $table->index('SalesRepresentativeID');
                $table->index('OrganizationID');
                $table->foreign('OrganizationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
                $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onDelete('cascade');
                $table->foreign('SalesRepresentativeID')->references('id')->on('users')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_sales_representative');
    }
}
