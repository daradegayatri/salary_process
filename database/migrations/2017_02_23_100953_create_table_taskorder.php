<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaskorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('task_order'))
        {
            Schema::create('task_order', function (Blueprint $table) {
                $table->increments('TaskOrderID');
                $table->integer('OrderID')->nullable();
                $table->integer('TasklistID')->nullable();
                $table->integer('OrderStatusID')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_order');
    }
}
