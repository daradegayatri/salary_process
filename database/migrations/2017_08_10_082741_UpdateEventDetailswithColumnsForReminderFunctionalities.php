<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventDetailswithColumnsForReminderFunctionalities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('event_details', 'RepeatTypeID')))
        {
            Schema::table('event_details', function (Blueprint $table)
            {
                $table->unsignedInteger('RepeatTypeID')->nullable();
            });
        }
        if (!(Schema::hasColumn('event_details', 'AlertTypeID')))
        {
            Schema::table('event_details', function (Blueprint $table)
            {
                $table->unsignedInteger('AlertTypeID')->nullable();
            });
        }
        if (!(Schema::hasColumn('event_details', 'TimeZone')))
        {
            Schema::table('event_details', function (Blueprint $table)
            {
                $table->string('TimeZone')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_details', function(Blueprint $table)
        {
            $table->dropcolumn('RepeatTypeID');
            $table->dropcolumn('AlertTypeID');
            $table->dropcolumn('TimeZone');
        });
    }
}
