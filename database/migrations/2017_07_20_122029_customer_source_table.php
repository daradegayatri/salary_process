<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customer_source')) {
            Schema::create('customer_source', function (Blueprint $table) {
                $table->increments('CustomerSourceID');
                $table->string('CustomerSourceType')->nullable();
                $table->integer('UserID')->unsigned()->default(0);
                $table->foreign('UserID')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('OrganisationID')->unsigned()->default(0);
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
                $table->boolean('is_latest')->default(0);
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_source');
    }
}
