<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTableWithUnitsCycleTypeAndCycleValueColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table)
        {
            $table->string('cycle_type')->nullable();
            $table->integer('cycle_value')->nullable();
            $table->string('units')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function(Blueprint $table)
        {
            $table->dropcolumn('cycle_type');
            $table->dropcolumn('cycle_value');
            $table->dropcolumn('units');
        });
    }
}
