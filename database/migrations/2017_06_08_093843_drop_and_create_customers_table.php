<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndCreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('customers', function (Blueprint $table) {
            $table->dropForeign('customers_billtoaddressid_foreign');
            $table->dropForeign('customers_customertypeid_foreign');
            $table->dropForeign('customers_parentsalesrep_foreign');
            $table->dropForeign('customers_principaladdressid_foreign');
            $table->dropForeign('customers_salesrep_foreign');
            $table->dropForeign('customers_shiptoaddressid_foreign');
            $table->dropForeign('customers_userid_foreign');
        });

        if (Schema::hasTable('customers')) {
            Schema::dropIfexists('customers');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->increments('CustomerID');
                $table->string('CompanyName')->nullable();
                $table->string('CustomerName')->nullable();
                $table->string('Phoneno')->nullable();
                $table->string('EmailID')->nullable();
                $table->unsignedInteger('UserID')->nullable();
                $table->unsignedInteger('CustomerTypeID')->nullable();
                $table->boolean('HasSocialMedia')->nullable();
                $table->unsignedInteger('SalesRep')->nullable();
                $table->unsignedInteger('ParentSalesRep')->nullable();
                $table->unsignedInteger('PrincipalAddressID')->nullable();
                $table->unsignedInteger('BillToAddressID')->nullable();
                $table->unsignedInteger('ShipToAddressID')->nullable();
                $table->string('Description')->nullable();
                $table->string('TransactionLink')->nullable();
                $table->boolean('HasSubCustomer')->nullable();
                $table->integer('NoOfSubCustomer')->nullable();
                $table->string('Group')->nullable();
                $table->integer('ParentCustId')->nullable();
                $table->boolean('ChildCustId')->nullable();
                $table->integer('Created_By')->nullable();
                $table->integer('Updated_By')->nullable();
                $table->integer('PrincipalSocialMedia')->unsigned()->nullable();
                $table->integer('SecondarySocialMedia')->unsigned()->nullable();
                $table->timestamps();
            });
            DB::update("ALTER TABLE customers AUTO_INCREMENT = 9000001");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
