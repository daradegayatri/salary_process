<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements('OrderID');
                $table->string('WorkOrderID')->nullable();
                $table->integer('UserID')->nullable();
                $table->integer('LocationID')->nullable();
                //$table->integer('WorkTypeID')->nullable();
                $table->integer('InventoryID')->nullable();
                $table->integer('InvoiceID')->nullable();
                $table->integer('OrderStatusID')->nullable();
                $table->integer('NoOfInventory')->nullable();
                $table->integer('CreatedBy')->nullable();
               //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('OrderStatusID');

            });
            DB::update("ALTER TABLE orders AUTO_INCREMENT = 5000001");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
