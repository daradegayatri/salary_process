<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerStagesTables extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('customer_stages')) {
            Schema::create('customer_stages', function (Blueprint $table) {
                $table->increments('CustomerStageID');
                $table->string('CustomerStage')->nullable();
                $table->timestamps();
            });
        }
    }
    public function down()
    {
        Schema::drop('customer_stages');
    }
}
