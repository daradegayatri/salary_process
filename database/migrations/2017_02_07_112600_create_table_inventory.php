<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('inventory')) {
            Schema::create('inventory', function (Blueprint $table) {
                $table->bigIncrements('InventoryID');
                $table->string('InventoryName')->nullable();
                $table->string('InventorydisplayID')->nullable();
                $table->string('Description')->nullable();
                $table->double('Price')->nullable();
                $table->integer('MinInventoryCount')->nullable();
                $table->integer('MaxInventoryCount')->nullable();
                $table->integer('AvaliableInventoryCount')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('MinInventoryCount');
                $table->index('InventoryName');
                $table->index('AvaliableInventoryCount');
                $table->index('MaxInventoryCount');
                $table->timestamps();
            });
            //$statement = "ALTER TABLE inventory AUTO_INCREMENT = 100001";
            DB::update("ALTER TABLE inventory AUTO_INCREMENT = 4000001");
           // DB::unprepared($statement);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory');
    }
}
