<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpateCustomerTableWithExtraField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if(!(Schema::hasColumn('customers', 'PrincipalSocialMedia') &&
            Schema::hasColumn('customers', 'SecondarySocialMedia')&&
            Schema::hasColumn('customers', 'Group') &&
            Schema::hasColumn('customers', 'ParentCustId') &&
            Schema::hasColumn('customers', 'ChildCustId')))
            \Schema::table('customers', function (Blueprint $table) {

                $table->integer('PrincipalSocialMedia')->unsigned()->nullable();
                $table->integer('SecondarySocialMedia')->unsigned()->nullable();
                $table->string('Group')->after('NoOfSubCustomer')->nullable();
                $table->integer('ParentCustId')->after('Group')->nullable();
                $table->boolean('ChildCustId')->after('ParentCustId')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->dropColumn('PrincipalSocialMedia');
            $table->dropColumn('SecondarySocialMedia');
            $table->dropColumn('ParentCustId');
            $table->dropColumn('ChildCustId');
            $table->dropColumn('Group');
        });
    }
}
