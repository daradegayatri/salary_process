<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrganizationTableWithExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!(Schema::hasColumn('organisation', 'Office') &&
            Schema::hasColumn('organisation', 'Street')&&
            Schema::hasColumn('organisation', 'Description') &&
            Schema::hasColumn('organisation', 'OrgLogo') &&
            Schema::hasColumn('organisation', 'OrgPhone')&&
            Schema::hasColumn('organisation', 'OrgWebAddress')&&
            Schema::hasColumn('organisation', 'OrgSocialMediaAccount')&&
            Schema::hasColumn('organisation', 'OrgEmail')&&
            Schema::hasColumn('organisation', 'OrgHierarchy')&&
            Schema::hasColumn('organisation', 'ParentOrgID')&&
            Schema::hasColumn('organisation', 'ChildOrgFlag ')&&
            Schema::hasColumn('organisation', 'ChildOrgID')))
            \Schema::table('organisation', function (Blueprint $table) {

                $table->string('Office')->after('Address')->nullable();
                $table->string('Street')->after('Office')->nullable();
                $table->string('Description')->after('ZipCode')->nullable();
                $table->string('OrgLogo')->after('OrganisationID')->nullable();
                $table->string('OrgPhone')->after('OrganisationName')->nullable();
                $table->string('OrgWebAddress')->after('OrgPhone')->nullable();
                $table->string('OrgSocialMediaAccount')->after('HasSocialMediaDetails')->nullable();
                $table->string('OrgEmail')->after('OrgSocialMediaAccount')->nullable();
                $table->string('OrgHierarchy')->after('OrgEmail')->nullable();
                $table->string('SecondaryContactName')->after('Description')->nullable();
                $table->integer('ParentOrgID')->after('Office')->nullable();
                $table->boolean('ChildOrgFlag')->after('ParentOrgID')->nullable();
                $table->integer('ChildOrgID')->after('ChildOrgFlag')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisation', function(Blueprint $table)
        {
            $table->dropColumn('Office');
            $table->dropColumn('Street');
            $table->dropColumn('Description');
            $table->dropColumn('OrgLogo');
            $table->dropColumn('OrgPhone');
            $table->dropColumn('OrgWebAddress');
            $table->dropColumn('OrgSocialMediaAccount');
            $table->dropColumn('OrgEmail');
            $table->dropColumn('OrgHierarchy');
            $table->dropColumn('SecondaryContactName');
            $table->dropColumn('ParentOrgID');
            $table->dropColumn('ChildOrgFlag');
            $table->dropColumn('ChildOrgID');
        });
    }
}
