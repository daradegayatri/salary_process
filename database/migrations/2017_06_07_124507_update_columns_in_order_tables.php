<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInOrderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('orders','OrderDescription')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('OrderDescription');
                $table->string('OrderDescription')->after('OrderName')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','HasAttachment')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->boolean('HasAttachment')->after('IsInternal')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','SeverityID')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('SeverityID')->after('IsInternal')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','PriorityID')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('PriorityID')->after('IsInternal')->nullable();
            });
        }
    }

    public function down()
    {
        \Schema::table('tasklist', function(Blueprint $table) {
            $table->dropColumn('HasAttachment');
            $table->dropColumn('SeverityID');
            $table->dropColumn('PriorityID');
        });

    }
}
