<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInTasklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('tasklist','TaskTypeID')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->unsignedInteger('TaskTypeID')->after('TaskSeverityID')->nullable();
            });
        }
        if(!Schema::hasColumn('tasklist','OrderID')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->unsignedInteger('OrderID')->after('TaskTypeID')->nullable();
            });
        }
        if(!Schema::hasColumn('tasklist','CustomerID')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->unsignedInteger('CustomerID')->after('OrderID')->nullable();
            });
        }
        if(!Schema::hasColumn('tasklist','DueDate')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->date('DueDate')->after('CustomerID')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('tasklist', function(Blueprint $table) {
            $table->dropColumn('TaskTypeID');
            $table->dropColumn('OrderID');
            $table->dropColumn('CustomerID');
            $table->dropColumn('DueDate');
        });
    }
}
