<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToOrganisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        if(!Schema::hasColumn('organisation','OrganisationTypeID')) {
            \Schema::table('organisation', function (Blueprint $table) {
                $table->unsignedInteger('OrganisationTypeID')->after('OrganisationID');
               // $foreignKeys = $this->listTableForeignKeys('organisation');
                //if(!in_array('organisation_organisationtypeid_foreign', $foreignKeys))
                 //   $table->foreign('OrganisationTypeID')->references('OrganisationTypeID')->on('organisation_type')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('organisation', function(Blueprint $table) {
            $foreignKeys = $this->listTableForeignKeys('organisation');
            if(in_array('organisation_organisationtypeid_foreign', $foreignKeys))                   $table->dropForeign('organisation_organisationtypeid_foreign');
            $table->dropColumn('OrganisationTypeID');
        });
    }
}
