<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomerSocialMediaType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('customers','PrincipalSocialMedias')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('PrincipalSocialMedia');
                $table->string('PrincipalSocialMedias')->after('PrincipalSocialMediaID')->nullable();
            });
        }
        if(!Schema::hasColumn('customers','SecondarySocialMedias')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('SecondarySocialMedia');
                $table->string('SecondarySocialMedias')->after('SecondarySocialMediaID')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('PrincipalSocialMedias');
            $table->dropColumn('SecondarySocialMedias');
        });
    }
}
