<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('invoices')) {
            Schema::create('invoices', function (Blueprint $table) {
                $table->bigIncrements('InvoiceID');
                $table->double('Rate')->nullable();
                $table->double('Amount')->nullable();
                $table->String('Description')->nullable();
                $table->integer('ToCompany')->nullable();
                $table->integer('ForCompany')->nullable();
                $table->integer('BillTo')->nullable();
                $table->integer('ShipTo')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
            DB::update("ALTER TABLE invoices AUTO_INCREMENT = 6000001");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
