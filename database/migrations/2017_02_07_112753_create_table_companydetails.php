<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanydetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('companydetails')) {
            Schema::create('companydetails', function (Blueprint $table) {
                $table->increments('CompanyDetailsID');
                $table->string('CompanyName')->nullable();
                $table->string('Address')->nullable();
                $table->string('Phoneno')->nullable();
                $table->string('EmailID')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companydetails');
    }
}
