<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderinventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_inventory'))
        {
            Schema::create('order_inventory', function (Blueprint $table) {
                $table->increments('OrderInventoryID');
                $table->integer('OrderID')->nullable();
                $table->integer('InventoryID')->nullable();
                $table->integer('NoOfInventory')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_inventory');
    }
}
