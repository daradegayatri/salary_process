<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMailForColumnSendMailstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('send_mails','MailFor'))
        {
            \Schema::table('send_mails', function (Blueprint $table) {
                $table->string('MailFor')->after('EmailID');
                $table->string('MailSourceID')->after('EmailID');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('send_mails', function(Blueprint $table) {
            $table->dropColumn('MailFor');
            $table->dropColumn('MailSourceID');
        });
    }
}
