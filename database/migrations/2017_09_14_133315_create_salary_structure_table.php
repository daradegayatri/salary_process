<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('salary_structure')) {
            Schema::create('salary_structure', function (Blueprint $table) {
                $table->Increments('id');
                $table->integer('salary_user_id')->unsigned();
                $table->foreign('salary_user_id')->references('id')->on('salary_users')->onUpdate('cascade')->onDelete('cascade');
                $table->decimal('basics',10,2)->nullable();
                $table->decimal('HRA',10,2)->nullable();
                $table->decimal('DA',10,2)->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_structure', function(Blueprint $table)
        {
            $table->dropForeign('salary_structure_salary_user_id_foreign');

        });
        Schema::dropIfExists('salary_structure');
    }
}
