<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaskstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('task_status')) {
            Schema::create('task_status', function (Blueprint $table) {
                $table->increments('TaskStatusID');
                $table->string('TaskStatus')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('TaskStatus');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_status');
    }
}
