<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
 *
 * Table Name: organisation
`OrganisationID`,	int PK
`OrganisationName`,	varchar
`Address`,	varchar
`Phoneno`,	varchar
`EmailID`,	varchar
`CreatedBy`,	datetime
`updated_by`,	int
`created_at`,	datetime
`updated_at`	int
*/
    public function up()
    {
        if (!Schema::hasTable('organisation')) {
            Schema::create('organisation', function (Blueprint $table) {
                $table->increments('OrganisationID');
                $table->string('OrganisationName')->nullable();
                $table->string('ContactPerson')->nullable();
                $table->string('Phoneno')->nullable();
                $table->string('EmailID')->nullable();
                $table->string('Address')->nullable();
                $table->string('City')->nullable();
                $table->string('State')->nullable();
                $table->string('Country')->nullable();
                $table->string('ZipCode')->nullable();
                $table->boolean('HasSocialMediaDetails')->nullable();
                $table->boolean('HasSubOrganisation')->nullable();
                $table->boolean('IsSubOrganisation')->nullable();
                $table->integer('Created_By')->nullable();
                $table->integer('Updated_By')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organisation');
    }
}
