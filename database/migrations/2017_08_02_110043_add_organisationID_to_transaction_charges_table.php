<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganisationIDToTransactionChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('transaction_charges', 'OrganisationID'))) {
            Schema::table('transaction_charges', function (Blueprint $table)
            {
                $table->unsignedInteger('OrganisationID')->nullable()->after('TransactionChargeID');
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_charges', function(Blueprint $table)
        {
            $table->dropForeign('transaction_charges_organisationid_foreign');
            $table->dropcolumn('OrganisationID');
        });
    }
}
