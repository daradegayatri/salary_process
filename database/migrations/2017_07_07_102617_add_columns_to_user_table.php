<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('users', 'Description'))) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('Description', 1024)->nullable()->after('AssignedTo');
            });
        }
        if (!(Schema::hasColumn('users', 'State'))) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('State')->nullable()->after('City');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (Schema::hasColumn('users', 'Description')) {
            Schema::table('users', function(Blueprint $table) {
                $table->dropColumn('Description');
            });
        }
        if (Schema::hasColumn('users', 'State')) {
            Schema::table('users', function(Blueprint $table) {
                $table->dropColumn('State');
            });
        }
    }

}
