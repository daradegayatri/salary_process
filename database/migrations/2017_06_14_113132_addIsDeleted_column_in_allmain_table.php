<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDeletedColumnInAllmainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('customers','IsDeleted')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('IsDeleted');
            });
        }
        if(!Schema::hasColumn('customers','IsDeleted')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->boolean('IsDeleted')->default('0');
            });
        }
        if(Schema::hasColumn('customers','ChildCustId')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('ChildCustId');
            });
        }
        if(!Schema::hasColumn('customers','ChildCustId')) {
            \Schema::table('customers', function (Blueprint $table) {
                $table->unsignedInteger('ChildCustId')->nullable();
            });
        }
        if(Schema::hasColumn('invoices','IsDeleted')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->dropColumn('IsDeleted');
            });
        }
        if(!Schema::hasColumn('invoices','IsDeleted')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->boolean('IsDeleted')->default('0');
            });
        }
        if(Schema::hasColumn('tasklist','IsDeleted')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->dropColumn('IsDeleted');
            });
        }
        if(!Schema::hasColumn('tasklist','IsDeleted')) {
            \Schema::table('tasklist', function (Blueprint $table) {
                $table->boolean('IsDeleted')->default('0');
            });
        }
        if(Schema::hasColumn('orders','IsDeleted')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('IsDeleted');
            });
        }

        if(!Schema::hasColumn('orders','IsDeleted')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->boolean('IsDeleted')->default('0');
            });
        }




    }
    public function down()
    {
        \Schema::table('tasklist', function(Blueprint $table) {
            $table->dropColumn('IsDeleted');
        });
        \Schema::table('invoices', function(Blueprint $table) {
            $table->dropColumn('IsDeleted');
        });
        \Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn('IsDeleted');
            $table->dropColumn('ChildCustId');
        });
        \Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('IsDeleted');
        });
    }
}
