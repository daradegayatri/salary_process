<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRepeatTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_repeat_types')) {
            Schema::create('event_repeat_types', function (Blueprint $table) {
                $table->increments('RepeatTypeID');
                $table->string('RepeatType')->nullable();
                $table->unsignedInteger('OrganizationID')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('RepeatType');
                $table->foreign('OrganizationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_repeat_types');
    }
}
