<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuborganisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('sub_organisations')) {
            Schema::create('sub_organisations', function (Blueprint $table) {
                $table->increments('SubOrganisationID');
                $table->unsignedInteger('ParentOrganisationID')->nullable();
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('sub_organisations');

                if(!in_array('sub_organisations_parentorganisationid_foreign', $foreignKeys))
                    $table->foreign('ParentOrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('sub_organisations_organisationid_foreign', $foreignKeys))
                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_organisations', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('sub_organisations');

            if(in_array('sub_organisations_parentorganisationid_foreign', $foreignKeys))      $table->dropForeign('sub_organisations_parentorganisationid_foreign');
            if(in_array('sub_organisations_organisationid_foreign', $foreignKeys))      $table->dropForeign('sub_organisations_organisationid_foreign');

        });
        Schema::drop('sub_organisations');
    }
}
