<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnnsInOrderTable extends Migration
{
    public function up()
    {
        //OrderID, WorkOrderID, UserID, LocationID, InventoryID, InvoiceID, JobID, OrderStatusID, NoOfInventory
        // OrderId, OrderName, OrderDescription, OrderType, OrderStatus, OrderFrom, OrderTo, ShipToAddress,
        // BillToAddress, Customer Id , Email, Phone, SignaturePad, OrderDescription, OrderValueTotal,
        // OrderTaxTotal, TaskId, Disclaimer, OrderShipDate, EstimatedShippingTime, ShipmentTrackingLink,
        // RelatedOrderId

        if(!Schema::hasColumn('orders','OrderName')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->string('OrderName')->after('OrderID')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderDescription')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->string('OrderDescription')->after('OrderName')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderTypeID')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->integer('OrderTypeID')->after('OrderDescription')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderFrom')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('OrderFrom')->after('OrderTypeID')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderTo')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->unsignedInteger('OrderTo')->after('OrderFrom')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','OrderShipDate')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->dateTime('OrderShipDate')->after('OrderTo')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','EstimatedShippingTime')) {
            \Schema::table('orders', function (Blueprint $table) {
                $table->dateTime('EstimatedShippingTime')->after('OrderShipDate')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','SignaturePad')){
            \Schema::table('orders', function (Blueprint $table) {
                $table->string('SignaturePad')->after('EstimatedShippingTime')->nullable();
            });
        }
        if(!Schema::hasColumn('orders','IsInternal')){
            \Schema::table('orders', function (Blueprint $table) {
                $table->boolean('IsInternal')->after('OrderTypeID')->nullable();// 1 for internal, 0 for external
            });
        }
    }
    public function down()
    {
        \Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('OrderName');
            $table->dropColumn('OrderDescription');
            $table->dropColumn('OrderTypeID');
            $table->dropColumn('OrderFrom');
            $table->dropColumn('OrderTo');
            $table->dropColumn('OrderShipDate');
            $table->dropColumn('EstimatedShippingTime');
            $table->dropColumn('SignaturePad');
            $table->dropColumn('IsInternal');
        });
    }
}
