<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_content', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('content');

            $table->integer('cms_category_id')->unsigned()->nullable();
            $table->foreign('cms_category_id')->references('id')->on('cms_categories')->onDelete('cascade');

            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('OrganisationID')->on('organisation')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_content');
    }
}
