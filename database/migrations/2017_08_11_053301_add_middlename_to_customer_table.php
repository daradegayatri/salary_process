<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMiddlenameToCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('customers', 'MiddleName'))) {
            Schema::table('customers', function (Blueprint $table) {
                $table->string('MiddleName')->nullable()->after('FirstName');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!(Schema::hasColumn('customers', 'MiddleName'))) {
            Schema::table('customers', function(Blueprint $table) {
                $table->dropColumn('MiddleName');
            });
        }
    }
}
