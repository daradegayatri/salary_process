<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('salary_users')) {
            Schema::create('salary_users', function (Blueprint $table) {
                $table->Increments('id');
                //$table->integer('UserID');
                $table->string('email')->unique()->nullable();
                $table->string('username')->unique();
                $table->string('password');
                $table->string('name');
                $table->string('firstname')->nullable();
                $table->string('lastname')->nullable();
                $table->string('phoneno')->nullable()->unique();
                $table->string('gender')->nullable();
                $table->string('address')->nullable();
                $table->string('city')->nullable();
                $table->string('country')->nullable();
                $table->string('postalcode')->nullable();
                $table->string('provider');
                $table->string('provider_id');
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_users');
    }
}
