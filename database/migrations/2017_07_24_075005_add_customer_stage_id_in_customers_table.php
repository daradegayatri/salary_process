<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerStageIdInCustomersTable extends Migration
{

    public function up()
    {
        if (!(Schema::hasColumn('customers', 'CustomerStageID'))) {
            Schema::table('customers', function (Blueprint $table){
                $table->integer('CustomerStageID')->nullable()->after('CustomerTypeID');
            });
        }
    }

    public function down()
    {
        if (Schema::hasColumn('customers', 'CustomerStageID')) {
            Schema::table('customers', function(Blueprint $table) {
                $table->dropColumn('CustomerStageID');
            });
        }
    }
}
