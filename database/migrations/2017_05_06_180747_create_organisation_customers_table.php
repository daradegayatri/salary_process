<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisationCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * Table Name: organisation_customers
id	int PK
OrganisationID	int
CustomerID	int
CreatedOn	datetime
CreatedBy	int
ModifiedOn	datetime
ModifiedBy	int
*/
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('organisation_customers')) {
            Schema::create('organisation_customers', function (Blueprint $table) {
                $table->increments('OrganisationCustomerID');
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->unsignedInteger('CustomerID')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('customers');

                if(!in_array('organisation_customers_organisationid_foreign', $foreignKeys))
                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
                if(!in_array('organisation_customers_customerid_foreign', $foreignKeys))
                    $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisation_customers', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('organisation_customers');

            if(in_array('organisation_customers_organisationid_foreign', $foreignKeys))      $table->dropForeign('organisation_customers_organisationid_foreign');
            if(in_array('organisation_customers_customerid_foreign', $foreignKeys))      $table->dropForeign('organisation_customers_customerid_foreign');

        });
        Schema::drop('organisation_customers');
    }
}
