<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if (!Schema::hasTable('order_from')) {
            Schema::create('order_from', function (Blueprint $table) {
                $table->increments('OrderFromID');
                $table->unsignedInteger('CustomerID')->nullable();
                $table->integer('Self')->nullable();
                $table->timestamps();
                $foreignKeys = $this->listTableForeignKeys('order_from');

                if(!in_array('order_from_customerid_foreign', $foreignKeys))
                    $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_from', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('order_from');

            if(in_array('order_from_customerid_foreign', $foreignKeys))      $table->dropForeign('order_from_customerid_foreign');

        });
        Schema::drop('order_from');
    }
}
