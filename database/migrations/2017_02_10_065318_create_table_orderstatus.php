<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_status')) {
            Schema::create('order_status', function (Blueprint $table) {
                $table->increments('OrderStatusID');
                $table->string('OrderStatus')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('OrderStatus');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_status');
    }
}
