<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_details')) {
            Schema::create('event_details', function (Blueprint $table) {
                $table->increments('EventDetailsID');
                $table->unsignedBigInteger('TaskID')->nullable();
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->unsignedInteger('CustomerID')->nullable();
                $table->date('StartDate')->nullable();
                $table->time('StartTime')->nullable();
                $table->date('EndDate')->nullable();
                $table->time('EndTime')->nullable();
                $table->string('Title',1024)->nullable();
                $table->string('Description',1024)->nullable();
                $table->string('Location')->nullable();
                $table->string('Organizer')->nullable();
                $table->timestamps();
                $table->index('TaskID');
                $table->index('OrganisationID');
                $table->index('EventDetailsID');
                $table->index('CustomerID');
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
                $table->foreign('TaskID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
                $table->foreign('CustomerID')->references('CustomerID')->on('customers')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('event_details');

    }
}
