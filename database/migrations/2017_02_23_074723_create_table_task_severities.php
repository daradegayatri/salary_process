<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaskSeverities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('task_severities')) {
            Schema::create('task_severities', function (Blueprint $table) {
                $table->increments('TaskSeverityID');
                $table->string('TaskSeverity')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('TaskSeverity');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_severities');
    }
}
