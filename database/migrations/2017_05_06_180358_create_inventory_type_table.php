<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_type', function(Blueprint $table) {
            $table->increments('InventoryTypeID');
            $table->string('InventoryType', 255)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('inventory_type');
    }
}
