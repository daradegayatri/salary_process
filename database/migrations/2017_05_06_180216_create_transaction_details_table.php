<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaction_details')) {
            Schema::create('transaction_details', function (Blueprint $table) {
                $table->increments('TransactionDetailsID');
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->unsignedInteger('CustomerID')->nullable();
                $table->unsignedInteger('OrderID')->nullable();
                $table->unsignedInteger('TransactionSourceID')->nullable();
                $table->unsignedInteger('SourceID')->nullable();
                $table->boolean('IsPaid')->nullable();
                $table->unsignedInteger('TransactionChargesID')->nullable();
                $table->timestamps();

            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_details');
    }
}
