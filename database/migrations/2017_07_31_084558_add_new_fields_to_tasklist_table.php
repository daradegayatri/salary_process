<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToTasklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('tasklist', 'cycle_type'))) {
            Schema::table('tasklist', function (Blueprint $table)
            {
                $table->string('cycle_type')->nullable();
            });
        }
        if (!(Schema::hasColumn('tasklist', 'cycle_value'))) {
            Schema::table('tasklist', function (Blueprint $table)
            {
                $table->integer('cycle_value')->nullable();
            });
        }
        if (!(Schema::hasColumn('tasklist', 'units'))) {
            Schema::table('tasklist', function (Blueprint $table)
            {
                $table->string('units')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasklist', function(Blueprint $table)
        {
            $table->dropcolumn('cycle_type');
            $table->dropcolumn('cycle_value');
            $table->dropcolumn('units');
        });
    }
}
