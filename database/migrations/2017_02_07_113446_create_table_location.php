<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('locations')) {
            Schema::create('locations', function (Blueprint $table) {
                $table->Increments('LocationID');
                $table->string('WorkLocationID')->nullable();
                $table->string('LocationName')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('LocationName');

            });

            DB::update("ALTER TABLE locations AUTO_INCREMENT = 3000001");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
