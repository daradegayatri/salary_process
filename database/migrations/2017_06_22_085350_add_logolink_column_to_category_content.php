<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogolinkColumnToCategoryContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('category_content', 'logolink'))) {
            Schema::table('category_content', function (Blueprint $table) {
                $table->string('logolink', 1024)->nullable()->after('organization_id');
            });
        }
    }

    public function down()
    {
        if (Schema::hasColumn('category_content', 'logolink')) {
            Schema::table('category_content', function(Blueprint $table) {
                $table->dropColumn('logolink');
            });
        }
    }
}
