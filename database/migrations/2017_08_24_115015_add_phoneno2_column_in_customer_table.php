<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneno2ColumnInCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('customers', 'Phoneno2')))
        {
            Schema::table('customers', function (Blueprint $table) {
                $table->integer('Phoneno2')->nullable()->after('Phoneno');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!(Schema::hasColumn('customers', 'Phoneno2')))
        {
            Schema::table('customers', function(Blueprint $table) {
                $table->dropColumn('Phoneno2');
            });
        }
    }
}
