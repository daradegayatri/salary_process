<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsdeletedColumntoInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('inventory','IsDeleted'))
        {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->boolean('IsDeleted')->after('AvaliableInventoryCount');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('inventory', function(Blueprint $table) {
            $table->dropColumn('IsDeleted');
        });
    }
}
