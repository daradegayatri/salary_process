<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceinventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('invoice_inventory'))
        {
            Schema::create('invoice_inventory', function (Blueprint $table) {
                $table->increments('InvoiceInventoryID');
                $table->integer('OrderID')->nullable();
                $table->integer('InventoryID')->nullable();
                $table->integer('NoOfInventory')->nullable();
                $table->integer('InventoryPrice')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_inventory');
    }
}
