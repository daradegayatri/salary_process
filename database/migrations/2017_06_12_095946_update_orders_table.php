<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (Schema::hasTable('orders')) {
            Schema::dropIfexists('orders');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements('OrderID');
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->string('OrderName')->nullable();
                $table->string('OrderDescription')->nullable();
                $table->integer('OrderTypeID')->nullable();
                $table->boolean('IsInternal')->nullable();
                $table->boolean('HasAttachment')->nullable();
                $table->unsignedInteger('SeverityID')->nullable();
                $table->unsignedInteger('PriorityID')->nullable();
                $table->string('OrderFrom')->nullable();
                $table->string('OrderTo')->nullable();
                $table->dateTime('OrderShipDate')->nullable();
                $table->dateTime('EstimatedShippingTime')->nullable();
                $table->string('SignaturePad')->nullable();
                $table->string('WorkOrderID')->nullable();
                $table->integer('UserID')->nullable();
                $table->integer('LocationID')->nullable();
                $table->integer('InventoryID')->nullable();
                $table->integer('InvoiceID')->nullable();
                $table->integer('JobID')->nullable();
                $table->integer('OrderStatusID')->nullable();
                $table->integer('NoOfInventory')->nullable();
                $table->string('ShipToAddress')->nullable();
                $table->string('BillToAddress')->nullable();
                $table->integer('OrderValueTotal')->nullable();
                $table->integer('OrderTaxTotal')->nullable();
                $table->unsignedInteger('TaskId')->nullable();
                $table->string('Disclaimer')->nullable();
                $table->string('ShipmentTrackingLink')->nullable();
                $table->integer('RelatedOrderId')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->boolean('IsDeleted')->nullable();
                $table->timestamps();
            });
            // DB::update("ALTER TABLE orders AUTO_INCREMENT = 5000001");
        }
        Schema::table('orders', function(Blueprint $table)
        {
            $table->unsignedInteger('UserID')->change();
            $table->unsignedInteger('LocationID')->change();
            $table->unsignedInteger('OrderStatusID')->change();


            $foreignKeys = $this->listTableForeignKeys('orders');

            if(!in_array('orders_userid_foreign', $foreignKeys))
                $table->foreign('UserID')->references('id')->on('users')->onDelete('cascade');
            if(!in_array('orders_locationid_foreign', $foreignKeys))
                $table->foreign('LocationID')->references('LocationID')->on('locations')->onDelete('cascade');
            if(!in_array('orders_orderStatusid_foreign', $foreignKeys))
                $table->foreign('OrderStatusID')->references('OrderStatusID')->on('order_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (Schema::hasTable('orders')) {
            Schema::dropIfexists('orders');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
