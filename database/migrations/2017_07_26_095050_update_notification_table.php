<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('notifications', function (Blueprint $table) {

            $foreignKeys = $this->listTableForeignKeys('notifications');

            if(in_array('notifications_locationid_foreign', $foreignKeys))         $table->dropForeign('notifications_locationid_foreign');
            if(in_array('notifications_notificationfor_foreign', $foreignKeys))      $table->dropForeign('notifications_notificationfor_foreign');
            if(in_array('notifications_tasklistid_foreign', $foreignKeys))      $table->dropForeign('notifications_tasklistid_foreign');

        });

        if(Schema::hasTable('notifications')) {
            Schema::dropIfexists('notifications');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        if (!Schema::hasTable('notifications'))
        {
            Schema::create('notifications', function (Blueprint $table) {
                $table->bigIncrements('NotificationID');
                $table->string('Notification')->nullable();
                $table->string('NotificationFor')->nullable();
                $table->integer('SourceID')->nullable();
                $table->string('SourceName')->nullable();
                $table->boolean('IsRead')->nullable();
                $table->integer('Status')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
        Schema::table('notifications', function(Blueprint $table)
        {
             $table->unsignedInteger('NotificationFor')->nullable()->change();
             $foreignKeys = $this->listTableForeignKeys('notifications');
            if(!in_array('notifications_notificationfor_foreign', $foreignKeys))
                $table->foreign('NotificationFor')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('notifications');
    }
}
