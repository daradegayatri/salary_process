<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAddressTable extends Migration
{

    /*
     * Table Name: customer_address
CustomerAddressID	int PK
Address1	string
Address2	string
City	string
State	string
Country	string
Zipcode	string
CreatedOn	datetime
CreatedBy	int
ModifiedOn	datetime
ModifiedBy	int
*/

    public function up()
    {
        if (!Schema::hasTable('customer_address')) {
            Schema::create('customer_address', function (Blueprint $table) {
                $table->increments('CustomerAddressID');
                $table->string('Address1')->nullable();
                $table->string('Address2')->nullable();
                $table->string('City')->nullable();
                $table->string('State')->nullable();
                $table->string('Country')->nullable();
                $table->string('Zipcode')->nullable();
                $table->integer('created_by')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_address');
    }
}
