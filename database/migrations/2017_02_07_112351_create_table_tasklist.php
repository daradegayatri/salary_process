<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tasklist')) {
            Schema::create('tasklist', function (Blueprint $table) {
                $table->bigIncrements('TasklistID');
                $table->string('TaskID')->nullable();
                $table->integer('JobID')->nullable();
                $table->integer('PriorityID')->nullable();
                $table->integer('LocationID')->nullable();
                $table->integer('TaskSeverityID')->nullable();
                $table->mediumText('Description')->nullable();
                $table->integer('StatusID')->nullable();
                $table->boolean('HasAttachment')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('JobID');
                $table->index('PriorityID');
                $table->index('TaskID');
                $table->index('LocationID');
                $table->index('StatusID');
                $table->timestamps();
            });

            DB::update("ALTER TABLE tasklist AUTO_INCREMENT = 200001");

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasklist');
    }
}
