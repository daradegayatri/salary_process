<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndCreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        Schema::table('orders', function (Blueprint $table) {
//            $table->dropForeign('orders_invoiceid_foreign');
//            $table->dropForeign('orders_jobid_foreign');
//            $table->dropForeign('orders_locationid_foreign');
//            $table->dropForeign('orders_orderstatusid_foreign');
//            $table->dropForeign('orders_organisationid_foreign');
//            $table->dropForeign('orders_userid_foreign');
//        });
//
//        if (Schema::hasTable('orders')) {
//            Schema::dropIfexists('orders');
//        }
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//
//        if (!Schema::hasTable('orders')) {
//            Schema::create('orders', function (Blueprint $table) {
//                $table->bigIncrements('OrderID');
//                $table->unsignedInteger('OrganisationID')->nullable();
//                $table->string('OrderName')->nullable();
//                $table->string('OrderDescription')->nullable();
//                $table->integer('OrderTypeID')->nullable();
//                $table->boolean('IsInternal')->nullable();
//                $table->boolean('HasAttachment')->after('IsInternal')->nullable();
//                //$table->unsignedInteger('SeverityID')->nullable();
//               // $table->unsignedInteger('PriorityID')->nullable();
//                $table->string('OrderFrom')->nullable();
//                $table->string('OrderTo')->nullable();
//                $table->dateTime('OrderShipDate')->nullable();
//                $table->string('EstimatedShippingTime')->nullable();
//                $table->string('SignaturePad')->nullable();
//                $table->string('WorkOrderID')->nullable();
//                $table->integer('UserID')->nullable();
//                $table->integer('LocationID')->nullable();
//                $table->integer('InventoryID')->nullable();
//                $table->integer('InvoiceID')->nullable();
//                $table->integer('JobID')->nullable();
//                $table->integer('OrderStatusID')->nullable();
//                $table->integer('NoOfInventory')->nullable();
//                $table->integer('ShipToAddress')->nullable();
//                $table->integer('BillToAddress')->nullable();
//                $table->integer('OrderValueTotal')->nullable();
//                $table->integer('OrderTaxTotal')->nullable();
//                $table->unsignedInteger('TaskId')->nullable();
//                $table->string('Disclaimer')->nullable();
//                $table->string('ShipmentTrackingLink')->nullable();
//                $table->integer('RelatedOrderId')->nullable();
//                $table->integer('CreatedBy')->nullable();
//                $table->integer('updated_by')->nullable();
//                $table->timestamps();
//            });
           // DB::update("ALTER TABLE orders AUTO_INCREMENT = 5000001");
 //       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //  Schema::drop('orders');
    }
}
