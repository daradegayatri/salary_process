<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsertaskstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //user task can be Completed or  Inprogress or  Hold  or Cancelled

        if (!Schema::hasTable('user_task_status')) {
            Schema::create('user_task_status', function (Blueprint $table) {
                $table->increments('UserTaskStatusID');
                $table->string('UserTaskStatus')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('UserTaskStatus');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_task_status');
    }
}
