<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //InvoiceID, Rate, Amount, Description, ToCompany, ForCompany, BillTo, ShipTo, CreatedBy, updated_by, created_at, updated_at
        \Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('Rate');
            $table->dropColumn('Description');
            $table->dropColumn('ToCompany');
            $table->dropColumn('ForCompany');
            $table->dropColumn('BillTo');
            $table->dropColumn('ShipTo');
        });

        if (!(Schema::hasColumn('invoices', 'InvoiceTo') &&
            Schema::hasColumn('invoices', 'InvoicedisplayID') &&
            Schema::hasColumn('invoices', 'RaisedInvoice'))
        ) {

            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('InvoicedisplayID')->after('Amount');
                $table->integer('InvoiceTo')->after('InvoicedisplayID');
                $table->string('OrderID')->after('InvoicedisplayID');
                $table->boolean('RaisedInvoice')->after('InvoiceTo');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (!(Schema::hasColumn('invoices', 'Rate') &&
            Schema::hasColumn('invoices', 'Description') &&
            Schema::hasColumn('invoices', 'ToCompany') &&
            Schema::hasColumn('invoices', 'ForCompany') &&
            Schema::hasColumn('invoices', 'BillTo') &&
            Schema::hasColumn('invoices', 'ShipTo'))
        ) {

            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('Rate');
                $table->string('Description');
                $table->integer('ToCompany');
                $table->integer('ForCompany');
                $table->integer('BillTo');
                $table->integer('ShipTo');
                //$table->string('rejected_by');
            });
        }

        \Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('InvoiceTo');
            $table->dropColumn('InvoicedisplayID');
            $table->dropColumn('OrderID');
            $table->dropColumn('RaisedInvoice');
        });


    }
}
