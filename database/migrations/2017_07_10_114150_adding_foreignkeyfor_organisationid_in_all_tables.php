<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingForeignkeyforOrganisationidInAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {

        Schema::table('users', function(Blueprint $table)
        {
            $table->unsignedInteger('OrganisationID')->change();
            $foreignKeys = $this->listTableForeignKeys('users');
            if(!in_array('users_organisationid_foreign', $foreignKeys))
                $table->foreign('organisationid')->references('OrganisationID')->on('organisation')->onDelete('cascade');
        });
        Schema::table('tasklist', function(Blueprint $table)
        {

            $table->unsignedInteger('OrganisationID')->change();
            $foreignKeys = $this->listTableForeignKeys('tasklist');
            if(!in_array('tasklist_organisationid_foreign', $foreignKeys))
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');

        });

        Schema::table('orders', function(Blueprint $table)
        {
            $table->unsignedInteger('OrganisationID')->change();


            $foreignKeys = $this->listTableForeignKeys('orders');

            if(!in_array('orders_organisationid_foreign', $foreignKeys))
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');

        });


        Schema::table('invoices', function(Blueprint $table)
        {
            $table->unsignedInteger('OrganisationID')->change();

            $foreignKeys = $this->listTableForeignKeys('invoices');

            if(!in_array('invoices_OrganisationID_foreign', $foreignKeys))
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
        });

        Schema::table('inventory', function(Blueprint $table)
        {
            $table->unsignedInteger('OrganisationID')->change();

            $foreignKeys = $this->listTableForeignKeys('inventory');

            if(!in_array('inventory_organisationid_foreign', $foreignKeys))
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');
        });

        Schema::table('transaction_details', function(Blueprint $table)
        {
            $table->unsignedInteger('OrganisationID')->change();


            $foreignKeys = $this->listTableForeignKeys('orders');

            if(!in_array('transaction_details_organisationid_foreign', $foreignKeys))
                $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onDelete('cascade');

        });

    }

    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('users');
            if(in_array('users_organisationid_foreign', $foreignKeys))                   $table->dropForeign('users_organisationid_foreign');
            $table->integer('OrganisationID')->nullable()->change();

        });
        Schema::table('tasklist', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('tasklist');

            if(in_array('tasklist_priorityid_foreign', $foreignKeys))            $table->dropForeign('tasklist_priorityid_foreign');
            if(in_array('tasklist_locationid_foreign', $foreignKeys))            $table->dropForeign('tasklist_locationid_foreign');
            if(in_array('tasklist_taskseverityid_foreign', $foreignKeys))        $table->dropForeign('tasklist_taskseverityid_foreign');
            if(in_array('tasklist_statusid_foreign', $foreignKeys))              $table->dropForeign('tasklist_statusid_foreign');

            $table->integer('PriorityID')->nullable()->change();
            $table->integer('LocationID')->nullable()->change();
            $table->integer('TaskSeverityID')->nullable()->change();
            $table->integer('StatusID')->nullable()->change();
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('tasklist');

            if(in_array('orders_userid_foreign', $foreignKeys))         $table->dropForeign('orders_userid_foreign');
            if(in_array('orders_locationid_foreign', $foreignKeys))     $table->dropForeign('orders_locationid_foreign');
            if(in_array('orders_invoiceid_foreign', $foreignKeys))      $table->dropForeign('orders_invoiceid_foreign');
            if(in_array('orders_jobid_foreign', $foreignKeys))          $table->dropForeign('orders_jobid_foreign');
            if(in_array('orders_orderstatusid_foreign', $foreignKeys))  $table->dropForeign('orders_orderstatusid_foreign');

            $table->integer('UserID')->nullable()->change();
            $table->integer('LocationID')->nullable()->change();
            $table->integer('InvoiceID')->nullable()->change();
            $table->integer('JobID')->nullable()->change();
            $table->integer('OrderStatusID')->nullable()->change();
        });


        Schema::table('invoices', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('invoices');

            if(in_array('invoices_invoiceto_foreign', $foreignKeys))     $table->dropForeign('invoices_invoiceto_foreign');
            $table->integer('InvoiceTo')->nullable()->change();
        });

    }
}
