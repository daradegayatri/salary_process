<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_message', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('email_template_id')->unsigned();
            $table->foreign('email_template_id')->references('id')->on('email_template')->onDelete('cascade');
            $table->string('subject');
            $table->text('html');
            $table->text('replacements')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_message');
    }
}
