<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnddateColumnToTasklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('tasklist', 'EndDate')))
        {
            Schema::table('tasklist', function (Blueprint $table) {
                $table->date('EndDate')->nullable()->after('DueDate');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!(Schema::hasColumn('tasklist', 'EndDate')))
        {
            Schema::table('tasklist', function(Blueprint $table) {
                $table->dropColumn('EndDate');
            });
        }
    }
}
