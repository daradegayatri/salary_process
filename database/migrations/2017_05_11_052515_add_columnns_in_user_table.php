<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnnsInUserTable extends Migration
{
      public function up()
    {
        if(!Schema::hasColumn('users','UserImage')) {
            \Schema::table('users', function (Blueprint $table) {
                $table->string('UserImage')->after('AssignedTo');
            });
        }
    }
    public function down()
    {
        \Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('UserImage');
        });
    }
}
