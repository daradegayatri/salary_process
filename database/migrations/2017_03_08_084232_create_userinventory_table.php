<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserinventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_inventory'))
        {
            Schema::create('user_inventory', function (Blueprint $table) {
                $table->increments('UserInventoryID');
                $table->integer('UserID')->nullable();
                $table->integer('InventoryID')->nullable();
                $table->integer('NoOfInventory')->nullable();
                $table->integer('IsReturned')->nullable();
                $table->integer('ReturnedInventoryCount')->nullable();
                $table->integer('AvaliableInventoryCount')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_inventory');
    }
}
