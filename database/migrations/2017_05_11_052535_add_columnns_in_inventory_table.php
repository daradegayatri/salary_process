<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnnsInInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('inventory','SKU')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('SKU')->after('Description')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','SkuCode')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('SkuCode')->after('SKU')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','IsSubAsset')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->boolean('IsSubAsset')->after('SkuCode')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','ParentAssetID')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->unsignedInteger('ParentAssetID')->after('IsSubAsset')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','ExpiryDate')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->date('ExpiryDate')->after('ParentAssetID')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','Margin')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('Margin')->after('Price')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','ThresholdPercent')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('ThresholdPercent')->after('Margin')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','CentralTaxPercent')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('CentralTaxPercent')->after('ThresholdPercent')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory',' StateTaxPercent')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('StateTaxPercent')->after('CentralTaxPercent')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','OtherTaxPercent')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('OtherTaxPercent')->after('StateTaxPercent')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','OtherCost')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('OtherCost')->after('OtherTaxPercent')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','Bucket')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('Bucket')->after('AvaliableInventoryCount')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','Shelf')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('Shelf')->after('Bucket')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','RFIDTag')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('RFIDTag')->after('Shelf')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','BarCode')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('BarCode')->after('RFIDTag')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','OrganisationID')) {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->string('OrganisationID')->after('Description')->nullable();
            });
        }
    }
    public function down()
    {
        \Schema::table('inventory', function(Blueprint $table) {
            $table->dropColumn('SKU');
            $table->dropColumn('SkuCode');
            $table->dropColumn('IsSubAsset');
            $table->dropColumn('ParentAssetID');
            $table->dropColumn('ExpiryDate');
            $table->dropColumn('Margin');
            $table->dropColumn('ThresholdPercent');
            $table->dropColumn('CentralTaxPercent');
            $table->dropColumn('StateTaxPercent');
            $table->dropColumn('OtherTaxPercent');
            $table->dropColumn('OtherCost');
            $table->dropColumn('Bucket');
            $table->dropColumn('Shelf');
            $table->dropColumn('RFIDTag');
            $table->dropColumn('BarCode');
            $table->dropColumn('OrganisationID');
        });
    }
}
