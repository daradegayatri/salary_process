<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrganisatioinidInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if(!Schema::hasColumn('users','OrganisationID'))
        {
            \Schema::table('users', function (Blueprint $table) {
                $table->unsignedInteger('OrganisationID')->after('UserRoleID');

//                $foreignKeys = $this->listTableForeignKeys('users');
//
//                if(!in_array('users_organisationid_foreign', $foreignKeys))
//                    $table->foreign('OrganisationID')->references('OrganisationID')->on('organisation')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
//            $foreignKeys = $this->listTableForeignKeys('users');
//
//            if(in_array('users_organisationid_foreign', $foreignKeys))      $table->dropForeign('users_organisationid_foreign');


        });
        \Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('OrganisationID');
        });
    }
}
