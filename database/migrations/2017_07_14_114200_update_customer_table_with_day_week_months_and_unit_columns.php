<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomerTableWithDayWeekMonthsAndUnitColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table)
        {
            $table->string('cycle_type')->nullable();
            $table->integer('cycle_value')->nullable();
            $table->string('units')->nullable();
            $table->string('jobtitle')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->dropcolumn('cycle_type');
            $table->dropcolumn('cycle_value');
            $table->dropcolumn('units');
            $table->dropcolumn('jobtitle');
        });
    }
}
