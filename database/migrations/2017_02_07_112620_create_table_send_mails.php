<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSendMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('send_mails'))
        {
            Schema::create('send_mails', function (Blueprint $table) {
                $table->increments('SendMailID');
                $table->string('UserID')->nullable();
                $table->string('Name')->nullable();
                $table->string('EmailID')->nullable();
                $table->string('Subject')->nullable();
                $table->string('Message')->nullable();
                $table->integer('Status')->nullable();//status can be 1 =>success, 0=>not sent, -1 =>failed
                $table->boolean('HasAttachment')->nullable(); // 0 if no attachment, 1 if has attachment
                //$table->dateTime('CreatedOn')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->index('Status');
                $table->index('EmailID');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('send_mails');
    }
}
