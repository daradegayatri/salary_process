<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssignusertask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assign_user_task')) {
            Schema::create('assign_user_task', function (Blueprint $table) {
                $table->increments('AssignUserTaskID');
                $table->integer('UserTaskStatusID')->nullable();
                $table->integer('TasklistID')->nullable();
                $table->integer('AssignedTo')->nullable();
                $table->integer('AssignedBy')->nullable();
                $table->integer('CreatedBy')->nullable();
                //$table->dateTime('CreatedOn')->nullable();
                //$table->dateTime('ModifiedOn')->nullable();
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->index('UserTaskStatusID');
                $table->index('AssignedTo');
                $table->index('AssignedBy');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assign_user_task');
    }
}
