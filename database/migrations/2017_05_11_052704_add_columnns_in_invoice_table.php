<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnnsInInvoiceTable extends Migration
{
    //( Standard , credit Memo , Debit Memo , Mixed invoice , Prepayment Invoice , Withholding Tax Invoice , Timesheet , Expenses , PO Default Invoice)
   //InvoiceID, Amount, InvoicedisplayID, OrderID, InvoiceTo, RaisedInvoice, CreatedBy, updated_by, created_at, updated_at
    /*
     *
     *
     * Invoice Id , InvoiceName , Raisedon , Dueon , PastDue , CustomerId (allows multiple selection) , OrderId (allows multiple selection) , Task Id (allows multiple selection) ,
InvoiceTypeID  ,Company Logo , Email To , SpecialInstructions , Description , Status (paid , not paid )*/


    public function up()
    {
        if(!Schema::hasColumn('invoices','InvoiceName')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('InvoiceName')->after('InvoiceID')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','InvoiceTypeID')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->date('InvoiceTypeID')->after('InvoiceName')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','Raisedon')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->dateTime('Raisedon')->after('RaisedInvoice')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','DueOn')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->dateTime('DueOn')->after('Raisedon')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','IsPending')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->boolean('IsPending')->after('DueOn')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','SpecialInstructions')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('SpecialInstructions',1024)->after('IsPending')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','Description')) {
            \Schema::table('invoices', function (Blueprint $table) {
                $table->string('Description',1024)->after('SpecialInstructions')->nullable();
            });
        }
        if(!Schema::hasColumn('invoices','IsPaid')){
            \Schema::table('invoices', function (Blueprint $table) {
                $table->boolean('IsPaid')->after('Description')->nullable();
            });
        }

    }
    public function down()
    {
        \Schema::table('invoices', function(Blueprint $table) {
            $table->dropColumn('InvoiceName');
            $table->dropColumn('InvoiceTypeID');
            $table->dropColumn('Raisedon');
            $table->dropColumn('DueOn');
            $table->dropColumn('IsPending');
            $table->dropColumn('Margin');
            $table->dropColumn('SpecialInstructions');
            $table->dropColumn('Description');
            $table->dropColumn('IsPaid');
        });
    }
}
