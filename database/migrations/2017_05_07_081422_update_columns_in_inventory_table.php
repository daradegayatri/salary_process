<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {
        if(!Schema::hasColumn('inventory','InventoryTypeID'))
        {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->unsignedInteger('InventoryTypeID')->after('InventoryDisplayID')->nullable();
//                $foreignKeys = $this->listTableForeignKeys('inventory');

//                if(!in_array('inventory_inventorytypeid_foreign', $foreignKeys))
//                    $table->foreign('InventoryTypeID')->references('InventoryTypeID')->on('inventory_type')->onUpdate('cascade')->onDelete('cascade');
            });
        }
        if(!Schema::hasColumn('inventory','Cost'))
        {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->double('Cost')->after('Price')->nullable();
            });
        }
        if(!Schema::hasColumn('inventory','Tax'))
        {
            \Schema::table('inventory', function (Blueprint $table) {
                $table->unsignedInteger('Tax')->after('Price')->nullable();
            });
        }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('inventory');

            if(in_array('inventory_inventorytypeid_foreign', $foreignKeys))      $table->dropForeign('inventory_inventorytypeid_foreign');


        });
        \Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('InventoryTypeID');
            $table->dropColumn('Cost');
            $table->dropColumn('Tax');
        });
    }
}
