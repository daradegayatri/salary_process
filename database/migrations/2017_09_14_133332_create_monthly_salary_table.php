<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlySalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('monthly_salary')) {
            Schema::create('monthly_salary', function (Blueprint $table) {
                $table->Increments('id');
                $table->integer('salary_user_id')->unsigned();
                $table->foreign('salary_user_id')->references('id')->on('salary_users')->onUpdate('cascade')->onDelete('cascade');
                $table->decimal('basics',10,2)->nullable();
                $table->decimal('HRA',10,2)->nullable();
                $table->decimal('DA',10,2)->nullable();
                $table->date('month')->nullable();
                $table->date('year')->nullable();

                $table->rememberToken();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_salary');
    }
}
