<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeystoalltables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
            return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }

    public function up()
    {

        Schema::table('users', function(Blueprint $table)
        {
            $table->unsignedInteger('AssignedTo')->change();
            $table->unsignedInteger('UserRoleID')->change();

            $foreignKeys = $this->listTableForeignKeys('users');

            if(!in_array('users_assignedto_foreign', $foreignKeys))
            $table->foreign('AssignedTo')->references('id')->on('users')->onDelete('cascade');
            if(!in_array('users_userroleid_foreign', $foreignKeys))
                $table->foreign('UserRoleID')->references('UserRoleID')->on('user_role')->onDelete('cascade');
        });

        Schema::table('user_inventory', function(Blueprint $table)
        {

                $table->unsignedInteger('UserID')->change();
            $table->unsignedBigInteger('InventoryID')->change();

            $foreignKeys = $this->listTableForeignKeys('user_inventory');

            if(!in_array('user_inventory_userid_foreign', $foreignKeys))
            $table->foreign('UserID')->references('id')->on('users')->onDelete('cascade');
            if(!in_array('user_inventory_inventoryid_foreign', $foreignKeys))
            $table->foreign('InventoryID')->references('InventoryID')->on('inventory')->onDelete('cascade');
        });

        Schema::table('tasklist', function(Blueprint $table)
        {
            $table->unsignedInteger('PriorityID')->change();
            $table->unsignedInteger('LocationID')->change();
            $table->unsignedInteger('TaskSeverityID')->change();
            $table->unsignedInteger('StatusID')->change();

            $foreignKeys = $this->listTableForeignKeys('tasklist');

            if(!in_array('tasklist_locationid_foreign', $foreignKeys))
            $table->foreign('LocationID')->references('LocationID')->on('locations')->onDelete('cascade');
            if(!in_array('tasklist_taskseverityid_foreign', $foreignKeys))
                $table->foreign('TaskSeverityID')->references('TaskSeverityID')->on('task_severities')->onDelete('cascade');
            if(!in_array('tasklist_statusid_foreign', $foreignKeys))
            $table->foreign('StatusID')->references('TaskStatusID')->on('task_status')->onDelete('cascade');
            if(!in_array('tasklist_priorityid_foreign', $foreignKeys))
            $table->foreign('PriorityID')->references('PriorityID')->on('priorities')->onDelete('cascade');
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $table->unsignedInteger('UserID')->change();
            $table->unsignedInteger('LocationID')->change();
            $table->unsignedBigInteger('InvoiceID')->change();
            $table->unsignedInteger('JobID')->change();
            $table->unsignedInteger('OrderStatusID')->change();


            $foreignKeys = $this->listTableForeignKeys('orders');

            if(!in_array('orders_userid_foreign', $foreignKeys))
                $table->foreign('UserID')->references('id')->on('users')->onDelete('cascade');
            if(!in_array('orders_locationid_foreign', $foreignKeys))
            $table->foreign('LocationID')->references('LocationID')->on('locations')->onDelete('cascade');
            if(!in_array('orders_invoiceid_foreign', $foreignKeys))
            $table->foreign('InvoiceID')->references('InvoiceID')->on('invoices')->onDelete('cascade');
            if(!in_array('orders_jobid_foreign', $foreignKeys))
            $table->foreign('JobID')->references('JobID')->on('jobs')->onDelete('cascade');
            if(!in_array('orders_orderStatusid_foreign', $foreignKeys))
            $table->foreign('OrderStatusID')->references('OrderStatusID')->on('order_status')->onDelete('cascade');
        });

        Schema::table('order_inventory', function(Blueprint $table)
        {
            $table->unsignedBigInteger('InventoryID')->change();
            $table->unsignedBigInteger('OrderID')->change();

            $foreignKeys = $this->listTableForeignKeys('order_inventory');

            if(!in_array('order_inventory_inventoryid_foreign', $foreignKeys))
            $table->foreign('InventoryID')->references('InventoryID')->on('inventory')->onDelete('cascade');
            if(!in_array('order_inventory_orderid_foreign', $foreignKeys))
            $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
        });

        Schema::table('invoices', function(Blueprint $table)
        {
            $table->unsignedInteger('InvoiceTo')->change();

            $foreignKeys = $this->listTableForeignKeys('invoices');

            if(!in_array('invoices_invoiceto_foreign', $foreignKeys))
            $table->foreign('InvoiceTo')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('comments', function(Blueprint $table)
        {
            $table->unsignedBigInteger('TasklistID')->change();
            $table->unsignedInteger('UserID')->change();

            $foreignKeys = $this->listTableForeignKeys('comments');

            if(!in_array('comments_tasklistid_foreign', $foreignKeys))
            $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
            if(!in_array('comments_userid_foreign', $foreignKeys))
            $table->foreign('UserID')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('assign_user_task', function(Blueprint $table)
        {
            $table->unsignedBigInteger('TasklistID')->change();
            $table->unsignedInteger('UserTaskStatusID')->change();
            $table->unsignedInteger('AssignedTo')->change();
            $table->unsignedInteger('AssignedBy')->change();

            $foreignKeys = $this->listTableForeignKeys('assign_user_task');

            if(!in_array('assign_user_task_usertaskstatusid_foreign', $foreignKeys))
             $table->foreign('UserTaskStatusID')->references('UserTaskStatusID')->on('user_task_status')->onDelete('cascade');

            if(!in_array('assign_user_task_tasklistid_foreign', $foreignKeys))
            $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');

            if(!in_array('assign_user_task_assignedto_foreign', $foreignKeys))
            $table->foreign('AssignedTo')->references('id')->on('users')->onDelete('cascade');

            if(!in_array('assign_user_task_assignedby_foreign', $foreignKeys))
            $table->foreign('AssignedBy')->references('id')->on('users')->onDelete('cascade');

        });

//        Schema::table('invoice_inventory', function(Blueprint $table)
//        {
//            $table->unsignedBigInteger('InventoryID')->change();
//            $table->unsignedBigInteger('OrderID')->change();
//            $table->foreign('InventoryID')->references('InventoryID')->on('inventory')->onDelete('cascade');
//            $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
//        });
//
//        Schema::table('task_order', function(Blueprint $table)
//        {
//            $table->unsignedBigInteger('TasklistID')->change();
//            $table->unsignedBigInteger('OrderID')->change();
//            $table->unsignedInteger('OrderStatusID')->change();
//
//            $table->foreign('TasklistID')->references('TasklistID')->on('tasklist')->onDelete('cascade');
//            $table->foreign('OrderStatusID')->references('id')->on('users')->onDelete('cascade');
//            $table->foreign('OrderID')->references('OrderID')->on('orders')->onDelete('cascade');
//        });


    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('users');

            if(in_array('users_assignedto_foreign', $foreignKeys))                   $table->dropForeign('users_assignedto_foreign');
            if(in_array('users_userroleid_foreign', $foreignKeys))                   $table->dropForeign('users_userroleid_foreign');

            $table->integer('AssignedTo')->nullable()->change();
            $table->integer('UserRoleID')->nullable()->change();
        });

        Schema::table('user_inventory', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('user_inventory');

            if(in_array('user_inventory_userid_foreign', $foreignKeys))              $table->dropForeign('user_inventory_userid_foreign');
            if(in_array('user_inventory_inventoryid_foreign', $foreignKeys))         $table->dropForeign('user_inventory_inventoryid_foreign');

            $table->integer('UserID')->nullable()->change();
            $table->bigInteger('InventoryID')->nullable()->change();
        });

        Schema::table('tasklist', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('tasklist');

            if(in_array('tasklist_priorityid_foreign', $foreignKeys))            $table->dropForeign('tasklist_priorityid_foreign');
            if(in_array('tasklist_locationid_foreign', $foreignKeys))            $table->dropForeign('tasklist_locationid_foreign');
            if(in_array('tasklist_taskseverityid_foreign', $foreignKeys))        $table->dropForeign('tasklist_taskseverityid_foreign');
            if(in_array('tasklist_statusid_foreign', $foreignKeys))              $table->dropForeign('tasklist_statusid_foreign');

            $table->integer('PriorityID')->nullable()->change();
            $table->integer('LocationID')->nullable()->change();
            $table->integer('TaskSeverityID')->nullable()->change();
            $table->integer('StatusID')->nullable()->change();
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('tasklist');

            if(in_array('orders_userid_foreign', $foreignKeys))         $table->dropForeign('orders_userid_foreign');
            if(in_array('orders_locationid_foreign', $foreignKeys))     $table->dropForeign('orders_locationid_foreign');
            if(in_array('orders_invoiceid_foreign', $foreignKeys))      $table->dropForeign('orders_invoiceid_foreign');
            if(in_array('orders_jobid_foreign', $foreignKeys))          $table->dropForeign('orders_jobid_foreign');
            if(in_array('orders_orderstatusid_foreign', $foreignKeys))  $table->dropForeign('orders_orderstatusid_foreign');

            $table->integer('UserID')->nullable()->change();
            $table->integer('LocationID')->nullable()->change();
            $table->integer('InvoiceID')->nullable()->change();
            $table->integer('JobID')->nullable()->change();
            $table->integer('OrderStatusID')->nullable()->change();
        });

        Schema::table('order_inventory', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('order_inventory');

            if(in_array('order_inventory_inventoryid_foreign', $foreignKeys))    $table->dropForeign('order_inventory_inventoryid_foreign');
            if(in_array('order_inventory_orderid_foreign', $foreignKeys))        $table->dropForeign('order_inventory_orderid_foreign');

            $table->integer('OrderID')->nullable()->change();
            $table->integer('InventoryID')->nullable()->change();
        });

        Schema::table('invoices', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('invoices');

            if(in_array('invoices_invoiceto_foreign', $foreignKeys))     $table->dropForeign('invoices_invoiceto_foreign');
            $table->integer('InvoiceTo')->nullable()->change();
        });



        Schema::table('comments', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('comments');

            if(in_array('comments_tasklistid_foreign', $foreignKeys))    $table->dropForeign('comments_tasklistid_foreign');
            if(in_array('comments_userid_foreign', $foreignKeys))        $table->dropForeign('comments_userid_foreign');

            $table->bigInteger('TasklistID')->nullable()->change();
            $table->integer('UserID')->nullable()->change();
        });
        Schema::table('assign_user_task', function(Blueprint $table)
        {
            $foreignKeys = $this->listTableForeignKeys('assign_user_task');

            if(in_array('assign_user_task_tasklistid_foreign', $foreignKeys))             $table->dropForeign('assign_user_task_tasklistid_foreign');
            if(in_array('assign_user_task_usertaskstatusid_foreign', $foreignKeys))       $table->dropForeign('assign_user_task_usertaskstatusid_foreign');
            if(in_array('assign_user_task_assignedto_foreign', $foreignKeys))             $table->dropForeign('assign_user_task_assignedto_foreign');
            if(in_array('assign_user_task_assignedby_foreign', $foreignKeys))             $table->dropForeign('assign_user_task_assignedby_foreign');

            $table->bigInteger('TasklistID')->nullable()->change();
            $table->integer('UserTaskStatusID')->nullable()->change();
            $table->integer('AssignedTo')->nullable()->change();
            $table->integer('AssignedBy')->nullable()->change();
        });

//        Schema::table('task_order', function(Blueprint $table)
//        {
//            $table->dropForeign('task_order_OrderID_foreign');
//            $table->dropForeign('task_order_TasklistID_foreign');
//            $table->dropForeign('task_order_OrderStatusID_foreign');
//
//            $table->integer('OrderID')->nullable()->change();
//            $table->bigInteger('TasklistID')->nullable()->change();
//            $table->integer('OrderStatusID')->nullable()->change();
//        });

//        Schema::table('invoice_inventory', function(Blueprint $table)
//        {
//            $table->dropForeign('invoice_inventory_InventoryID_foreign');
//            $table->dropForeign('invoice_inventory_OrderID_foreign');
//
//            $table->integer('OrderID')->nullable()->change();
//            $table->integer('InventoryID')->nullable()->change();
//        });








    }
}
