<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDropAndCreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('invoices')) {
            Schema::create('invoices', function (Blueprint $table) {
                $table->bigIncrements('InvoiceID');
                $table->String('InvoiceName')->nullable();
                $table->string('InvoiceTypeID')->nullable();
                $table->double('Amount')->nullable();
                $table->string('InvoicedisplayID')->nullable();
                $table->unsignedInteger('OrganisationID')->nullable();
                $table->string('OrderID')->nullable();
                $table->integer('InvoiceTo')->nullable();
                $table->boolean('RaisedInvoice')->nullable();
                $table->date('Raisedon')->nullable();
                $table->date('DueOn')->nullable();
                $table->string('PastDue')->nullable();
                $table->boolean('IsPending')->nullable();
                $table->string('SpecialInstructions',1024)->nullable();
                $table->string('Description',1024)->nullable();
                $table->boolean('IsPaid')->nullable();
                $table->string('CompanyLogo')->nullable();
                $table->string('EmailTo')->nullable();
                $table->integer('CreatedBy')->nullable();
                $table->integer('updated_by')->nullable();
                $table->integer('CustomerID')->nullable();
                $table->string('TaskID')->nullable();
                $table->timestamps();
            });
            DB::update("ALTER TABLE invoices AUTO_INCREMENT = 6000001");
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
