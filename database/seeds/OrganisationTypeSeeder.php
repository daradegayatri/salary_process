<?php

use Illuminate\Database\Seeder;
use App\Models\Users\OrganisationType;

class OrganisationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * Public
    Private
    Government
    Non-Government


     */
    public $OrganisationType=[
        [
            "OrganisationType"=>"Profit",
        ],
        [
            "OrganisationType"=>"Non-Profit",
        ],
        [
            "OrganisationType"=>"Government",
        ],

    ];

    public function run()
    {
        foreach($this->OrganisationType as $OrganisationTypes){
            $Organisation_Type= OrganisationType::where('OrganisationType','=',$OrganisationTypes['OrganisationType'])->get();
            $count= count($Organisation_Type);
            if($count == 0)
            {
                $now = date('Y-m-d H:i:s');
                $name=OrganisationType::create([
                    "OrganisationType"=>$OrganisationTypes["OrganisationType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
