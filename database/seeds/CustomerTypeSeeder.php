<?php

use Illuminate\Database\Seeder;
use App\Models\Users\CustomerType;
use Carbon\Carbon;

class CustomerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $customer=[
        [
            "CustomerType"=>"Internal",
        ],
        [
            "CustomerType"=>"Partner",
        ],
        [
            "CustomerType"=>"Channel",
        ],
        [
            "CustomerType"=>"Referral",
        ],
        [
            "CustomerType"=>"Chain",
        ],
        [
            "CustomerType"=>"Individual",
        ],
        [
            "CustomerType"=>"Lead",
        ],

    ];

    public function run()
    {

        foreach($this->customer as $customer){
            $customertype= CustomerType::where('CustomerType','=',$customer['CustomerType'])->get();
            $count= count($customertype);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=CustomerType::create([
                    "CustomerType"=>$customer["CustomerType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
