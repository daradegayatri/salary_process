<?php

use Illuminate\Database\Seeder;
use App\Models\Tasks\Taskstatus;
use Carbon\Carbon;

class TaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * Assigned
    Unassigned
    Blocked
    Deleted
     *  Open , Fulfilled ,Rejected

     */
    public $taskstatus=[
        [
            "TaskStatus"=>"Open",
        ],
        [
            "TaskStatus"=>"Fulfilled",
        ],
        [
            "TaskStatus"=>"Rejected",
        ],
//        [
//           "TaskStatus"=>"Open",
//        ],
//        [
//            "TaskStatus"=>"Work in Progress",
//        ],
//        [
//            "TaskStatus"=>"Pending",
//        ]
//        ,
//        [
//            "TaskStatus"=>"Closed",
//        ]
    ];

    public function run()
    {
        foreach($this->taskstatus as $task){
            $taskdetails= Taskstatus::where('TaskStatus','=',$task['TaskStatus'])->get();
            $count= count($taskdetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Taskstatus::create([
                    "TaskStatus"=>$task["TaskStatus"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
