<?php

use Illuminate\Database\Seeder;
use App\Models\General\Locations;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  //LocationID, WorkLocationID, LocationName, CreatedBy, updated_by, created_at, updated_at
    public $location = [
        [
             'LocationID'=>'3000001',
             'LocationName' => 'Lake',
             'created_by' => '1',
             'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000002',
            'LocationName' => 'Volleyball',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000003',
            'LocationName' => 'Tennis',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000004',
            'LocationName' => 'Archery',
            'created_by' => '1',
            'updated_by' => '1',
        ],

        [
            'LocationID'=>'3000005',
            'LocationName' => 'Rugby',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000006',
            'LocationName' => 'ACI',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000007',
            'LocationName' => 'Dining Hall',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000008',
            'LocationName' => 'Kitchen',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000009',
            'LocationName' => 'Dorms 1',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000010',
            'LocationName' => 'Dorms 2',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000011',
            'LocationName' => 'Dorms 3',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000012',
            'LocationName' => 'Dorms 4',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000013',
            'LocationName' => 'Dorms 5',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000014',
            'LocationName' => 'Visitor Center',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000015',
            'LocationName' => 'Track',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000016',
            'LocationName' => 'Throw',
            'created_by' => '1',
            'updated_by' => '1',
        ],

        [
            'LocationID'=>'3000017',
            'LocationName' => 'BMX-Beijing,London,Development',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000018',
            'LocationName' => 'Ropes',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000019',
            'LocationName' => 'Criterion Course',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000020',
            'LocationName' => 'Jogging Trail',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000021',
            'LocationName' => 'Soccer',
            'created_by' => '1',
            'updated_by' => '1',
        ],
        [
            'LocationID'=>'3000022',
            'LocationName' => 'Field Hockey',
            'created_by' => '1',
            'updated_by' => '1',
        ],

    ];
    public function run()
    {
        foreach($this->location as $locations){
            $inventory_details= Locations::where('LocationName','=',$locations['LocationName'])->get();
            $count= count($inventory_details);
            if($count == 0){
                $name=Locations::create([
                    'LocationID'=>$locations['LocationID'],
                    'LocationName'=> $locations['LocationName'],
                    'CreatedBy'=>$locations['created_by'],
                    'updated_by'=>$locations['updated_by'],
                ]);
                $id = $name->id;
                $locid = 'WL'.$id;
                DB::table('locations')->where('LocationID', '=', [$id])->update(array('WorkLocationID' => $locid));


            }
        }
    }

}
