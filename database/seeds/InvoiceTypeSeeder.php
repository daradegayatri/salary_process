<?php

use Illuminate\Database\Seeder;
use App\Models\Orders\InvoiceType;

class InvoiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //Standard , credit Memo , Debit Memo , Mixed invoice ,
    // Prepayment Invoice , Withholding Tax Invoice , Timesheet , Expenses , PO Default Invoice
    public $invoicetype=[
        [
            "InvoiceType"=>"Standard",
        ],
        [
            "InvoiceType"=>"Credit Memo",
        ],
        [
            "InvoiceType"=>"Debit Memo",
        ],
        [
            "InvoiceType"=>"Mixed invoice",
        ],
        [
            "InvoiceType"=>"Prepayment Invoice",
        ],
        [
            "InvoiceType"=>"Withholding Tax Invoice",
        ],
        [
            "InvoiceType"=>"Timesheet",
        ],
        [
            "InvoiceType"=>"Expenses",
        ],
        [
            "InvoiceType"=>"PO Default Invoice",
        ],

    ];

    public function run()
    {
        foreach($this->invoicetype as $invoicetypes){
            $invoice_type= InvoiceType::where('InvoiceType','=',$invoicetypes['InvoiceType'])->get();
            $count= count($invoice_type);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=InvoiceType::create([
                    "InvoiceType"=>$invoicetypes["InvoiceType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
