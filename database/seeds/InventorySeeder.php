<?php

use Illuminate\Database\Seeder;
use App\Models\Inventory\Inventory;
use Illuminate\Support\Facades\DB;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //InventoryID, InventoryName, InventorydisplayID, Description, Price,
    // MinInventoryCount, MaxInventoryCount, AvaliableInventoryCount, CreatedBy, updated_by, created_at, updated_at
    public $inventory = [
        [
            'InventoryID' =>'4000001',
            'InventoryName' =>'Hammer',
            'InventoryTypeID' => '2',
            'OrganisationID' =>'8000001',
            'SKU' =>'234',
            'SkuCode' =>'sk234',
            'ParentAssetID'=>'',
            'IsSubAsset'=>'0',
            'ExpiryDate' => '2017-06-03',
            'Margin' => '10',
            'ThresholdPercent' => '2',
            'CentralTaxPercent' => '2',
            'StateTaxPercent' =>'2',
            'OtherTaxPercent' => '2',
            'OtherCost' => '10',
            'Cost' => '50',
            'Price' => '60.74',
            'Tax' => '10',
            'Bucket' => '1',
            'Shelf' => '1',
            'RFIDTag' => '1',
            'BarCode' => '23456789',
            'MinInventoryCount' => '5',
            'Description' => 'Hammer',
            'MaxInventoryCount' => '100',
            'AvaliableInventoryCount' => '100',
            'IsDeleted' =>'0',
            'CreatedBy' => '100003',

        ],
        [
            'InventoryID' =>'4000002',
            'InventoryName' =>'Drill',
            'InventoryTypeID' => '2',
            'OrganisationID' =>'8000001',
            'SKU' =>'235',
            'SkuCode' =>'sk235',
            'ParentAssetID'=>'',
            'IsSubAsset'=>'0',
            'ExpiryDate' => '2017-06-03',
            'Margin' => '10',
            'ThresholdPercent' => '2',
            'CentralTaxPercent' => '2',
            'StateTaxPercent' =>'2',
            'OtherTaxPercent' => '2',
            'OtherCost' => '50',
            'Cost' => '50',
            'Price' => '100.74',
            'Tax' => '10',
            'Bucket' => '1',
            'Shelf' => '1',
            'RFIDTag' => '1',
            'BarCode' => '23456789',
            'MinInventoryCount' => '5',
            'Description' => 'Drill',
            'MaxInventoryCount' => '110',
            'AvaliableInventoryCount' => '110',
            'IsDeleted' =>'0',
            'CreatedBy' => '100003',
        ],
        [
            'InventoryID' =>'4000003',
            'InventoryName' =>'Dropper',
            'InventoryTypeID' => '2',
            'OrganisationID' =>'8000001',
            'SKU' =>'236',
            'SkuCode' =>'sk236',
            'ParentAssetID'=>'',
            'IsSubAsset'=>'0',
            'ExpiryDate' => '2017-06-03',
            'Margin' => '20',
            'ThresholdPercent' => '2',
            'CentralTaxPercent' => '2',
            'StateTaxPercent' =>'2',
            'OtherTaxPercent' => '2',
            'OtherCost' => '10',
            'Cost' => '50',
            'Price' => '70.74',
            'Tax' => '10',
            'Bucket' => '1',
            'Shelf' => '1',
            'RFIDTag' => '1',
            'BarCode' => '23456789',
            'MinInventoryCount' => '5',
            'Description' => 'Dropper',
            'MaxInventoryCount' => '80',
            'AvaliableInventoryCount' => '80',
            'IsDeleted' =>'0',
            'CreatedBy' => '100003',
        ],
        [
            'InventoryID' =>'4000004',
            'InventoryName' =>'Hand Saw',
            'InventoryTypeID' => '2',
            'OrganisationID' =>'8000001',
            'SKU' =>'237',
            'SkuCode' =>'sk237',
            'ParentAssetID'=>'',
            'IsSubAsset'=>'0',
            'ExpiryDate' => '2017-06-03',
            'Margin' => '10',
            'ThresholdPercent' => '2',
            'CentralTaxPercent' => '2',
            'StateTaxPercent' =>'2',
            'OtherTaxPercent' => '2',
            'OtherCost' => '30',
            'Cost' => '50',
            'Price' => '90.74',
            'Tax' => '10',
            'Bucket' => '1',
            'Shelf' => '1',
            'RFIDTag' => '1',
            'BarCode' => '23456789',
            'MinInventoryCount' => '5',
            'Description' => 'Dropper',
            'MaxInventoryCount' => '100',
            'AvaliableInventoryCount' => '100',
            'IsDeleted' =>'0',
            'CreatedBy' => '100003',
        ],

    ];
    public function run()
    {
        foreach($this->inventory as $inventories){
            $inventory_details= Inventory::where('InventoryName','=',$inventories['InventoryName'])->get();
            $count= count($inventory_details);
            if($count == 0){
                $name=Inventory::create([
                    'InventoryID' =>$inventories['InventoryID'],
                    'InventoryName' =>$inventories['InventoryName'],
                    'InventoryTypeID' => $inventories['InventoryTypeID'],
                    'OrganisationID' =>$inventories['OrganisationID'],
                    'SKU' =>$inventories['SKU'],
                    'SkuCode' =>$inventories['SkuCode'],
                    'ParentAssetID'=>$inventories['ParentAssetID'],
                    'IsSubAsset'=>$inventories['IsSubAsset'],
                    'ExpiryDate' => $inventories['ExpiryDate'],
                    'Margin' => $inventories['Margin'],
                    'ThresholdPercent' => $inventories['ThresholdPercent'],
                    'CentralTaxPercent' =>$inventories['CentralTaxPercent'],
                    'StateTaxPercent' =>$inventories['StateTaxPercent'],
                    'OtherTaxPercent' =>$inventories['OtherTaxPercent'],
                    'OtherCost' => $inventories['OtherCost'],
                    'Cost' => $inventories['Cost'],
                    'Price' => $inventories['Price'],
                    'Tax' => $inventories['Tax'],
                    'Bucket' => $inventories['Bucket'],
                    'Shelf' => $inventories['Shelf'],
                    'RFIDTag' =>$inventories['RFIDTag'],
                    'BarCode' => $inventories['BarCode'],
                    'MinInventoryCount' => $inventories['MinInventoryCount'],
                    'Description' => $inventories['Description'],
                    'MaxInventoryCount' => $inventories['MaxInventoryCount'],
                    'AvaliableInventoryCount' => $inventories['AvaliableInventoryCount'],
                    'IsDeleted' =>$inventories['IsDeleted'],
                    'CreatedBy' => $inventories['CreatedBy'],
                ]);
                $id = $name->id;
                $invid = 'INV'.$id;
                DB::table('inventory')->where('InventoryID', '=', [$id])->update(array('InventorydisplayID' => $invid));
            }
        }
    }
}
