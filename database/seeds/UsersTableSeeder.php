<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Administrator')
            ->first()
            ->UserRoleID;

        $supervisor_role = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Supervisor')
            ->first()
            ->UserRoleID;

        $employee_role  = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Employee')
            ->first()
            ->UserRoleID;

        $worker_role  = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Contractor')
            ->first()
            ->UserRoleID;
        $customer_role  = DB::table('user_role')
            ->select('UserRoleID')
            ->where('UserRole', 'Customer')
            ->first()
            ->UserRoleID;

        $faker = Faker::create();

        if (env('APP_ENV') == 'local') {
            DB::table('users')->insert(array(
                array(
                    'id'=>'100001',
                    'name'=> 'James ',
                    'FirstName' => 'James',
                    'LastName' => '',
                    'UserRoleID' => $admin_role,
//                    'OrganisationID' =>'8000001',
//                    'AssignedTo' => '',
                    'email' => 'deepika@nitaipartners.com',
                    'PhoneNo' => '98574123572',
                    'username' => 'administrator',
                    'password' => Hash::make('administrator')
                ),

                array(
                    'id'=>'100002',
                    'name'=> 'John',
                    'FirstName' => 'John',
                    'LastName' => '',
                    'UserRoleID' => $admin_role,
//                    'OrganisationID' =>'8000002',
//                    'AssignedTo' => '',
                    'email' => 'Johnorg@demo.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'org2_administrator',
                    'password' => Hash::make('administrator')
                ),

                array(
                    'id'=>'100003',
                    'name'=> 'Supervisor User',
                    'FirstName' => 'Supervisor',
                    'LastName' => 'User',
                    'UserRoleID' => $supervisor_role,
//                    'OrganisationID' =>'8000001',
//                    'AssignedTo' => '',
                    'email' => 'supervisor@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'supervisor',
                    'password' => Hash::make('supervisor')
                ),

                array(
                    'id'=>'100004',
                    'name'=> 'John Bill',
                    'FirstName' => 'John',
                    'LastName' => 'Bill',
                    'UserRoleID' => $supervisor_role,
//                    'OrganisationID' =>'8000002',
//                    'AssignedTo' => '',
                    'email' => 'johnbill@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'johnbill',
                    'password' => Hash::make('supervisor')
                ),

                array(
                    'id'=>'100005',
                    'name'=> 'Employee User',
                    'FirstName' => 'Employee',
                    'LastName' => 'User',
                    'UserRoleID' => $employee_role,
//                    'OrganisationID' =>'8000001',
//                    'AssignedTo' => '100003',
                    'email' => 'employee@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'employee',
                    'password' => Hash::make('employee')
                ),

                array(
                    'id'=>'100006',
                    'name'=> 'Steve William',
                    'FirstName' => 'Steve',
                    'LastName' => 'William',
                    'UserRoleID' => $employee_role,
//                    'OrganisationID' =>'8000002',
//                    'AssignedTo' => '100004',
                    'email' => 'stevewilliam@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'stevewilliam',
                    'password' => Hash::make('employee')
                ),

                array(
                    'id'=>'100007',
                    'name'=> 'Worker User',
                    'FirstName' => 'Worker',
                    'LastName' => 'User',
                    'UserRoleID' => $worker_role,
//                    'OrganisationID' =>'8000001',
//                    'AssignedTo' => '',
                    'email' => 'worker@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'worker',
                    'password' => Hash::make('worker')
                ),

                array(
                    'id'=>'100008',
                    'name'=> 'Jane William',
                    'FirstName' => 'Jane',
                    'LastName' => 'William',
                    'UserRoleID' => $worker_role,
//                    'OrganisationID' =>'8000002',
//                    'AssignedTo' => '',
                    'email' => 'janewilliam@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'janewilliam',
                    'password' => Hash::make('worker')
                ),

                array(
                    'id'=>'100009',
                    'name'=> 'David Parker',
                    'FirstName' => 'David',
                    'LastName' => 'Parker',
                    'UserRoleID' => $customer_role,
//                    'OrganisationID' =>'8000001',
//                    'AssignedTo' => '',
                    'email' => 'davidparker@test.com',
                    'PhoneNo' => $faker->numberBetween(8888888888,9999999999),
                    'username' => 'davidparker',
                    'password' => Hash::make('davidparker')
                ),
            ));
        }      
    }
}