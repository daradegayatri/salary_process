<?php

use Illuminate\Database\Seeder;

class OrganisationCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organisation_customers')->delete();
        $posts=[
            ['OrganisationID'=>'8000001','CustomerID'=>'9000001'],

        ];

        DB::table('organisation_customers')->insert($posts);
    }
}
