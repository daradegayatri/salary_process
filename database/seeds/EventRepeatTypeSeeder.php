<?php

use Illuminate\Database\Seeder;
use App\Models\General\EventRepeatType;

class EventRepeatTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $filesource=[
        [
            "RepeatType"=>"Daily",
        ],
        [
            "RepeatType"=>"Weekly",
        ],
        [
            "RepeatType"=>"Monthly",
        ],
        [
            "RepeatType"=>"Quaterly",
        ],
        [
            "RepeatType"=>"HalfYearly",
        ],
        [
            "RepeatType"=>"Yearly",
        ],
        [
            "RepeatType"=>"None",
        ],

    ];

    public function run()
    {

        foreach($this->filesource as $filesource){
            $filesourcedetails= EventRepeatType::where('RepeatType','=',$filesource['RepeatType'])->get();
            $count= count($filesourcedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=EventRepeatType::create([
                    "RepeatType"=>$filesource["RepeatType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
