<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
    */
    protected $tables =[
        //'users',
        //'password_resets',
        'user_role',
        // 'work_type',
//        'file_attachment',
//        'jobs',
//        //'tasklist',
//        'task_status',
//        'file_source',
//        'user_task_status',
        //'inventory',
        //'send_mails',
        //'orders',
        //'invoices',
        //'companydetails',
        //'comments',
       // 'priorities',
        //'assign_user_task',
        //'locations',
       // 'order_status',
        'oauth_scopes',
        'oauth_grants',
        'oauth_grant_scopes',
        'oauth_clients',
        'oauth_client_endpoints',
        'oauth_client_scopes',
        'oauth_client_grants',
        'oauth_sessions',
        'oauth_session_scopes',
        'oauth_auth_codes',
        'oauth_auth_code_scopes',
        'oauth_access_tokens',
        'oauth_access_token_scopes',
        'oauth_refresh_tokens',
        //'notifications',
//        'task_severities',
//        //'organisation',
//        'transaction_source',
//        //'transaction_log_monthly',
//        'inventory_type',
//        //'customer_address',
//        //'customers',
//        'customer_types',
//        //'customers_socialmedia_details',
//        //'organisation_customers',
//        'social_media_type',
//        'sub_customers',
//       // 'sub_organisations',
//       // 'transaction_charges',
//        //'transaction_details',
//        'organisation_type',
    ];

    public function run()
    {
        Model::unguard();
        if (env('APP_ENV') == 'local') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');

            // truncate all tables in tables list
            foreach ($this->tables as $table) {
                DB::table($table)->truncate();
            }
        }

        // These seeders run regardless of environment
        // They are not client-specific seeders
       // $this->call(seeds\project\configuration\OAuthSecuritySeeder::class);
//        $this->call('PrioritiesSeeder');

        $this->call('UsersRoleSeeder');
//
        $this->call('UsersTableSeeder');

        if (env('APP_ENV') == 'local') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
        Model::reguard();
        Artisan::call('cache:clear');
    }
}