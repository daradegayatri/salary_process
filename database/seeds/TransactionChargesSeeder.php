<?php

use Illuminate\Database\Seeder;

class TransactionChargesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    //TransactionChargeID, TransactionCharge, TransactionSourceID, TransactionNo_RangeFrom, TransactionNo_RangeTo, Min_Monthly_Amt,
    // Country, CountryCode, Currency, created_at, updated_at
    public function run()
    {
        DB::table('transaction_charges')->delete();
        $posts=[
            [
                'TransactionChargeID'=>1,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>2,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>3,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>4,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>5,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>6,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>7,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>8,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'India',
                'CountryCode'=>'IND',
                'Currency' =>'INR'
            ],
            [
                'TransactionChargeID'=>9,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>10,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>11,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>12,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>13,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>14,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>15,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>16,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'United States of America',
                'CountryCode'=>'US',
                'Currency' =>'USD'
            ],
            [
                'TransactionChargeID'=>17,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>18,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>19,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>20,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'50',
                'TransactionNo_RangeFrom'=> '0',
                'TransactionNo_RangeTo' => '100',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>21,
                'TransactionSourceID'=>'1',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>22,
                'TransactionSourceID'=>'2',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>23,
                'TransactionSourceID'=>'3',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ],
            [
                'TransactionChargeID'=>24,
                'TransactionSourceID'=>'4',
                'TransactionCharge' =>'40',
                'TransactionNo_RangeFrom'=> '101',
                'TransactionNo_RangeTo' => '500',
                'Min_Monthly_Amt' =>'1000',
                'Country' =>'Madagascar',
                'CountryCode'=>'MG',
                'Currency' =>'MGA'
            ]
        ];

        DB::table('transaction_charges')->insert($posts);
    }
}
