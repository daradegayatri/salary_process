<?php

use Illuminate\Database\Seeder;
use App\Models\Tasks\TaskSeverities;
use Carbon\Carbon;

class TaskSeveritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $taskseverity=[
        [
            "TaskSeverity"=>"Critical",
        ],
        [
            "TaskSeverity"=>"Major",
        ],
        [
            "TaskSeverity"=>"Minor",
        ]
    ];
    public function run()
    {
        foreach($this->taskseverity as $task){

            $taskseveritydetails= TaskSeverities::where('TaskSeverity','=',$task['TaskSeverity'])->get();
            $count= count($taskseveritydetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=TaskSeverities::create([
                    "TaskSeverity"=>$task["TaskSeverity"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
