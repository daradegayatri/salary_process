<?php

use Illuminate\Database\Seeder;

class SubOrganisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_organisations')->delete();
        $posts=[
            ['ParentOrganisationID'=>'8000002','OrganisationID'=>'8000003'],

        ];

        DB::table('sub_organisations')->insert($posts);
    }
}
