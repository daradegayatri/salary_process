<?php

use Illuminate\Database\Seeder;
use App\Models\General\Jobs;

class JobTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //JobID, JobName, JobDescription, CreatedBy, updated_by, created_at, updated_at
    public $jobtype=[
          [

            "JobName"=>"Fixing",
            "JobDescription"=>"Fix the damaged appliances",
        ],
        [

            "JobName"=>"Cleaning",
            "JobDescription"=>"Clean rooms, halls",
        ]
    ];
    public function run()
    {

        foreach($this->jobtype as $job){
        $jobtypedetails= Jobs::where('JobName','=',$job['JobName'])->get();
        $count= count($jobtypedetails);
        if($count == 0){
            $now = date('Y-m-d H:i:s');
            $name=Jobs::create([
                "JobName"=>$job["JobName"],
                "JobDescription"=>$job["JobDescription"],
                'created_at' => $now,
                'updated_at' => $now
            ]);
        }
    }
    }
}
