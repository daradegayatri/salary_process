<?php

use Illuminate\Database\Seeder;
use App\Models\Users\Userrole;
use Carbon\Carbon;

class UsersRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /*
     * SuperAdmin
(Organisation)Administrator
Supervisor
Employee
Customer
Sales Representative
Contractor
*/
    public $roles=[
        [
            "UserRole"=>"Administrator",
        ],
        [
            "UserRole"=>"Supervisor",
        ],
        [
            "UserRole"=>"Employee",
        ],
        [
            "UserRole"=>"Contractor",
        ],
        [
            "UserRole"=>"Sales Representative",
        ],
        [
            "UserRole"=>"Customer",
        ],
        [
            "UserRole"=>"SuperAdmin",
        ],


    ];

    public function run()
    {

        foreach($this->roles as $role){
            $userroles= Userrole::where('UserRole','=',$role['UserRole'])->get();
            $count= count($userroles);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Userrole::create([
                    "UserRole"=>$role["UserRole"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
