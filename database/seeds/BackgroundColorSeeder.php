<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class BackgroundColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('theme_color')->truncate();
        $input = array(

            'header' => '#ffffff',
            'footer' => '#abc1d6',
            'side_menu' => '#abc1d6',
            'created_by' => '100009',
            'updated_by' => '100009',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

//            [
//                'name' => 'header',
//                'value' => '#ffffff',
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//
//            ],
//            [
//                'name' => 'footer',
//                'value' => '#abc1d6',
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//
//            ],
//            [
//                'name' => 'side_menu',
//                'value' => '#abc1d6',
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//
//            ],
        );
        DB::table('theme_color')->insert($input);
    }
}
