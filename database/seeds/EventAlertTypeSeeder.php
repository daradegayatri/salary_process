<?php

use Illuminate\Database\Seeder;
use App\Models\General\EventAlertType;

class EventAlertTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */ public $filesource=[
    [
        "AlertType"=>"Daily",
    ],
    [
        "AlertType"=>"1 hour before event",
    ],
    [
        "AlertType"=>"2 hours before event",
    ],
    [
        "AlertType"=>"3 hours before event",
    ],
    [
        "AlertType"=>"1 day before event",
    ],
    [
        "AlertType"=>"2 days before event",
    ],
    [
        "AlertType"=>"On event day",
    ],
    [
        "AlertType"=>"1 Week Before event",
    ],
    [
        "AlertType"=>"1 Month Before event",
    ],
    [
        "AlertType"=>"None",
    ],

];

    public function run()
    {

        foreach($this->filesource as $filesource){

            $filesourcedetails= EventAlertType::where('AlertType','=',$filesource['AlertType'])->get();
            $count= count($filesourcedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=EventAlertType::create([
                    "AlertType"=>$filesource["AlertType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
