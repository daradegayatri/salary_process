<?php

use Illuminate\Database\Seeder;
use App\Models\CMS\CMSCategory;
use App\Models\CMS\CMSContent;

class CMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $CMS_Data = [
        	[
        		"name"=>"Header Color",
                "value"=>"header_color",
                "content"=>[
                    [
                        "content"=>'#ffffff'
                    ],
                ]
        	],
        	[
        		"name"=>"Footer Color",
                "value"=>"footer_color",
                "content"=>[
                    [
                        "content"=>'#ffffff'
                    ],
                ]
        	],
            [
                "name"=>"Logo",
                "value"=>"logo",
                "content"=>[
                    [
                        "content"=>'/images/2017_06_19_Mobi-comm.svg'
                    ],
                ]
            ],
            [
                "name"=>"Logo Link",
                "value"=>"logo_link",
                "content"=>[
                    [
                        "content"=>''
                    ],
                ]
            ],
        ];

    	$organizations = \App\Models\Users\Organisation::all()->lists('OrganisationID');

        $existingCategoryRecords = CMSCategory::all();

        $existingCategories = [];
        foreach ($existingCategoryRecords as $existingCategoryRecord) {
            $existingCategories[$existingCategoryRecord->name] = $existingCategoryRecord->id;
        }

        $existingContentRecords = CMSContent::all();
        $existingContents = [];
        $existingOrganizationContents = [];
        foreach ($existingContentRecords as $existingContentRecord) {
            $existingContents[$existingContentRecord->cms_category_id] = '';
        }
        foreach ($existingContentRecords as $existingContentRecord) {
            $existingOrganizationContents[$existingContentRecord->cms_category_id][$existingContentRecord->organization_id] = '';
        }

        foreach ($CMS_Data as $data) {
        	$categoryId = 0;
            if (isset($existingCategories[$data['name']])) {
                // category name exists
                $categoryId = $existingCategories[$data['name']];
            } else {
                // category name does not exist, create it
                $category = CMSCategory::create([
                    'name' => $data['name'],
                    'value' => $data['value']
                ]);
                $categoryId = $category->id;
            }

            foreach ($organizations as $organizationId) {    
                foreach ($data['content'] as $content_data) {
                    if (!isset($existingContents[$categoryId])) {
                        // content does not exist, create it
                        $content = CMSContent::create([
                            'cms_category_id' => $categoryId,
                            'content' => $content_data['content'],
                            'organization_id' => $organizationId,
                        ]);
                    } elseif (isset($existingContents[$categoryId]) && !isset($existingOrganizationContents[$categoryId][$organizationId])) {
                        $content = CMSContent::create([
                            'cms_category_id' => $categoryId,
                            'content' => $content_data['content'],
                            'organization_id' => $organizationId,
                        ]);
                    }
                }
            } 
        }
    }
}
