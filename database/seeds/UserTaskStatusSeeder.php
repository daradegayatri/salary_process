<?php

use Illuminate\Database\Seeder;
use App\Models\Users\Usertaskstatus;
use Carbon\Carbon;

class UserTaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *Completed
    Inprogress
    Hold
    Cancelled

     * @return void
     */
    public $usertaskstatus=[
        [
            "UserTaskStatus"=>"Completed",
        ],
        [
            "UserTaskStatus"=>"Inprogress",
        ],
        [
            "UserTaskStatus"=>"OnHold",
        ],
        [
            "UserTaskStatus"=>"Cancelled",
        ]
    ];

    public function run()
    {
        foreach($this->usertaskstatus as $usertask){
            $usertaskdetails= Usertaskstatus::where('UserTaskStatus','=',$usertask['UserTaskStatus'])->get();
            $count= count($usertaskdetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Usertaskstatus::create([
                    "UserTaskStatus"=>$usertask["UserTaskStatus"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
