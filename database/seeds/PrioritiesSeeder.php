<?php

use Illuminate\Database\Seeder;
use App\Models\General\Priorities;
use Carbon\Carbon;

class PrioritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $priorities=[
        [
            "Priority"=>"High",
        ],
        [
            "Priority"=>"Low",
        ],
        [
            "Priority"=>"Medium",
        ]
    ];

    public function run()
    {

        foreach($this->priorities as $priority){

            $prioritiesdetails= Priorities::where('Priority','=',$priority['Priority'])->get();
            $count= count($prioritiesdetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Priorities::create([
                    "Priority"=>$priority["Priority"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
