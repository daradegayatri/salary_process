<?php

use Illuminate\Database\Seeder;
use App\Models\Orders\Orderstatus;
use Carbon\Carbon;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $orderstatus=[
        [
            "OrderStatus"=>"Open",
        ],
        [
            "OrderStatus"=>"Fulfilled",
        ],
        [
            "OrderStatus"=>"Approved",
        ],
        [
            "OrderStatus"=>"Rejected",
        ]
//        [
//            "OrderStatus"=>"Raised Return Order",
//        ]

    ];

    public function run()
    {
        foreach($this->orderstatus as $order){
            $orderstatusedetails= Orderstatus::where('OrderStatus','=',$order['OrderStatus'])->get();
            $count= count($orderstatusedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Orderstatus::create([
                    "OrderStatus"=>$order["OrderStatus"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
