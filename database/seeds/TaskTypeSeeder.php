<?php

use Illuminate\Database\Seeder;
use App\Models\Tasks\TaskType;

class TaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * Email , Social Media Message , Appointment, Reminder , SMS Text
     */

    public $taskstatus=[
        [
            "TaskType"=>"Email",
        ],
        [
            "TaskType"=>"Social Media Message",
        ],
        [
            "TaskType"=>"Appointment",
        ],
        [
           "TaskType"=>"Reminder",
        ],
        [
            "TaskType"=>"SMS",
        ],
        [
            "TaskType"=>"Call",
        ],
    ];
    public function run()
    {
        foreach($this->taskstatus as $task){
            $taskdetails= TaskType::where('TaskType','=',$task['TaskType'])->get();
            $count= count($taskdetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=TaskType::create([
                    "TaskType"=>$task["TaskType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }

}
