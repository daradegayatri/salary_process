<?php

use Illuminate\Database\Seeder;
use App\Models\Inventory\InventoryType;
use Carbon\Carbon;

class InventoryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $inventory=[
        [
            "InventoryType"=>"Product",
        ],
        [
            "InventoryType"=>"Assets",
        ],



    ];

    public function run()
    {

        foreach($this->inventory as $inventory){
            $inventorytype= InventoryType::where('InventoryType','=',$inventory['InventoryType'])->get();
            $count= count($inventorytype);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=InventoryType::create([
                    "InventoryType"=>$inventory["InventoryType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
