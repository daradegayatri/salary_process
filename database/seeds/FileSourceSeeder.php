<?php

use Illuminate\Database\Seeder;
use App\Models\Files\Filesource;
use Carbon\Carbon;

class FileSourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $filesource=[
        [
            "FileSource"=>"Task",
        ],
        [
            "FileSource"=>"Comment",
        ],
        [
            "FileSource"=>"Mail",
        ],
        [
             "FileSource"=>"Order",
        ],
        [
            "FileSource"=>"Signature",
        ],
        [
            "FileSource"=>"Template",
        ],
        [
            "FileSource"=>"Inventory",
        ],
        [
            "FileSource"=>"Appointment",
        ]
    ];

    public function run()
    {

        foreach($this->filesource as $filesource){

            $filesourcedetails= Filesource::where('FileSource','=',$filesource['FileSource'])->get();
            $count= count($filesourcedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Filesource::create([
                    "FileSource"=>$filesource["FileSource"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
