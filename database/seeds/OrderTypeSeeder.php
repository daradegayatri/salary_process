<?php

use Illuminate\Database\Seeder;
use App\Models\Orders\Ordertype;
use Carbon\Carbon;

class OrderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
//RMA, sales order, service order
    public $ordertypes=[
        [
            "OrderType"=>"RMA",
        ],
        [
            "OrderType"=>"Sales",
        ],
        [
            "OrderType"=>"Service",
        ],
        [
            "OrderType"=>"Purchase",
        ],
        [
            "OrderType"=>"Back Order",
        ],
        [
            "OrderType"=>"Internal",
        ]
    ];
    public function run()
    {
        foreach($this->ordertypes as $ordertype){

            $ordertypedetails= Ordertype::where('OrderType','=',$ordertype['OrderType'])->get();
            $count= count($ordertypedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Ordertype::create([
                    "OrderType"=>$ordertype["OrderType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
