<?php

use Illuminate\Database\Seeder;
use App\Models\Users\Customers;
use App\Models\Users\CustomerAddress;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //CustomerID, CustomerName, Phoneno, EmailID, UserID, CustomerTypeID, HasSocialMedia, SalesRep, ParentSalesRep,
    // PrincipalAddressID, BillToAddressID, ShipToAddressID, Description, TransactionLink, HasSubCustomer, NoOfSubCustomer,
    // Created_By, Updated_By, created_at, updated_at

    //CustomerAddressID, Address1, Address2, City, State, Country, Zipcode, created_by, updated_by, created_at, updated_at
    public $customeraddress = [
        [

            'Address1' => 'Aarey Colony',
            'Address2' => 'Goregaon East ',
            'City' => 'Mumbai ',
            'State' => 'Maharashtra',
            'Country' => 'India',
            'Zipcode' => '400005',
            'created_by' => '100005',
            'updated_by' => '100003',

        ],
        [

            'Address1' => 'Aarey Market',
            'Address2' => 'Goregaon East ',
            'City' => 'Mumbai ',
            'State' => 'Maharashtra',
            'Country' => 'India',
            'Zipcode' => '400005',
            'created_by' => '100005',
            'updated_by' => '100003',

        ],


    ];

    public $customer = [
        [
            'CustomerID'=>'9000001',
            'CustomerName' => 'David Parker',
            'Phoneno' => '98574123572 ',
            'EmailID' => 'davidparker@test.com',
            'UserID' => '100009',
            'CustomerTypeID' => '1',
            'HasSocialMedia' => '0',
            'SalesRep' => '100005',
            'ParentSalesRep' => '100003',
            'PrincipalAddressID' => '1',
            'BillToAddressID' =>1,
            'ShipToAddressID' => 1,
            'Description' =>'',
            'TransactionLink' => '',
            'HasSubCustomer' => '0',
            'NoOfSubCustomer' => '0',
            'Created_By' => 1,
            'Updated_By' => 1,
        ],

    ];
    public function run()
    {
        foreach($this->customeraddress as $customeraddress){

                $name=CustomerAddress::create([
                    'Address1'=>$customeraddress['Address1'],
                    'Address2'=> $customeraddress['Address2'],
                    'City'=>$customeraddress['City'],
                    'State'=>$customeraddress['State'],
                    'Country'=>$customeraddress['Country'],
                    'Zipcode'=>$customeraddress['Zipcode'],
                    'created_by'=>$customeraddress['created_by'],
                    'updated_by'=>$customeraddress['updated_by'],
                ]);

        }

        foreach($this->customer as $customer){
            $customer_details= Customers::where('CustomerName','=',$customer['CustomerName'])->get();
            $count= count($customer_details);
            if($count == 0){
                $name=Customers::create([
                    'CustomerID'=>$customer['CustomerID'],
                    'CustomerName'=> $customer['CustomerName'],
                    'Phoneno'=>$customer['Phoneno'],
                    'EmailID'=>$customer['EmailID'],
                    'UserID'=>$customer['UserID'],
                    'CustomerTypeID'=>$customer['CustomerTypeID'],
                    'HasSocialMedia'=>$customer['HasSocialMedia'],
                    'SalesRep'=>$customer['SalesRep'],
                    'ParentSalesRep'=>$customer['ParentSalesRep'],
                    'PrincipalAddressID'=>$customer['PrincipalAddressID'],
                    'BillToAddressID'=>$customer['BillToAddressID'],
                    'ShipToAddressID'=>$customer['ShipToAddressID'],
                    'Description'=>$customer['Description'],
                    'TransactionLink'=>$customer['TransactionLink'],
                    'Description'=>$customer['Description'],
                    'HasSubCustomer'=>$customer['HasSubCustomer'],
                    'NoOfSubCustomer'=>$customer['NoOfSubCustomer'],
                    'Created_By'=>$customer['Created_By'],
                    'Updated_By'=>$customer['Created_By'],
                ]);
            }
        }
    }
}
