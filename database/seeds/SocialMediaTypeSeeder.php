<?php

use Illuminate\Database\Seeder;
use App\Models\General\SocialMediaType;


class SocialMediaTypeSeeder extends Seeder
{
    public $socialMedia=[
        [
            "SocialMediaType"=>"Facebook",
        ],
        [
            "SocialMediaType"=>"Twitter",
        ],
        [
            "SocialMediaType"=>"LinkedIn",
        ],



    ];

    public function run()
    {

        foreach($this->socialMedia as $socialMedia){
            $socialMediatype= SocialMediaType::where('SocialMediaType','=',$socialMedia['SocialMediaType'])->get();
            $count= count($socialMediatype);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=SocialMediaType::create([
                    "SocialMediaType"=>$socialMedia["SocialMediaType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
