<?php

use Illuminate\Database\Seeder;
use App\Models\Users\CustomerStage;
use Carbon\Carbon;


class CustomerStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $customer=[
        [
            "CustomerStage"=>"Beginner",
        ],
        [
            "CustomerStage"=>"Intermediate",
        ],
        [
            "CustomerStage"=>"Final",
        ]
    ];

    public function run()
    {

        foreach($this->customer as $customer){
            $customertype= CustomerStage::where('CustomerStage','=',$customer['CustomerStage'])->get();
            $count= count($customertype);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=CustomerStage::create([
                    "CustomerStage"=>$customer["CustomerStage"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
