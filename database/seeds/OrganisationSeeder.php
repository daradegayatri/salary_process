<?php

use Illuminate\Database\Seeder;
use App\Models\Users\Organisation;

class OrganisationSeeder extends Seeder
{
   /*
    * OrganisationID, OrganisationName, ContactPerson, Phoneno, EmailID, Address, City, State, Country, ZipCode, HasSocialMediaDetails HasSubOrganisation, IsSuborganisation, Created_By, Updated_By, created_at, updated_at*/

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $organisation = [
        [
            'OrganisationID'=>'8000001',
            'OrganisationName' => 'James Org',
            'ContactPerson' => 'James ',
            'Phoneno' => '98574123572',
            'EmailID' => 'deepika@nitaipartners.com',
            'Address' => 'Goregaon East',
            'City' => 'Mumbai',
            'State' => 'Maharashtra',
            'Country' => 'India',
            'ZipCode' => '400067',
            'HasSubOrganisation' =>0,
            'IsSubOrganisation' => 0,
            'HasSocialMediaDetails' =>0,
            'Created_By' => 1,
            'Updated_By' => 1,
        ],
        [

            'OrganisationID'=>'8000002',
            'OrganisationName' => 'John Org',
            'ContactPerson' => 'John ',
            'Phoneno' => '98574123572',
            'EmailID' => 'Johnorg@demo.com',
            'Address' => 'Goregaon West',
            'City' => 'Mumbai',
            'State' => 'Maharashtra',
            'Country' => 'India',
            'ZipCode' => '400067',
            'HasSubOrganisation' =>1,
            'IsSubOrganisation' => 0,
            'HasSocialMediaDetails' =>0,
            'Created_By' => 1,
            'Updated_By' => 1,
        ],
        [
            'OrganisationID'=>'8000003',
            'OrganisationName' => 'John SubOrg',
            'ContactPerson' => 'John suborg ',
            'Phoneno' => '98574123572',
            'EmailID' => 'Johnsuborg@demo.com',
            'Address' => 'Dadar West',
            'City' => 'Mumbai',
            'State' => 'Maharashtra',
            'Country' => 'India',
            'ZipCode' => '400067',
            'HasSubOrganisation' =>0,
            'IsSubOrganisation' => 1,
            'HasSocialMediaDetails' =>0,
            'Created_By' => 1,
            'Updated_By' => 1,
        ],
    ];
    public function run()
    {
        foreach($this->organisation as $organisation){
            $inventory_details= Organisation::where('OrganisationName','=',$organisation['OrganisationName'])->get();
            $count= count($inventory_details);
            if($count == 0){
                $name=Organisation::create([
                    'OrganisationID'=>$organisation['OrganisationID'],
                    'OrganisationName'=> $organisation['OrganisationName'],
                    'ContactPerson'=>$organisation['ContactPerson'],
                    'Phoneno'=>$organisation['Phoneno'],
                    'EmailID'=>$organisation['EmailID'],
                    'Address'=>$organisation['Address'],
                    'City'=>$organisation['City'],
                    'State'=>$organisation['State'],
                    'Country'=>$organisation['Country'],
                    'ZipCode'=>$organisation['ZipCode'],
                    'HasSubOrganisation'=>$organisation['HasSubOrganisation'],
                    'IsSubOrganisation'=>$organisation['IsSubOrganisation'],
                    'HasSocialMediaDetails'=>$organisation['HasSocialMediaDetails'],
                    'Created_By'=>$organisation['Created_By'],
                    'Updated_By'=>$organisation['Created_By'],
                ]);
            }
        }


        }

}
