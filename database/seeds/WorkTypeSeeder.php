<?php

use Illuminate\Database\Seeder;
use App\Models\General\Worktype;
use Carbon\Carbon;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $worktypes=[
        [
            "WorkType"=>"Carpentery",
        ],
        [
            "WorkType"=>"Ivory",
        ],
        [
            "WorkType"=>"Plumbing",
        ]
    ];

    public function run()
    {

        foreach($this->worktypes as $worktype){

            $worktypedetails= Worktype::where('WorkType','=',$worktype['WorkType'])->get();
            $count= count($worktypedetails);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=Worktype::create([
                    "WorkType"=>$worktype["WorkType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
