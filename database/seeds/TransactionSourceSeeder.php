<?php

use Illuminate\Database\Seeder;

class TransactionSourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_source')->delete();
        $posts=[
            ['TransactionSourceID'=>1,'TransactionFor'=>'order'],
            ['TransactionSourceID'=>2,'TransactionFor'=>'invoice'],
            ['TransactionSourceID'=>3,'TransactionFor'=>'task'],
            ['TransactionSourceID'=>4,'TransactionFor'=>'customer'],
            ['TransactionSourceID'=>5,'TransactionFor'=>'user'],
            ['TransactionSourceID'=>6,'TransactionFor'=>'inventory']
        ];

        DB::table('transaction_source')->insert($posts);
    }
}
