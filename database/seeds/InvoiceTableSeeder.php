<?php

use Illuminate\Database\Seeder;
use App\Models\Orders\InvoiceType;
class InvoiceTableSeeder extends Seeder
{
    public $invoice=[
        [
            "InvoiceType"=>"Standard",
        ],
        [
            "InvoiceType"=>"Credit Memo",
        ],
        [
            "InvoiceType"=>"Debit Memo",
        ],
        [
            "InvoiceType"=>"Mixed Invoice",
        ],
        [
            "InvoiceType"=>"Prepayment Invoice",
        ],
        [
            "InvoiceType"=>"Withholding Tax Invoice",
        ],
        [
            "InvoiceType"=>"Timesheet",
        ],
        [
            "InvoiceType"=>"Expenses",
        ],
        [
            "InvoiceType"=>"PO Default Invoice",
        ]
    ];

    public function run()
    {

        foreach($this->invoice as $invoice){
            $invoicetype= InvoiceType::where('InvoiceType','=',$invoice['InvoiceType'])->get();
            $count= count($invoicetype);
            if($count == 0){
                $now = date('Y-m-d H:i:s');
                $name=InvoiceType::create([
                    "InvoiceType"=>$invoice["InvoiceType"],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }
        }
    }
}
