<form method='post' id='formdata' action='https://secure.authorize.net/gateway/transact.dll'>
    <input type='hidden' name='x_login' value={{$api_login_id}} />
    <input type='hidden' name='x_fp_hash' value={{$fingerprint}} />
    <input type='hidden' name='x_amount' value={{$amount}} />
    <input type='hidden' name='x_fp_timestamp' value={{$fp_timestamp}} />
    <input type='hidden' name='x_fp_sequence' value={{$fp_sequence}} />
    <input type='hidden' name='x_invoice_num' value={{$transactionId}} />
    <input type='hidden' name='x_receipt_link_method' value='POST' />
    <input type='hidden' name='x_receipt_link_url' value={{$relay_url}} />
    <input type='hidden' name='x_receipt_link_text' value='Click here to return to our home page' />
    <input type='hidden' name='x_token' value={{$token}} />
    <input type='hidden' name='x_user_id' value={{$user_id}} />
    <input type='hidden' name='x_organisation_id' value={{$org_id}} />
    <input type='hidden' name='x_transactionlog_id' value={{$transaction_log_id}} />
    <input type='hidden' name='x_redirect_url' value={{$redirecturl}} />

    <input type='hidden' name='x_first_name' value={{($org_name)}} />
    <input type='hidden' name='x_address' value={{$org_address}} />
    <input type='hidden' name='x_city' value={{$org_city}} />
    <input type='hidden' name='x_state' value={{$org_state}} />
    <input type='hidden' name='x_zip' value={{$org_zipcode}} />
    <input type='hidden' name='x_country' value={{$org_country}} />
    <input type='hidden' name='x_phone' value={{$org_phone}} />
    <input type='hidden' name='x_email' value={{$org_email}} />


    <input type='hidden' name='x_ship_to_first_name' value={{$org_name}} />
    <input type='hidden' name='x_ship_to_address' value={{$org_address}} />
    <input type='hidden' name='x_ship_to_city' value={{$org_city}} />
    <input type='hidden' name='x_ship_to_state' value={{$org_state}} />
    <input type='hidden' name='x_ship_to_zip' value={{$org_zipcode}} />
    <input type='hidden' name='x_ship_to_country' value={{$org_country}} />
    <input type='hidden' name='x_ship_to_phone' value={{$org_phone}} />
    <input type='hidden' name='x_ship_to_email' value={{$org_email}} />




    <input type='hidden' name='x_version' value='3.1'>
    <input type='hidden' name='x_show_form' value='payment_form'>
    <input type='hidden' name='x_test_request' value='false' />
    <input type='hidden' name='x_method' value='cc' />
</form>

<script type='text/javascript'>
    document.getElementById('formdata').submit();
</script>