<html>
<body>
<h1>CHULA VISTA TRAINING CENTER</h1>
<h3>RELEASE OF LIABILITY, WAIVER OF RIGHTS, & ASSUMED RISK AGREEMENT</h3>
<p>This document affects your legal rights: Read carefully before signing.<br>
I,<?php echo $name;?> (the “Participant”), hereby acknowledge that I have voluntary elected to participate in the following activity
    (the “Activity”) on the territory of the Chula Vista Elite Athlete Training Center (“CVTC”) or adjacent premises operated by ELITE ATHLETE SERVICES LLC, a
California limited liability company (the “Operator”), including the territory of the Easton Archery Center of Excellence
(the “Archery Center”) operated by the Easton Foundation, Easton Sports Development Foundation and Easton Sports
Development Foundation II (collectively, the “Easton Foundations”):
<ul>
 <li>Enter, access, visit, occupy, operate, participate, engage or get involved in any physical, sport, work,
     professional, business, academic, educational, training, recreational, dietary, tourist, leisure, service or
     spectator activity;</li>
    <li>Use the CVTC facilities (including but not limited to any and all sports, recreational, spectator, tourist,
        medical, rehabilitation, educational, training, service, dietary, or administrative facilities), furniture, fixtures,
        supplies or equipment (including but not limited to any and all sports, sports medicine, medical,
        rehabilitation, recreational, training, tourist, leisure, service, dining, or spectator equipment); and/or</li>
    <li>Use of the Workshop or of the Workshop tools, the gym, equipment, facilities and/or services of, or provided
        at, the Archery Center</li>
</ul>
<p>In consideration for being permitted to participate in the Activity, I hereby acknowledge and agree to the following:</p>
<ol>
    <li>ELECTIVE PARTICIPATION: I acknowledge that my participation is elective and that the Activity may be
        unsupervised. I have inspected the area in which the Activity is taking place and determined that the area is free of defects
        and suitable for the intended Activity.”</li>
    <li>AGREEMENT TO FOLLOW DIRECTIONS: I agree to follow the rules for the Activity provided, and to follow
        directions given, to me by the Operator, including, but not limited to its owners, directors, officers, employees or agents,
        or the leaders of the Activity. I grant the Operator, including, but not limited to its owners, directors, officers, employees
        or agents, the right to terminate my participation in the Activity if it is determined that my conduct is detrimental to the
        best interests of other participants or violates any rule or direction of the Operator.</li>
    <li>INDEPENDENT CONTRACTORS: I acknowledge that the Operator has no control over and assumes no
        responsibility for the actions of any independent contractors providing any services for the Activity.</li>
    <li>USE OF MY LIKENESS: I understand that during the Activity I may be photographed or videotaped. To the fullest
        extent allowed by law, I waive all rights of publicity or privacy or pre-approval that I have for any such likeness of me or
        use of my name in connection with such likeness, and I grant to the Operator and its assignees permission to copyright,
        use and publish (including by electronic means) such likeness of me, whether in whole or part, in any form, without
        restrictions, and for any purpose.</li>
    <li>INFORMED CONSENT: I have been informed of and I understand the various aspects of the Activity. I understand
        that as a Participant in the Activity I could sustain serious property and/or personal damage, injuries, trauma, unwanted
        contact, harassment, illness, loss, disability or even death as a consequence of not only the Operator’s actions, inactions or
        negligence, but also the actions, inactions or negligence of others, conditions of facilities and/or equipment used, and that
        there may be other risks known or not known to me or not reasonably foreseeable at this time. I further understand and
        agree that any property and/or personal damage, injury, trauma, unwanted contact, harassment, illness, loss, disability or
        death that I may sustain by any means is my sole responsibility, and the Operator, and its agents, employees, shareholders,
        officers, directors, and management bear no responsibility therefore.</li>
    <li>
        RELEASE AND WAIVER OF LIABILITY: I, on behalf of myself, my personal representatives, heirs, executors,
        administrators, agents, and assigns, HEREBY RELEASE, WAIVE, DISCHARGE, AND COVENANT NOT TO SUE
        the Operator, the City of Chula Vista, the United States Olympic Committee and the Easton Foundations, including their
        respective owners, directors, officers, trustees, elected officials, shareholders, employees, instructors, agents, independent
        contractors, representatives and volunteers (the “Released Parties”) for any and all liability, including any and all claims,
        1.1.17 2
        demands, causes of action (known or unknown), suits, or judgments of any and every kind (including attorneys' fees),
        arising from, or in connection with, any damage, injury, trauma, unwanted contact, harassment, illness, loss, disability or
        death that occur to me, to any other person, or to any property during the Activity or in any way related to the Activity,
        including during transit or transportation to or from the Activity, REGARDLESS OF WHETHER THE DAMAGE,
        INJURY, TRAUMA, UNWANTED CONTACT, HARASSMENT, ILLNESS, LOSS, DISABILITY OR DEATH IS
        CAUSED BY THE NEGLIGENCE OF THE RELEASED PARTIES OR OTHERWISE. This RELEASE AND
        WAIVER includes claims for strict liability for abnormally dangerous activities. By giving this release I expressly waive
        any rights I may have under California Civil Code Section 1542, which provides that:
        “A general release does not extend to claims which the creditor does not know or suspect to exist in his or
        her favor at the time of executing the release, which if known by him or her must have materially affected
        his or her settlement with the debtor.”
    </li>
    <li>ASSUMPTION OF RISK: I understand that there are potential dangers incidental to my participation in the Activity,
        some of which may be DANGEROUS OR HAZARDOUS TO ME AND MY PERSONAL PROPERTY, and which
        may expose me to the risk of property and/or personal damage, injuries, trauma, unwanted contact, harassment, illness,
        loss, disability, paralysis, dismemberment or even death. I understand that these potential risks include, but are not limited
        to: participation or engagement in a physical activity or sport; attendance of a sporting or other event; equipment failures;
        lack of fitness or conditioning; hypothermia; heat stroke; drowning; death; travel; lodging; consumption of liquids or
        food; weather conditions; sexual harassment; bullying; unpredictable currents; hostile or aggressive wildlife; criminal
        activities; negligent, reckless, or willful acts of others; negligent first aid operations or procedures of Released Parties; and
        other risks that are unknown at this time. I KNOWINGLY AND VOLUNTARILY ASSUME ALL SUCH RISKS,
        BOTH KNOWN AND UNKNOWN, EVEN IF ARISING FROM THE NEGLIGENCE OF THE RELEASED
        PARTIES, and assume full responsibility for my participation in the Activity. I agree to wear the provided safety
        equipment, including safety googles and gloves when required, and I agree that my hair will be pulled back into a ponytail
        to avoid risk of being caught in tools, equipment or machinery located on the premises of CVEOTC, the Archery Center
        or adjacent territories.</li>
    <li> INDEMNIFICATION HOLD HARMLESS AND DEFEND: I, on behalf of myself, my estate, personal
        representatives, heirs, executors, administrators, successors, beneficiaries, agents and assignees, agree to indemnify, hold
        harmless, and defend the Released Parties from any and all liability, including any and all claims, demands, causes of
        action (known or unknown), suits, or judgments of any and every kind (including attorney’s fees), arising from, or in
        connection with, any property and/or personal damage, injury, trauma, illness, loss, unwanted contact, harassment,
        disability, paralysis, dismemberment or death that I may suffer as a result of my participation in the Activity or in any way
        related to the Activity, REGARDLESS OF WHETHER THE DAMAGE, INJURY, TRAUMA, ILLNESS, LOSS,
        UNWANTED CONDUCT, HARASSMENT, DISABILITY, PARALYSIS, DISMEMBERMENT OR DEATH IS
        CAUSED BY THE NEGLIGENCE OF THE RELEASED PARTIES OR OTHERWISE.</li>
    <li>CHOICE OF LAW/SEVERABILITY: I hereby agree that this Agreement shall be construed in accordance with the
        law of the State of California exclusive of its choice of law rules and that this Agreement is intended to be as broad and
        inclusive as permitted by such law. I further agree that if any portion hereof is held invalid, the balance shall,
        notwithstanding, continue in full force and effect.</li>
    <li>DISPUTE RESOLUTION: Any dispute, claim or controversy arising out of or relating to this Agreement or the
        breach, termination, enforcement, interpretation or validity thereof, including the determination of the scope or
        applicability of this agreement to arbitrate, shall be determined by arbitration in San Diego, California before a sole
        arbitrator. The arbitration shall be administered by JAMS pursuant to its Comprehensive Arbitration Rules and
        Procedures. Judgment on the Award may be entered in any court having jurisdiction. This clause shall not preclude
        parties from seeking provisional remedies in aid of arbitration from a court of appropriate jurisdiction.</li>
    <li>ACKNOWLEDGEMENT: I HAVE READ THIS AGREEMENT AND FULLY UNDERSTAND ITS TERMS. I
        AM AWARE THAT THIS AGREEMENT INCLUDES A RELEASE AND WAIVER OF LIABILITY, AN
        ASSUMPTION OF RISK, AND AN AGREEMENT TO INDEMNIFY THE RELEASED PARTIES. I
        UNDERSTAND I HAVE GIVEN UP SUBSTANTIAL RIGHTS BY SIGNING THIS AGREEMENT, AND SIGN
        IT FREELY AND VOLUNTARILY WITHOUT ANY INDUCEMENT. NO ORAL REPRESENATATIONS,
        1.1.17 3
        STATEMENTS, OR OTHER INDUCEMENTS TO SIGN THIS AGREEMENT HAVE BEEN MADE APART FROM
        WHAT IS CONTAINED IN THIS DOCUMENT.</li>
</ol>

<p> <br>
Date: <?php echo $date;?><br>
Name Printed: <?php echo $name;?><br>
    <br><img  src="<?php echo $signature;?>" /> <br>


If participant is a minor, signature of parent or responsible adult is required below:
In consideration of the minor child being permitted to participate in the Activity, I accept and agree to the full contents of
this agreement. I certify that I have the authority to sign on behalf of the minor child and to make decisions for the minor
child regarding this Activity. I also agree to RELEASE, HOLD HARMLESS, INDEMNIFY AND DEFEND the
Released Parties from all liabilities and claims that arise in any way from any damage, injury, trauma, illness, loss,
unwanted contact, harassment, disability or death that occurs to the minor child during the Activity or in any way
related to the Activity. This includes any claim of the minor and any claim arising from the negligence of the Released
Parties. I understand that nothing in this agreement is intended to release claims for any liability that California law does
not permit to be excluded by agreement.
</p>

</body>

</html>
